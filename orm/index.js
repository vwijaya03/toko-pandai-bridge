import User from '~/orm/user'
import Pickup from '~/orm/pickup'
import Bank from '~/orm/bank'
import Bfi from '~/orm/bfi'
import Aca from '~/orm/aca'
import AcaProduct from '~/orm/aca_product'
import BankAccount from '~/orm/bank_account'
import Informasi from '~/orm/informasi'
import App_menu from '~/orm/app_menu'
import Invoice from '~/orm/invoice'
import MobileVersion from '~/orm/mobile_version'
import Notification from '~/orm/notif'
import Otp from '~/orm/otp'
import Logs from '~/orm/logs'
import FMCG from '~/orm/fmcg'
import RegisteredLeCode from '~/orm/data_registered_le_code'
import RequestLimit from '~/orm/requestLimit'
import UserUniqueCode from '~/orm/users_unique_code'
import UserAdditionalData from '~/orm/users_additional_data'
import Vouchers from '~/orm/vouchers'
import TOPUP from '~/orm/topup'

User.hasMany(BankAccount, {foreignKey: 'user_id' });
BankAccount.belongsTo(User, {foreignKey: 'user_id'});
BankAccount.belongsTo(Bank, {foreignKey: 'bank_id'})
Bank.hasMany(BankAccount, {foreignKey: 'bank_id'});
FMCG.hasMany(UserUniqueCode, {foreignKey: 'fmcg_id'});

export { Aca }
export { AcaProduct }
export { User }
export { Bfi }
export { Bank }
export { BankAccount }
export { Pickup }
export { Informasi }
export { App_menu }
export { Invoice }
export { MobileVersion }
export { Notification }
export { Otp }
export { Logs }
export { FMCG }
export { RegisteredLeCode }
export { RequestLimit }
export { UserUniqueCode }
export { UserAdditionalData }
export { Vouchers }
export { TOPUP }