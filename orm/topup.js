import { orm } from '~/services/mariadb'
import Sequelize from 'sequelize'

const TOPUP = orm.define('topups', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    user_id: {
        type: Sequelize.INTEGER, allowNull:false
    },
    va: {
        type: Sequelize.STRING, allowNull:false
    },
    nama_toko: {
        type: Sequelize.STRING, allowNull:false
    },
    amount: {
        type: Sequelize.DECIMAL, allowNull:false
    },
    createdAt: {
        type: 'TIMESTAMP', allowNull:true
    },
    updatedAt: {
        type: 'TIMESTAMP', allowNull:true
    }
});

export default TOPUP