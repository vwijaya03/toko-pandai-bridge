import { orm } from '~/services/mariadb'
import Sequelize from 'sequelize'

const RequestLimit = orm.define('request_limits', {
    id: { type: Sequelize.INTEGER, field: 'id', primaryKey: true, autoIncrement: true },
    accessToken: { type: Sequelize.STRING, allowNull:true },
    lastRequestDate: { type: Sequelize.STRING, allowNull:true },
    limitRequestDate: { type: Sequelize.STRING, allowNull:true },
});

export default RequestLimit