'use strict';

const { OAuth2Client } = require('google-auth-library');
import _ from 'lodash'
import util from 'util'
import moment from 'moment'
import sha1 from 'sha1'; 

import middleware from '~/api/controllers/auth'
import base_repo from '~/api/controllers/base'

import service_bank_account from '~/services/bank_account'
import service_fmcg from '~/services/fmcg'
import service_otp from '~/services/otp'
import service_user from '~/services/user'
import service_user_additional_data from '~/services/user_additional_data'
import service_user_unique_code from '~/services/user_unique_code'
import service_formatter from '~/services/formatter'


import helper from '~/utils/helper'

import api_backend from '~/external_api/api_backend'

import { User } from '~/orm/index'
import { Logs } from '~/orm/index'
import { UserUniqueCode } from '~/orm/index'
import { allowed_user_status } from '~/properties'
import { ACCESS_TOKEN_INTERNAL_DEV } from '~/properties'
import { USERTYPES } from '~/properties'
import { UserQuery } from '~/orm/user'
import { Op } from '~/utils/helper'

import SigninError from '~/response/error/SigninError'
import UnauthorizedRequestError from '~/response/error/UnauthorizedRequestError'
import ChangePasswordError from '~/response/error/ChangePasswordError'
import CustomError from '~/response/error/CustomError'
import CustomSuccess from '~/response/success/CustomSuccess'

const acl_service= ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];

async function update_version(req, res) {
    const token = req.swagger.params.token.value;
    const user_id = req.swagger.params.user_id.value;
    const version = req.swagger.params.version.value;
    try {
        let auth_user = await middleware.verify({token: token, acl: acl_service, is_allowed_access_token_static: false});
        let api_response = await api_backend.update_version({user_id:user_id,version:version});
        res.json(api_response);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal menambahkan pin, '+err;
        }
        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function get_running_text(req, res) {
    const token = req.swagger.params.token.value;
    const user_id = req.swagger.params.user_id.value;
    try {
        let auth_user = await middleware.verify({token: token, acl: acl_service, is_allowed_access_token_static: false});
        let api_response = await api_backend.get_running_text({user_id:user_id,token:token});
        res.json(api_response);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal menambahkan pin, '+err;
        }
        res.status(err.code).json({code: err.code, message: err.message});
    }
}
async function agent_response(req, res) {
    const token = req.swagger.params.token.value;
    const le_code = _.isUndefined(req.swagger.params.le_code.value)?'':req.swagger.params.le_code.value;
    const dt_code = _.isUndefined(req.swagger.params.dt_code.value)?'':req.swagger.params.dt_code.value;
    const response = _.isUndefined(req.swagger.params.response.value)?'':req.swagger.params.response.value;
    const invoice_id = _.isUndefined(req.swagger.params.invoice_id.value)?'':req.swagger.params.invoice_id.value;
    
    try {
//        let auth_user = await middleware.verify({token: token, acl: acl_service, is_allowed_access_token_static: false});
        let api_response = await api_backend.agent_response({le_code:le_code,dt_code:dt_code,response:response,invoice_id:invoice_id});
        res.json(api_response);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal menambahkan pin, '+err;
        }
        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function agent_faktur(req, res) {
    const token = req.swagger.params.token.value;
    const le_code = _.isUndefined(req.swagger.params.le_code.value)?'':req.swagger.params.le_code.value;
    const dt_code = _.isUndefined(req.swagger.params.dt_code.value)?'':req.swagger.params.dt_code.value;
    const invoice_id = _.isUndefined(req.swagger.params.invoice_id.value)?'':req.swagger.params.invoice_id.value;
    
    try {
//        let auth_user = await middleware.verify({token: token, acl: acl_service, is_allowed_access_token_static: false});
        let api_response = await api_backend.agent_faktur({le_code:le_code,dt_code:dt_code,invoice_id:invoice_id});
        res.json(api_response);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal menambahkan pin, '+err;
        }
        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function agent_ceknik(req, res) {
    const token = req.swagger.params.token.value;
    const nik_agent = _.isUndefined(req.swagger.params.nik_agent.value)?'':req.swagger.params.nik_agent.value;
    const invoice_id = _.isUndefined(req.swagger.params.invoice_id.value)?'':req.swagger.params.invoice_id.value;
    
    try {
//        let auth_user = await middleware.verify({token: token, acl: acl_service, is_allowed_access_token_static: false});
        let api_response = await api_backend.agent_ceknik({nik_agent:nik_agent,invoice_id:invoice_id});
        res.json(api_response);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal menambahkan pin, '+err;
        }
        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function notifications_response(req, res) {
    const token = req.swagger.params.token.value;
    const le_code = _.isUndefined(req.swagger.params.le_code.value)?'':req.swagger.params.le_code.value;
    const dt_code = _.isUndefined(req.swagger.params.dt_code.value)?'':req.swagger.params.dt_code.value;
    const confirm = _.isUndefined(req.swagger.params.confirm.value)?'':req.swagger.params.confirm.value;
    const invoice_id = _.isUndefined(req.swagger.params.invoice_id.value)?'':req.swagger.params.invoice_id.value;
    
    try {
        let auth_user = await middleware.verify({token: token, acl: acl_service, is_allowed_access_token_static: false});
        let api_response = await api_backend.notifications_response({le_code:le_code,dt_code:dt_code,invoice_id:invoice_id,confirm:confirm,token:token});
        res.json(api_response);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal menambahkan pin, '+err;
        }
        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function updateGiroPayment(req, res) {
    const token = req.swagger.params.token.value;
    const invoice_id = req.swagger.params.invoice_id.value;
    const dt_code = req.swagger.params.dt_code.value;
    const fmcg_id = req.swagger.params.fmcg_id.value;
    try {
//        let auth_user = await middleware.verify({token: token, acl: acl_service, is_allowed_access_token_static: false});
        let api_response = await api_backend.updateGiroPayment({invoice_id:invoice_id,dt_code:dt_code,fmcg_id:fmcg_id});
        res.json(api_response);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal menambahkan pin, '+err;
        }
        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function send_message_gratika(req, res) {
    const token = req.swagger.params.token.value;
    const pesan = req.swagger.params.pesan.value;
    const phone_no = req.swagger.params.phone_no.value;
    try {
        var today = new Date(); 
        let time_setting = moment(today).format('YYYYMMDD');
        let hasil = sha1(time_setting);
        
        if(hasil != token){
            throw new CustomError(400, 'Token Salah!');
        }
        let api_response = await api_backend.send_message_gratika({pesan:pesan,phone_no:phone_no});
        res.json(api_response);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal menambahkan pin, '+err;
        }
        res.status(err.code).json({code: err.code, message: err.message});
    }
}

export { update_version }
export { get_running_text }
export { agent_response }
export { agent_faktur }
export { agent_ceknik }
export { notifications_response }
export { updateGiroPayment }
export { send_message_gratika }