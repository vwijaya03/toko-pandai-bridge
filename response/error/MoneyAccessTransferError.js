class MoneyAccessTransferError extends Error {
	constructor(message) {
        super(message);
        this.code = 400;
        this.err_code = 4100;
        this.message = 'Gagal melakukan transfer, '+message;
    }
}

export default MoneyAccessTransferError