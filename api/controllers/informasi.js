import _, { iteratee } from 'lodash'
import util from 'util'

import middleware from '~/api/controllers/auth'

import service_informasi from '~/services/informasi'
import service_app_menu from '~/services/app_menu'
import service_notification from '~/services/notification'
import service_fmcg from '~/services/fmcg'

import helper from '~/utils/helper'

import MoneyAccess from '~/external_api/moneyaccess'

import { Logs } from '~/orm/index'
import { USERTYPES } from '~/properties'
import { FMCG } from '~/orm/index'

import CustomError from '~/response/error/CustomError'
import CustomSuccess from '~/response/success/CustomSuccess'



const acl_informasi = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];

async function get_detail_informasi_dan_promosi(req, res) {
    const token = req.swagger.params.token.value;
    const id = req.swagger.params.id.value;

    try {
        Logs.create({action: 'get_detail_informasi_dan_promosi', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_informasi, is_allowed_access_token_static: false});
        let response = await service_informasi.getOne({id: id, is_delete: 0});

        response.createdAt = helper.formatDateTime(response.createdAt);
        response.updatedAt = helper.formatDateTime(response.updatedAt);

        let out = {};
        out.promotion = response;

        res.json(out);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal mendapatkan data detail informasi dan promosi, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function get_informasi_dan_promosi(req, res) {
    const token = req.swagger.params.token.value;
    const page = _.isUndefined(req.swagger.params.page.value) ? 1 : req.swagger.params.page.value;
    const limit = _.isUndefined(req.swagger.params.limit.value) ? 15 : req.swagger.params.limit.value;

    try {
        Logs.create({action: 'get_informasi_dan_promosi', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        await middleware.verify({token: token, acl: acl_informasi, is_allowed_access_token_static: false});
   
        let response = await service_informasi.get({is_delete: 0}, {
            page: page,
            limit:limit
        });

        for(let i = 0; i < response.length; i++){
            
            let arrName = []
            let arrId = response[i].fmcg_id.split(',').map(Number)
            for(let j=0; j < arrId.length; j++){
                
                if(arrId[j] !== 0){
                    let name = await service_fmcg.getOneFmcgWithFilter({id:arrId[j]})
                    arrName.push(name.dataValues.name)
                }
            }
            response[i].dataValues.fmcg_name = arrName
        }
        
        let count = await service_informasi.count({is_delete: 0}, {});

        response.map(async (value, index) => {
            value.dataValues.fmcg_id = value.fmcg_id.split(',').map(Number)
            value.dataValues.user_type = (value.user_type === null)?[]:value.user_type.split(',').map(Number)
            value.dataValues.createdAt = helper.formatDateTime(value.createdAt);
            value.dataValues.updatedAt = helper.formatDateTime(value.updatedAt);
        });

        let out = {};
        
        out.promotions = response;
        out.totaldata = count;

        res.json(out);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal mendapatkan data informasi dan promosi, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function add_informasi_dan_promosi(req, res) {
    const token = req.swagger.params.token.value;
    const param_title = req.swagger.params.title;
    const param_content = req.swagger.params.content;
    const param_img_path = req.swagger.params.img_path;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_user_type = req.swagger.params.user_type;

    let title = _.isUndefined(param_title.value)?'':param_title.value;
    let content = _.isUndefined(param_content.value)?'':param_content.value;
    let img_path = _.isUndefined(param_img_path.value)?'':param_img_path.value;
    let fmcg_id= _.isUndefined(param_fmcg_id.value)?'':param_fmcg_id.value;
    let user_type = _.isUndefined(param_user_type.value)?'':param_user_type.value;

    let result_message = JSON.stringify({
        title: title,
        body: content
    });

    try {
        Logs.create({action: 'add_informasi_dan_promosi', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_informasi, is_allowed_access_token_static: true});
        let informasi_dan_promosi_args = {};
        informasi_dan_promosi_args.title = title;
        informasi_dan_promosi_args.content = content;
        informasi_dan_promosi_args.img_path = img_path;
        informasi_dan_promosi_args.fmcg_id = fmcg_id;
        informasi_dan_promosi_args.user_type = user_type
        informasi_dan_promosi_args.createdAt = helper.formatCurrentDateTime();
        informasi_dan_promosi_args.updatedAt = helper.formatCurrentDateTime();

        await service_informasi.createInformasiDanPromosi(informasi_dan_promosi_args);

        let notification_args = {};
        notification_args.to = "/topics/promotion-notification";
        notification_args.type = "data";
        notification_args.message = result_message;
        notification_args.state = 0;
        notification_args.createdAt = helper.formatCurrentDateTime();
        notification_args.updatedAt = helper.formatCurrentDateTime();

        await service_notification.addNotification(notification_args);

        res.json(new CustomSuccess(200, 'Data informasi dan promosi berhasil di tambahkan'));
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal menambahkan data detail informasi dan promosi, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function kios(req,res){
    const token = req.swagger.params.token.value;

    try {
        Logs.create({action: 'kios', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_informasi, is_allowed_access_token_static: false});
        let out = {}
        if(auth_user.dataValues.mobile_version.substring(0, 2) < 55){
                out = {menus: await service_app_menu.getbyid({})}
        }else{out = {menus: await service_app_menu.getbyids({})}}
        res.json(out);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal mendapatkan data kios, '+err;
        }

        res.status(400).json({code: 400, message: err.message});
    }
}

async function othermenus(req,res){
    const token = req.swagger.params.token.value;

    try {
        Logs.create({action: 'othermenus', description: helper.getSimpleSwaggerParams(req.swagger.params)});
        
        let auth_user = await middleware.verify({token: token, acl: acl_informasi, is_allowed_access_token_static: false});
        let menus, role_name = '';

        role_name = await middleware.getUSERTYPESKey(USERTYPES, auth_user.dataValues.type);

        if(auth_user.dataValues.type == 0) {
            menus = await service_app_menu.get({scope : 'other', role: role_name});
        } else if(auth_user.dataValues.type == 2) {
            menus = await service_app_menu.get({scope : 'other', role: role_name});
        } else {
            menus = [];
        }

        let out = {};
        out.menus = menus;

        res.json(out);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal mendapatkan data menu lain, '+err;
        }

        res.status(400).json({code: 400, message: err.message});
    }
}

async function othermenus_proteksi_pandai(req,res){
    const token = req.swagger.params.token.value;

    try {
        Logs.create({action: 'othermenus', description: helper.getSimpleSwaggerParams(req.swagger.params)});
        
        let auth_user = await middleware.verify({token: token, acl: acl_informasi, is_allowed_access_token_static: false});
        let menus, role_name = '';

        role_name = await middleware.getUSERTYPESKey(USERTYPES, auth_user.dataValues.type);

        if(auth_user.dataValues.type == 0) {
            menus = await service_app_menu.get({scope : 'asuransi', role: role_name});
        } else if(auth_user.dataValues.type == 2) {
            menus = await service_app_menu.get({scope : '0', role: role_name});
        } else {
            menus = [];
        }

        let out = {};
        out.menus = menus;

        res.json(out);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal mendapatkan data menu lain, '+err;
        }

        res.status(400).json({code: 400, message: err.message});
    }
}

async function hide_informasi(req,res){
    const token = req.swagger.params.token.value;
    const param_id = req.swagger.params.id;
    const param_is_delete = req.swagger.params.is_delete;

    let id = _.isUndefined(param_id.value)?'':param_id.value;
    let is_delete = _.isUndefined(param_is_delete.value)?'':param_is_delete.value;

    try {
        Logs.create({action: 'hide_delete', description: helper.getSimpleSwaggerParams(req.swagger.params)});
        
        await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});
    
        await service_informasi.hide({id:id,is_delete:is_delete})
        res.json({code:200,message:'success update status'});
        
        
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal mendapatkan data menu lain, '+err;
        }

        res.status(400).json({code: 400, message: err.message});
    }
}

export { kios }
export { othermenus }
export { get_detail_informasi_dan_promosi }
export { get_informasi_dan_promosi }
export { add_informasi_dan_promosi }
export { othermenus_proteksi_pandai }
export { hide_informasi }