import service_notification from '~/services/notification'

import { orm } from '~/services/mariadb'
import { MobileVersion } from '~/orm/index'

async function get(filter, options) {
	options = options || {};
	let result_set = '';

	if(filter) {
        result_set = await MobileVersion.findAll({where: filter});
    } else {
        result_set = await MobileVersion.findAll();
    }

    if (!result_set || Object.keys(result_set).length == 0) {
        if (options.throwIfEmpty) {
            throw new NotificationNotFoundError();
        }
    }

    return result_set;
}

async function getOne() {
	let response = await orm.query('SELECT * FROM mobile_versions WHERE createdAt = (SELECT MAX(createdAt) FROM mobile_versions)');

	if (response == null || response == undefined) {
        return [];
    }
    return response[0];
}

async function addMobileVersion({version=null, download_link=null, force=null, update_description=null, createdAt=null, updatedAt=null}={}) {
	let args = {
        version:            version, 
        download_link:      download_link,
        force:              force,
        update_description: update_description,
        createdAt:          createdAt,
        updatedAt:          updatedAt
    };

    let response = await MobileVersion.create(args);

    let message = { title:'Versi Baru Tersedia', message:"Version: " + version + ", Update description: "+ update_description };
    let notification_args = {
        to: '/topics/mobile-version',
        type: 'data',
        message: JSON.stringify(message),
        state: 0,
        createdAt: createdAt,
        updatedAt: updatedAt
    };

    await service_notification.addNotification(notification_args);

    return response;
}

let obj = {}
obj.get = get
obj.getOne = getOne
obj.addMobileVersion = addMobileVersion

export default obj