class FmcgGetTotalUnpaidAmountError extends Error {
	constructor(message) {
        super(message);
        this.code = 400;
        this.err_code = 4300;
        this.message = 'Gagal menampilkan jumlah invoice yang belum terbayar, '+message;
    }
}

export default FmcgGetTotalUnpaidAmountError