import { orm } from '~/services/mariadb'
import Sequelize from 'sequelize'


const Invoice = orm.define('invoice', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    session_id : Sequelize.STRING,
    file_name : Sequelize.STRING,
    dt_code : Sequelize.STRING,
    le_code :Sequelize.STRING,
    outlet_code : Sequelize.STRING,
    cash_memo_total_amount : Sequelize.DECIMAL,
    cash_memo_balance_amount : Sequelize.DECIMAL,
    cashmemo_type :Sequelize.STRING,
    invoice_id : Sequelize.STRING,
    invoice_sales_date : Sequelize.DATE,
    invoice_due_date : Sequelize.DATE,
    invoice_payment_status_paid : Sequelize.DECIMAL,
    invoice_payment_status_unpaid :Sequelize.DECIMAL,
    invoice_details : Sequelize.TEXT,
    approval_date : Sequelize.DATE,
    status : Sequelize.INTEGER
}, {
    timestamps: false,
    freezeTableName: true,

  // define the table's name
  tableName: 'invoice'
});

export default Invoice
