import _ from 'lodash'
import moment from 'moment'

function parseHistory({response=null}={}) {
    if(response.message != null) {
        return [];
    }

    let transDateMoment = null;
    let formattedList = response.map(item => {
        let formatted = {};

        transDateMoment = moment(item.trans_date, "YYYY-MM-DD HH:mm:ss");
        formatted.valdoTxId = item.transid;
        formatted.historyid = item.historyid;
        formatted.transact_date = Number((transDateMoment.unix() * 1000) + '');
        formatted.date_time = Number((transDateMoment.unix() * 1000) + '');
        formatted.amount = item.amount;

        if(item.trans_type == 'transfer') {
            formatted.type = 0; 
            formatted.description = item.desc;
            formatted.balance = (item.saldo) ? Number(_.replace(item.saldo, /,/g, "")) : 0;
            formatted.dk = item.dk;
        }else if(item.trans_type == 'payment_unilever') {
            formatted.type = 0; 
            formatted.description = item.desc;
            formatted.balance = (item.saldo) ? Number(_.replace(item.saldo, /,/g, "")) : 0;
            formatted.dk = item.dk;
        }else if(item.trans_type == 'payment_kgrosir') {
            formatted.type = 0; 
            formatted.description = item.desc;
            formatted.balance = (item.saldo) ? Number(_.replace(item.saldo, /,/g, "")) : 0;
            formatted.dk = item.dk;
        }
        else if(item.trans_type == 'payment_limit') {
            formatted.type = 0; 
            formatted.description = item.desc;
            formatted.balance = (item.saldo) ? Number(_.replace(item.saldo, /,/g, "")) : 0;
            formatted.dk = item.dk;
        }else if(item.trans_type == 'payment_walls') {
            formatted.type = 0; 
            formatted.description = item.desc;
            formatted.balance = (item.saldo) ? Number(_.replace(item.saldo, /,/g, "")) : 0;
            formatted.dk = item.dk;
        }else if(item.trans_type == 'payment_kppob') {
            formatted.type = 0; 
            formatted.description = item.desc;
            formatted.balance = (item.saldo) ? Number(_.replace(item.saldo, /,/g, "")) : 0;
            formatted.dk = item.dk;
        }else if(item.trans_type == 'payment_abc') {
            formatted.type = 0; 
            formatted.description = item.desc;
            formatted.balance = (item.saldo) ? Number(_.replace(item.saldo, /,/g, "")) : 0;
            formatted.dk = item.dk;
        }else if(item.trans_type == 'payment_umum') {
            formatted.type = 0; 
            formatted.description = item.desc;
            formatted.balance = (item.saldo) ? Number(_.replace(item.saldo, /,/g, "")) : 0;
            formatted.dk = item.dk;
        }else if(item.trans_type == 'payment_lakme') {
            formatted.type = 0; 
            formatted.description = item.desc;
            formatted.balance = (item.saldo) ? Number(_.replace(item.saldo, /,/g, "")) : 0;
            formatted.dk = item.dk;
        }else if(item.trans_type == 'payment_fflag') {
            formatted.type = 0; 
            formatted.description = item.desc;
            formatted.balance = (item.saldo) ? Number(_.replace(item.saldo, /,/g, "")) : 0;
            formatted.dk = item.dk;
        }else if(item.trans_type == 'payment_essilor') {
            formatted.type = 0; 
            formatted.description = item.desc;
            formatted.balance = (item.saldo) ? Number(_.replace(item.saldo, /,/g, "")) : 0;
            formatted.dk = item.dk;
        }else if(item.trans_type == 'payment_borwita') {
            formatted.type = 0; 
            formatted.description = item.desc;
            formatted.balance = (item.saldo) ? Number(_.replace(item.saldo, /,/g, "")) : 0;
            formatted.dk = item.dk;
        }else if(item.trans_type == 'payment_teguk') {
            formatted.type = 0; 
            formatted.description = item.desc;
            formatted.balance = (item.saldo) ? Number(_.replace(item.saldo, /,/g, "")) : 0;
            formatted.dk = item.dk;
        }else if(item.trans_type == 'payment_horeca') {
            formatted.type = 0; 
            formatted.description = item.desc;
            formatted.balance = (item.saldo) ? Number(_.replace(item.saldo, /,/g, "")) : 0;
            formatted.dk = item.dk;
        }else if(item.trans_type == 'payment_ksayur') {
            formatted.type = 0; 
            formatted.description = item.desc;
            formatted.balance = (item.saldo) ? Number(_.replace(item.saldo, /,/g, "")) : 0;
            formatted.dk = item.dk;
        }else if(item.trans_type == 'payment_ondemand') {
            formatted.type = 0; 
            formatted.description = item.desc;
            formatted.balance = (item.saldo) ? Number(_.replace(item.saldo, /,/g, "")) : 0;
            formatted.dk = item.dk;
        }else if(item.trans_type == 'topup') {
            formatted.type = 1;
            formatted.description = item.desc;
            formatted.balance = (item.saldo) ? Number(_.replace(item.saldo, /,/g, "")) : 0;
            formatted.dk = item.dk;
        }else if(item.trans_type == 'topup_pbl') {
            formatted.type = 1;
            formatted.description = item.desc;
            formatted.balance = (item.saldo) ? Number(_.replace(item.saldo, /,/g, "")) : 0;
            formatted.dk = item.dk;
        }
        else if(item.trans_type == 'topup-reversal') {
            formatted.type = 2;
            formatted.description = item.desc;
            formatted.balance = (item.saldo) ? Number(_.replace(item.saldo, /,/g, "")) : 0;
            formatted.dk = item.dk;
        } else if(item.trans_type == 'disbursment') {
            formatted.type = 3;
            formatted.description = item.desc;
            formatted.balance = (item.saldo) ? Number(_.replace(item.saldo, /,/g, "")) : 0;
            formatted.dk = item.dk;
        } else if(item.trans_type == 'pickup') {
            formatted.type = 4; 
            formatted.description = item.desc;
            formatted.balance = (item.saldo) ? Number(_.replace(item.saldo, /,/g, "")) : 0;
            formatted.dk = item.dk;
        } else if(item.trans_type == 'transfer_cl') {
            formatted.type = 5; 
            formatted.description = item.desc;
            formatted.balance = (item.saldo) ? Number(_.replace(item.saldo, /,/g, "")) : 0;
            formatted.dk = item.dk;
        } else if(item.trans_type == 'ppob') {
            formatted.type = 6; 
            formatted.description = item.desc;
            formatted.balance = (item.saldo) ? Number(_.replace(item.saldo, /,/g, "")) : 0;
            formatted.dk = item.dk;
        } else if( item.trans_type == 'ppob_pbl') {
            formatted.type = 6; 
            formatted.description = item.desc;
            formatted.balance = (item.saldo) ? Number(_.replace(item.saldo, /,/g, "")) : 0;
            formatted.dk = item.dk;
        }  else if(item.trans_type == 'cashback') {
            formatted.type = 7; 
            formatted.description = item.desc;
            formatted.balance = (item.saldo) ? Number(_.replace(item.saldo, /,/g, "")) : 0;
            formatted.dk = item.dk;
        }
        else if(item.trans_type == 'transfer_orl') {
            formatted.type = 0; 
            formatted.description = item.desc;
            formatted.balance = (item.saldo) ? Number(_.replace(item.saldo, /,/g, "")) : 0;
            formatted.dk = item.dk;
        }else if(item.trans_type == 'fee_tpn') {
            formatted.type = 11; 
            formatted.description = item.desc;
            formatted.balance = (item.saldo) ? Number(_.replace(item.saldo, /,/g, "")) : 0;
            formatted.dk = item.dk;
        }
        
        return formatted;
        // formatted._source = 'moneyaccess'; // to differentiate the source of the record.
        // formatted.type = item.trans_type;
        // formatted.description = item.desc;
        // formatted.dk = item.dk;

        // if (item.saldo) {
        //     formatted.saldo = _.replace(item.saldo, /,/g, "");
        // } else {
        //     formatted.saldo = 0;
        // }
    });

    formattedList.pagination = response.pagination;
    formattedList.pagenumber = response.pagenumber;
    formattedList.itempage = response.itempage;
    formattedList.totalpage = response.totalpage;
    formattedList.totalitem = response.totalitem;

    let out = formattedList;
    return out;
}

let obj = {};
obj.parseHistory = parseHistory;

export default obj