import { orm } from '~/services/mariadb'
import Sequelize from 'sequelize'

const User = orm.define('new_users', {
    user_id: {
        type: Sequelize.INTEGER, field: 'user_id',
        primaryKey: true,
        autoIncrement: true
    },
    fullname: {
        type: Sequelize.STRING, allowNull:true
    },
    nama_toko: {
        type: Sequelize.STRING, allowNull:true
    },
    password: {
        type: Sequelize.STRING, allowNull:false,
        get(key) {
            var value = this.getDataValue(key);
            if (value instanceof Buffer) {
                value = value.toString('utf8');
            } else {
                value = '';
            }
            return value;
        }
    },
    password_changed: {
        type: Sequelize.STRING, allowNull:true
    },
    phone: {
        type: Sequelize.STRING, allowNull:false
    },
    tanggal_lahir: {
        type: Sequelize.STRING, allowNull:true
    },
    tempat_lahir: {
        type: Sequelize.STRING, allowNull:true
    },
    ktp: {
        type: Sequelize.STRING, allowNull:true
    },
    email: {
        type: Sequelize.STRING, allowNull:false, unique:true
    },
    pin: {
        type: Sequelize.STRING, allowNull:true
    },
    type: {
        type: Sequelize.INTEGER, allowNull:false
    },
    salt: {
        type: Sequelize.STRING, allowNull:false, unique:true
    },
    valdoAccount: {
        type: Sequelize.STRING, allowNull:true, field: 'valdo_account'
    },
    fcm_id: {
        type: Sequelize.STRING, allowNull:true, field: 'fcm_id'
    },
    imei: {
        type: Sequelize.STRING, allowNull:true
    },
    accessToken: {
        type: Sequelize.STRING, allowNull:false, field: 'access_token'
    },
    accessTokenExpiry: {
        type: 'TIMESTAMP', allowNull:false, field: 'access_token_expiry'
    },
    join_date: {
        type: Sequelize.DATE, allowNull:true
    },
    status: {
        type: Sequelize.INTEGER, allowNull:false
    },
    mobile_version: {
        type: Sequelize.STRING, allowNull:true
    },
    register_type: {
        type: Sequelize.STRING, allowNull:true
    },
}, {
    freezeTableName: true, // Model tableName will be the same as the model name
    timestamps: false
});

function queryTypes({filter=null, typesList=null}={}) {
    if (! typesList) {
        return;
    }

    filter.$or = typesList.map(type => {
        return {'type':type};
    });
}

export default User

let query = {};
query.types = queryTypes;

export { query as UserQuery }