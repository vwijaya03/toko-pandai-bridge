import _ from 'lodash'
import util from 'util'

import middleware from '~/api/controllers/auth'
import { Logs } from '~/orm/index'

import service_vouchers from '~/services/vouchers'
const acl_get_voucher_detail = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];
const acl_get_list_vouchers = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];

async function get_list_vouchers(req, res) {
    const token = req.swagger.params.token.value;
    let invoice_id = req.swagger.params.invoice_id.value;
    try{
        Logs.create({action: 'get_list_vouchers', description: `param req token: ${token}, invoice_id: ${invoice_id}`});
        let auth_user = await middleware.verify({token: token, acl: acl_get_list_vouchers, is_allowed_access_token_static: false});
        let response = await service_vouchers.get();

        let out = {};
        out.vouchers = response;
        res.json(out);
    } catch(err){
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal melihat list voucher, '+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function get_voucher_detail(req, res) {
    const token = req.swagger.params.token.value;
    let voucher_id = req.swagger.params.voucher_id.value;

    try{
        Logs.create({action: 'get_voucher_detail', description: `param req token: ${token}, voucher_id: ${voucher_id}`});

        let auth_user = await middleware.verify({token: token, acl: acl_get_voucher_detail, is_allowed_access_token_static: false});
        let response = await service_vouchers.getOne({id: voucher_id});

        let out = {};
        out.voucher = response;
        res.json(out);
    } catch(err){
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal melihat data voucher, '+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

export { get_list_vouchers }
export { get_voucher_detail }