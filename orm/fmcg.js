import { orm } from '~/services/mariadb'
import Sequelize from 'sequelize'

const FMCG = orm.define('fmcgs', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: Sequelize.STRING, allowNull:false
    },
    payment_type: {
        type: Sequelize.STRING, allowNull:false
    },
    limit_unique_code: {
        type: Sequelize.INTEGER, allowNull:false
    },
    url: {
        type: Sequelize.STRING, allowNull:false
    },
    fmcg_token: {
        type: Sequelize.STRING, allowNull:false
    },
    fmcg_icon: {
        type: Sequelize.STRING, allowNull:false
    },
    is_topex: {
        type: Sequelize.BOOLEAN, allowNull:false
    },
    uploadable_file: {
        type: Sequelize.INTEGER, allowNull:false
    },
    createdAt: {
        type: Sequelize.DATE, allowNull:true
    },
    delete: {
        type: Sequelize.INTEGER, allowNull:false
    },
});

export default FMCG