import request from 'request'
import util from 'util'

import { Logs } from '~/orm/index'

import ResponseAdapter from '~/adapter/ResponseAdapter'

import PPOBCheckPriceError from '~/response/error/PPOBCheckPriceError'
import PPOBInquiryError from '~/response/error/PPOBInquiryError'
import PPOBPaymentServiceError from '~/response/error/PPOBPaymentServiceError'
import PPOBTopupServiceError from '~/response/error/PPOBTopupServiceError'
import PPOBReceiptError from '~/response/error/PPOBReceiptError'

const protocol = 'http://';
const host = '192.168.12.2';
const prefix = '/api/v1/';

const url_ppob_receipt = protocol+host+prefix+'trx_his/detil';
const url_ppob_price_service = protocol+host+prefix+'ppob/price_service';
const url_ppob_topup_service = protocol+host+prefix+'ppob/topup_service';
const url_ppob_inquiry_service = protocol+host+prefix+'ppob/inquiry_service';
const url_ppob_payment_service = protocol+host+prefix+'ppob/payment_service';
const url_ppob_lookup_service = protocol+host+prefix+'ppob/trans';

let tokenkey = 'gUxOsYYXbeywpM2xaV37RaNvdZ5nJdI8d8AdYvy4';

function commonHeaders() {
    return {
        'Accept': 'application/json',
        'Cache-Control': 'no-cache',
        'apigatekey': 'webKEY123456789'
    };
}

function authHeaders(headers) {
    headers = headers || {};
    headers['Authorization'] = 'Bearer ' + tokenkey;
    return headers;
}

function handleResponse(rawJsonResponse, errorClass, resolve, reject, requestError, function_name) {
    try {
		let {error:err, response:successJsonResp, pagination: pagination} = ResponseAdapter.parse({response:JSON.parse(rawJsonResponse)});
        
		if(requestError != null) {
			reject(new errorClass(requestError));
		}

		if(successJsonResp.code == 400 || successJsonResp.code == 401 || err) {
			reject(new errorClass(err.message));
		} else {
                    resolve(successJsonResp);
		}
	} catch(err) {
		Logs.create({action: `error ${function_name}`, description: util.inspect(err), additional_description: util.inspect(rawJsonResponse)});

		reject(new errorClass('terjadi kesalahan silahkan coba lagi'));
    }
    //let {error:err, response:successJsonResp, pagination: pagination} = ResponseAdapter.parse({response:JSON.parse(rawJsonResponse)});

    // if (err) {
    //     reject(new errorClass(err.message));

    // } else {
    //     successJsonResp = Object.assign(successJsonResp, pagination);
    //     resolve(successJsonResp);
    // }
}

function handleResponses(rawJsonResponse, errorClass, resolve, reject, requestError, function_name, fmcg_name) {

	try {
		let successJsonResp = JSON.parse(rawJsonResponse);
	
		if(requestError != null) {
			reject(new errorClass(requestError));
		}

		if(successJsonResp.code == 400 || successJsonResp.code == 401) {
			reject(new errorClass(successJsonResp.message));
		} else {
			resolve(successJsonResp);
		}
	} catch(err) {
		Logs.create({action: `error ${function_name} ${fmcg_name}`, description: util.inspect(err), additional_description: util.inspect(rawJsonResponse)});

		reject(new errorClass('terjadi kesalahan silahkan coba lagi'));
	}
	
}

function handleResponseError(errorClass, resolve, reject, err, function_name) {
	Logs.create({action: `error ${function_name}`, description: util.inspect(err)});
	
	reject(new errorClass(err));
}

function ppob_check_price({product_type=null, provider=null}={}) {

    return new Promise(function(resolve, reject) {
        let form = { product_type: product_type, provider: provider};
        let headers = commonHeaders();
        console.log(form,"form ppob ===========================================")
        request.post( {url:url_ppob_price_service, headers:headers, form:form}, function(err, httpResponse, body) {
            if(err) {
	    		return err;
            }
            
            handleResponse(body, PPOBCheckPriceError, resolve, reject, err, 'ppob_check_price');
        }).on('error', function(err) {
			handleResponseError(PPOBCheckPriceError, resolve, reject, err, 'ppob_check_price');
		});
    });
}

function ppob_inquiry({product_code=null, customer_number1=null, customer_number2=null, customer_number3=null, misc=null}={})
{
    return new Promise(function(resolve, reject) {
        let form = {product_code:product_code, customer_number1:customer_number1, customer_number2:customer_number2, customer_number3:customer_number3, misc:misc};
        let headers = commonHeaders();
        
        request.post( {url:url_ppob_inquiry_service, headers:headers, form:form}, function(err, httpResponse, body) {
            if(err) {
	    		return err;
            }
            
            handleResponse(body, PPOBInquiryError, resolve, reject, err, 'ppob_inquiry');
        }).on('error', function(err) {
			handleResponseError(PPOBInquiryError, resolve, reject, err, 'ppob_inquiry');
		});
    });
}

function ppob_topup_service({model=null, product_code=null, customer_number1=null, vaccount=null, pin=null}={}) {

    return new Promise(function(resolve, reject) {
        let form = {model:model, product_code:product_code, customer_number1:customer_number1, vaccount: vaccount, pin:pin};
        let headers = commonHeaders();
        request.post( {url:url_ppob_topup_service, headers:headers, form:form}, function(err, httpResponse, body) {
            if(err) {
	    		return err;
            }
            
            handleResponse(body, PPOBTopupServiceError, resolve, reject, err, 'ppob_topup_service');
        }).on('error', function(err) {
			handleResponseError(PPOBTopupServiceError, resolve, reject, err, 'ppob_topup_service');
		});
    });
}

function ppob_payment_service({product_code=null, customer_number1=null, customer_number2=null, customer_number3=null, nominal=null, ref_number=null, misc=null, vaccount=null, pin=null}={})
{
    return new Promise(function(resolve, reject) {
        let form = {product_code:product_code, customer_number1:customer_number1, customer_number2:customer_number2, customer_number3:customer_number3, nominal:nominal, ref_number:ref_number, misc:misc, vaccount:vaccount, pin:pin};
        let headers = commonHeaders();

        request.post( {url:url_ppob_payment_service, headers:headers, form:form}, function(err, httpResponse, body) {
            if(err) {
	    		return err;
            }
            
            handleResponse(body, PPOBPaymentServiceError, resolve, reject, err, 'ppob_payment_service');
        }).on('error', function(err) {
			handleResponseError(PPOBPaymentServiceError, resolve, reject, err, 'ppob_payment_service');
		});
    });
}

function ppob_receipt({historyid=null}={}) {
    return new Promise(function(resolve, reject) {
        let form = {historyid: historyid};
        let headers = commonHeaders();

        request.post( {url:url_ppob_receipt, headers:headers, form:form}, function(err, httpResponse, body) {
            if(err) {
	    		return err;
            }
            
            handleResponse(body, PPOBReceiptError, resolve, reject, err, 'ppob_receipt');
        }).on('error', function(err) {
			handleResponseError(PPOBReceiptError, resolve, reject, err, 'ppob_receipt');
		});
    });
}

function ppob_lookup({refnumber=null}={}) {
    return new Promise(function(resolve, reject) {
        let form = {RefNumber: refnumber};
        let headers = commonHeaders();

        request.post( {url:url_ppob_lookup_service, headers:headers, form:form}, function(err, httpResponse, body) {
            if(err) {
                return err;
            }
            
            handleResponse(body, PPOBReceiptError, resolve, reject, err, 'ppob_receipt');
        }).on('error', function(err) {
            handleResponseError(PPOBReceiptError, resolve, reject, err, 'ppob_receipt');
        });
    });
}

let obj = {};

obj.ppob_check_price = ppob_check_price;
obj.ppob_inquiry = ppob_inquiry;
obj.ppob_topup_service = ppob_topup_service;
obj.ppob_payment_service = ppob_payment_service;
obj.ppob_receipt = ppob_receipt;
obj.ppob_lookup =ppob_lookup;

export default obj
