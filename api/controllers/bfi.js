'use strict';

import _ from 'lodash'
import util from 'util'
import moment from 'moment'
import xmlParser from 'xml2json'
import soapRequest from 'easy-soap-request'

import { ACCESS_TOKEN_INTERNAL_DEV } from '~/properties'
import { Logs } from '~/orm/index'

import middleware from '~/api/controllers/auth'
import service_bfi from '~/services/bfi'
import service_pelunasan_manual from '~/services/pelunasan_manual'
import helper from '~/utils/helper'
import CustomError from '~/response/error/CustomError'
import CustomSuccess from '~/response/success/CustomSuccess'
import UnauthorizedRequestError from '~/response/error/UnauthorizedRequestError'

const acl_bfi= ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];

let bfi_url = 'https://app.bfi.co.id/WebServerService/MobileApps/wsBA.asmx?op=';
let hosts = 'app.bfi.co.id';
let SOAPAction = 'http://matel/WSBA/';

async function inputLeadCustomerAndAsset(req,res){
	const log_action = "BFI : inputLeadCustomerAndAsset";
	const token = req.swagger.params.token.value;

	let CustomerName = req.swagger.params.name.value;
	let Address = req.swagger.params.address.value;
	let MobilePhone = req.swagger.params.phone.value;
	let AssetName = req.swagger.params.asset_name.value;
        let ManufacturingYear = req.swagger.params.manufacturing_year.value;
	let tenor = req.swagger.params.tenor.value;
	let param_installment = req.swagger.params.installment.value;
        let param_FundingAmount = req.swagger.params.fundingamount.value;
	let FundingAmount = param_FundingAmount.replace(/[^0-9]/g, '');
        let installment = param_installment.replace(/[^0-9]/g, '');

	Logs.create({action: log_action, description: "CustomerName: "+CustomerName+", Address: "+Address+", MobilePhone: "+MobilePhone,created_at: Date.now(),updated_at: Date.now()});

	try {
		let auth_user = await middleware.verify({token: token, acl: acl_bfi, is_allowed_access_token_static: false});
//
//		const url = bfi_url+'inputLeadCustomer';
//		const headers = {
//			'Content-Type': 'text/xml',
//			'Host':hosts,
//			'Content-Length': 'length',
//			'SOAPAction':SOAPAction+'inputLeadCustomer'
//		};
//		let xml =
//		`<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
//		<soap:Body>
//		 <inputLeadCustomer xmlns="http://matel/WSBA">
//		   <CustomerID></CustomerID>
//		   <InputBy>999SB00053</InputBy>
//		   <BranchID>999</BranchID>
//		   <BranchIDSurvey>999</BranchIDSurvey>
//		   <CustomerType>NEW</CustomerType>
//		   <CustomerLevel>level 1</CustomerLevel>
//		   <CustomerName>Tokopandai-`+CustomerName+`</CustomerName>
//		   <Address>`+Address+`</Address>
//		   <MobilePhone>`+MobilePhone+`</MobilePhone>
//		   <ResidenceAreaPhone>021</ResidenceAreaPhone>
//		   <ResidencePhone>12133</ResidencePhone>
//		   <CompanyAreaPhone>021</CompanyAreaPhone>
//		   <CompanyPhone>12345</CompanyPhone>
//		   <Email>topan@gmail.com</Email>
//		   <IndustryType>0010001001</IndustryType>
//		   <KTP></KTP>
//		   <BranchAgreementActive></BranchAgreementActive>
//		   <AgreementActive></AgreementActive>
//		   <IsDigital>0</IsDigital>
//		   <CustomerProfile>Tokopandai</CustomerProfile>
//		   <UserNameAPI>Tokopandai</UserNameAPI>
//		   <PasswordAPI>Tokopandai02112018</PasswordAPI>
//		   <ReffCustomerID></ReffCustomerID>
//		   <SourceChannel>Tokopandai</SourceChannel>
//		 </inputLeadCustomer>
//		</soap:Body>
//		</soap:Envelope>`;
//
//		const response = await soapRequest(url, headers, xml);
//		const body = response.response.body;
//		const statusCode = response.response.statusCode;
//
//		let data = JSON.parse(xmlParser.toJson(body));
//		let print =JSON.parse(data["soap:Envelope"]["soap:Body"]["inputLeadCustomerResponse"]["inputLeadCustomerResult"]);
		let cusappid=helper.currentMilisecondsTime();
                let args = {
			customer_name:CustomerName,
			address:Address,
			mobile_phone:MobilePhone,
			cust_appid:cusappid,
			user_id :auth_user.dataValues.user_id
		};

		service_bfi.insert(args);

		inputLeadAsset(req,res,cusappid,AssetName,ManufacturingYear,installment,FundingAmount,tenor);

	} catch(err) {
		if(err.code === undefined) {
			 err.code = 400;
			 err.message = 'Insert Gagal :'+err;
		}
		err.message = (err.message === undefined) ? err : err.message;
		res.status(400).json({code: 400, message: err.message});
	}
}

async function inputLeadAsset(req,res,Cust_APPID,AssetName,ManufacturingYear,installment,FundingAmount,tenor){
	const log_action = "BFI : inputLeadAsset";
	Logs.create({action: log_action, description: "Cust_APPID: "+Cust_APPID+", AssetName: "+AssetName+", ManufacturingYear: "+ManufacturingYear+", installment: "+installment+", FundingAmount: "+FundingAmount+", tenor: "+tenor ,created_at: Date.now(),updated_at: Date.now()});

	try {
//		const url = bfi_url+'inputLeadCustomer';
//		const headers = {
//			'Content-Type': 'text/xml',
//			'Host':hosts,
//			'Content-Length': 'length',
//			'SOAPAction':SOAPAction+'inputLeadAsset'
//		};
//
//		let xml =
//		`<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
//		<soap:Body>
//		<inputLeadAsset xmlns="http://matel/WSBA">
//			<Cust_APPID>`+Cust_APPID+`</Cust_APPID>
//			<ProductCategory>NDF Motorcycle</ProductCategory>
//			<SupplierID></SupplierID>
//			<ActivityID>DRSLG</ActivityID>
//			<ReffCustomerID></ReffCustomerID>
//			<ReffBranchEmployeeID></ReffBranchEmployeeID>
//			<ReffEmployeeID></ReffEmployeeID>
//			<ReffBranchBA></ReffBranchBA>
//			<ReffBA></ReffBA>
//			<BranchID>999</BranchID>
//			<BranchIDSurvey>999</BranchIDSurvey>
//			<AssetSeqNo>1</AssetSeqNo>
//			<AssetName>`+AssetName+`</AssetName>
//			<ManufacturingYear>`+ManufacturingYear+`</ManufacturingYear>
//			<StatusData></StatusData>
//			<LicensePlate></LicensePlate>
//			<NoSTNK>0</NoSTNK>
//			<FundingAmount>`+FundingAmount+`</FundingAmount>
//			<NTF>0</NTF>
//			<Installment>`+installment+`</Installment>
//			<Tenor>`+tenor+`</Tenor>
//			<BranchID></BranchID>
//			<LoginID>999SB00053</LoginID>
//			<UserNameAPI>Tokopandai</UserNameAPI>
//			<PasswordAPI>Tokopandai02112018</PasswordAPI>
//		</inputLeadAsset>
//		</soap:Body>
//		</soap:Envelope>`;
//		const response = await soapRequest(url, headers, xml);
//		const body = response.response.body;
//		const statusCode = response.response.statusCode;
//		let data = await JSON.parse(xmlParser.toJson(body));
//		let datas = await JSON.parse(data["soap:Envelope"]["soap:Body"]["inputLeadAssetResponse"]["inputLeadAssetResult"]);
//		if(datas[0].Result == 99) {
//			service_bfi.delete_pengajuan({cust_appid:Cust_APPID});
//			res.status(400).json({code: 400, message:'Duplicate Phone Number Insert.'});
//		} else {
			let args = {
				asset_name: AssetName,
				manufacturing_year: ManufacturingYear,
				funding_amount:FundingAmount,
				installment:installment,
				tenor:tenor,
				cust_appid:Cust_APPID
			};
			service_bfi.update(args);
			res.status(200).json({code: 200, status: 'success'});
//		}
	} catch(err) {
		if(err.code === undefined) {
			 err.code = 400;
			 err.message = 'Insert Gagal :'+err;
		}
		err.message = (err.message === undefined) ? err : err.message;
		res.status(400).json({code: 400, message: err.message});
	}
}

async function getStatusTracking(req,res){
	const log_action = "BFI : getStatusTracking";
	const token = req.swagger.params.token.value;
	let cust_appid = req.swagger.params.cust_appid.value;
	try {
		let auth_user = await middleware.verify({token: token, acl: acl_bfi, is_allowed_access_token_static: false});
		Logs.create({action: log_action, description: "Cust App ID: "+cust_appid,created_at: Date.now(),updated_at: Date.now()});

//		const url = bfi_url+'getStatusTracking';
//		const headers = {
//			'Content-Type': 'text/xml',
//			'Host':hosts,
//			'Content-Length': 'length',
//			'SOAPAction':SOAPAction+'getStatusTracking'
//		};
//		let xml =
//		`<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
//		<soap:Body>
//		<getStatusTracking xmlns="http://matel/WSBA">
//		<UserNameAPI>Tokopandai</UserNameAPI>
//		<PasswordAPI>Tokopandai02112018</PasswordAPI>
//		<CustAppID>`+cust_appid+`</CustAppID>
//		<AssetSeqNo>1</AssetSeqNo>
//		</getStatusTracking>
//		</soap:Body>
//		</soap:Envelope>`;
//		const response = await soapRequest(url, headers, xml);
//		const body = response.response.body;
//		const statusCode = response.response.statusCode;
//		let data = await JSON.parse(xmlParser.toJson(body));
//
//		if(JSON.parse(data["soap:Envelope"]["soap:Body"]["getStatusTrackingResponse"]["getStatusTrackingResult"]) == ''){
//			res.status(400).json({code: 400, message: "Data Kosong"});
//		} else {
//			let stat = await JSON.parse(data["soap:Envelope"]["soap:Body"]["getStatusTrackingResponse"]["getStatusTrackingResult"]);
//			let args = {
//                  fundingamount:stat[0].FundingAmount,
//                  telestatus: stat[0].TeleStatus,
//                  teledatetime:stat[0].TeleDatetime,
//                  telereason:stat[0].TeleReason,
//                  surveydscheduledatetime:stat[0].SurveyScheduleDatetime,
//                  surveyrescheduledatetime:stat[0].SurveyRescheduleDatetime,
//                  surveystatus:stat[0].SurveyStatus,
//                  surveydatetime:stat[0].SurveyDatetime,
//                  surveyreason:stat[0].SurveyReason,
//                  approvalstatus: stat[0].ApprovalStatus,
//                  approvaldatetime: stat[0].ApprovalDatetime,
//                  golivestatus: stat[0].GoliveStatus,
//                  golivedatetime:stat[0].GoliveDatetime,
//                  result:stat[0].Result,
//                  notes:stat[0].Notes,
//                  cust_appid:cust_appid
//              };
//        new Promise((resolve, reject) => {
//				return Promise.resolve()
//				.then(() => {
//					return service_bfi.updateStatus(args);
//				}).then((p_result)=>{
//					(async () => {
             let view = await service_bfi.getOneStatusTracking({cust_appid:cust_appid,p_result:p_result});
             let detail_toko = await service_pelunasan_manual.detail_toko({user_id:view[0]['user_id']})

             var bfi = {};
             //bfi.id= view[0]['id'];
             bfi.customer_name= view[0]['customer_name'];
             bfi.address= view[0]['address'];
             bfi.mobile_phone= view[0]['mobile_phone'];
             bfi.asset_name= view[0]['asset_name'];
             bfi.manufacturing_year= view[0]['manufacturing_year'];
             bfi.funding_amount_pengajuan= view[0]['funding_amount'];
             bfi.funding_amount_approved= Number(view[0]['fundingamount']);
             bfi.installment= view[0]['installment'];
             bfi.tenor= view[0]['tenor'];
             bfi.cust_appid= view[0]['cust_appid'];
             bfi.telestatus= view[0]['telestatus'];
             bfi.teledatetime= view[0]['teledatetime'];
             bfi.telereason= view[0]['telereason'];
             bfi.surveydscheduledatetime= view[0]['surveydscheduledatetime'];
             bfi.surveyrescheduledatetime= view[0]['surveyrescheduledatetime'];
             bfi.surveystatus= view[0]['surveystatussurveystatus'];
             bfi.surveydatetime= view[0]['surveydatetime'];
             bfi.surveyreason= view[0]['surveyreason'];
             bfi.approvalstatus= view[0]['approvalstatus'];
             bfi.approvaldatetime= view[0]['approvaldatetime'];
             bfi.golivestatus= view[0]['golivestatus'];
             bfi.golivedatetime= view[0]['golivedatetime'];
             bfi.result= view[0]['result'];
	     bfi.notes= view[0]['notes'];
	     bfi.toko = detail_toko[0]
             bfi.createdAt= view[0]['createdAt'];
	     bfi.updatedAt= view[0]['updatedAt'];
            res.status(200).json({result:bfi});
    }
	} catch(err) {
		if(err.code === undefined) {
			err.code = 400;
			err.message = 'Gagal :'+err;
		}

		err.message = (err.message === undefined) ? err : err.message;
		res.status(400).json({code: 400, message: err.message});
	}
}

async function getSchedulerStatus(req,res){
	const token = req.swagger.params.token.value;

	try {
		let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});
		//Logs.create({action: log_action, description: "Cust App ID: "+cust_appid,created_at: Date.now(),updated_at: Date.now()});

		let dataCust = await service_bfi.getAllCustAppid()

		for(let i = 0; i < dataCust.length; i++){
			const url = bfi_url+'getStatusTracking';
			const headers = {
				'Content-Type': 'text/xml',
				'Host':hosts,
				'Content-Length': 'length',
				'SOAPAction':SOAPAction+'getStatusTracking'
			};
			let xml =
			`<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
			<soap:Body>
			<getStatusTracking xmlns="http://matel/WSBA">
			<UserNameAPI>Tokopandai</UserNameAPI>
			<PasswordAPI>Tokopandai02112018</PasswordAPI>
			<CustAppID>`+dataCust[i].cust_appid+`</CustAppID>
			<AssetSeqNo>1</AssetSeqNo>
			</getStatusTracking>
			</soap:Body>
			</soap:Envelope>`;
			const response = await soapRequest(url, headers, xml);
			const body = response.response.body;
			const statusCode = response.response.statusCode;
			let data = await JSON.parse(xmlParser.toJson(body));
				let args = {
					  fundingamount:dataCust[i].FundingAmount,
					  telestatus: dataCust[i].TeleStatus,
					  teledatetime:dataCust[i].TeleDatetime,
					  telereason:dataCust[i].TeleReason,
					  surveydscheduledatetime:dataCust[i].SurveyScheduleDatetime,
					  surveyrescheduledatetime:dataCust[i].SurveyRescheduleDatetime,
					  surveystatus:dataCust[i].SurveyStatus,
					  surveydatetime:dataCust[i].SurveyDatetime,
					  surveyreason:dataCust[i].SurveyReason,
					  approvalstatus: dataCust[i].ApprovalStatus,
					  approvaldatetime: dataCust[i].ApprovalDatetime,
					  golivestatus: dataCust[i].GoliveStatus,
					  golivedatetime:dataCust[i].GoliveDatetime,
					  result:dataCust[i].Result,
					  notes:dataCust[i].Notes,
					  cust_appid:dataCust[i].cust_appid
				  };
			new Promise((resolve, reject) => {
					return Promise.resolve()
					.then(() => {
						return service_bfi.updateStatus(args);
					}).then((p_result)=>{
						(async () => {
						  let view = await service_bfi.getOneStatusTracking({cust_appid:dataCust[i].cust_appid,p_result:p_result});
	
				 var bfi = {};
				 //bfi.id= view[0]['id'];
				 bfi.customer_name= view[0]['customer_name'];
				 bfi.address= view[0]['address'];
				 bfi.mobile_phone= view[0]['mobile_phone'];
				 bfi.asset_name= view[0]['asset_name'];
				 bfi.manufacturing_year= view[0]['manufacturing_year'];
				 bfi.funding_amount_pengajuan= view[0]['funding_amount'];
				 bfi.funding_amount_approved= Number(view[0]['fundingamount']);
				 bfi.installment= view[0]['installment'];
				 bfi.tenor= view[0]['tenor'];
				 bfi.cust_appid= view[0]['cust_appid'];
				 bfi.telestatus= view[0]['telestatus'];
				 bfi.teledatetime= view[0]['teledatetime'];
				 bfi.telereason= view[0]['telereason'];
				 bfi.surveydscheduledatetime= view[0]['surveydscheduledatetime'];
				 bfi.surveyrescheduledatetime= view[0]['surveyrescheduledatetime'];
				 bfi.surveystatus= view[0]['surveystatussurveystatus'];
				 bfi.surveydatetime= view[0]['surveydatetime'];
				 bfi.surveyreason= view[0]['surveyreason'];
				 bfi.approvalstatus= view[0]['approvalstatus'];
				 bfi.approvaldatetime= view[0]['approvaldatetime'];
				 bfi.golivestatus= view[0]['golivestatus'];
				 bfi.golivedatetime= view[0]['golivedatetime'];
				 bfi.result= view[0]['result'];
				 bfi.notes= view[0]['notes'];
				 bfi.create_at= view[0]['createdAt'];
				 bfi.update_at= view[0]['updatedAt'];

				await service_bfi.updateCustData(bfi)
				
								})();
						})
				});
		// }
		}
	res.status(200).json({code:200,message:'OK'})
	} catch(err) {
		if(err.code === undefined) {
			err.code = 400;
			err.message = 'Gagal :'+err;
		}

		err.message = (err.message === undefined) ? err : err.message;
		res.status(400).json({code: 400, message: err.message});
	}

}

async function getStatusTrackingAll(req,res){
	const token = req.swagger.params.token.value;
	const param_page = req.swagger.params.page;
	const param_items_per_page = req.swagger.params.items_per_page;
	const param_sorting = req.swagger.params.sorting;
	const param_sortingBy = req.swagger.params.sortingBy;
	const param_cust_appid = req.swagger.params.cust_appid;
	const param_name = req.swagger.params.name;
	const param_date = req.swagger.params.date;
	const param_filter_date_from = req.swagger.params.filter_date_from;
	const param_filter_date_to = req.swagger.params.filter_date_to;

	let date = _.isUndefined(param_date.value)?null:param_date.value;
	let filter_date_from = _.isUndefined(param_filter_date_from.value)?null:param_filter_date_from.value;
	let filter_date_to = _.isUndefined(param_filter_date_to.value)?null:param_filter_date_to.value;
	let startPage = _.isUndefined(param_page.value)?0:param_page.value;
	let itemsPerPage = _.isUndefined(param_items_per_page.value)?15:param_items_per_page.value;
	let name = _.isUndefined(param_name.value) ? null : param_name.value;
	let cust_appid = _.isUndefined(param_cust_appid.value) ? null : param_cust_appid.value;
	let sorting = _.isUndefined(param_sorting.value) ? null : param_sorting.value;
	let sortingBy = _.isUndefined(param_sortingBy.value) ? null : param_sortingBy.value;

	startPage = (startPage < 1 ) ? 1 : startPage;
	try {
		Logs.create({action: 'getStatusTrackingAll', description: helper.getSimpleSwaggerParams(req.swagger.params)});

		let auth_user = await middleware.verify({token: token, acl: acl_bfi, is_allowed_access_token_static: false});
		let user_id =auth_user.dataValues.user_id;
		let role_type = auth_user.dataValues.type;
		let all = await service_bfi.getAllBfi({type:role_type,user_id:user_id,cust_appid:cust_appid,date:date,sorting:sorting,sortingBy:sortingBy,name:name,
			startPage:startPage,itemsPerPage:itemsPerPage,filter_date_from:filter_date_from,filter_date_to:filter_date_to});
		for(let i=0; i < all["list"].length; i++){
			let detail_toko = await service_pelunasan_manual.detail_toko({user_id:all["list"][i]["user_id"]})
			delete all["list"][i]["user_id"]
			all["list"][i]["toko"] = detail_toko[0]
		}
		let {list:data, total:total_bfi} = all;
		let total_pages = Math.ceil(total_bfi / itemsPerPage);
		let out = {};
		out.page = startPage;
		out.total_pages = total_pages;
		out.size = data.length;
		out.total_bfi = total_bfi;
		out.items_per_page = itemsPerPage;
		out.bfi = data;
		res.status(200).json(out);
	} catch(err) {
		if(err.code === undefined) {
			err.code = 400;
			err.message = 'Gagal :'+err;
		}
		err.message = (err.message === undefined) ? err : err.message;
		res.status(400).json({code: 400, message: err.message});
	}
}

export { getStatusTracking }
export { inputLeadCustomerAndAsset }
export { getStatusTrackingAll }
export { getSchedulerStatus }

