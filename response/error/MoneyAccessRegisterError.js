class MoneyAccessRegisterError extends Error {
	constructor(message) {
        super(message);
        this.code = 400;
        this.err_code = 4100;
        this.message = 'Register gagal, '+message;
    }
}

export default MoneyAccessRegisterError