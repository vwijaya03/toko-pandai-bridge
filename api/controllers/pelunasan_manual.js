'use strict';

import _ from 'lodash'
import request from 'request'
import moment from 'moment'
import sha1 from 'sha1'; 

import middleware from '~/api/controllers/auth'
import util from 'util'
import helper from '~/utils/helper'

import service_invoice from '~/services/invoice'
import service_fmcg from '~/services/fmcg'
import service_pelunasan_manual from '~/services/pelunasan_manual'
import service_data from '~/services/user_additional_data'

import ApiFmcg from '~/external_api/api_top'
import Apiuli from '~/external_api/api_uli'
import MoneyAccess from '~/external_api/moneyaccess'
import ApiPohonDana from '~/external_api/api_top_pohon_dana'

import { Invoice } from '~/orm/index'
import { Logs } from '~/orm/index'
import { Task } from '~/orm/index'

import UnauthorizedRequestError from '~/response/error/UnauthorizedRequestError'
import CustomError from '~/response/error/CustomError'
import CustomSuccess from '~/response/success/CustomSuccess'

const acl_manual_transfer = ['super_admin'];

//let ip_payment = "192.168.13.2";
//let apigatekey='webKEY123456789';

let ip_payment = "192.168.12.2";
let apigatekey='webKEY123456789';


function trans_id() {
    var d = new Date();
    var n = d.getTime();
    return n;
}

function send_result_borwita(input){
    try {
        request({
            headers: {
                'app-id': 'topan',
                'app-secret': 'topan-ordering-secret',
                'Content-Type': 'application/json'
            },
            url: 'http://192.168.13.5/api-ordering/public/api/v1/invoice-result',
            method: "POST",
            json: true,
            body: input
        }, async function (error, result, body){
            return body
        })
    } catch (error) {
    }
}

async function pelunasan_manual(req,res){
    const token = req.swagger.params.token.value;
    const param_invoice_id = req.swagger.params.invoice_id;
    const param_dt_code = req.swagger.params.dt_code;
    const amount = req.swagger.params.amount.value;

    let fmcg_id = 13;
    let invoice_id = _.isUndefined(param_invoice_id.value)?null:param_invoice_id.value;
    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;

    try {
        let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});
  
        Logs.create({action: 'get_one_invoice', description: helper.getSimpleSwaggerParams(req.swagger.params)});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
          if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal menampilkan data invoice, data fmcg tidak tersedia');
        }

        let invoice_args = {};
        invoice_args.url = result_fmcg.dataValues.url;
        invoice_args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        invoice_args.fmcg_name = result_fmcg.dataValues.name;
        invoice_args.invoice_id = invoice_id;
        invoice_args.dt_code = dt_code;
        
        let api_response_fmcg = await Apiuli.get_one_invoice(invoice_args);
        let invoice = api_response_fmcg;
        
        Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} get_one_invoice`, description: util.inspect(api_response_fmcg)});

        let transid = 'TR'+trans_id();

        let vacc_dist = await service_pelunasan_manual.get_dt_unicode({dt_code:invoice.dt_code})
        let vacc_toko = await service_pelunasan_manual.get_le_unicode({le_code:invoice.le_code}) 
        
        let out = '';
        let api_response = await MoneyAccess.inquiry({mobnum: vacc_toko[0].valdo_account, vaccount: vacc_toko[0].valdo_account, usertype: vacc_toko[0].type});
 
        Logs.create({action: 'api response moneyaccess get_balance inquiry', description: util.inspect(api_response)});
         delete api_response.userid;
        delete api_response.vacc_number;
        
        out = api_response;

         if(invoice.balance_amount == 0){
              throw new CustomError(400, 'Invoice sudah lunas');
        }
        if(out.balance < amount){
              throw new CustomError(400, 'Saldo tidak cukup');
        }
        if (amount > invoice.balance_amount){
            throw new CustomError(400, 'Inputan lebih besar dari inputan invoice');
        }
        if(out.balance == 0 ){
             throw new CustomError(400, 'Saldo kosong');
        }
       
        let invoice_balance = invoice.balance_amount - amount;
       
        let invoice_update = {};

        invoice_update.url = result_fmcg.dataValues.url;
        invoice_update.fmcg_token = result_fmcg.dataValues.fmcg_token;
        invoice_update.fmcg_name = result_fmcg.dataValues.name;
        invoice_update.le_code = invoice.le_code;
        invoice_update.dt_code = dt_code;
        invoice_update.amount = invoice_balance;
        invoice_update.invoice_id = invoice_id;
        
        let invoice_update_res = await ApiFmcg.updatePaymentPembiayaan(invoice_update);
        
        let input_to_transactions = {};

        input_to_transactions.url = result_fmcg.dataValues.url;
        input_to_transactions.fmcg_token = result_fmcg.dataValues.fmcg_token;
        input_to_transactions.fmcg_name = result_fmcg.dataValues.name;
        input_to_transactions.le_code = invoice.le_code;
        input_to_transactions.dt_code = dt_code;
        input_to_transactions.invoice_id = invoice_id;
        input_to_transactions.amount = amount;
        input_to_transactions.valdo_tx_id = "TR"+invoice_id+moment().format('YYYYMMDDHHmmss');
        input_to_transactions.transact_date = moment().format('YYYY-MM-DD HH:mm:ss');
        input_to_transactions.currency_code = "IDR";
        input_to_transactions.type = 0;
        input_to_transactions.distributor_id = vacc_dist[0].id;
        input_to_transactions.retailer_id = vacc_toko[0].id;  
        input_to_transactions.from_user_id = vacc_toko[0].user_id;
        input_to_transactions.to_user_id = vacc_dist[0].user_id;

        let input_to_transactions_res = await ApiFmcg.insert_manual_transaction(input_to_transactions);
        
        
        let data = {
            'dt_code': invoice.dt_code,
            'le_code': invoice.le_code,
            'amount': amount,
            'vaccount_to': vacc_dist[0].valdo_account,
            'vaccount_from': vacc_toko[0].valdo_account,
            'transid': "TR"+invoice_id+moment().format('YYYYMMDDHHmmss'),
            'desc' : `Pembayaran invoice ${invoice_id} Rp. ${helper.IDRformat(amount)} dari ${vacc_toko[0].fullname} (${vacc_toko[0].valdo_account}) ke ${vacc_dist[0].fullname} (${vacc_dist[0].valdo_account})`,
        }
      
        request({
            headers: {
                'apigatekey': apigatekey
            },
            url: 'http://'+ip_payment+'/api/v1/admin/trf',
            method: "POST",
            json: true,   
            formData: data
        }, function (error, result, body){
              res.status(200).json({code:200, message:body});
             
        })
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = ''+err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function pelunasan_manual_tanpa_saldo(req,res){
    const token = req.swagger.params.token.value;
    const param_invoice_id = req.swagger.params.invoice_id;
    const param_dt_code = req.swagger.params.dt_code;
    const amount = req.swagger.params.amount.value;

    let fmcg_id = 13;
    let invoice_id = _.isUndefined(param_invoice_id.value)?null:param_invoice_id.value;
    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;

    try {
        let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});
  
        Logs.create({action: 'get_one_invoice', description: helper.getSimpleSwaggerParams(req.swagger.params)});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
          if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal menampilkan data invoice, data fmcg tidak tersedia');
        }

        let invoice_args = {};
        invoice_args.url = result_fmcg.dataValues.url;
        invoice_args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        invoice_args.fmcg_name = result_fmcg.dataValues.name;
        invoice_args.invoice_id = invoice_id;
        invoice_args.dt_code = dt_code;
        
        let api_response_fmcg = await Apiuli.get_one_invoice(invoice_args);
        let invoice = api_response_fmcg;
        
        Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} get_one_invoice`, description: util.inspect(api_response_fmcg)});


        let vacc_dist = await service_pelunasan_manual.get_dt_unicode({dt_code:invoice.dt_code})
        let vacc_toko = await service_pelunasan_manual.get_le_unicode({le_code:invoice.le_code}) 

        if (amount > invoice.balance_amount){
            throw new CustomError(400, 'inputan lebih besar dari inputan invoice');
        }
         if(invoice.balance_amount == 0){
              throw new CustomError(400, 'Invoice sudah lunas');
        }
        
       
        let invoice_balance = invoice.balance_amount - amount;
       
        let invoice_update = {};

        invoice_update.url = result_fmcg.dataValues.url;
        invoice_update.fmcg_token = result_fmcg.dataValues.fmcg_token;
        invoice_update.fmcg_name = result_fmcg.dataValues.name;
        invoice_update.le_code = invoice.le_code;
        invoice_update.dt_code = dt_code;
        invoice_update.amount = invoice_balance;
        invoice_update.invoice_id = invoice_id;
        
        let invoice_update_res = await ApiFmcg.updatePaymentPembiayaan(invoice_update);
        
        let input_to_transactions = {};

        input_to_transactions.url = result_fmcg.dataValues.url;
        input_to_transactions.fmcg_token = result_fmcg.dataValues.fmcg_token;
        input_to_transactions.fmcg_name = result_fmcg.dataValues.name;
        input_to_transactions.le_code = invoice.le_code;
        input_to_transactions.dt_code = dt_code;
        input_to_transactions.invoice_id = invoice_id;
        input_to_transactions.amount = amount;
        input_to_transactions.valdo_tx_id = "TR"+invoice_id+moment().format('YYYYMMDDHHmmss');
        input_to_transactions.transact_date = moment().format('YYYY-MM-DD HH:mm:ss');
        input_to_transactions.currency_code = "IDR";
        input_to_transactions.type = 0;
        input_to_transactions.distributor_id = vacc_dist[0].id;
        input_to_transactions.retailer_id = vacc_toko[0].id;  
        input_to_transactions.from_user_id = vacc_toko[0].user_id;
        input_to_transactions.to_user_id = vacc_dist[0].user_id;

        let input_to_transactions_res = await ApiFmcg.insert_manual_transaction(input_to_transactions);
        res.status(200).json({code:200, message:input_to_transactions_res});

    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = ''+err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function get_pelunasan_manual(req,res){
    const token = req.swagger.params.token.value;
    const param_invoice_id = req.swagger.params.invoice_id;
    const param_dt_code = req.swagger.params.dt_code;

    let fmcg_id = 13;
    let invoice_id = _.isUndefined(param_invoice_id.value)?null:param_invoice_id.value;
    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;

    try {
        let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});
  
        Logs.create({action: 'get_one_invoice', description: helper.getSimpleSwaggerParams(req.swagger.params)});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
          if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal menampilkan data invoice, data fmcg tidak tersedia');
        }

        let invoice_args = {};
        invoice_args.url = result_fmcg.dataValues.url;
        invoice_args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        invoice_args.fmcg_name = result_fmcg.dataValues.name;
        invoice_args.invoice_id = invoice_id;
        invoice_args.dt_code = dt_code;
        
        let api_response_fmcg = await Apiuli.get_one_invoice(invoice_args);
        let invoice = api_response_fmcg;
        
        Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} get_one_invoice`, description: util.inspect(api_response_fmcg)});

        let transid = 'TR'+trans_id();

        let vacc_dist = await service_pelunasan_manual.get_dt_unicode({dt_code:invoice.dt_code})
        let vacc_toko = await service_pelunasan_manual.get_le_unicode({le_code:invoice.le_code}) 
        let out = '';
        let api_response = await MoneyAccess.inquiry({mobnum: vacc_toko[0].valdo_account, vaccount: vacc_toko[0].valdo_account, usertype: vacc_toko[0].type});
        Logs.create({action: 'api response moneyaccess get_balance inquiry', description: util.inspect(api_response)});

        delete api_response.userid;
        delete api_response.vacc_number;
        
        out = api_response;
        
        let response_out = {}; 
        response_out.saldo_toko = out.balance
        response_out.data_toko = vacc_toko
        response_out.data_distributor =vacc_dist
        response_out.data_invoice=invoice
        
        res.status(200).json({code:200, message:response_out});
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = ''+err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function get_pelunasan_manual_all(req,res){
    const token = req.swagger.params.token.value;
    const invoice_id = req.swagger.params.invoice_id.value;
    
    let fmcg_id = 13;
    try {
        let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});
  
        Logs.create({action: 'get_one_invoice', description: helper.getSimpleSwaggerParams(req.swagger.params)});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
          if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal menampilkan data invoice, data fmcg tidak tersedia');
        }

        let invoice_args = {};
        invoice_args.url = result_fmcg.dataValues.url;
        invoice_args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        invoice_args.fmcg_name = result_fmcg.dataValues.name;
        invoice_args.invoice_id = invoice_id;
        
        let api_response_fmcg = await Apiuli.get_manual_invoice(invoice_args);
        let invoice = api_response_fmcg;
        
        Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} get_one_invoice`, description: util.inspect(api_response_fmcg)});

        
        res.status(200).json({api_response_fmcg});
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = ''+err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function get_agent_details(req,res){
    const token = req.swagger.params.token.value;

    try{
        let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});

        let data_agent = await service_pelunasan_manual.get_agent();

        res.status(200).json({code:200, body:data_agent})

    }catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = ''+err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function get_all_user_top(req,res){
    const token = req.swagger.params.token.value;
    const param_page = req.swagger.params.page;
    const param_items_per_page = req.swagger.params.items_per_page;
    const param_le_code = req.swagger.params.le_code;

    let startPage = _.isUndefined(param_page.value)?0:param_page.value;
    let itemsPerPage = _.isUndefined(param_items_per_page.value)?15:param_items_per_page.value;
    let le_code = _.isUndefined(param_le_code.value)?0:param_le_code.value;
    startPage = (startPage < 1 ) ? 1 : startPage;

    try{
        let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});

        let data_user = await service_pelunasan_manual.user_detail({fmcg_id:1, startPage:startPage, itemsPerPage:itemsPerPage,le_code:le_code});
        let count = await service_pelunasan_manual.countData({fmcg_id:1, startPage:startPage, itemsPerPage:itemsPerPage,le_code:le_code})
        let total_pages = Math.ceil(count[0].total / itemsPerPage);
        let out = {};
        out.page = startPage;
        out.total_pages = total_pages;
        out.size = data_user.length;
        out.items_per_page = itemsPerPage;
        out.invoice = data_user;

        res.status(200).json(out);

    

    }catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = ''+err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}


async function pelunasan_manual_walls(req,res){
    const token = req.swagger.params.token.value;
    const param_invoice_id = req.swagger.params.invoice_id;
    const param_dt_code = req.swagger.params.dt_code;
    const amount = req.swagger.params.amount.value;

    let fmcg_id = 2;
    let invoice_id = _.isUndefined(param_invoice_id.value)?null:param_invoice_id.value;
    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;

    try {
        let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});
  
        Logs.create({action: 'get_one_invoice', description: helper.getSimpleSwaggerParams(req.swagger.params)});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
          if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal menampilkan data invoice, data fmcg tidak tersedia');
        }

        let invoice_args = {};
        invoice_args.url = result_fmcg.dataValues.url;
        invoice_args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        invoice_args.fmcg_name = result_fmcg.dataValues.name;
        invoice_args.invoice_id = invoice_id;
        invoice_args.dt_code = dt_code;
        
        let api_response_fmcg = await Apiuli.get_one_invoice(invoice_args);
        let invoice = api_response_fmcg;
        
        Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} get_one_invoice`, description: util.inspect(api_response_fmcg)});

        let transid = 'TR'+trans_id();

        let vacc_dist = await service_pelunasan_manual.get_dt_unicode({dt_code:invoice.dt_code})
        let vacc_toko = await service_pelunasan_manual.get_le_unicode({le_code:invoice.le_code}) 
        
        let out = '';
        let api_response = await MoneyAccess.inquiry({mobnum: vacc_toko[0].valdo_account, vaccount: vacc_toko[0].valdo_account, usertype: vacc_toko[0].type});
 
        Logs.create({action: 'api response moneyaccess get_balance inquiry', description: util.inspect(api_response)});

        delete api_response.userid;
        delete api_response.vacc_number;
        
        out = api_response;

         if(invoice.balance_amount == 0){
              throw new CustomError(400, 'Invoice sudah lunas');
        }
        if(out.balance < amount){
              throw new CustomError(400, 'Saldo tidak cukup');
        }
        if (amount > invoice.balance_amount){
            throw new CustomError(400, 'Inputan lebih besar dari inputan invoice');
        }
        if(out.balance == 0 ){
             throw new CustomError(400, 'Saldo kosong');
        }
       
        let invoice_balance = invoice.balance_amount - amount;
       
        let invoice_update = {};

        invoice_update.url = result_fmcg.dataValues.url;
        invoice_update.fmcg_token = result_fmcg.dataValues.fmcg_token;
        invoice_update.fmcg_name = result_fmcg.dataValues.name;
        invoice_update.le_code = invoice.le_code;
        invoice_update.dt_code = dt_code;
        invoice_update.amount = invoice_balance;
        invoice_update.invoice_id = invoice_id;
        
        let invoice_update_res = await ApiFmcg.updatePaymentPembiayaan(invoice_update);
        
        let input_to_transactions = {};

        input_to_transactions.url = result_fmcg.dataValues.url;
        input_to_transactions.fmcg_token = result_fmcg.dataValues.fmcg_token;
        input_to_transactions.fmcg_name = result_fmcg.dataValues.name;
        input_to_transactions.le_code = invoice.le_code;
        input_to_transactions.dt_code = dt_code;
        input_to_transactions.invoice_id = invoice_id;
        input_to_transactions.amount = amount;
        input_to_transactions.valdo_tx_id = "TR"+invoice_id+moment().format('YYYYMMDDHHmmss');
        input_to_transactions.transact_date = moment().format('YYYY-MM-DD HH:mm:ss');
        input_to_transactions.currency_code = "IDR";
        input_to_transactions.type = 0;
        input_to_transactions.distributor_id = vacc_dist[0].id;
        input_to_transactions.retailer_id = vacc_toko[0].id;  
        input_to_transactions.from_user_id = vacc_toko[0].user_id;
        input_to_transactions.to_user_id = vacc_dist[0].user_id;
        let input_to_transactions_res = await ApiFmcg.insert_manual_transaction(input_to_transactions);
        
        
        
        let data = {
            'dt_code': invoice.dt_code,
            'le_code': invoice.le_code,
            'amount': amount,
            'vaccount_to': vacc_dist[0].valdo_account,
            'vaccount_from': vacc_toko[0].valdo_account,
            'transid': "TR"+invoice_id+moment().format('YYYYMMDDHHmmss'),
            'desc' : `Pembayaran invoice ${invoice_id} Rp. ${helper.IDRformat(amount)} dari ${vacc_toko[0].fullname} (${vacc_toko[0].valdo_account}) ke ${vacc_dist[0].fullname} (${vacc_dist[0].valdo_account})`,
        }
      
        request({
            headers: {
                'apigatekey': apigatekey
            },
            url: 'http://'+ip_payment+'/api/v1/admin/trf',
            method: "POST",
            json: true,   
            formData: data
        }, function (error, result, body){
              res.status(200).json({code:200, message:body});
             
        })
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = ''+err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function pelunasan_manual_tanpa_saldo_walls(req,res){
    const token = req.swagger.params.token.value;
    const param_invoice_id = req.swagger.params.invoice_id;
    const param_dt_code = req.swagger.params.dt_code;
    const amount = req.swagger.params.amount.value;

    let fmcg_id = 2;
    let invoice_id = _.isUndefined(param_invoice_id.value)?null:param_invoice_id.value;
    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;

    try {
        let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});
  
        Logs.create({action: 'get_one_invoice', description: helper.getSimpleSwaggerParams(req.swagger.params)});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
          if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal menampilkan data invoice, data fmcg tidak tersedia');
        }

        let invoice_args = {};
        invoice_args.url = result_fmcg.dataValues.url;
        invoice_args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        invoice_args.fmcg_name = result_fmcg.dataValues.name;
        invoice_args.invoice_id = invoice_id;
        invoice_args.dt_code = dt_code;
        
        let api_response_fmcg = await Apiuli.get_one_invoice(invoice_args);
        let invoice = api_response_fmcg;
        
        Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} get_one_invoice`, description: util.inspect(api_response_fmcg)});


        let vacc_dist = await service_pelunasan_manual.get_dt_unicode({dt_code:invoice.dt_code})
        let vacc_toko = await service_pelunasan_manual.get_le_unicode({le_code:invoice.le_code}) 

        if (amount > invoice.balance_amount){
            throw new CustomError(400, 'inputan lebih besar dari inputan invoice');
        }
         if(invoice.balance_amount == 0){
              throw new CustomError(400, 'Invoice sudah lunas');
        }
        
       
        let invoice_balance = invoice.balance_amount - amount;
       
        let invoice_update = {};

        invoice_update.url = result_fmcg.dataValues.url;
        invoice_update.fmcg_token = result_fmcg.dataValues.fmcg_token;
        invoice_update.fmcg_name = result_fmcg.dataValues.name;
        invoice_update.le_code = invoice.le_code;
        invoice_update.dt_code = dt_code;
        invoice_update.amount = invoice_balance;
        invoice_update.invoice_id = invoice_id;
        
        let invoice_update_res = await ApiFmcg.updatePaymentPembiayaan(invoice_update);
        
        let input_to_transactions = {};

        input_to_transactions.url = result_fmcg.dataValues.url;
        input_to_transactions.fmcg_token = result_fmcg.dataValues.fmcg_token;
        input_to_transactions.fmcg_name = result_fmcg.dataValues.name;
        input_to_transactions.le_code = invoice.le_code;
        input_to_transactions.dt_code = dt_code;
        input_to_transactions.invoice_id = invoice_id;
        input_to_transactions.amount = amount;
        input_to_transactions.valdo_tx_id = "TR"+invoice_id+moment().format('YYYYMMDDHHmmss');
        input_to_transactions.transact_date = moment().format('YYYY-MM-DD HH:mm:ss');
        input_to_transactions.currency_code = "IDR";
        input_to_transactions.type = 0;
        input_to_transactions.distributor_id = vacc_dist[0].id;
        input_to_transactions.retailer_id = vacc_toko[0].id;  
        input_to_transactions.from_user_id = vacc_toko[0].user_id;
        input_to_transactions.to_user_id = vacc_dist[0].user_id;

        let input_to_transactions_res = await ApiFmcg.insert_manual_transaction(input_to_transactions);
        res.status(200).json({code:200, message:input_to_transactions_res});

    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = ''+err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function get_pelunasan_manual_walls(req,res){
    const token = req.swagger.params.token.value;
    const param_invoice_id = req.swagger.params.invoice_id;
    const param_dt_code = req.swagger.params.dt_code;

    let fmcg_id = 2;
    let invoice_id = _.isUndefined(param_invoice_id.value)?null:param_invoice_id.value;
    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;

    try {
        let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});
  
        Logs.create({action: 'get_one_invoice', description: helper.getSimpleSwaggerParams(req.swagger.params)});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
          if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal menampilkan data invoice, data fmcg tidak tersedia');
        }

        let invoice_args = {};
        invoice_args.url = result_fmcg.dataValues.url;
        invoice_args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        invoice_args.fmcg_name = result_fmcg.dataValues.name;
        invoice_args.invoice_id = invoice_id;
        invoice_args.dt_code = dt_code;
        
        let api_response_fmcg = await Apiuli.get_one_invoice(invoice_args);
        let invoice = api_response_fmcg;
        
        Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} get_one_invoice`, description: util.inspect(api_response_fmcg)});

        let transid = 'TR'+trans_id();

        let vacc_dist = await service_pelunasan_manual.get_dt_unicode({dt_code:invoice.dt_code})
        let vacc_toko = await service_pelunasan_manual.get_le_unicode({le_code:invoice.le_code}) 
        let out = '';
        let api_response = await MoneyAccess.inquiry({mobnum: vacc_toko[0].valdo_account, vaccount: vacc_toko[0].valdo_account, usertype: vacc_toko[0].type});
        Logs.create({action: 'api response moneyaccess get_balance inquiry', description: util.inspect(api_response)});

        delete api_response.userid;
        delete api_response.vacc_number;
        
        out = api_response;
        
        let response_out = {}; 
        response_out.saldo_toko = out.balance
        response_out.data_toko = vacc_toko
        response_out.data_distributor =vacc_dist
        response_out.data_invoice=invoice
        
        res.status(200).json({code:200, message:response_out});
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = ''+err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function pelunasan_manual_lakme(req,res){
    const token = req.swagger.params.token.value;
    const param_invoice_id = req.swagger.params.invoice_id;
    const param_dt_code = req.swagger.params.dt_code;
    const param_amount = req.swagger.params.amount;
    const param_retur = req.swagger.params.retur

    let fmcg_id = 15;
    let invoice_id = _.isUndefined(param_invoice_id.value)?null:param_invoice_id.value;
    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
    let amount = _.isUndefined(param_amount.value)?0:param_amount.value;
    let retur = _.isUndefined(param_retur.value)?0:param_retur.value;

    try{

        let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});
  
        Logs.create({action: 'get_one_invoice', description: helper.getSimpleSwaggerParams(req.swagger.params)});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
          if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal menampilkan data invoice, data fmcg tidak tersedia');
        }

        let invoice_args = {};
        invoice_args.url = result_fmcg.dataValues.url;
        invoice_args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        invoice_args.fmcg_name = result_fmcg.dataValues.name;
        invoice_args.invoice_id = invoice_id;
        invoice_args.dt_code = dt_code;
        
        let api_response_fmcg = await Apiuli.get_one_invoice(invoice_args);
        let invoice = api_response_fmcg;
        
        Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} get_one_invoice`, description: util.inspect(api_response_fmcg)});

        let transid = 'LM'+trans_id();

        let vacc_dist = await service_pelunasan_manual.get_dt_unicode({dt_code:dt_code})
        let vacc_toko = await service_pelunasan_manual.get_le_unicode({le_code:invoice.le_code}) 
        
        let out = '';
        let api_response = await MoneyAccess.inquiry({mobnum: vacc_toko[0].valdo_account, vaccount: vacc_toko[0].valdo_account, usertype: vacc_toko[0].type});
 
        Logs.create({action: 'api response moneyaccess get_balance inquiry', description: util.inspect(api_response)});

        delete api_response.userid;
        delete api_response.vacc_number;
        
        out = api_response;

         if(invoice.balance_amount == 0){
              throw new CustomError(400, 'Invoice sudah lunas');
        }
        if(out.balance < amount){
              throw new CustomError(400, 'Saldo tidak cukup');
        }
        if (amount > invoice.balance_amount){
            throw new CustomError(400, 'Pembayaran lebih besar dari jumlah tagihan invoice');
        }
        if(out.balance == 0 ){
             throw new CustomError(400, 'Saldo kosong');
        }
        if(amount <= 0){
            throw new CustomError(400, 'Jumlah pembayaran tidak boleh Rp. 0');
        }
        if(retur > invoice.totalRemainingUnboundRetur){
            throw new CustomError(400, 'Retur yang di-input lebih besar dari total retur. Total retur : Rp. '+invoice.totalRemainingUnboundRetur);
        }
       
        let invoice_balance = invoice.balance_amount - (amount + retur);
       
        let invoice_update = {};
        invoice_update.url = result_fmcg.dataValues.url;
        invoice_update.fmcg_token = result_fmcg.dataValues.fmcg_token;
        invoice_update.fmcg_name = result_fmcg.dataValues.name;
        invoice_update.le_code = invoice.le_code;
        invoice_update.dt_code = dt_code;
        invoice_update.amount = invoice_balance;
        invoice_update.invoice_id = invoice_id;
        
        let invoice_update_res = await ApiFmcg.updatePaymentPembiayaan(invoice_update);
        
        let input_to_transactions = {};
        input_to_transactions.url = result_fmcg.dataValues.url;
        input_to_transactions.fmcg_token = result_fmcg.dataValues.fmcg_token;
        input_to_transactions.fmcg_name = result_fmcg.dataValues.name;
        input_to_transactions.le_code = 'payment';
        input_to_transactions.dt_code = dt_code;
        input_to_transactions.invoice_id = invoice_id;
        input_to_transactions.invoice_code = dt_code + invoice_id;
        input_to_transactions.amount = amount;
        input_to_transactions.valdo_tx_id = "LM"+moment().format('YYYYMMDDHHmmss');
        input_to_transactions.transact_date = moment().format('YYYY-MM-DD HH:mm:ss');
        input_to_transactions.currency_code = "IDR";
        input_to_transactions.type = 0;
        input_to_transactions.distributor_id = vacc_dist[0].id;
        input_to_transactions.retailer_id = vacc_toko[0].id;  
        input_to_transactions.from_user_id = vacc_toko[0].user_id;
        input_to_transactions.to_user_id = vacc_dist[0].user_id;

        let input_to_transactions_res = await ApiFmcg.insert_manual_transaction(input_to_transactions);

        let unboundReturs = invoice.unboundReturs
        let usedUnboundReturForTransactionTopan = [];
        let invoice_id_retur
        

        if(unboundReturs.length > 0){
            if(retur > 0) {
                let temp = retur;
    
                unboundReturs.forEach( async result => {
                    
                    if(Number(temp) >= Number(result.cash_memo_balance_amount)) {
    
                        if(result.invoice_id === undefined || result.invoice_id === null){
                            invoice_id_retur = result.cashmemo_type+'/'+result.id
                        }else{
                            invoice_id_retur = result.invoice_id
                        }
                        
                        usedUnboundReturForTransactionTopan.push({fromUserId: vacc_toko[0].user_id, toUserId: vacc_dist[0].user_id, retailer_id: vacc_toko[0].id, distributor_id: vacc_dist[0].id, type: 0, currency_code: "IDR", transact_date: moment().format('YYYY-MM-DD HH:mm:ss'), valdo_tx_id:"LM"+moment().format('YYYYMMDDHHmmss'),amount: Number(result.cash_memo_balance_amount),invoice_id:invoice_id,invoice_id_retur:invoice_id_retur,dt_code:dt_code,dt_code_retur:dt_code,id_retur:result.id,invoice_code:invoice_id_retur+dt_code})
    
                        temp = temp - result.cash_memo_balance_amount;
                        //result.cash_memo_balance_amount = 0;
    
                        let retur_update = {};
                        retur_update.url = result_fmcg.dataValues.url;
                        retur_update.fmcg_token = result_fmcg.dataValues.fmcg_token;
                        retur_update.fmcg_name = result_fmcg.dataValues.name;
                        retur_update.le_code = invoice.le_code;
                        retur_update.dt_code = dt_code;
                        retur_update.amount = 0;
                        retur_update.invoice_id = result.invoice_id;
                        
                        let retur_update_res = await ApiFmcg.updatePaymentPembiayaan(retur_update);
                        
                    } else if(Number(temp) <= Number(result.cash_memo_balance_amount)) {
    
                        if(Number(temp) > 0) {
                            if(result.invoice_id === undefined || result.invoice_id === null){
                                invoice_id_retur = result.cashmemo_type+'/'+result.id
                            }else{
                                invoice_id_retur = result.invoice_id
                            }
    
                            usedUnboundReturForTransactionTopan.push({fromUserId: vacc_toko[0].user_id, toUserId: vacc_dist[0].user_id, retailer_id: vacc_toko[0].id, distributor_id: vacc_dist[0].id, type: 0, currency_code: "IDR", transact_date: moment().format('YYYY-MM-DD HH:mm:ss'), valdo_tx_id:"LM"+moment().format('YYYYMMDDHHmmss'),amount: Number(temp),invoice_id:invoice_id,invoice_id_retur:invoice_id_retur,dt_code:dt_code,dt_code_retur:dt_code,id_retur:result.id,invoice_code:invoice_id_retur+dt_code})
                            
                            let retur_balance_amount = result.cash_memo_balance_amount - temp;
                            temp = 0;
    
                            let retur_update = {};
                            retur_update.url = result_fmcg.dataValues.url;
                            retur_update.fmcg_token = result_fmcg.dataValues.fmcg_token;
                            retur_update.fmcg_name = result_fmcg.dataValues.name;
                            retur_update.le_code = invoice.le_code;
                            retur_update.dt_code = dt_code;
                            retur_update.amount = retur_balance_amount;
                            retur_update.invoice_id = result.invoice_id;
                            
                            let retur_update_res = await ApiFmcg.updatePaymentPembiayaan(retur_update);
                        }    
                    }
                });
            }
        }
        
        if(usedUnboundReturForTransactionTopan.length > 0){

            usedUnboundReturForTransactionTopan.forEach( async result => {
                let input_to_transactions = {};
                input_to_transactions.url = result_fmcg.dataValues.url;
                input_to_transactions.fmcg_token = result_fmcg.dataValues.fmcg_token;
                input_to_transactions.fmcg_name = result_fmcg.dataValues.name;
                input_to_transactions.le_code = invoice.le_code;
                input_to_transactions.dt_code = result.dt_code;
                input_to_transactions.dt_code_retur = result.dt_code_retur;
                input_to_transactions.invoice_id = result.invoice_id;
                input_to_transactions.invoice_id_retur = result.invoice_id_retur;
                input_to_transactions.invoice_code = result.invoice_code;
                input_to_transactions.id_retur = result.id_retur;
                input_to_transactions.amount = result.amount;
                input_to_transactions.valdo_tx_id = result.valdo_tx_id;
                input_to_transactions.transact_date = result.transact_date;
                input_to_transactions.currency_code = result.currency_code;
                input_to_transactions.type = result.type;
                input_to_transactions.distributor_id =vacc_dist[0].id;
                input_to_transactions.retailer_id = vacc_toko[0].id;  
                input_to_transactions.from_user_id = result.fromUserId;
                input_to_transactions.to_user_id = result.toUserId;

                let input_to_transactions_res = await ApiFmcg.insert_manual_retur_transaction(input_to_transactions);
            })
        }
        
        let data = {
            'dt_code': invoice.dt_code,
            'le_code': invoice.le_code,
            'amount': amount,
            'vaccount_to': vacc_dist[0].valdo_account,
            'vaccount_from': vacc_toko[0].valdo_account,
            'transid': "TR"+moment().format('YYYYMMDDHHmmss'),
            'desc' : `Pembayaran invoice ${invoice_id} Rp. ${helper.IDRformat(amount)} dari ${vacc_toko[0].fullname} (${vacc_toko[0].valdo_account}) ke ${vacc_dist[0].fullname} (${vacc_dist[0].valdo_account})`,
        }
      
        request({
            headers: {
                'apigatekey': apigatekey
            },
            url: 'http://'+ip_payment+'/api/v1/admin/trf',
            method: "POST",
            json: true,   
            formData: data
        }, function (error, result, body){
              res.status(200).json({code:200, message:body});
             
        })
    }catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = ''+err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function pelunasan_manual_all_fmcg(req,res){
    const token = req.swagger.params.token.value;
    const param_invoice_id = req.swagger.params.invoice_id;
    const param_dt_code = req.swagger.params.dt_code;
    const amount = req.swagger.params.amount.value;
    const fmcg_id = req.swagger.params.fmcg_id.value;

    let invoice_id = _.isUndefined(param_invoice_id.value)?null:param_invoice_id.value;
    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;

    try {
//        let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});
        var today = new Date(); 
        let time_setting = moment(today).format('YYYYMMDD');
        let hasil = sha1(time_setting);
        
        if(hasil != token){
            throw new CustomError(400, 'Token Salah!');
        }
        
        Logs.create({action: 'get_one_invoice', description: helper.getSimpleSwaggerParams(req.swagger.params)});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
          if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal menampilkan data invoice, data fmcg tidak tersedia');
        }

        let invoice_args = {};
        invoice_args.url = result_fmcg.dataValues.url;
        invoice_args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        invoice_args.fmcg_name = result_fmcg.dataValues.name;
        invoice_args.invoice_id = invoice_id;
        invoice_args.dt_code = dt_code;
        
        let api_response_fmcg = await Apiuli.get_one_invoice(invoice_args);
        let invoice = api_response_fmcg;
        Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} get_one_invoice`, description: util.inspect(api_response_fmcg)});

        let transid = 'TR'+trans_id();

        let vacc_dist = await service_pelunasan_manual.get_dt_unicode({dt_code:dt_code})
        let vacc_toko = await service_pelunasan_manual.get_le_unicode({le_code:invoice.le_code}) 

        let out = '';
        let api_response = await MoneyAccess.inquiry({mobnum: vacc_toko[0].valdo_account, vaccount: vacc_toko[0].valdo_account, usertype: vacc_toko[0].type});
 
        Logs.create({action: 'api response moneyaccess get_balance inquiry', description: util.inspect(api_response)});
         delete api_response.userid;
        delete api_response.vacc_number;
        
        out = api_response;

       if(invoice.balance_amount == 0){
              throw new CustomError(400, 'Invoice sudah lunas');
        }
        if(out.balance < amount){
              throw new CustomError(400, 'Saldo tidak cukup');
        }
        if (amount > invoice.balance_amount){
            throw new CustomError(400, 'Inputan lebih besar dari inputan invoice');
        }
        if(out.balance == 0 ){
             throw new CustomError(400, 'Saldo kosong');
        }
       
        let invoice_balance = invoice.balance_amount - amount;
       
//        ApiPohonDana.update_payment_fmcg(invoice_update);
        let desc = "Bayar";
        if(fmcg_id ==24){
             desc = `Top UP ${invoice_id} Rp. ${helper.IDRformat(amount)} dari ${vacc_toko[0].fullname}(${invoice.nama_gerai}) (${vacc_toko[0].valdo_account}) ke Teguk HQ (${vacc_dist[0].valdo_account})`;
        }else{
             desc = `Pembayaran invoice ${invoice_id} Rp. ${helper.IDRformat(amount)} dari ${vacc_toko[0].fullname} (${vacc_toko[0].valdo_account}) ke ${vacc_dist[0].fullname} (${vacc_dist[0].valdo_account})`;
        }
        let data = {
            'trans_type':result_fmcg.dataValues.payment_type,
            'dt_code': dt_code,
            'le_code': invoice.le_code,
            'amount': amount,
            'vaccount_to': vacc_dist[0].valdo_account,
            'vaccount_from': vacc_toko[0].valdo_account,
            'transid': "TR"+invoice_id.replace(/\//g, '')+moment().format('YYYYMMDDHHmmss'),
            'desc' : desc,
        }

        
      
        request({
            headers: {
                'apigatekey': apigatekey
            },
            url: 'http://'+ip_payment+'/api/v1/admin2/trf',
            method: "POST",
            json: true,   
            formData: data
        }, function (error, result, body){
            if(body.status.code === 200){
                
                let invoice_update = {};
                invoice_update.le_code = invoice.le_code;
                invoice_update.invoice_id = invoice_id;
                invoice_update.amount = amount;
                invoice_update.fmcg_id = fmcg_id;
                invoice_update.paid_by = "Manual"

                ApiPohonDana.update_payment_fmcg(invoice_update);

                let input_to_transactions = {};

                input_to_transactions.fmcg_id = fmcg_id;
                input_to_transactions.dt_code = dt_code;
                input_to_transactions.invoice_id = invoice_id;
                input_to_transactions.amount = amount;
                input_to_transactions.invoice_code = dt_code+invoice_id;
                input_to_transactions.valdo_tx_id = "TR"+invoice_id+moment().format('YYYYMMDDHHmmss');
                input_to_transactions.distributor_id = vacc_dist[0].id;
                input_to_transactions.retailer_id = vacc_toko[0].id;  
                input_to_transactions.from_user_id = vacc_toko[0].user_id;
                input_to_transactions.to_user_id = vacc_dist[0].user_id;
                ApiPohonDana.multifmcg_transactions(input_to_transactions)
                
                if(fmcg_id == 21){
                    let desc_bw = (invoice.balance_amount - amount) > 0 ?"Titip bayar sebagian" : "Pelunasan manual full";
                    let desc = {}
                    desc.dt_code= dt_code,
                    desc.outlet_code= invoice.le_code,
                    desc.le_code= invoice.le_code,
                    desc.invoice_id= invoice_id,
                    desc.transact_date= moment(Date.now()).format('YYYY-MM-DD HH:mm:ss'),
                    desc.applied_amount = amount,
                    desc.remaining_amount= invoice.balance_amount - amount,
                    desc.description= desc_bw;
                    desc.invoice_retur = ''
                    let result = send_result_borwita(desc)
                }

            }
              res.status(200).json({code:200, message:body});
             
        })
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = ''+err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function pelunasan_manual_all_fmcg_no_saldo(req,res){
    const token = req.swagger.params.token.value;
    const param_invoice_id = req.swagger.params.invoice_id;
    const param_dt_code = req.swagger.params.dt_code;
    const amount = req.swagger.params.amount.value;
    const fmcg_id = req.swagger.params.fmcg_id.value;

    let invoice_id = _.isUndefined(param_invoice_id.value)?null:param_invoice_id.value;
    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;

    try {
//        let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});
        var today = new Date(); 
        let time_setting = moment(today).format('YYYYMMDD');
        let hasil = sha1(time_setting);
        
        if(hasil != token){
            throw new CustomError(400, 'Token Salah!');
        }
        
        Logs.create({action: 'get_one_invoice', description: helper.getSimpleSwaggerParams(req.swagger.params)});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
          if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal menampilkan data invoice, data fmcg tidak tersedia');
        }

        let invoice_args = {};
        invoice_args.url = result_fmcg.dataValues.url;
        invoice_args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        invoice_args.fmcg_name = result_fmcg.dataValues.name;
        invoice_args.invoice_id = invoice_id;
        invoice_args.dt_code = dt_code;
        
        let api_response_fmcg = await Apiuli.get_one_invoice(invoice_args);
        let invoice = api_response_fmcg;
        if(invoice.balance_amount == 0){
              throw new CustomError(400, 'Invoice sudah lunas');
        }
        if (amount > invoice.balance_amount){
            throw new CustomError(400, 'Inputan lebih besar dari inputan invoice');
        }else{
        Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} get_one_invoice`, description: util.inspect(api_response_fmcg)});

        let transid = 'TR'+trans_id();

        let vacc_dist = await service_pelunasan_manual.get_dt_unicode({dt_code:dt_code})
        let vacc_toko = await service_pelunasan_manual.get_le_unicode({le_code:invoice.le_code}) 

                
        let invoice_update = {};
        invoice_update.le_code = invoice.le_code;
        invoice_update.invoice_id = invoice_id;
        invoice_update.amount = amount;
        invoice_update.fmcg_id = fmcg_id;
        invoice_update.paid_by = "Manual"

        let payment = await ApiPohonDana.update_payment_fmcg(invoice_update);
        console.log(invoice_update,"++++++++++++++++++++++++++++++++++++")
        console.log(payment,"-------------------------------------")

        let input_to_transactions = {};

        input_to_transactions.fmcg_id = fmcg_id;
        input_to_transactions.dt_code = dt_code;
        input_to_transactions.invoice_id = invoice_id;
        input_to_transactions.amount = amount;
        input_to_transactions.invoice_code = dt_code+invoice_id;
        input_to_transactions.valdo_tx_id = "TR"+invoice_id+moment().format('YYYYMMDDHHmmss');
        input_to_transactions.distributor_id = vacc_dist[0].id;
        input_to_transactions.retailer_id = vacc_toko[0].id;  
        input_to_transactions.from_user_id = vacc_toko[0].user_id;
        input_to_transactions.to_user_id = vacc_dist[0].user_id;
        let trans = await ApiPohonDana.multifmcg_transactions(input_to_transactions)

        if(fmcg_id == 21){
            let desc_bw = (invoice.balance_amount - amount) > 0 ?"Titip bayar sebagian" : "Pelunasan manual full";
            let desc = {}
            desc.dt_code= dt_code,
            desc.outlet_code= invoice.le_code,
            desc.le_code= invoice.le_code,
            desc.invoice_id= invoice_id,
            desc.transact_date= moment(Date.now()).format('YYYY-MM-DD HH:mm:ss'),
            desc.applied_amount = amount,
            desc.remaining_amount= invoice.balance_amount - amount,
            desc.description= desc_bw;
            desc.invoice_retur = ''
            let result = send_result_borwita(desc)
        }
        res.status(200).json({code:200, message:"Payment success"});
    }
       
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = ''+err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

export { pelunasan_manual_lakme }
export { get_agent_details }
export { pelunasan_manual }
export { get_pelunasan_manual }
export { get_pelunasan_manual_all }
export { pelunasan_manual_tanpa_saldo }
export { get_all_user_top }
export { pelunasan_manual_walls }
export { get_pelunasan_manual_walls }
export { pelunasan_manual_tanpa_saldo_walls }
export { pelunasan_manual_all_fmcg }
export { pelunasan_manual_all_fmcg_no_saldo }