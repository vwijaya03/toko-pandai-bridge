class NotificationDestinationNotFoundError extends Error {
	constructor(code, message) {
        super(message);
        this.code = 400;
        this.message = 'Gagal mendapatkan tujuan notifikasi';
    }
}

export default NotificationDestinationNotFoundError