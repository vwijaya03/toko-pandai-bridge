import { orm } from '~/services/mariadb'
import Sequelize from 'sequelize'

const UserUniqueCode = orm.define('users_unique_codes', {
    id: { type: Sequelize.INTEGER, field: 'id', primaryKey: true, autoIncrement: true },
    user_id: { type: Sequelize.STRING, allowNull:false },
    fmcg_id: { type: Sequelize.STRING, allowNull:false },
    unique_code: { type: Sequelize.STRING, allowNull:false },
    description: { type: Sequelize.STRING, allowNull:true },
    createdAt: { type: 'TIMESTAMP', allowNull:true },
    updatedAt: { type: 'TIMESTAMP', allowNull:true }
});

export default UserUniqueCode