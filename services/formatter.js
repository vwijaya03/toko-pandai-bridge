import _ from 'lodash'
import moment from 'moment'
/**
Gets formatter for the entity
@param {String} typeId - (optional) type id. if not specified will use type of entity to figure out formatter.
@param {Object} entity - ORM entity model
*/
function getFormatterFn({entity=null, typeId=null}={}) {

    if (typeId === 'user') {
        return formatUser;
    }
    if (typeId === 'informasi') {
        return formatInformasi;
    }
    else if (typeId === 'transaction') {
        return formatTransaction;
    }
    else if (typeId === 'invoice') {
        return formatInvoice;
    }
    else if (typeId === 'distributor') {
        return formatDistributor;
    }
    else if (typeId === 'retailer') {
        return formatRetailer;
    }
    return null;
}

/**
applies transformation on entity given the descriptor.
@param {Object} desc - descriptor
@param {Sequelize.Model} entity - ORM entity model.
*/
function apply(desc, entity) {

    if (!entity) {
        return null;
    }

    try {
        let out = {};

        for (let key in desc) {

            if (desc.hasOwnProperty(key)) {
                let transform = desc[key];
                let new_key = key;

                let entity_value = null;

                if (! _.isUndefined(entity[key])) {
                    entity_value = entity[key];
                } else {
                    // check if has default value
                    const defaultValue = transform.default_value;
                    if (! _.isNull(defaultValue) && ! _.isUndefined(defaultValue)) {
                        entity_value = defaultValue;
                    } else {
                        entity_value = null;
                    }
                }

                if (transform.rename) {
                    new_key = transform.rename;
                }

                if (_.isFunction(transform)) {
                    if (_.isArray(entity_value)) {
                        out[new_key] = entity_value.map(item => {
                            return transform(item);
                        });
                    } else {
                        let _transformed_value = transform(entity_value);
                        if (! _.isNull(_transformed_value)) {
                            out[new_key] = _transformed_value;
                        }
                    }
                }

                else {
                    if (transform.type) {
                        if (transform.type === 'number') {
                            if (/^\d+$/.test(entity_value)) {
                                out[new_key] = parseInt(entity_value);
                            }
                        }
                        else if (transform.type === 'date') {
                            let formatted_date_str = entity_value+'';
                            let pattern = "YYYY-MM-DD";
                            let momentObj = null;
                            if (transform.date_format) {
                                if (transform.date_format == 'iso8601') {
                                    pattern = "YYYY-MM-DD'T'HH:mm:ssZZ";
                                }
                            }

                            if (moment.isMoment(entity_value)) {
                                momentObj = moment;
                            } else if (_.isNumber(entity_value)) {
                                // assuming entity_value is unix epoch in milliseconds in seconds.
                                momentObj = moment(entity_value);
                            }
                            if (momentObj) {
                                formatted_date_str = momentObj.format(pattern);
                            }
                            out[new_key] = formatted_date_str;
                        }
                    }
                    else if (_.isArray(entity_value)) {
                        out[new_key] = entity_value;
                    }
                    else if (_.isNumber(entity_value)) {
                        out[new_key] = entity_value+'';
                    }
                    else if (_.isBoolean(entity_value)) {
                        out[new_key] = entity_value;
                    }
                    else if (_.isNull(entity_value)) {
                        // exclude NULL value

                        // out[new_key] = entity_value;
                    }
                    else {
                       
                        out[new_key] = (entity_value+'').trim();
                    }
                }
            }
        }

        return out;
    }
    catch(err) {
    }
}

/**

old version of format function

@param {Array | Object} obj - inputs to be formatted, either a list or single item.
@param {String} typeId - (Optional) use formatter for specific type id. if specified will override automatic formatter selection based on instance type.
@param {Object} options - output options
@returns {Array} - formatted objects
*/
function format({obj=null, typeId=null, clazz={}, options={}}={}) {
    try {
        let is_array = (obj instanceof Array);
        let item = {};
        if (is_array) {
            if (obj.length > 0) {
                item = obj[0];
            }
        } else {
            item = obj;
        }

        let fn = getFormatterFn({entity:item, typeId:typeId});
        if (!fn) {
            return obj;
        }

        let out = [];
        if (is_array) {
            out = obj.map((item)=>{
                return fn(item, options);
            });
        } else {
            if (item) {
                out = fn(item, options);
            }
        }
        return out;
    } catch (err) {
    }
}

/**
generates 'user' output format.
@returns {Object} - formatted user
*/
function formatUser(user, options) {
    const desc = {
        "id": {},
        "username": {},
        "email": {},
        "pin": {},
        "type": {type:'number'},
        "fullname": {rename:'name'},
        "nama_toko": {},
        "password_changed": {},
        "valdoAccount": {rename:'valdo_account'},
        "retailerId": {type:'number', rename:'retailer_id'},
        "distributorId": {type:'number', rename:'distributor_id'},
        "salesId": {type:'number', rename:'sales_id'},
        "onbehalf": {},
        "reference": {},
        "phone": {},
        "roles":{},
        "banks":{},
        "retailer": formatRetailer,
        "distributor": formatDistributor,
        "sales": formatSales,
        "pickup": formatPickup
    };
    if (options && options.dev) {
        Object.assign(desc, {
            "id": {type:"string", rename:null},
            "password": {type:"string"},
            "accessToken": {type:"string", rename:'access_token'},
            "accessTokenExpiry": {type:"string", rename:null, rename:'access_token_expiry'}
        });
    }
    if (options && options.excludes) {
        options.excludes.forEach(item => {
            delete desc[item];
        });
    }
    let out = apply(desc, user);
    return out;
}

function formatRetailer(entity, options) {
    const desc = {
        "id": {type:"number"},
        "outletCode": {rename:'outlet_code'},
        "name": {},
        "le_code_dry": {},
        "le_code_ice": {},
        "le_code_ufs": {}
    };
    let out = apply(desc, entity);
    return out;
}

function formatInformasi(entity, options) {
    const desc = {
        "id": {type:"number"},
        "createdAt": {type:'date'},
        "updatedAt": {type:'date'},
        "title": {},
        "content": {},
        "image": {}
    };
    let out = apply(desc, entity);
    return out;
}

function formatTransaction(entity, options) {
    const desc = {
        "id": {},
        "toUserId": {rename: 'to_user_id'},
        "fromUserId": {rename: 'from_user_id'},
        "invoiceId": {rename:'invoice_id'},
        "retailerId": {rename: 'retailer_id'},
        "distributorId": {rename: 'distributor_id'},
        "currencyCode": {rename: 'currency_code'},
        "type": {type:'number'},
        "amount": {},
        "balance":{type:"number"},
        "description":{},
        "dk":{},
        "valdoTxId": {},
        "historyid": {},
        'transactDate': {type:'number', rename: 'transact_date'},
        'date_time': {type:"number"}
        // 'transactDate': {type:'date', date_format:'iso8601', rename: 'transact_date'}
        // '__derive__transactDate': {name: 'transact_date_iso8601'}
    };
    let out = apply(desc, entity);
    return out;
}

function formatDistributor(entity, options) {
    const desc = {
        "id": {type:'number'},
        "distributorCode": {rename:'dt_code'},
        "name": {}
    };
    let out = apply(desc, entity);
    return out;
}

function formatSales(entity, options) {
    const desc = {
        "sales_id": {type:'number'},
        "sales_code": {}
    };
    let out = apply(desc, entity);
    return out;
}

function formatPickup(entity, options) {
    const desc = {
        "id": {type:'number'},
        "pickup_code": {}
    };
    let out = apply(desc, entity);
    return out;
}

function formatInvoice(entity, options) {
    const desc = {
        // "id": {},
        "invoice_id":{rename:'id'},
        "distributorCode": {rename: 'dt_code'},
        "outletCode": {rename:'outlet_code'},
        "le_code": {},
        "outlet_name": {},
        "cash_memo_total_amount":{rename:'total_amount'},
        "cash_memo_balance_amount":{rename:'balance_amount'},
        "cashmemo_type":{},
        "invoice_payment_status_paid":{rename:'payment_status_paid'},
        "invoice_payment_status_unpaid":{rename:'payment_status_unpaid'},
        "invoice_sales_date":{rename:'sales_date', type: 'number'},
        "invoice_due_date":{rename:'due_date', type: 'number'},
        "invoice_payment_status_paid":{},
        "transact_date":{rename:'paid_date', type: 'number'},
        "is_paid":{},
        "is_refunded":{},
        "has_refund":{},
        "status": {},
        "distributor":formatDistributorItem,
        "retur": {},
        "transactions": {},
        "potongan": {},
        "total_potongan": {type: 'number'},
        "paid_amount": {},
        "products":formatInvoiceItem
    };

    applyExcluded({desc:desc, options:options});
    
    let out = apply(desc, entity);

    return out;
}

/**
remove attributes in descriptor.

@param {Object} desc - descriptor
@param {Object} options - format options
*/
function applyExcluded({desc=null, options=null}={}) {
    if (options) {
        if (options.attributes) {
            if (_.isArray(options.attributes)) {
                options.attributes.forEach(item => {
                    let re = /\!(\w+)/;
                    let match = item.match(re);
                    if (match) {
                        let name = match[1];
                        delete desc[name];
                    }
                });
            }
        }
    }
}

function formatInvoiceItem(entity) {
    const desc = {
        "product": {rename:'id'},
        "product_name": {rename:'name'},
        "quantity": {},
        "product_price_cs": {rename:'price_cs'},
        "product_price_dz": {rename:'price_dz'},
        "product_price_pc": {rename:'price_pc'}
    };
    let out = apply(desc, entity);
    return out;
}

function formatDistributorItem(entity) {
    const desc = {
        "dt_code": {},
        "name": {},
    };
    let out = apply(desc, entity);
    return out;
}

// function formatInvoiceItem(entity) {
//     if (DEBUG_LOG) console.log('formatInvoiceItem. entity:', entity);
//     const desc = {
//         "product": {rename:'id'},
//         "product_name": {rename:'name'},
//         "quantity": {},
//         "product_price_cs": {rename:'price_cs'},
//         "product_price_dz": {rename:'price_dz'},
//         "product_price_pc": {rename:'price_pc'}
//     };
//     let out = apply(desc, entity);
//     return out;
// }

function formatRetailBalance(entity) {
    const desc = {
        "balance": {type:"string", rename:null},
    };
    let out = apply(desc, entity);
    return out;
}

function IDRformat(angka) {
    let temprev = parseInt(angka, 10);
    let positive = true;
    
    if(temprev < 0) {
        temprev = temprev * -1;
        positive = false;
    }

    let rev = temprev.toString().split('').reverse().join('');
    let rev2 = '';
    for(let i = 0; i < rev.length; i++){
        rev2  += rev[i];
        if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
            rev2 += '.';
        }
    }

    if(!positive) {
        return '(IDR ' + rev2.split('').reverse().join('') + ')';
    }

    // return 'IDR ' + rev2.split('').reverse().join('');
    return rev2.split('').reverse().join('');
}

let obj = {};
obj.format = format;
obj.formatUser = formatUser;
obj.formatRetailer = formatRetailer;
obj.formatInformasi = formatInformasi;
obj.formatDistributor = formatDistributor;
obj.formatTransaction = formatTransaction;
obj.formatInvoice = formatInvoice;
obj.formatRetailBalance = formatRetailBalance;
obj.IDRformat = IDRformat;

export default obj