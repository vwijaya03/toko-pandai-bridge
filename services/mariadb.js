'use strict';

import { data } from '~/properties';
import Sequelize from 'sequelize'
import log from '~/utils/console_log'

// 192.168.13.4 Development
// 192.168.12.4 Production

const mariadb_config = data.mariadb;

let host = mariadb_config.host;
let port = mariadb_config.port;
let username = mariadb_config.user;
let database = mariadb_config.db;
let password = mariadb_config.password;

let orm = new Sequelize(database, username, password, {
	host: host,
	dialect: 'mysql',
	//dialectOptions: {
    	//useUTC: false, // for reading from database
    //},
    timezone: '+07:00',
	// logging: false,
	freezeTableName: true,
	pool: {
		max: 100,
		min: 0,
		idle: 200000,
		acquire: 1000000
	},
	operatorsAliases: false
});

function init() {
    return checkConnection();
}

function checkConnection() {
    
    log.info('checking db connection');

    return orm.query("SELECT 1;").spread((results, metadata) => {
	  	// Results will be an empty array and metadata will contain the number of affected rows.
	  	log.info('DB connection ok.');
	});
}

let obj = {};
obj.init = init;
obj.checkConnection = checkConnection;

export default obj
export { orm }