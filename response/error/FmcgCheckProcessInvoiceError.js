class FmcgCheckProcessInvoiceError extends Error {
	constructor(message) {
        super(message);
        this.code = 400;
        this.err_code = 4300;
        this.message = 'Pengecekan proses invoice gagal, '+message;
    }
}

export default FmcgCheckProcessInvoiceError