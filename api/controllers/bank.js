'use strict';
import _ from 'lodash'
import moment from 'moment'
import util from 'util'

import middleware from '~/api/controllers/auth'

import helper from '~/utils/helper'

import { Bank } from '~/orm/index'
import { Logs } from '~/orm/index'

import service_bank from '~/services/bank'

import BankNotFoundError from '~/response/error/BankNotFoundError'
import CustomError from '~/response/error/CustomError'
import CustomSuccess from '~/response/success/CustomSuccess'

const acl_add_bank = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center','finance'];
const acl_get_bank = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center','finance'];

async function add_bank(req, res) {
    const token = req.swagger.params.token.value;
    const name = req.swagger.params.name.value;
    
    try {
        Logs.create({action: 'add_bank', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_add_bank, is_allowed_access_token_static: true});
        let response = await service_bank.get({name: name});

        if(response == null || response.length == 0) {
        	service_bank.add({name: name});
        }

        res.json(new CustomSuccess(200, 'Data berhasil di tambahkan'));
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal menambahkan data bank, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function get_bank(req, res) {
    const token = req.swagger.params.token.value;
    const id = _.isUndefined(req.swagger.params.id.value)?'':req.swagger.params.id.value;
    
    try {

        Logs.create({action: 'get_bank', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_get_bank, is_allowed_access_token_static: true});
        let out = {};
        
        if(id.length > 0) {
            let response = await service_bank.getOne({id: id});

            if(response == null) {
                throw new CustomError(400, 'Data bank tidak di temukan');
            } else {
                out.bank = await service_bank.getOne({id: id});
            }
        } else {
            out.banks = await service_bank.get();
        }

        res.json(out);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal menampilkan data bank, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

export { add_bank }
export { get_bank }