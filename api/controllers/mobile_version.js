import _ from 'lodash'
import util from 'util'
import fs from 'fs'
import ApkReader from 'adbkit-apkreader'
import path from 'path'

import middleware from '~/api/controllers/auth'

import service_mobile_version from '~/services/mobile_version'

import helper from '~/utils/helper'

import { Logs } from '~/orm/index'

import CustomError from '~/response/error/CustomError'

const acl_mobile_version = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];

function add_mobile_version(req, res) {
	
	const token = req.swagger.params.token.value;
    const acl = acl_mobile_version;
    const requested_resource = {};

    const param_file = req.swagger.params.file;
    const param_force = req.swagger.params.force;
    const param_update_description = req.swagger.params.update_description;
    
    let file = _.isUndefined(param_file.value)?'':param_file.value[0];
    let force = _.isUndefined(param_force.value)?'':param_force.value;
    let update_description = _.isUndefined(param_update_description.value)?'':param_update_description.value;
    let path_dir = __dirname+'/../../static_file/apk';
    let manifest;
    
    try {
        Logs.create({action: 'add_mobile_version', description: helper.getSimpleSwaggerParams(req.swagger.params)});

    	return Promise.resolve()
    	.then( () => {
    		return middleware.verify({token: token, acl: acl_mobile_version, is_allowed_access_token_static: true});
    	})
    	.then( () => {
    		
    		if (path.extname(file.originalname) == ".apk") {
	            try {
	                let stats = fs.statSync(path_dir);
	                if (!stats.isDirectory()) {
	                    fs.unlinkSync(path_dir);
	                    fs.mkdirSync(path_dir);
	                }
	            } catch(err) {
	                fs.mkdirSync(path_dir);
	            } finally {
	                return;
	            }
	        } else {
	            throw new CustomError(400, "Extensi file bukan '.apk'");
	        }
    	})
        .then( () => {
        	return ApkReader.open(file.path)
	        .then(reader => reader.readManifest()).then((m) => {
	            manifest = m
	            path_dir = path_dir + '/' + manifest.versionName + '.apk';
	            fs.renameSync(file.path, path_dir);
	            return;
	        });
        })
        .then( () => {
        	let args = {};
        	let dformat = helper.formatCurrentDateTime();

	        args.version = manifest.versionName;
	        args.download_link = '/apk/download.html';
	        args.force = force;
	        args.update_description = update_description;
	        args.createdAt = dformat;
	        args.updatedAt = dformat;

	        return service_mobile_version.addMobileVersion(args).then(response => {
	            return response;
	        });
        })
        .then(response => {
        	response.dataValues.createdAt = helper.formatDateTime(response.dataValues.createdAt);
        	response.dataValues.updatedAt = helper.formatDateTime(response.dataValues.updatedAt);
        	
        	res.json(response);
        });
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal menambahkan mobile version, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function get_mobile_version(req, res) {
	const token = req.swagger.params.token.value;

	try {
        Logs.create({action: 'get_mobile_version', description: helper.getSimpleSwaggerParams(req.swagger.params)});
        
        let auth_user = await middleware.verify({token: token, acl: acl_mobile_version, is_allowed_access_token_static: true});
        let response = await service_mobile_version.getOne();
        let out = {};

        if (response.length > 0){
        	response[0].createdAt = helper.formatDateTime(response[0].createdAt);
            response[0].updatedAt = helper.formatDateTime(response[0].updatedAt);
        	response[0].force = (response[0].force == 1)?true:false;

            out.version = response;
        } else {
            out.version = []
        }

        res.json(out);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal mendapatkan mobile version, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

export { add_mobile_version }
export { get_mobile_version }