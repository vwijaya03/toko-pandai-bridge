class FmcgUploadDataPembatalanPotonganError extends Error {
	constructor(message) {
        super(message);
        this.code = 400;
        this.err_code = 4300;
        this.message = 'Gagal upload data pembatalan potongan, '+message;
    }
}

export default FmcgUploadDataPembatalanPotonganError