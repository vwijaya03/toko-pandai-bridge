class FmcgMakePaymentError extends Error {
	constructor(message) {
        super(message);
        this.code = 400;
        this.err_code = 4300;
        this.message = 'Gagal melakukan pembayaran invoice, '+message;
    }
}

export default FmcgMakePaymentError