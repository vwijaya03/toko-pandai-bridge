'use strict';

import middleware from '~/api/controllers/auth'

import request from 'request'

import helper from '~/utils/helper'

import { Logs } from '~/orm/index'

import service_aca_product from '~/services/aca_product'
import api_backend from '~/external_api/api_backend'

import CustomSuccess from '~/response/success/CustomSuccess'

const acl_add_insurance = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];
const acl_get_insurance = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];

const url_api_aca = 'http://192.168.12.6:21002'

async function addProductInsurance(req,res){
    const token = req.swagger.params.token.value;
    const name = req.swagger.params.name.value;
    const jmlhPremi = req.swagger.params.jmlhPremi.value;
    const KdProduk = req.swagger.params.KdProduk.value;
    const ProductDesc = req.swagger.params.ProductDesc.value;
    const santunan = req.swagger.params.santunan.value;
    
    try {
        Logs.create({action: 'Add Product ACA', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        request({
            headers: {
                'token': token
            },
            url: url_api_aca+'/insurance/add-product',
            method: "POST",
            formData: {
                "name":name,
                "jmlhPremi":jmlhPremi,
                "KdProduk":KdProduk,
                "ProductDesc":ProductDesc,
                "santunan":santunan
            }
        }, function (error, result, body){
            let response = JSON.parse(body)
            res.json(response)
        })
        
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal menambahkan data product asuransi, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function getProductInsurance(req, res) {
    const token = req.swagger.params.token.value;

    try {
        Logs.create({action: 'get_aca_products', description: helper.getSimpleSwaggerParams(req.swagger.params)});

    let api_response = await api_backend.get_aca_produk({token:token});
        res.json(api_response);
        } catch(err) {
            if(err.code === undefined) {
                err.code = 400;
                err.message = 'Gagal menampilkan data product, '+err;
            }

            res.status(err.code).json({code: err.code, message: err.message});
        }
    }

export { addProductInsurance }
export { getProductInsurance }