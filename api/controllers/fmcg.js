import _ from 'lodash'
import util from 'util'
import fs from 'fs'
import path from 'path'

import middleware from '~/api/controllers/auth'

import service_fmcg from '~/services/fmcg'

import helper from '~/utils/helper'

import { Logs } from '~/orm/index'

import CustomError from '~/response/error/CustomError'
import CustomSuccess from '~/response/success/CustomSuccess'

const acl_fmcg = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center','finance'];

async function add_fmcg(req, res) {
	const token = req.swagger.params.token.value;
    const name = _.isUndefined(req.swagger.params.name.value)?'':req.swagger.params.name.value;
    const limit_unique_code = _.isUndefined(req.swagger.params.limit_unique_code.value)?0:req.swagger.params.limit_unique_code.value;
    const url = _.isUndefined(req.swagger.params.url.value)?'':req.swagger.params.url.value;
    const fmcg_token = _.isUndefined(req.swagger.params.fmcg_token.value)?'':req.swagger.params.fmcg_token.value;
    const fmcg_icon = _.isUndefined(req.swagger.params.file.value)?'':req.swagger.params.file.value[0];

    let path_dir = __dirname+'/../../static_file/fmcg-icon';

    try {
        Logs.create({action: 'add_fmcg', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_fmcg, is_allowed_access_token_static: true});

        let response = await service_fmcg.get({name: name});

        if(response.length > 0) {

        } else {
            if (path.extname(fmcg_icon.originalname) == ".png" || path.extname(fmcg_icon.originalname) == ".jpg" || path.extname(fmcg_icon.originalname) == ".jpeg") {
                let stats = fs.statSync(path_dir);
                
                if (!stats.isDirectory()) {
                    fs.unlinkSync(path_dir);
                    fs.mkdirSync(path_dir);
                }

                path_dir = path_dir + '/' + fmcg_icon.originalname;

                fs.renameSync(fmcg_icon.path, path_dir);

                await service_fmcg.add({name: name, limit_unique_code: limit_unique_code, url: url, fmcg_token: fmcg_token, fmcg_icon: fmcg_icon.originalname});
	        } else {
	            throw new CustomError(400, "Format icon salah");
            }
        }

        res.json(new CustomSuccess(200, 'FMCG berhasil di tambahkan'));
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal menambahkan fmcg, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function get_fmcg(req, res) {
	const token = req.swagger.params.token.value;

	try {
        Logs.create({action: 'get_fmcg', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_fmcg, is_allowed_access_token_static: true});

        let response = await service_fmcg.getFmcgOnly({delete: 0});

        let out = {};
        out.result = response;

        res.json(out);

    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal mendapatkan data fmcg, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

export { add_fmcg }
export { get_fmcg }