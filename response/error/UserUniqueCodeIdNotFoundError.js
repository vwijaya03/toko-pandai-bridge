class UserUniqueCodeIdNotFoundError extends Error {
	constructor(code, message) {
        super(message);
        this.code = 400;
        this.message = 'Unique code Id user tidak di temukan';
    }
}

export default UserUniqueCodeIdNotFoundError