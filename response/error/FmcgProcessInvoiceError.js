class FmcgProcessInvoiceError extends Error {
	constructor(message) {
        super(message);
        this.code = 400;
        this.err_code = 4300;
        this.message = 'Gagal memproses data invoice, '+message;
    }
}

export default FmcgProcessInvoiceError