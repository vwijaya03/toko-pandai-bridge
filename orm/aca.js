import { orm } from '~/services/mariadb'
import Sequelize from 'sequelize'


const Aca = orm.define('aca_insurance', {
    id: {
        type: Sequelize.INTEGER, field: 'id',
        primaryKey: true,
        autoIncrement: true
    },
    user_id: {
        type: Sequelize.INTEGER, allowNull:true
    },
    NoTransaksi: {
        type: Sequelize.STRING, allowNull:true
    },
    tglTransaksi: {
        type: 'TIMESTAMP', allowNull:true
    },
    kdTracking: {
        type: Sequelize.STRING, allowNull:true
    },
    kdsource: {
        type: Sequelize.STRING, allowNull:true
    },
    id_KdProduk: {
        type: Sequelize.INTEGER, allowNull:true
    },
    KdProduk: {
        type: Sequelize.STRING, allowNull:true
    },
    kdOutlet: {
        type: Sequelize.STRING, allowNull:true
    },
    Nama: {
        type: Sequelize.STRING, allowNull:true
    },
    KTP: {
        type: Sequelize.STRING, allowNull:true
    },
    MSISDN: {
        type: Sequelize.STRING, allowNull:true
    },
    Alamat: {
        type: Sequelize.INTEGER, allowNull:true
    },
    kodePos: {
        type: Sequelize.STRING, allowNull:true
    },
    tglLahir: {
        type: Sequelize.DATE, allowNull:true
    },
    AhliWaris: {
        type: Sequelize.STRING, allowNull:true
    },
    tglAwal: {
        type: Sequelize.DATE, allowNull:true
    },
    tglAkhir: {
        type: Sequelize.DATE, allowNull:true
    },
    Email: {
        type: Sequelize.STRING, allowNull:true
    },
    Reff0: {
        type: Sequelize.STRING, allowNull:true
    },
    Reff1: {
        type: Sequelize.STRING, allowNull:true
    },
    Reff2: {
        type: Sequelize.STRING, allowNull:true
    },
    Reff3: {
        type: Sequelize.STRING, allowNull:true
    },
    Reff4: {
        type: Sequelize.STRING, allowNull:true
    },
    Reff5: {
        type: Sequelize.STRING, allowNull:true
    },
    Reff6: {
        type: Sequelize.STRING, allowNull:true
    },
    Reff7: {
        type: Sequelize.STRING, allowNull:true
    },
    TglRespon: {
        type: Sequelize.DATE, allowNull:true
    },
    Response: {
        type: Sequelize.STRING, allowNull:true
    },
    ResponseDesc: {
        type: Sequelize.STRING, allowNull:true
    },
    PIN: {
        type: Sequelize.STRING, allowNull:true
    },
    SerialNo: {
        type: Sequelize.STRING, allowNull:true
    },
    Barcode: {
        type: Sequelize.STRING, allowNull:true
    },
    Premi: {
        type: Sequelize.DECIMAL, allowNull:true
    },
    SupportInfo: {
        type: Sequelize.STRING, allowNull:true
    },
    createdAt: {
        type: 'TIMESTAMP', allowNull:true
    },
    updatedAt: {
        type: 'TIMESTAMP', allowNull:true
    },
    status: {
        type: Sequelize.INTEGER, allowNull:true
    },
    payment_status: {
        type: Sequelize.INTEGER
    },
    img_file_name: {
        type: Sequelize.STRING, allowNull:true
    },
    certificate: {
        type: Sequelize.STRING, allowNull:true
    }
}, {
    freezeTableName: true, // Model tableName will be the same as the model name
    timestamps: false
});

export default Aca
