class FmcgTaskListPembatalanPotonganError extends Error {
	constructor(message) {
        super(message);
        this.code = 400;
        this.err_code = 4300;
        this.message = 'Gagal menampilkan data pembatalan potongan yang sudah terupload, '+message;
    }
}

export default FmcgTaskListPembatalanPotonganError