import _ from 'lodash'
import util from 'util'

import middleware from '~/api/controllers/auth'
import helper from '~/utils/helper'
import service_notification from '~/services/notification'

import { User } from '~/orm/index'
import { Logs } from '~/orm/index'
import { Op } from '~/utils/helper'
import { allowed_user_status } from '~/properties'

import NotificationDestinationNotFoundError from '~/response/error/NotificationDestinationNotFoundError'

const acl_otp = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];

async function get_notification(req, res) {
	const token = req.swagger.params.token.value;

	try {
        Logs.create({action: 'get_notification', description: helper.getSimpleSwaggerParams(req.swagger.params)});

    	let auth_user = await middleware.verify({token: token, acl: acl_otp, is_allowed_access_token_static: true});
    	let response = await service_notification.get();

        response.map( (value, index) => {
            value.dataValues.createdAt = helper.formatDateTime(value.createdAt);
            value.dataValues.updatedAt = helper.formatDateTime(value.updatedAt);
        });

    	let out = {};
    	out.notification = response;

    	res.json(out);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal mendapatkan notifikasi, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function add_notification(req, res) {
	const token = req.swagger.params.token.value;

    const param_to = req.swagger.params.to;
    const param_type = req.swagger.params.type;
    const param_title = req.swagger.params.title;
    const param_message = req.swagger.params.message;

    let to = _.isUndefined(param_to.value)?'':param_to.value;
    let type = _.isUndefined(param_type.value)?'':param_type.value;
    let title = _.isUndefined(param_title.value)?'':param_title.value;
    let message = _.isUndefined(param_message.value)?'':param_message.value;
    let result_message = JSON.stringify({
    	title: title,
    	body: message
    });

    try {
        Logs.create({action: 'add_notification', description: helper.getSimpleSwaggerParams(req.swagger.params)});

    	let auth_user = await middleware.verify({token: token, acl: acl_otp, is_allowed_access_token_static: true});
    	
    	if (auth_user) {
            if (to){

            } else {
                switch (type){
                    case "email": {
                        to = auth_user.dataValues.email
                    } break;
                    case "sms": {
                        to = auth_user.dataValues.phone
                    } break;
                    case "notification": 
                    case "data": {
                        to = auth_user.dataValues.fcm_id
                    } break;
                }
            } 
        } else {
        	if (to) {
                let multiple_to = to.split(/[\s\,]/gi).filter(Boolean);
                let users = await User.findAll({ where: {valdo_account: multiple_to, [Op.or]: allowed_user_status} });

                if (users.length > 0){
                    multiple_to = [];
                    for (let i = 0; i < users.length; i++){
                        let user = users[i];
                        switch (type){
                            case "email": {
                                multiple_to.push(user.email);
                            } break;
                            case "sms": {
                                multiple_to.push(user.phone);
                            } break;
                            case "notification": 
                            case "data": {
                                multiple_to.push(user.fcm_id);
                            } break;
                        }
                    }

                    to = multiple_to.join(", ");
                }
            } else {
                throw new NotificationDestinationNotFoundError();
            }
        }

        if(to === undefined || to == null) {
        	throw new NotificationDestinationNotFoundError();
        }
        
        let dformat = helper.formatCurrentDateTime();
        let args = {};

        args.to = to;
        args.type = type;
        args.message = result_message;
        args.state = 0;
        args.createdAt = dformat;
        args.updatedAt = dformat;

        let response = await service_notification.addNotification(args);
        response.dataValues.createdAt = helper.formatDateTime(response.dataValues.createdAt);
        response.dataValues.updatedAt = helper.formatDateTime(response.dataValues.updatedAt);

        res.json(response);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal menambahkan notifikasi, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

export { get_notification }
export { add_notification }