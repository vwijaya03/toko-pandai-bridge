'use strict';
import _ from 'lodash'
import util from 'util'
import middleware from '~/api/controllers/auth'
import moment from 'moment'
import request from 'request'

import service_fmcg from '~/services/fmcg'
import service_user from '~/services/user'
import service_pelunasan_manual from '~/services/pelunasan_manual'
import helper from '~/utils/helper'



import ApiTop from '~/external_api/api_top'
import ApiFmcg from '~/external_api/api_uli'
import ApiPohonDana from '~/external_api/api_top_pohon_dana'
import MoneyAccess from '~/external_api/moneyaccess'


let apigatekey= 'webKEY123456789';
let url_trf = '192.168.12.2';
let pd_url = "http://192.168.12.7:3005";


import { Logs } from '~/orm/index'

import CustomError from '~/response/error/CustomError'

const acl_calculator = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];
const acl_apply_loan = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];
const acl_get_invoices = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];
const acl_repayment = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];


async function calculator(req, res) {
    const token = req.swagger.params.token.value;
    const param_amount = req.swagger.params.amount;
    const param_discount = req.swagger.params.discount;
    const param_tenor = req.swagger.params.tenor;
    const iso8601format = "YYYY-MM-DD";

    let amount = _.isUndefined(param_amount.value) ? 0 : param_amount.value;
    let discount = _.isUndefined(param_discount.value) ? 0 : param_discount.value;
    let tenor = _.isUndefined(param_tenor.value) ? 0 : param_tenor.value;
    let data_unique_code = '';
    let fmcg_top =5;
    let admin= 0;
    
    
    try {
        Logs.create({action: 'calculator top', description: helper.getSimpleSwaggerParams(req.swagger.params)});
             
        let auth_user = await middleware.verify({token: token, acl: acl_calculator, is_allowed_access_token_static: false});
        let result_fmcg_top = await service_fmcg.getOneFmcgWithFilter({id: fmcg_top});   

        let invoice_args = {
            url: result_fmcg_top.dataValues.url,
            fmcg_token: result_fmcg_top.dataValues.fmcg_token,
            fmcg_name: result_fmcg_top.dataValues.name,
            amount: amount,
            tenor: tenor,
            discount: discount
        }
       

        let calculator = await ApiPohonDana.calculator(invoice_args)
        let week = {};
        if (calculator.status === 'Success') {
                let potongan = parseInt(calculator.week[0].administrasi) * 0
                if((((calculator.week[0].loan_payback)-potongan)/calculator.week[0].loan_length % 1) !== 0)
                {
                    admin= parseInt((parseInt(calculator.week[0].loan_payback)-potongan)/calculator.week[0].loan_length)+1;
                }
                else{
                    admin= parseInt((parseInt(calculator.week[0].loan_payback)-potongan)/calculator.week[0].loan_length);
                }
               week = [{
                   "loan_amount": parseInt(calculator.week[0].loan_amount)+".0000",
                   "loan_payback": parseInt(calculator.week[0].loan_payback)-potongan+".0000",
                   "administrasi": parseInt(calculator.week[0].administrasi)+".0000",
                   "loan_length": calculator.week[0].loan_length,
                   "installment": parseInt(admin)+".0000",
                   "total_potongan": potongan+".0000",
               }]
            }
            else
            {
                week = (calculator);
            }
            let weeks = {}
            weeks.status = calculator.status;
            weeks.message = calculator.status;
            weeks.week = week;
            res.status(200).json(weeks)
        
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data calculator TOP, '+err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function apply_loan(req, res) {
    const token = req.swagger.params.token.value;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_le_code = req.swagger.params.le_code;
    const param_dt_code = req.swagger.params.dt_code;
    const param_amount = req.swagger.params.amount;
    const param_invoice_id = req.swagger.params.invoice_id;
    const param_tenor = req.swagger.params.tenor;
    const param_principal_code = req.swagger.params.principal_code;
    const param_total_potongan = req.swagger.params.total_potongan;
    const param_pin = req.swagger.params.pin;


    const iso8601format = "YYYY-MM-DD";
    
    let pin_user = _.isUndefined(param_pin.value)?null:param_pin.value;
    let fmcg_id = _.isUndefined(param_fmcg_id.value)?null:param_fmcg_id.value;
    let dt_code = _.isUndefined(param_dt_code.value) ? null : param_dt_code.value;
    let le_code = _.isUndefined(param_le_code.value) ? null : param_le_code.value;
    let amount = _.isUndefined(param_amount.value) ? null : param_amount.value;
    let invoice_number = _.isUndefined(param_invoice_id.value) ? null : param_invoice_id.value;
    let tenor = _.isUndefined(param_tenor.value) ? null : param_tenor.value;
    let principal_code = _.isUndefined(param_principal_code.value) ? null : param_principal_code.value;
    let total_potongan = _.isUndefined(param_total_potongan.value) ? null : param_total_potongan.value;
    let discounts = '';
    let data_unique_code = '';

    try {
        Logs.create({action: 'apply_loan', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_apply_loan, is_allowed_access_token_static: false});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});

        if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal apply loan, data fmcg  tidak tersedia');
        }
        
        let result_fmcg_top = await service_fmcg.getOneFmcgWithFilter({id: 5});

        if(result_fmcg_top == null) {
            throw new CustomError(400, 'Gagal apply loan, data fintech tidak tersedia');
        }
        
        let user_salt = auth_user.dataValues.salt;
        
        let newpin = await service_user.getHashedPassword(user_salt, pin_user.toString());
        if(auth_user.dataValues.pin != newpin) {
            throw new CustomError(400, 'Pin Salah');
        }

        let api_unpaid = await ApiPohonDana.check_fintech_unpaid({le_code:le_code});

        if (api_unpaid.totalInvoice !== 0){
          throw new CustomError(400, 'Maaf pengajuan limit tidak dapat diproses. Selesaikan pembayaran tagihan Anda');
        }


        let api_transact = await ApiPohonDana.checktransactions({dt_code:dt_code,invoice_id:invoice_number,fmcg_id:fmcg_id});

        if (api_transact.totalTransaksi !== 0){
          throw new CustomError(400, 'Pengajuan sebagian ditolak');
        }

        if (amount < 100000){
          throw new CustomError(400, 'Invoice Minimal Rp. 100.000');
        }
    
        let args ={
           "dt_code": dt_code,
           "le_code": le_code+"",
           "invoice_number": invoice_number,
           "amount": amount,
           "tenor": tenor,
           "discount" : parseFloat(total_potongan)
         };
       request({
           headers: {
           'Content-Type': 'application/json'
           },
            url: pd_url+"/pinjaman/appPinjaman",
            method: "POST",
            json: true,
            body: args
          }, function (error, response, body){
              if(body.status === 'Success'){     
                   let argss = {
                    url: result_fmcg_top.dataValues.url,
                    fmcg_token : result_fmcg_top.dataValues.fmcg_token,
                    fmcg_name : result_fmcg_top.dataValues.name,
                    user_id:auth_user.dataValues.user_id,
                    le_code:le_code,
                    dt_code:dt_code,
                    amount:amount,
                    total_amount_invoice:parseInt(amount),
                    invoice_id:invoice_number,
                    tenor:tenor,
                    loan_type:"Credit",
                    loan_by:"Pohon Dana",
                    principal_code:fmcg_id,
                    total_potongan:total_potongan
                  }
                  let invoice_update = {};
                   invoice_update.le_code = le_code;
                   invoice_update.invoice_id = invoice_number;
                   invoice_update.amount = amount;
                   invoice_update.fmcg_id = fmcg_id;
                   invoice_update.paid_by = "Limit Pandai"
                   
                  ApiPohonDana.update_payment_fmcg(invoice_update);
                  ApiPohonDana.insert_loan_history(argss)
                  res.status(200).json(body);
              }else
              {
                  res.status(400).json(body);
              }
        });
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan update invoice, '+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function payment_to_distributor(req,res,token){
    let fmcg_top = 5;
    let fmcg_id = 13;
    let vacc_pohonDana = '21000000102';

    try{
        let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});

        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
        if(result_fmcg.dataValues == null) {
            throw new CustomError(400, 'Gagal menampilkan data payment_distributor, data fmcg tidak tersedia');
        }

        let result_fmcg_top = await service_fmcg.getOneFmcgWithFilter({id: fmcg_top});
        if(result_fmcg_top.dataValues == null) {
            throw new CustomError(400, 'Gagal menampilkan data payment_distributor, data fmcg tidak tersedia');
        }

        let args = {
            url: result_fmcg_top.dataValues.url,
            fmcg_token: result_fmcg_top.dataValues.fmcg_token,
            fmcg_name: result_fmcg_top.dataValues.name
        }

        let unpaid_invoices = await ApiPohonDana.get_unpaid_invoices(args)

        if(unpaid_invoices.length !== 0){
            for(let i = 0; i < unpaid_invoices.length; i++){

                let dist = await service_pelunasan_manual.get_dt_unicode({dt_code:unpaid_invoices[i].dt_code})
                let toko = await service_pelunasan_manual.get_le_unicode({le_code:unpaid_invoices[i].le_code}) 
                
                let payment = {
                    trans_type:"transfer",
                    status:"success", 
                    vacc_from: vacc_pohonDana,
                    vacc_to:[
                        {vacc_number:dist[0].valdo_account, amount: parseInt(unpaid_invoices[i].amount),desc:`Pembayaran invoice ${unpaid_invoices[i].invoice_no} Rp. ${helper.IDRformat(unpaid_invoices[i].amount)} dari Limit Pandai (${vacc_pohonDana}) ke ${dist[0].fullname} (${dist[0].valdo_account})`}
                    ],
                    pin:'123456'
                }
                request({
                    headers: {
                        'apigatekey': apigatekey,
                        'Content-Type': 'application/json'
                    },
                    url: 'http://'+url_trf+'/api/v1/multi_vacc/trf',
                    method: "POST",
                    json: true,   
                    body: payment
                }, async function (error, result, body){
                        if(body.status.code === 200){
                            let update_data = {
                                invoice_code:unpaid_invoices[i].invoice_code,
                                payment_status: 1,
                                fmcg_token: result_fmcg_top.dataValues.fmcg_token,
                                fmcg_name: result_fmcg_top.dataValues.name,
                                url: result_fmcg_top.dataValues.url
                            }
                            let update_payment_status = await ApiPohonDana.update_payment_status_distributor(update_data)
                            let input_to_transactions = {};
                            input_to_transactions.token = token;
                            input_to_transactions.url = result_fmcg.dataValues.url;
                            input_to_transactions.fmcg_token = result_fmcg.dataValues.fmcg_token;
                            input_to_transactions.fmcg_name = result_fmcg.dataValues.name;
                            input_to_transactions.le_code = unpaid_invoices[i].le_code;
                            input_to_transactions.dt_code = unpaid_invoices[i].dt_code;
                            input_to_transactions.invoice_id = unpaid_invoices[i].invoice_no;
                            input_to_transactions.invoice_code = unpaid_invoices[i].dt_code+unpaid_invoices[i].invoice_no;
                            input_to_transactions.amount = parseInt(unpaid_invoices[i].amount);
                            input_to_transactions.valdo_tx_id = "TR"+moment().format('YYYYMMDDHHmmss');
                            input_to_transactions.transact_date = moment().format('YYYY-MM-DD HH:mm:ss');
                            input_to_transactions.currency_code = "IDR";
                            input_to_transactions.type = 0;
                            input_to_transactions.distributor_id = dist[0].id;
                            input_to_transactions.retailer_id = toko[0].id;  
                            input_to_transactions.from_user_id = 0;
                            input_to_transactions.to_user_id = dist[0].user_id;
                            let input_to_transactions_res = await ApiTop.insert_manual_transaction(input_to_transactions); 
                        }
                    })

                    Logs.create({action: 'repayment_to_distributor', description: 'token:'+token+',le_code:'+unpaid_invoices[i].le_code+',dt_code:'+unpaid_invoices[i].le_code+',invoice_no:'+unpaid_invoices[i].invoice_no});
            }
        }
        else{
            throw new CustomError(400,'Unpaid invoice is not found!')
        }

        let out = {
            total_unpaid_invoices : unpaid_invoices.length,
            message: 'Success'
        }
    }catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data invoice, '+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}



async function get_invoice(req,res){
    const token = req.swagger.params.token.value;
    const param_lecode = req.swagger.params.le_code;
    const param_dtcode = req.swagger.params.dt_code;
    const param_invoice_no = req.swagger.params.invoice_no;
    const param_page = req.swagger.params.page;
    const param_items_per_page = req.swagger.params.items_per_page;
    const param_sorting = req.swagger.params.sorting;

    let le_code = _.isUndefined(param_lecode.value)?null:param_lecode.value;
    let page = _.isUndefined(param_page.value)?0:param_page.value;
    let items_per_page = _.isUndefined(param_items_per_page.value)?15:param_items_per_page.value;
    let dt_code = _.isUndefined(param_dtcode.value) ? null : param_dtcode.value;
    let invoice_no = _.isUndefined(param_invoice_no.value) ? null : param_invoice_no.value;
    let sorting = _.isUndefined(param_sorting.value) ? null : param_sorting.value;
   
    const iso8601format = "YYYY-MM-DD";
    let data_unique_code = '';
    let fmcg_id = 13
    let fmcg_top = 5

    try {
     
        let auth_user = await middleware.verify({token: token, acl: acl_get_invoices, is_allowed_access_token_static: false});
        let result_fmcg_top = await service_fmcg.getOneFmcgWithFilter({id: fmcg_top});
        if(result_fmcg_top.dataValues === null) {
            throw new CustomError(400, 'Gagal memproses data invoice, data fmcg tidak tersedia');
        }
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
        if(result_fmcg.dataValues === null) {
            throw new CustomError(400, 'Gagal memproses data invoice, data fmcg tidak tersedia');
        }
        let dist_detail = await service_pelunasan_manual.get_dt_code({user_id:auth_user.dataValues.user_id})
        let dist_dt_code = dist_detail[0].unique_code;

        if(auth_user.dataValues.type === 1){
            let data_dist = {
                url : result_fmcg_top.dataValues.url,
                fmcg_token : result_fmcg_top.dataValues.fmcg_token,
                fmcg_name : result_fmcg_top.dataValues.name,
                dist_dt_code : dist_dt_code
            }
    
            let get_all_le_code = await ApiPohonDana.get_all_le_code(data_dist)
            
            for(let i=0;i<get_all_le_code.length;i++){
                if(i === 0 && get_all_le_code.length === 1){
                    le_code = get_all_le_code[i].le_code
                }
                else if(i === 0 && get_all_le_code.length !== 1){
                    le_code = get_all_le_code[i].le_code+","
                }
                else if(i === get_all_le_code.length-1 && get_all_le_code.length !== 1){
                    le_code += get_all_le_code[i].le_code
                }
                else{
                    le_code += get_all_le_code[i].le_code+","
                }
                
            }
        }
        
        let invoice_args = {};
        invoice_args.url = result_fmcg_top.dataValues.url;
        invoice_args.fmcg_token = result_fmcg_top.dataValues.fmcg_token;
        invoice_args.fmcg_name = result_fmcg_top.dataValues.name;
        invoice_args.le_code = le_code;
        invoice_args.user_id = auth_user.dataValues.user_id;
        invoice_args.role_type = auth_user.dataValues.type;
        invoice_args.dt_code = dt_code;
        invoice_args.invoice_no = invoice_no;
        invoice_args.dist_dt_code = dist_dt_code;
        invoice_args.page = page;
        invoice_args.items_per_page = items_per_page;
        invoice_args.sorting = sorting;
        
        let api_response_fmcg = await ApiPohonDana.get_invoice(invoice_args);
        res.json(api_response_fmcg);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data invoice, '+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function get_one_invoice(req, res) {
    const token = req.swagger.params.token.value;
    const param_le_code = req.swagger.params.le_code;
    const param_invoice_id = req.swagger.params.invoice_id;
    const param_dt_code = req.swagger.params.dt_code;
    const param_tenor = req.swagger.params.tenor;
    
    const iso8601format = "YYYY-MM-DD";

    let le_code = _.isUndefined(param_le_code.value) ? null : param_le_code.value;
    let dt_code = _.isUndefined(param_dt_code.value) ? null : param_dt_code.value;
    let invoice_id = _.isUndefined(param_invoice_id.value) ? null : param_invoice_id.value;
    let tenor = _.isUndefined(param_tenor.value) ? null : param_tenor.value;
    
    let data_unique_code = '';
    let fmcg_id =5;
    var ten = tenor.split(" ", 1);
    try {

        let auth_user = await middleware.verify({token: token, acl: acl_get_invoices, is_allowed_access_token_static: false});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});

       
        if(result_fmcg.dataValues == {}) {
            throw new CustomError(400, 'Gagal memproses data invoice, data fmcg tidak tersedia');
        }
        let invoice_args = {};
        invoice_args.url = result_fmcg.dataValues.url;
        invoice_args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        invoice_args.fmcg_name = result_fmcg.dataValues.name;
        invoice_args.le_code = le_code;
        invoice_args.invoice_id = invoice_id;
        invoice_args.dt_code = dt_code;
        invoice_args.tenor = ten;
        invoice_args.user_id = auth_user.dataValues.user_id;
        
        let api_response_fmcg = await ApiPohonDana.get_one_invoice(invoice_args);

        //Logs.create({action: `api response fmcg get one top`, description: util.inspect(api_response_fmcg)});

        res.json(api_response_fmcg);
        } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data top, '+err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function repayment(req, res) {
    const token = req.swagger.params.token.value;
    const pin = req.swagger.params.pin.value;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_tenor = req.swagger.params.tenor;
    const param_amount = req.swagger.params.amount;
    const param_invoice_number = req.swagger.params.invoice_id;
    const param_le_code = req.swagger.params.le_code;
    const param_dt_code = req.swagger.params.dt_code;

    const iso8601format = "YYYY-MM-DD";

    let fmcg_id = _.isUndefined(param_fmcg_id.value)?null:param_fmcg_id.value;
    let amount = _.isUndefined(param_amount.value) ? null : param_amount.value;
    let tenor = _.isUndefined(param_tenor.value) ? null : param_tenor.value;
    let invoice_number = _.isUndefined(param_invoice_number.value) ? null : param_invoice_number.value;
    let le_code = _.isUndefined(param_le_code.value) ? null : param_le_code.value;
    let dt_code = _.isUndefined(param_dt_code.value) ? null : param_dt_code.value;

    let data_unique_code = '';
    let vacc_pohonDana = '21000000101';
    let vacc_topan = '2010001162';
    let fmcg_top = 5;
    let out = 0;
    
    try {

        let auth_user = await middleware.verify({token: token, acl: acl_repayment, is_allowed_access_token_static: false});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
        if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal menampilkan data invoice, data fmcg tidak tersedia');
        }
        // let cek = await ApiPohonDana.cek_saldo({le_code:le_code});
        // if(cek.code == 502) {
        //     throw new CustomError(400, 'Server Sedang Maintenance');
        // }
        let result_fmcg_top = await service_fmcg.getOneFmcgWithFilter({id: fmcg_top});
        if(result_fmcg_top == null) {
            throw new CustomError(400, 'Gagal menampilkan data invoice, data fmcg tidak tersedia');
        }

        let actual_pin = await service_user.getHashedPassword(auth_user.dataValues.salt, pin);
        let expected_pin = auth_user.dataValues.pin;
        if (expected_pin != actual_pin) {
            throw new CustomError(400, 'PIN salah');
        }

        // let today = new Date()
        // let current_time = today.getHours()
        // let time_setting = moment(today.setHours(current_time)).format('HH:mm:ss')

        // if(time_setting > '21:00:00'){
        //     throw new CustomError(400, 'Waktu transaksi telah ditutup!');
        // }
        
        let vacc_toko = auth_user.dataValues.valdo_account
        let nama_toko = auth_user.dataValues.fullname

        let inv = {}
        inv.url = result_fmcg_top.dataValues.url;
        inv.fmcg_token = result_fmcg_top.dataValues.fmcg_token;
        inv.fmcg_name = result_fmcg_top.dataValues.name;
        inv.invoice_number = invoice_number;
        inv.invoice_payment_status = 'unpaid';

        let args ={}
        args.url = result_fmcg.dataValues.url;
        args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        args.fmcg_name = result_fmcg.dataValues.name;
        args.invoice_id = invoice_number;
        args.dt_code = dt_code

        let invoice_pd = {
            le_code:le_code,
            dt_code:dt_code,
            invoice_number:invoice_number,
            tenor: tenor
            
        }
       
        let get_data_pd = await ApiPohonDana.get_inv_pd(invoice_pd)
        
        let get_data_pd_new=JSON.parse(get_data_pd);
        if(get_data_pd_new.status === "failed"){
              throw new CustomError(400, 'invoice tidak ditemukan');
        }
        
        let distributor = await service_pelunasan_manual.get_dt_unicode({dt_code:dt_code})
        let retailer = await service_pelunasan_manual.get_le_unicode({le_code:le_code})  
                 
        let api_response = await MoneyAccess.inquiry({mobnum: vacc_toko, vaccount: vacc_toko, usertype: auth_user.dataValues.type});
        out = api_response;
        if(out.balance < get_data_pd_new.data[0].amount){
              throw new CustomError(400, 'Saldo tidak cukup');
        }

        let get_top_invoice = await ApiPohonDana.get_all_tenor(inv)
        if(get_top_invoice["data"].length === 0){
            throw new CustomError(400,'Invoice tidak ditemukan atau sudah dibayar')
        }
        let unpaid_invoices = await get_top_invoice["data"].filter(function(item){
            return item.invoice_payment_status == "unpaid"
        })


        for(let i = 0; i < unpaid_invoices.length; i++){
            
            if(Number(tenor) > Number(unpaid_invoices[i]["cash_memo_doc_no"].split("-")[1])){
                //throw new CustomError(400,'Invoice cicilan ke - '+unpaid_invoices[i]["cash_memo_doc_no"].split("-")[1]+' Anda belum dibayar')
                res.status(400).json({code:400,message:'Invoice cicilan ke - '+unpaid_invoices[i]["cash_memo_doc_no"].split("-")[1]+' Anda belum dibayar'}) 
            }
            else if(Number(tenor) === Number(unpaid_invoices[i]["cash_memo_doc_no"].split("-")[1])){
                if(get_top_invoice["data"].length === 0){
                    throw new CustomError(400,'Invoice tidak ditemukan atau sudah dibayar')
                }

        let get_tenor = get_top_invoice.data[0].cash_memo_doc_no.split('-')
        
        let tenor = get_tenor[1];
        var ten = tenor.split(" ", 1);

        if(Object.keys(get_top_invoice).length !== 0){
           
            let payment = {
                trans_type:"transfer",
                status:"success", 
                 vacc_from: vacc_toko,
                vacc_to:[
                    {vacc_number:vacc_pohonDana, amount: get_data_pd_new.data[0].amount, desc:`Pembayaran invoice ${invoice_number}-${parseInt(tenor)} (${le_code}) Rp. ${helper.IDRformat(amount)} dari ${nama_toko} (${vacc_toko}) ke Limit Pandai (${vacc_pohonDana})`}
                ],
                pin:pin
            }

            request({
                headers: {
                    'apigatekey': apigatekey,
                    'Content-Type': 'application/json'
                },
                url: "http://"+url_trf+"/api/v1/multi_vacc/trf",
                method: "POST",
                json: true,   
                body: payment
            }, async function (error, result, body){
                let top_money_access = body.result
                let top_money_access_response = {
                    code: body.status.code,
                    desc: body.result.desc
                }
                                               
                Logs.create({action: `api money access TOP`, description: 'response'+top_money_access});
                if(body.status.code === 200){

            let args = {
                "le_code": le_code,
                "dt_code": dt_code,
                "cicilan_ke": tenor,
                "invoice_number":invoice_number,
                "terbayar": amount
            }
                request({
                    headers: {
                        'Content-Type': 'application/json'                                   
                    },
                    url: "http://192.168.12.7:10018/repayment",
                    method: "POST",
                    json: true,
                    body: args
                    }, async function (error, response, body){
                        let pd_on_inv = {};
                        pd_on_inv.token = token;
                        pd_on_inv.le_code = le_code;
                       
                        let invoicess = await ApiPohonDana.get_one_pd_inv(pd_on_inv);
                        if(response.statusCode===400){
                            throw new CustomError(400,body.message)
                        }
                        
                        let repayment_response = {
                            message: body.message,
                            sisa: body.status
                        }

                        let input_to_transactions = {};

                        input_to_transactions.fmcg_id = 5;
                        input_to_transactions.dt_code = dt_code;
                        input_to_transactions.invoice_id = invoice_number+'-'+tenor;
                        input_to_transactions.amount = amount;
                        input_to_transactions.invoice_code = dt_code+invoice_number+'-'+tenor;
                        input_to_transactions.valdo_tx_id = "TR"+invoice_number+moment().format('YYYYMMDDHHmmss');
                        input_to_transactions.distributor_id = 0;
                        input_to_transactions.retailer_id = 0;  
                        input_to_transactions.from_user_id = 0;
                        input_to_transactions.to_user_id =0;

                        let insert_manual_transaction = await ApiPohonDana.multifmcg_transactions(input_to_transactions)

                        let out = {
                            response : repayment_response,
                            top_money_access_desc : top_money_access_response
                        } 
                        res.status(200).json({code:200, body:out})
                     });
                }
            })
    } 
     else{
                throw new CustomError(400, 'Invoice tidak sesuai!')
            }  
            }
        }  

    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data invoice, '+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function payment_distributor(req, res) {
    
    const token = req.swagger.params.token.value;
    const param_le_code = req.swagger.params.le_code;
    const param_dt_code = req.swagger.params.dt_code;
    const param_invoice_no = req.swagger.params.invoice_number;
    const param_principal_code = req.swagger.params.principal_code;

    const iso8601format = "YYYY-MM-DD";

    let dt_code = _.isUndefined(param_dt_code.value) ? 0 : param_dt_code.value;
    let le_code = _.isUndefined(param_le_code.value) ? 0 : param_le_code.value;
    let invoice_no = _.isUndefined(param_invoice_no.value) ? 0 : param_invoice_no.value;
    let principal_code = _.isUndefined(param_principal_code.value) ? 0 : param_principal_code.value;
    let fmcg_id = 13
    let fmcg_top = 5
    
    
    try {
        Logs.create({action: 'payment_distributor', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
        if(result_fmcg.dataValues == null) {
            throw new CustomError(400, 'Gagal menampilkan data payment_distributor, data fmcg tidak tersedia');
        }
        let result_fmcg_top = await service_fmcg.getOneFmcgWithFilter({id: fmcg_top});
        if(result_fmcg_top.dataValues == null) {
            throw new CustomError(400, 'Gagal menampilkan data payment_distributor, data fmcg tidak tersedia');
        }

        let invoice = {
            url: result_fmcg_top.dataValues.url,
            fmcg_token : result_fmcg_top.dataValues.fmcg_token,
            fmcg_name : result_fmcg_top.dataValues.name,
            le_code:le_code,
            dt_code:dt_code,
            invoice_id:invoice_no
          }
        let loan_history = await ApiPohonDana.get_loan_history(invoice);
        
        let invoice_args = {};
        invoice_args.url = result_fmcg_top.dataValues.url;
        invoice_args.fmcg_name = result_fmcg_top.dataValues.name;
        invoice_args.fmcg_token = result_fmcg_top.dataValues.fmcg_token;
        invoice_args.fmcg_id = fmcg_id;
        invoice_args.dt_code = dt_code;
        invoice_args.le_code = le_code;
        invoice_args.invoice_no = invoice_no;
        invoice_args.tenor = loan_history.tenor;
        invoice_args.amount = parseInt(loan_history.amount);
        invoice_args.total_tagihan = parseInt(loan_history.total_amount_invoice);
        invoice_args.principal_code = fmcg_id;
       
        let api_response = await ApiPohonDana.payment_distributor(invoice_args);

        payment_to_distributor(req,res);

    res.status(200).json({code:200,status:'success', body:api_response.body});
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal, '+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function payment_to_distributor(req, res, token) {
    let fmcg_top = 5;
    let fmcg_id = 13;
    let vacc_fit = '21000000102';
//    try {
       // let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});

        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
        if (result_fmcg.dataValues == null) {
            throw new CustomError(400, 'Gagal menampilkan data payment_distributor, data fmcg tidak tersedia');
        }

        let result_fmcg_top = await service_fmcg.getOneFmcgWithFilter({id: fmcg_top});
        if (result_fmcg_top.dataValues == null) {
            throw new CustomError(400, 'Gagal menampilkan data payment_distributor, data fmcg tidak tersedia');
        }

        let args = {
            url: result_fmcg_top.dataValues.url,
            fmcg_token: result_fmcg_top.dataValues.fmcg_token,
            fmcg_name: result_fmcg_top.dataValues.name
        }

        let unpaid_invoices = await ApiPohonDana.get_unpaid_invoices(args)
       

        if (unpaid_invoices.length !== 0) {
            for (let i = 0; i < unpaid_invoices.length; i++) {

                let dist = await service_pelunasan_manual.get_dt_unicode({dt_code: unpaid_invoices[i].dt_code})
                let toko = await service_pelunasan_manual.get_le_unicode({le_code: unpaid_invoices[i].le_code})
                let toko_data = await service_pelunasan_manual.get_le_aditional({le_code: unpaid_invoices[i].le_code})
                let nama_toko = '';
                if (toko_data.length === 0)
                {
                    nama_toko = 'Tidak ada nama';
                } else {
                    nama_toko = toko_data[0].value;
                }

                let payment = {
                    trans_type: "transfer",
                    status: "success",
                    vacc_from: vacc_fit,
                    vacc_to: [
                        {vacc_number: dist[0].valdo_account, amount: parseInt(unpaid_invoices[i].amount), desc: `Pembayaran invoice ${unpaid_invoices[i].invoice_no} (${nama_toko})  Rp. ${helper.IDRformat(unpaid_invoices[i].amount)} dari Limit Pandai (${vacc_fit}) ke ${dist[0].fullname} (${dist[0].valdo_account})`}
                    ],
                    pin: '123456'
                }
                request({
                    headers: {
                        'apigatekey': apigatekey,
                        'Content-Type': 'application/json'
                    },
                    url: 'http://' + url_trf + '/api/v1/multi_vacc/trf',
                    method: "POST",
                    json: true,
                    body: payment
                }, async function (error, result, body) {

                    if (body.status.code === 200) {
                        let update_data = {
                            invoice_code:unpaid_invoices[i].invoice_code,
                            payment_status: 1,
                            fmcg_token: result_fmcg_top.dataValues.fmcg_token,
                            fmcg_name: result_fmcg_top.dataValues.name,
                            url: result_fmcg_top.dataValues.url
                        }
                        let update_payment_status = await ApiPohonDana.update_payment_status_distributor(update_data)
                        

                        let input_to_transactions = {};
                        input_to_transactions.token = token;
                        input_to_transactions.url = result_fmcg.dataValues.url;
                        input_to_transactions.fmcg_token = result_fmcg.dataValues.fmcg_token;
                        input_to_transactions.fmcg_name = result_fmcg.dataValues.name;
                        input_to_transactions.le_code = unpaid_invoices[i].le_code;
                        input_to_transactions.dt_code = unpaid_invoices[i].dt_code;
                        input_to_transactions.invoice_id = unpaid_invoices[i].invoice_no;
                        input_to_transactions.invoice_code = unpaid_invoices[i].dt_code+unpaid_invoices[i].invoice_no;
                        input_to_transactions.amount = unpaid_invoices[i].amount;
                        input_to_transactions.valdo_tx_id = "TR"+moment().format('YYYYMMDDHHmmss');
                        input_to_transactions.transact_date = moment().format('YYYY-MM-DD HH:mm:ss');
                        input_to_transactions.currency_code = "IDR";
                        input_to_transactions.type = 0;
                        input_to_transactions.distributor_id = dist[0].id;
                        input_to_transactions.retailer_id = toko[0].id;  
                        input_to_transactions.from_user_id = 0;
                        input_to_transactions.to_user_id = dist[0].user_id;
                        let input_to_transactions_res = await ApiPohonDana.insert_manual_transaction(input_to_transactions); 
                        
                   }
                })

                Logs.create({action: 'repayment_to_distributor', description: 'token:' + token + ',le_code:' + unpaid_invoices[i].le_code + ',dt_code:' + unpaid_invoices[i].le_code + ',invoice_no:' + unpaid_invoices[i].invoice_no});
            }
        } else {
            throw new CustomError(400, 'Unpaid invoice is not found!')
        }

        let out = {
            total_unpaid_invoices: unpaid_invoices.length,
            message: 'Success'
        }
//    } catch (err) {
//        if (err.code === undefined) {
//            err.code = 400;
//            err.err_code = '';
//            err.message = 'Gagal menampilkan data invoice, ' + err;
//        }
//
//        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
//    }
}

async function repayment_to_distributor(req, res) {
   const token = req.swagger.params.token.value;
    let fmcg_top = 5;
    let fmcg_id = 13;
    let vacc_fit = '21000000102';
    try {
        let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});

        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
        if (result_fmcg.dataValues == null) {
            throw new CustomError(400, 'Gagal menampilkan data payment_distributor, data fmcg tidak tersedia');
        }

        let result_fmcg_top = await service_fmcg.getOneFmcgWithFilter({id: fmcg_top});
        if (result_fmcg_top.dataValues == null) {
            throw new CustomError(400, 'Gagal menampilkan data payment_distributor, data fmcg tidak tersedia');
        }

        let args = {
            url: result_fmcg_top.dataValues.url,
            fmcg_token: result_fmcg_top.dataValues.fmcg_token,
            fmcg_name: result_fmcg_top.dataValues.name
        }

        let unpaid_invoices = await ApiPohonDana.get_unpaid_invoices(args)
       

        if (unpaid_invoices.length !== 0) {
            for (let i = 0; i < unpaid_invoices.length; i++) {

                let dist = await service_pelunasan_manual.get_dt_unicode({dt_code: unpaid_invoices[i].dt_code})
                let toko = await service_pelunasan_manual.get_le_unicode({le_code: unpaid_invoices[i].le_code})
                let toko_data = await service_pelunasan_manual.get_le_aditional({le_code: unpaid_invoices[i].le_code})
                let nama_toko = '';
                if (toko_data.length === 0)
                {
                    nama_toko = 'Tidak ada nama';
                } else {
                    nama_toko = toko_data[0].value;
                }

                let payment = {
                    trans_type: "transfer",
                    status: "success",
                    vacc_from: vacc_fit,
                    vacc_to: [
                        {vacc_number: dist[0].valdo_account, amount: parseInt(unpaid_invoices[i].amount), desc: `Pembayaran invoice ${unpaid_invoices[i].invoice_no} (${nama_toko})  Rp. ${helper.IDRformat(unpaid_invoices[i].amount)} dari Limit Pandai (${vacc_fit}) ke ${dist[0].fullname} (${dist[0].valdo_account})`}
                    ],
                    pin: '123456'
                }
                request({
                    headers: {
                        'apigatekey': apigatekey,
                        'Content-Type': 'application/json'
                    },
                    url: 'http://' + url_trf + '/api/v1/multi_vacc/trf',
                    method: "POST",
                    json: true,
                    body: payment
                }, async function (error, result, body) {

                    if (body.status.code === 200) {
                        let update_data = {
                            invoice_code:unpaid_invoices[i].invoice_code,
                            payment_status: 1,
                            fmcg_token: result_fmcg_top.dataValues.fmcg_token,
                            fmcg_name: result_fmcg_top.dataValues.name,
                            url: result_fmcg_top.dataValues.url
                        }
                        let update_payment_status = await ApiPohonDana.update_payment_status_distributor(update_data)
                        

                        let input_to_transactions = {};
                        input_to_transactions.token = token;
                        input_to_transactions.url = result_fmcg.dataValues.url;
                        input_to_transactions.fmcg_token = result_fmcg.dataValues.fmcg_token;
                        input_to_transactions.fmcg_name = result_fmcg.dataValues.name;
                        input_to_transactions.le_code = unpaid_invoices[i].le_code;
                        input_to_transactions.dt_code = unpaid_invoices[i].dt_code;
                        input_to_transactions.invoice_id = unpaid_invoices[i].invoice_no;
                        input_to_transactions.invoice_code = unpaid_invoices[i].dt_code+unpaid_invoices[i].invoice_no;
                        input_to_transactions.amount = unpaid_invoices[i].amount;
                        input_to_transactions.valdo_tx_id = "TR"+moment().format('YYYYMMDDHHmmss');
                        input_to_transactions.transact_date = moment().format('YYYY-MM-DD HH:mm:ss');
                        input_to_transactions.currency_code = "IDR";
                        input_to_transactions.type = 0;
                        input_to_transactions.distributor_id = dist[0].id;
                        input_to_transactions.retailer_id = toko[0].id;  
                        input_to_transactions.from_user_id = 0;
                        input_to_transactions.to_user_id = dist[0].user_id;
                        let input_to_transactions_res = await ApiPohonDana.insert_manual_transaction(input_to_transactions); 
                        
                   }
                })

                Logs.create({action: 'repayment_to_distributor', description: 'token:' + token + ',le_code:' + unpaid_invoices[i].le_code + ',dt_code:' + unpaid_invoices[i].le_code + ',invoice_no:' + unpaid_invoices[i].invoice_no});
            }
        } else {
            throw new CustomError(400, 'Unpaid invoice is not found!')
        }

        let out = {
            total_unpaid_invoices: unpaid_invoices.length,
            message: 'Success'
        }
        res.status(200).json({code:200, body: out})
    } catch (err) {
        if (err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data invoice, ' + err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}


async function update_invoice_pd(req, res) {
    const token = req.swagger.params.token.value;
    let data_unique_code = '';
    let fmcg_id =5;
    try {

        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});

       
        if(result_fmcg.dataValues == {}) {
            throw new CustomError(400, 'Gagal memproses data invoice, data fmcg tidak tersedia');
        }
        let invoice_args = {};
        invoice_args.url = result_fmcg.dataValues.url;
        invoice_args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        invoice_args.fmcg_name = result_fmcg.dataValues.name;
        
        let api_response_fmcg = await ApiPohonDana.update_invoice(invoice_args);

        res.json(api_response_fmcg);
        } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data top, '+err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

export { calculator }
export { apply_loan }
export { payment_distributor }
export { get_invoice }
export { get_one_invoice }
export { repayment }
export { repayment_to_distributor }
export { update_invoice_pd }