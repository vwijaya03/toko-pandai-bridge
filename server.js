'use strict';

import cors from 'cors'
import express from 'express'
import multer from 'multer'
import path from 'path'
import SwaggerExpress from 'swagger-express-mw';
import bodyParser from 'body-parser';
import service_mariadb from '~/services/mariadb'

import log from '~/utils/console_log'

import { Logs } from '~/orm/index'

process.on('uncaughtException', function (err) {
	Logs.create({action: 'error uncaughtException', description: `uncaughtException: ${err.message}, err stack: ${err.stack}`});
	process.exit(1);
});

let app = require('express')();
let config = {
  	appRoot: __dirname // required config
};
let port = process.env.PORT || 21010;

// Untuk bisa langsung akses /apk/versi_android.apk
app.use(express.static('static_file'));

SwaggerExpress.create(config, function(err, swaggerExpress) {
	if (err) { 
		throw err; 
	}

	// Init multer untuk upload
	let upload = multer({dest: path.resolve('/tmp', './')});

	app.use(cors());
	app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
	// app.use(upload.single('file'));
	// app.use(upload.any());
    // app.use(formidable.parse());

    // Di gunakan untuk menerima upload file dengan field name : file
    app.use(upload.fields([{name: 'file'}]));
	let server = app.listen(port);

	// Dari app listen, di set timeout server nya menjadi sekian detik
	server.timeout = 60000;
	server.keepAliveTimeout = 60000;

	// install middleware
	swaggerExpress.register(app);
});

async function run() {
	try {
		await service_mariadb.init();
		log.info('running on port '+port);
		log.info('server is ready');
	} catch(err) {
		Logs.create({action: 'error run', description: JSON.stringify(err)});
	}
}

let obj = {
	run: run
}

export default obj
export { app }