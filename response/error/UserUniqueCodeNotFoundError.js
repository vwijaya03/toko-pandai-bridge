class UserUniqueCodeNotFoundError extends Error {
	constructor(code, message) {
        super(message);
        this.code = 400;
        this.message = 'Unique code user tidak di temukan';
    }
}

export default UserUniqueCodeNotFoundError