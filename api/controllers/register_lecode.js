'use strict';

import _ from 'lodash'
import util from 'util'
import fs from 'fs'
import moment from 'moment'

import middleware from '~/api/controllers/auth'
import base_repo from '~/api/controllers/base'

import service_excel from '~/services/excel'
import service_invoice from '~/services/invoice'
import service_fmcg from '~/services/fmcg'
import service_user from '~/services/user'
import service_user_unique_code from '~/services/user_unique_code'
import service_register_lecode from '~/services/register_lecode'

import helper from '~/utils/helper'

import ApiUli from '~/external_api/api_uli'
import ApiTop from '~/external_api/api_top'

import { Logs } from '~/orm/index'
import { RegisteredLeCode } from '~/orm/index'
import { Op } from '~/utils/helper'

import CustomError from '~/response/error/CustomError'
import CustomSuccess from '~/response/success/CustomSuccess'

import csvToJson from "convert-csv-to-json";

const acl_task_list = ['distributor', 'distributor_manager', 'super_admin', 'call_center','finance'];

async function upload_data_lecode(req, res) {
    const token = req.swagger.params.token.value;
    const param_filename = req.swagger.params.filename;

    let filename = _.isUndefined(param_filename.value)?null:param_filename.value;

    try {
        Logs.create({action: 'upload_data_lecode', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_task_list, is_allowed_access_token_static: false});
        let dest_dir = __dirname+'/../../static_file/data-lecode/'+filename;

        let dataCsv = csvToJson.fieldDelimiter(',').getJsonFromCsv(dest_dir);

        for (let i = 0; i < dataCsv.length; i++) {
            let result = await RegisteredLeCode.findOne({where: {fmcg_id: dataCsv[i].fmcg, jenis: dataCsv[i].jenis, le_code: dataCsv[i].le_code} });

		    if(result == null) {
          	    await RegisteredLeCode.create({
                    user_id: auth_user.dataValues.user_id,
                    fmcg_id: dataCsv[i].fmcg_id,
                    le_code: dataCsv[i].le_code,
                    jenis: dataCsv[i].jenis,
                    retailer: dataCsv[i].retailer,
                    address: dataCsv[i].address,
                    dt_code: dataCsv[i].dt_code
                });
            }
        }

        res.json({code: 200, message: 'Data berhasil diupload'});
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal upload data lecode, '+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function getLeCode(req, res) {
    const token = req.swagger.params.token.value;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_page = req.swagger.params.page;
    const param_items_per_page = req.swagger.params.items_per_page;

    let fmcg_id = _.isUndefined(param_fmcg_id.value)?null:param_fmcg_id.value;
    let startPage = _.isUndefined(param_page.value)?0:param_page.value;
    let itemsPerPage = _.isUndefined(param_items_per_page.value)?15:param_items_per_page.value;

    startPage = (startPage < 1 ) ? 1 : startPage;

    const limit = itemsPerPage;
    const offset = ( startPage - 1 ) * itemsPerPage;

    try {
        Logs.create({action: 'getLeCode', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_task_list, is_allowed_access_token_static: false});
        
        let where = {user_id: auth_user.dataValues.user_id};

        let result_obj = await base_repo.get({entity:RegisteredLeCode, where:where, limit:limit, offset:offset, attributes: ['id', 'le_code', 'jenis', 'retailer']});

        const {total:total, result_list:result_list} = result_obj;
        let total_pages = Math.ceil(total / itemsPerPage);
        let out = {};
        
        out.page = startPage;
        out.total_pages = total_pages;
        out.total_items = total;
        out.items_per_page = itemsPerPage;
        out.size = result_list.length;
        out.result = result_list;

        res.json(out);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data lecode, '+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function postEditLeCode(req, res) {
    const token = req.swagger.params.token.value;
    const param_id = req.swagger.params.id;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_lecode = req.swagger.params.lecode;
    const param_jenis = req.swagger.params.jenis;
    const param_retailer = req.swagger.params.retailer;

    let id = _.isUndefined(param_id.value)?null:param_id.value;
    let fmcg_id = _.isUndefined(param_fmcg_id.value)?null:param_fmcg_id.value;
    let lecode = _.isUndefined(param_lecode.value)?"":param_lecode.value;
    let jenis = _.isUndefined(param_jenis.value)?"":param_jenis.value;
    let retailer = _.isUndefined(param_retailer.value)?"":param_retailer.value;
    
    try {
        Logs.create({action: 'postEditLeCode', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_task_list, is_allowed_access_token_static: false});

        let existDataLecode = await service_register_lecode.getOne({fmcg_id: fmcg_id, jenis: jenis, le_code: lecode, id: {[Op.ne]: id} });

        if(existDataLecode != null) {
            throw new CustomError(400, 'Data Le Code telah sudah ada');
        }
        
        await RegisteredLeCode.update(
            { fmcg_id: fmcg_id, le_code: lecode, jenis: jenis, retailer: retailer },
            { where: { id: id }
        })

        res.json(new CustomSuccess(200, 'Data berhasil diubah'));
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data lecode, '+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function getLeCodeReg(req, res) {
    const token = req.swagger.params.token.value;
    const param_le_code = req.swagger.params.le_code;

    let le_code = _.isUndefined(param_le_code.value)?null:param_le_code.value;

    try {
        Logs.create({action: 'getLeCode', description: helper.getSimpleSwaggerParams(req.swagger.params)});
        let auth_user = await middleware.verify({token: token, acl: "", is_allowed_access_token_static: true});
        let result_obj = await service_register_lecode.getOnes({le_code:le_code});
        let respon = {}
        if(result_obj === null){
            respon.code = 400;
            respon.message = "Le Code Tidak Terdaftar dimaster Data";
        }else{
            respon.code = 200;
            respon.data = result_obj[0];
        }
        res.status(respon.code).json(respon);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data lecode, '+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

export { upload_data_lecode }
export { getLeCode }
export { postEditLeCode }
export { getLeCodeReg }