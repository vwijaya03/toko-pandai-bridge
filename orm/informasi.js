import { orm } from '~/services/mariadb'
import Sequelize from 'sequelize'

const Informasi = orm.define('informasi', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    createdAt: {
        type: 'TIMESTAMP', allowNull:true
    },
    updatedAt: {
        type: 'TIMESTAMP', allowNull:true
    },
    title: {
        type: Sequelize.STRING, allowNull:false
    },
    content: {
        type: Sequelize.STRING, allowNull:false
    },
    img_path: {
        type: Sequelize.STRING, allowNull:true
    },
    fmcg_id: {
        type: Sequelize.STRING, allowNull:true
    },
    user_type: {
        type: Sequelize.STRING, allowNull:true
    },
    is_delete: {
        type: Sequelize.INTEGER, allowNull:true
    }
});

export default Informasi
