'use strict';

import _ from 'lodash';
import moment from 'moment'
import crypto from 'crypto'
import RandomString from 'randomstring'
import NodeMailer from 'nodemailer'

import helper from '~/utils/helper'

import { Op } from '~/utils/helper'
import { User } from '~/orm/index'
import { UserUniqueCode } from '~/orm/index'
import { allowed_user_status } from '~/properties'
import service_otp from '~/services/otp';
import CustomError from '~/response/error/CustomError'
import UserNotFoundError from '~/response/error/UserNotFoundError'

function authUser({token=null}={}) {

	return User.findOne({
		attributes: ['user_id', 'email', 'fullname', 'nama_toko', 'phone', 'tanggal_lahir', 'tempat_lahir', 'ktp', 'type', 'imei', 'valdo_account', 'salt', 'password', 'pin', 'access_token', 'access_token_expiry', 'fcm_id', 'join_date', 'status','mobile_version'],
		where: {
			access_token: token,
			[Op.or]: allowed_user_status
		}
	});
}

function getOneByEmail({email=null}={}) {

	return User.findOne({
		attributes: ['user_id', 'email', 'fullname', 'nama_toko', 'phone', 'tanggal_lahir', 'tempat_lahir', 'ktp', 'type', 'imei', 'valdo_account', 'salt', 'password', 'pin', 'access_token', 'access_token_expiry', 'fcm_id', 'join_date'],
		where: {
			email: email,
			[Op.or]: allowed_user_status
		}
	});
}

function getOneByPhone({phone=null}={}) {

	return User.findOne({
		attributes: ['user_id', 'email', 'fullname', 'nama_toko', 'phone', 'tanggal_lahir', 'tempat_lahir', 'ktp', 'type', 'imei', 'valdo_account', 'salt', 'password', 'pin', 'access_token', 'access_token_expiry', 'fcm_id', 'join_date'],
		where: {
			phone: phone,
			[Op.or]: allowed_user_status,
                       
		}
	});
}


function getHashedPassword(salt, rawPassword) {
    let hasher = crypto.createHmac('sha512', salt); /** Hashing algorithm sha512 */
    hasher.update(rawPassword);
    let value = hasher.digest('hex');
    return value;
}

function getHashedPin(salt, rawPin) {
    var hasher = crypto.createHmac('sha512', salt); /** Hashing algorithm sha512 */
    hasher.update(rawPin);
    var value = hasher.digest('hex');
    return value;
}

function renewAccessToken({user=null}={}) {

	const user_salt = user.salt.toString('utf8');
    const time_millis = moment.utc().unix() * 1000;
    const token = crypto.createHmac('sha256', user_salt).update(''+time_millis).digest('hex');

    let accessTokenExpiryDate = new Date();
    accessTokenExpiryDate.setDate(accessTokenExpiryDate.getDate() + 30);

    User.update({
        accessToken: token,
        accessTokenExpiry: accessTokenExpiryDate
    }, {where:{user_id: user.dataValues.user_id}});

    return token;
}

function getOneByVA({vaccount=null}={}) {

	return User.findOne({
		attributes: ['user_id', 'email', 'fullname', 'nama_toko', 'phone', 'tanggal_lahir', 'tempat_lahir', 'ktp', 'type', 'valdo_account', 'access_token', 'fcm_id', 'access_token_expiry'],
		where: {
			valdo_account: vaccount,
			[Op.or]: allowed_user_status
		}
	});
}

function changepassword({user_id=null, user_salt=null, newpassword=null}={}) {

    let hashedNewPassword = getHashedPassword(user_salt, newpassword);

    User.update(
        { password: hashedNewPassword, password_changed: 'false' },
        { where: { user_id: user_id }}
    );
}

async function konfirmasi_reset_password({from_email=null, otp=null}={}) {
    
	const salt = crypto.randomBytes(64).toString('base64');
    let new_password = RandomString.generate({
        length: 6,
        numeric: true
    });
    let hashedNewPassword = getHashedPassword(salt, new_password);

//    let result = await service_otp.verifyOtp(null, otp, from_email, 2);
    
    let out = {};

//    if(_.size(result) > 0) {
        let response = await User.findOne({where: {email: from_email }});
        if(response != null) {
            User.update(
                { password: hashedNewPassword, salt: salt, password_changed: 'true' },
                { where: { email: from_email }}
            );

            let transporter = NodeMailer.createTransport({
                  host: 'smtp.gmail.com',//smtp.gmail.com  //in place of service use host...
                  secure: true,//true
                  port: 465,//465
                  auth: {
                  user: 'cstokpandai@gmail.com',
                  pass: 'topan123456'
                    }, tls: {
                      rejectUnauthorized: false
                    }
            });

            let mailOptions = {
                from: 'Toko Pandai Customer Service',
                to: from_email,
                subject: 'Konfirmasi Perubahan Kata Sandi',
                text: 'Ini adalah password baru anda: '+ new_password,
                html: 'Ini adalah password baru anda: '+ new_password
            };
            console.log(mailOptions)
            transporter.sendMail(mailOptions, function(error, info) {
                if (error) {
                } else {
                    transporter.close();
                }

            });
        }
        else{
           throw new UserNotFoundError();
        }
//    } else {
//        throw new CustomError(400, 'Link Expired');
//    }
}

function resetpin({pin=null, user_salt=null, user_id=null, email=null}={}) {

    let hashedPin = getHashedPassword(user_salt, pin.toString());

    User.update(
        { pin: hashedPin },
        { where: { user_id: user_id }}
    );

    let transporter = NodeMailer.createTransport({
        host: 'smtp.gmail.com',//smtp.gmail.com  //in place of service use host...
                  secure: true,//true
                  port: 465,//465
                  auth: {
                  user: 'cstokpandai@gmail.com',
                  pass: 'topan123456'
                    }, tls: {
                      rejectUnauthorized: false
                    }
    });

    let mailOptions = {
        from: 'Toko Pandai Customer Service',
        to: email,
        subject: 'Konfirmasi Perubahan Pin',
        text: 'Ini adalah pin sementara anda: '+ pin,
        html: 'Ini adalah pin sementara anda: '+ pin
    };
    transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
        } else {
            transporter.close();
        }

    });
}

function changepin({user_id=null, user_salt=null, pin=null}={}) {
    let hashedPin = getHashedPassword(user_salt, pin.toString());

    return User.update(
        { pin: hashedPin },
        { where: { user_id: user_id }
    });
}

async function reset_password_by_call_center({fullname=null, pob=null, dob=null}={}) {
    const salt = crypto.randomBytes(64).toString('base64');
    let new_password = RandomString.generate({
        length: 6,
        numeric: true
    });
    let hashedNewPassword = getHashedPassword(salt, new_password);

    let response = await User.findOne({where: {fullname: fullname, tanggal_lahir: dob, tempat_lahir: pob}});

    if(response != null)
    {
        User.update(
            { password: hashedNewPassword, salt: salt, password_changed: 'true' },
            { where: { email: response.email }}
        );

        let transporter = NodeMailer.createTransport({
            host: 'smtp.gmail.com',//smtp.gmail.com  //in place of service use host...
                  secure: true,//true
                  port: 465,//465
                  auth: {
                  user: 'cstokpandai@gmail.com',
                  pass: 'topan123456'
                    }, tls: {
                      rejectUnauthorized: false
                    }
        });

        let mailOptions = {
            from: 'Toko Pandai Customer Service',
            to: from_email,
            subject: 'Konfirmasi Perubahan Kata Sandi',
            text: 'Ini adalah password baru anda: '+ new_password,
            html: 'Ini adalah password baru anda: '+ new_password
        };
        transporter.sendMail(mailOptions, function(error, info) {
            if (error) {
            } else {
                transporter.close();
            }

        });
    } else {
        throw new UserNotFoundError();
    }
}

async function getOneUserByCallCenter({fullname=null, pob=null, dob=null}={}) {
    return User.findOne({
        attributes: ['user_id', 'email', 'fullname', 'nama_toko', 'phone', 'tanggal_lahir', 'tempat_lahir', 'type', 'valdo_account', 'salt'],
        where: {
            fullname: fullname,
            tanggal_lahir: dob,
            tempat_lahir: pob,
            [Op.or]: allowed_user_status
        }
    });
}

function register({imei=null, email=null, password=null, phone=null, usertype=null}={}) {
    // generate secret salt
    const salt = crypto.randomBytes(64).toString('base64');

    const access_token = crypto.randomBytes(64).toString('base64');
    const access_token_expiry = helper.formatCustomCurrentDateTime(null, 30, null, null, null);

    // generate hashed password
    let hashedPassword = getHashedPassword(salt, password);

    let args = {};
    args.imei = imei;
    args.email = email;
    args.password = hashedPassword;
    args.password_changed = 'false';
    args.phone = phone;
    args.type = usertype;
    args.salt = salt;
    args.pin = null;
    args.accessToken = access_token;
    args.accessTokenExpiry = access_token_expiry;
    args.join_date = helper.formatCurrentDateTime();
    args.status = 0;

    return User.create(args);
}

async function get(filter, options) {

    options = options || {};
    let result_set = '';

    if(filter) {
        result_set = await User.findAll({where: filter});
    } else {
        result_set = await User.findAll();
    }

    if (!result_set || Object.keys(result_set).length == 0) {
        if (options.throwIfEmpty) {
            throw new UserNotFoundError();
        }
    }

    return result_set;
}

async function getOne(filter, options) {

    options = options || {};
    let result_set = '';

    result_set = await User.findOne({where: filter});

    if (!result_set || Object.keys(result_set).length == 0) {
        if (options.throwIfEmpty) {
            throw new UserNotFoundError();
        }
    }

    return result_set;
}

function update({user_id=null, fullname=null, nama_toko=null, tanggal_lahir=null, tempat_lahir=null, ktp=null}={}) {
    return User.update(
        { fullname: fullname, nama_toko: nama_toko, tanggal_lahir: tanggal_lahir, tempat_lahir: tempat_lahir, ktp: ktp },
        { where: { user_id: user_id }}
    );
}

function updateFcm({user_id=null, fcm_id=null}={}) {
    return User.update(
        { fcm_id: fcm_id},
        { where: { user_id: user_id }}
    );
}

function updateVA({user_id=null, valdo_account=null}={}) {
    return User.update(
        { valdoAccount: valdo_account},
        { where: { user_id: user_id }}
    );
}

function register_manual({fullname=null,nama_toko=null,imei=null,email=null,pin=null, password=null, phone=null, usertype=null,register_type=null}={}) {
    // generate secret salt
    const salt = crypto.randomBytes(64).toString('base64');

    const access_token = crypto.randomBytes(64).toString('base64');
    const access_token_expiry = helper.formatCustomCurrentDateTime(null, 30, null, null, null);

    // generate hashed password
    let hashedPassword = getHashedPassword(salt, password);
    let hashedPin = getHashedPassword(salt, pin.toString());
    let args = {};
    args.fullname = fullname;
    args.nama_toko = nama_toko;
    args.imei = imei;
    args.unique_phone=imei;
    args.email = email;
    args.password = hashedPassword;
    args.password_changed = 'false';
    args.phone = phone;
    args.type = usertype;
    args.salt = salt;
    args.pin = hashedPin;
    args.ktp = helper.currentMilisecondsTime();
    args.tanggal_lahir ="1990-01-01";
    args.tempat_lahir = "Jakarta";
    args.accessToken = access_token;
    args.accessTokenExpiry = access_token_expiry;
    args.join_date = helper.formatCurrentDateTime();
    args.status = 0;
    args.register_type = register_type;

    return User.create(args);
}



let obj = {}
obj.authUser = authUser
obj.get = get
obj.getOne = getOne
obj.getOneByVA = getOneByVA
obj.register = register
obj.update = update
obj.updateFcm = updateFcm
obj.updateVA = updateVA
obj.getOneByEmail = getOneByEmail
obj.getOneUserByCallCenter = getOneUserByCallCenter
obj.getHashedPassword = getHashedPassword
obj.getHashedPin = getHashedPin
obj.renewAccessToken = renewAccessToken
obj.changepassword = changepassword
obj.konfirmasi_reset_password = konfirmasi_reset_password
obj.reset_password_by_call_center = reset_password_by_call_center
obj.changepin = changepin
obj.resetpin = resetpin
obj.getOneByPhone =getOneByPhone
obj.register_manual = register_manual

export default obj
