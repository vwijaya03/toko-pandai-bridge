import _ from 'lodash'
import util from 'util'

import middleware from '~/api/controllers/auth'

import service_user from '~/services/user'

import MoneyAccess from '~/external_api/moneyaccess'
import helper from '~/utils/helper'

import { Logs } from '~/orm/index'

import CustomError from '~/response/error/CustomError'
import CustomSuccess from '~/response/success/CustomSuccess'
import UserNotFoundError from '~/response/error/UserNotFoundError'

const acl_call_center = ['call_center', 'super_admin'];

async function call_center_reset_password(req, res) {
	const token = req.swagger.params.token.value;
    const param_fullname = req.swagger.params.fullname;
    const param_pob = req.swagger.params.pob;
    const param_dob = req.swagger.params.dob;

    let fullname = _.isUndefined(param_fullname.value)?'':param_fullname.value;
    let pob = _.isUndefined(param_pob.value)?'':param_pob.value;
    let dob = _.isUndefined(param_dob.value)?'':param_dob.value;

    try {
        Logs.create({action: 'call_center_reset_password', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_call_center, is_allowed_access_token_static: false});

        if(fullname == null || fullname == "") {
            throw new CustomError(400, 'Nama lengkap harus di isi');
        } if(pob == null || pob == "") {
            throw new CustomError(400, 'Tempat lahir harus di isi');
        } if(dob == null || dob == "") {
            throw new CustomError(400, 'Tanggal lahir harus di isi');
        }

        let args = {};
        args.fullname = fullname;
        args.pob = pob;
        args.dob = dob;

        await service_user.reset_password_by_call_center(args);

      	res.json(new CustomSuccess(200, 'Reset password berhasil'));
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Call center reset password gagal, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function call_center_reset_pin(req, res) {
	const token = req.swagger.params.token.value;
    const param_fullname = req.swagger.params.fullname;
    const param_pob = req.swagger.params.pob;
    const param_dob = req.swagger.params.dob;

    let fullname = _.isUndefined(param_fullname.value)?'':param_fullname.value;
    let pob = _.isUndefined(param_pob.value)?'':param_pob.value;
    let dob = _.isUndefined(param_dob.value)?'':param_dob.value;

    try {
        Logs.create({action: 'call_center_reset_pin', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_call_center, is_allowed_access_token_static: false});

        if(fullname == null || fullname == "") {
            throw new CustomError(400, 'Nama lengkap harus di isi');
        } if(pob == null || pob == "") {
            throw new CustomError(400, 'Tempat lahir harus di isi');
        } if(dob == null || dob == "") {
            throw new CustomError(400, 'Tanggal lahir harus di isi');
        }

        let args = {};
        args.fullname = fullname;
        args.pob = pob;
        args.dob = dob;

        let response_user = await service_user.getOneUserByCallCenter(args);

        if(response_user == null) {
        	throw new UserNotFoundError();
        }

        let moneyaccess_args = {
            vaccount: response_user.dataValues.valdo_account,
            usertype: response_user.dataValues.type
        };

        let api_response = await MoneyAccess.resetpin(moneyaccess_args);

        Logs.create({action: 'api response money access call_center_reset_pin resetpin', description: util.inspect(api_response)});

        let user_args = {};

        user_args.pin = api_response.userinfo.new_pin;
        user_args.user_salt = response_user.dataValues.salt;
        user_args.user_id = response_user.dataValues.user_id;
        user_args.email = response_user.dataValues.email;

        await service_user.resetpin(user_args);

        res.json(new CustomSuccess(200, 'Reset pin berhasil'));
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Call center reset pin gagal, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function call_center_get_customer_data(req, res) {
	const token = req.swagger.params.token.value;
    const param_fullname = req.swagger.params.fullname;
    const param_pob = req.swagger.params.pob;
    const param_dob = req.swagger.params.dob;

    let fullname = _.isUndefined(param_fullname.value)?'':param_fullname.value;
    let pob = _.isUndefined(param_pob.value)?'':param_pob.value;
    let dob = _.isUndefined(param_dob.value)?'':param_dob.value;

    try {
        Logs.create({action: 'call_center_get_customer_data', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_call_center, is_allowed_access_token_static: false});

        if(fullname == null || fullname == "") {
            throw new CustomError(400, 'Nama lengkap harus di isi');
        } if(pob == null || pob == "") {
            throw new CustomError(400, 'Tempat lahir harus di isi');
        } if(dob == null || dob == "") {
            throw new CustomError(400, 'Tanggal lahir harus di isi');
        }

        let args = {};
        args.fullname = fullname;
        args.pob = pob;
        args.dob = dob;

        let response_user = await service_user.getOneUserByCallCenter(args);

        if(response_user == null) {
        	throw new UserNotFoundError();
        }

        let out = {};
        out.fullname = response_user.dataValues.fullname;
        out.nama_toko = response_user.dataValues.nama_toko;
        out.phone = response_user.dataValues.phone;
        out.tanggal_lahir = response_user.dataValues.tanggal_lahir;
        out.tempat_lahir = response_user.dataValues.tempat_lahir;

        res.json(out);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Call center reset password gagal, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

export { call_center_reset_password }
export { call_center_reset_pin }
export { call_center_get_customer_data }