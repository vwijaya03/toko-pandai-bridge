import request from 'request'
import util from 'util'

import { Logs } from '~/orm/index'

import ResponseAdapter from '~/adapter/ResponseAdapter'

const url_notif = "192.168.12.7:10013";
const url_multifintech = 'http://192.168.12.6:21001/'

let tokenkey = 'gUxOsYYXbeywpM2xaV37RaNvdZ5nJdI8d8AdYvy4';

import FmcgGetInvoicesError from '~/response/error/FmcgGetInvoicesError'
import FmcgCheckProcessInvoiceError from '~/response/error/FmcgCheckProcessInvoiceError'

function commonHeaders() {
    return {
        'Accept': 'application/json',
        'Cache-Control': 'no-cache',
        'apigatekey': 'webKEY123456789'
    };
}

function authHeaders(headers) {
    headers = headers || {};
    headers['Authorization'] = 'Bearer ' + tokenkey;
    return headers;
}

function handleResponse(rawJsonResponse, errorClass, resolve, reject, requestError, function_name, fmcg_name) {

	try {
		let successJsonResp = JSON.parse(rawJsonResponse);
	
		if(requestError != null) {
			reject(new errorClass(requestError));
		}

		if(successJsonResp.code == 400 || successJsonResp.code == 401) {
			reject(new errorClass(successJsonResp.message));
		} else {
			resolve(successJsonResp);
		}
	} catch(err) {
		Logs.create({action: `error ${function_name} ${fmcg_name}`, description: util.inspect(err), additional_description: util.inspect(rawJsonResponse)});

		reject(new errorClass('terjadi kesalahan silahkan coba lagi'));
	}
	
}
function handleResponseError(errorClass, resolve, reject, err, function_name) {
	Logs.create({action: `error ${function_name}`, description: util.inspect(err)});
	
	reject(new errorClass(err));
}


function update_version({user_id=null, version=null}={}) {
     return new Promise(function(resolve, reject) {
     let url= 'http://'+url_notif+'/updatedVersion';
          let args ={  user_id: user_id, version: version }
          let headers={ 'Postman-Token': '3f73a5de-f0f9-4c47-9947-096dad7321a8',
               'cache-control': 'no-cache',
               'Content-Type': 'application/json',
               token: '1f8l4ckp1nk1ny0ur4r34th3n91rlS93n3r4t10nm4k3y0urf33lth3h34tth1S1SSh0S13y0ukn0w' }
            request.post( {url:url, headers:headers, form: args}, function(err, httpResponse, body) {
            if(err) {
                return err;
            }
            handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'update_version', "");
        }).on('error', function(err) {
            handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'update_version', "");
        });
     });
}

function get_running_text({user_id=null,token=null}={}) {
    return new Promise(function(resolve, reject) {
          let url= `http://192.168.12.7:10013/notifications/running-text`;
          let args ={ user_id }
          let headers={ 'Postman-Token': '3f73a5de-f0f9-4c47-9947-096dad7321a8',
               'cache-control': 'no-cache',
               'Content-Type': 'application/json',
               token: token }
            request.get( {url:url, headers:headers, qs: args}, function(err, httpResponse, body) {
            if(err) {
                return err;
            }
            handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get_running_text', "");
        }).on('error', function(err) {
            handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get_running_text', "");
        });
    });
}

function get_invoice({le_code=null,page=null,items_per_page=null,token=null}={}) {
    return new Promise(function(resolve, reject) {
          let url= 'http://localhost:21001/invoice/get-invoices';
          let args ={le_code:le_code,page:page,items_per_page:items_per_page}
          let headers={token:token}
            request.get({url:url, headers:headers, formData: args}, function(err, httpResponse, body) {
            if(err) {
                return err;
            }
            handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get_invoice', "");
        }).on('error', function(err) {
            handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get_invoice', "");
        });
    });
}

function get_invoiceadmin({role_type=null,dist_dt_code=null,dt_code=null,le_code=null,invoice_payment_status=null,due_date_from=null,due_date_to=null,sorting=null,page=null,items_per_page=null,token=null}={}) {
    return new Promise(function(resolve, reject) {
          let url= 'http://localhost:21001/invoice/admin/get-invoices';
          let args ={role_type:role_type,dist_dt_code:dist_dt_code,dt_code:dt_code,le_code:le_code,invoice_payment_status:invoice_payment_status,due_date_from:due_date_from,due_date_to:due_date_to,sorting:sorting,page:page,items_per_page:items_per_page}
          let headers={token:token}
            request.get({url:url, headers:headers, formData: args}, function(err, httpResponse, body) {
            if(err) {
                return err;
            }
            handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get_invoice', "");
        }).on('error', function(err) {
            handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get_invoice', "");
        });
    });
}

function get_one_invoice({le_code=null,dt_code=null,invoice_id=null,tenor=null,paid_by=null,token=null}={}) {
    return new Promise(function(resolve, reject) {
          let url= 'http://localhost:21001/invoice/get-one-invoice';
          let args ={le_code:le_code,dt_code:dt_code,invoice_id:invoice_id,tenor:tenor,paid_by:paid_by,token:token}
          let headers={token:token}
            request.get({url:url, headers:headers, formData: args}, function(err, httpResponse, body) {
            if(err) {
                return err;
            }
            handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get_invoice', "");
        }).on('error', function(err) {
            handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get_invoice', "");
        });
    });
}

let obj = {};

obj.update_version = update_version
obj.get_running_text = get_running_text
obj.get_invoice = get_invoice
obj.get_invoiceadmin = get_invoiceadmin
obj.get_one_invoice = get_one_invoice
export default obj
