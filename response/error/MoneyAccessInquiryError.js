class MoneyAccessInquiryError extends Error {
	constructor(message) {
        super(message);
        this.code = 400;
        this.err_code = 4100;
        this.message = 'Gagal mendapatkan informasi saldo, '+message;
    }
}

export default MoneyAccessInquiryError