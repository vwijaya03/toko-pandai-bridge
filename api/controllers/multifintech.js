'use strict';

import _ from 'lodash'
import util from 'util'
import fs from 'fs'
import middleware from '~/api/controllers/auth'
import request from 'request'
import moment from 'moment'

import { Logs } from '~/orm/index'

import api_multifintech from '~/external_api/api_multifintech'

import CustomError from '~/response/error/CustomError'
import CustomSuccess from '~/response/success/CustomSuccess'

const acl_get_invoices = ['retailer', 'distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];

let url_multifintech = 'http://192.168.12.6:21001/'

async function get_invoices(req,res){
    const token = req.swagger.params.token.value;
    const param_lecode = req.swagger.params.le_code;
    const param_page = req.swagger.params.page;
    const param_items_per_page = req.swagger.params.items_per_page;
    const le_code = _.isUndefined(param_lecode.value)? null : param_lecode.value;
    const page = _.isUndefined(param_page.value)? 0 : param_page.value;
    const items_per_page = _.isUndefined(param_items_per_page.value)? 15 : param_items_per_page.value;
    try{
    let allinvoice = await api_multifintech.get_invoice({le_code:le_code,page:page,items_per_page:items_per_page,token:token});
        allinvoice.invoices.forEach(item => {
        item.user_id = item.user_id
        item.invoice_code = item.invoice_code
        item.overdue = item.overdue
        item.cash_memo_doc_no = item.cash_memo_doc_no
        item.invoice_id = item.invoice_id
        item.dt_code = item.dt_code
        item.le_code = item.le_code
        item.tenor = item.tenor
        item.id = item.id
        item.outletcode = item.outletcode
        item.total_amount = item.total_amount
        item.balance_amount = item.balance_amount
        item.total_potongan = item.total_potongan
        item.status = item.status
        item.invoice_payment_status_paid = item.invoice_payment_status
        item.payment_status_unpaid = item.payment_status_unpaid
        item.sales_date = item.sales_date
        item.due_date = item.due_date
        if(item.paid_date === "" || item.paid_date === null){
            delete item.paid_date
        }else if(item.paid_date !== "" || item.paid_date !== null){
            item.paid_date = item.paid_date
        }
        item.is_paid = item.is_paid
        item.is_refunded = item.is_refunded
        item.has_refund = item.has_refund
        item.distributor = item.distributor
        item.tenor_ke = item.tenor_ke
        item.paid_by = item.paid_by
        item.outlet_name = item.outlet_name
        item.fmcg_id = item.fmcg_id
    })
    res.json(allinvoice)
    }catch(err){
        if (err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data Bayar Nanti, ' + err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function get_invoices_admin(req,res){
    const token = req.swagger.params.token.value;
    const param_dist_dtcode = req.swagger.params.dist_dt_code;
    const param_dtcode = req.swagger.params.dt_code;
    const param_lecode = req.swagger.params.le_code;
    const param_invoice_payment_status = req.swagger.params.invoice_payment_status;
    const param_due_date_from = req.swagger.params.due_date_from;
    const param_due_date_to = req.swagger.params.due_date_to;
    // const param_overdue = req.swagger.params.overdue;
    const param_sorting = req.swagger.params.sorting;
    const param_page = req.swagger.params.page;
    const param_items_per_page = req.swagger.params.items_per_page;

    const dist_dt_code = _.isUndefined(param_dist_dtcode.value)? "" : param_dist_dtcode.value;
    const dt_code = _.isUndefined(param_dtcode.value)? "" : param_dtcode.value;
    const le_code = _.isUndefined(param_lecode.value)? "" : param_lecode.value;
    const invoice_payment_status = _.isUndefined(param_invoice_payment_status.value)? "" : param_invoice_payment_status.value;
    const due_date_from = _.isUndefined(param_due_date_from.value)? "" : param_due_date_from.value;
    const due_date_to = _.isUndefined(param_due_date_to.value)? "" : param_due_date_to.value;
    const sorting = _.isUndefined(param_sorting.value)? "" : param_sorting.value;
    const page = _.isUndefined(param_page.value)? 0 : param_page.value;
    const items_per_page = _.isUndefined(param_items_per_page.value)? 15 : param_items_per_page.value;

    try{
        let auth_user = await middleware.verify({token: token, acl: acl_get_invoices, is_allowed_access_token_static: false});
        let allinvoice = await api_multifintech.get_invoice_admin({role_type:role_type,dist_dt_code:dist_dt_code,dt_code:dt_code,le_code:le_code,invoice_payment_status:invoice_payment_status,due_date_from:due_date_from,due_date_to:due_date_to,sorting:sorting,page:page,items_per_page:items_per_page,token:token});
            response.invoices.forEach(item => {
                item.user_id = item.user_id
                item.invoice_code = item.invoice_code
                item.overdue = item.overdue
                item.cash_memo_doc_no = item.cash_memo_doc_no
                item.invoice_id = item.invoice_id
                item.dt_code = item.dt_code
                item.le_code = item.le_code
                item.tenor = item.tenor
                item.id = item.id
                item.outletcode = item.outletcode
                item.total_amount = item.total_amount
                item.balance_amount = item.balance_amount
                item.total_potongan = item.total_potongan
                item.status = item.status
                item.invoice_payment_status_paid = item.invoice_payment_status
                item.payment_status_unpaid = item.payment_status_unpaid
                item.sales_date = item.sales_date
                item.due_date = item.due_date
                if(item.paid_date === ""){
                    delete item.paid_date
                }else if(item.paid_date !== ""){
                    item.paid_date = item.paid_date
                }
                item.is_paid = item.is_paid
                item.is_refunded = item.is_refunded
                item.has_refund = item.has_refund
                item.distributor = item.distributor
                item.tenor_ke = item.tenor_ke
                item.paid_by = item.paid_by
                item.outlet_name = item.outlet_name
                item.fmcg_id = item.fmcg_id
            })
            res.json(allinvoice);
    }catch(err){
        if (err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data Bayar Nanti, ' + err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function get_one_invoice(req,res){
    const token = req.swagger.params.token.value;
    const param_lecode = req.swagger.params.le_code;
    const param_dtcode = req.swagger.params.dt_code;
    const param_invoice_id = req.swagger.params.invoice_id;
    const param_tenor = req.swagger.params.tenor;
    const param_paid_by = req.swagger.params.paid_by;
    const le_code = _.isUndefined(param_lecode.value)? null : param_lecode.value;
    const dt_code = _.isUndefined(param_dtcode.value)? null : param_dtcode.value;
    const invoice_id = _.isUndefined(param_invoice_id.value)? null : param_invoice_id.value;
    const tenor = _.isUndefined(param_tenor.value)? null : param_tenor.value;
    const paid_by = _.isUndefined(param_paid_by.value)? null : param_paid_by.value;

    try{
        let allinvoice = await api_multifintech.get_one_invoice({le_code:le_code,dt_code:dt_code,invoice_id:invoice_id,tenor:tenor,paid_by:paid_by,token:token});
                if(allinvoice.paid_date === "" || allinvoice.paid_date === null){
                    delete allinvoice.paid_date
                }
                res.json(allinvoice)
    }catch(err){
        if (err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data Bayar Nanti, ' + err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

export { get_invoices }
export { get_invoices_admin }
export { get_one_invoice }
