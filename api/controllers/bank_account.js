import _ from 'lodash'
import util from 'util'

import middleware from '~/api/controllers/auth'

import MoneyAccess from '~/external_api/moneyaccess'

import service_bank_account from '~/services/bank_account'
import service_bank from '~/services/bank'

import helper from '~/utils/helper'

import { orm } from '~/services/mariadb'
import { Logs } from '~/orm/index'

import CustomError from '~/response/error/CustomError'
import CustomSuccess from '~/response/success/CustomSuccess'

const acl_bank_account = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center','finance'];

async function get_user_bank_account(req, res) {
    const token = req.swagger.params.token.value;

    try {
        Logs.create({action: 'get_user_bank_account', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_bank_account, is_allowed_access_token_static: false});
        let args = {};
        args.user_id = auth_user.dataValues.user_id;

        let response = await service_bank_account.getBankAccountOnly(args);
        for (let i= 0; i < response.length;i++){
            response[i].dataValues.icon = response[i].bank.icon
        }
        res.json(response);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal mendapatkan data akun bank, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function add_bank_account(req, res) {
    const token = req.swagger.params.token.value;
    const param_name = req.swagger.params.name;
    const param_no_rekening = req.swagger.params.no_rekening;
    const param_bank_id = req.swagger.params.bank_id;

    let name = _.isUndefined(param_name.value)?'':param_name.value;
    let no_rekening = _.isUndefined(param_no_rekening.value)?'':param_no_rekening.value;
    let bank_id = _.isUndefined(param_bank_id.value)?'':param_bank_id.value;

    try {
        Logs.create({action: 'add_bank_account', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_bank_account, is_allowed_access_token_static: false});

        let response = await service_bank.getOne({id: bank_id});

        if(response == null) {
            throw new CustomError(400, 'Data bank tidak di temukan');
        }

        let bank_account_response = await service_bank_account.getOne({user_id: auth_user.dataValues.user_id, no_rekening: no_rekening, bank_id: bank_id});
        
        if(bank_account_response == null) {
            let args = {};
            args.user_id = auth_user.dataValues.user_id;
            args.no_rekening = no_rekening;
            args.bank_id = bank_id;
            args.name = name;
            
            let result_bank_account = await service_bank_account.createBankAccount(args);
            let bank_account = await service_bank_account.getOneBankAccountAfterInsert({id: result_bank_account.id});
            bank_account.dataValues.icon = bank_account.dataValues.bank.icon
            delete bank_account.dataValues.new_user;
            
            res.json(helper.customSuccessResponse(new CustomSuccess(200, 'Data berhasil di tambahkan'), 'account', bank_account));
        } else {
            res.json(helper.customSuccessResponse(new CustomSuccess(200, 'Data berhasil di tambahkan'), 'account', {}));
        }

    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal menambahkan data akun bank, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function update_bank_account(req, res) {
    const token = req.swagger.params.token.value;
    const id_bank_account = req.swagger.params.id_bank_account.value;
    const param_name = req.swagger.params.name;
    const param_no_rekening = req.swagger.params.no_rekening;
    const param_bank_id = req.swagger.params.bank_id;

    let name = _.isUndefined(param_name.value)?'':param_name.value;
    let no_rekening = _.isUndefined(param_no_rekening.value)?'':param_no_rekening.value;
    let bank_id = _.isUndefined(param_bank_id.value)?'':param_bank_id.value;

    try {
        Logs.create({action: 'update_bank_account', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_bank_account, is_allowed_access_token_static: false});
        let response = await service_bank.getOne({id: bank_id});

        if(response == null) {
            throw new CustomError(400, 'Data bank tidak di temukan');
        }
        
        let args = {};
        args.id_bank_account = id_bank_account;
        args.user_id = auth_user.dataValues.user_id;
        args.name = name;
        args.no_rekening = no_rekening;
        args.bank_id = bank_id;

        await service_bank_account.updateBankAccount(args);
        response = await service_bank_account.getOneBankAccountAfterInsert({id: id_bank_account});
        response.dataValues.icon = response.dataValues.bank.icon;
        if(response == null) {
            response = {};
        } else {
            delete response.dataValues.new_user;
        }        

        res.json(helper.customSuccessResponse(new CustomSuccess(200, 'Data berhasil di ubah'), 'account', response));
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal mengubah data akun bank, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function update_bank_account_android(req, res) {
    const token = req.swagger.params.token.value;
    const id_bank_account = req.swagger.params.id_bank_account.value;
    const param_no_rekening = req.swagger.params.no_rekening;

    let no_rekening = _.isUndefined(param_no_rekening.value)?'':param_no_rekening.value;

    try {
        Logs.create({action: 'update_bank_account_android', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_bank_account, is_allowed_access_token_static: false});
        let args_check_bank_account = {};
        args_check_bank_account.accountnumber = no_rekening;
        args_check_bank_account.usertype = auth_user.dataValues.type;

        if(no_rekening == null || no_rekening == '') 
        {
            throw new CustomError(400, 'Nomor rekening tidak boleh kosong');
        }

        let api_response = await MoneyAccess.account_inquiry_name(args_check_bank_account);

        Logs.create({action: 'api response money access update_bank_account_android account_inquiry_name', description: util.inspect(api_response)});

        let args = {};
        args.id_bank_account = id_bank_account;
        args.user_id = auth_user.dataValues.user_id;
        args.no_rekening = no_rekening;

        service_bank_account.updateBankAccountAndroid(args);

        res.json(new CustomSuccess(200, 'Data berhasil di ubah'));
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal mengubah data akun bank, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function destroy_bank_account(req, res) {
    const token = req.swagger.params.token.value;
    const id_bank_account = req.swagger.params.id_bank_account.value;

    try {
        Logs.create({action: 'destroy_bank_account', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_bank_account, is_allowed_access_token_static: false});
        let args = {};
        args.id_bank_account = id_bank_account;
        args.user_id = auth_user.dataValues.user_id;

        service_bank_account.destroyBankAccount(args);

        res.json(helper.customSuccessResponse(new CustomSuccess(200, 'Data berhasil di hapus'), 'id_bank_account', Number(id_bank_account)));
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal menghapus data akun bank, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

export { get_user_bank_account }
export { add_bank_account }
export { update_bank_account }
export { update_bank_account_android }
export { destroy_bank_account }