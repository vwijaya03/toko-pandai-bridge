import { orm } from '~/services/mariadb'
import { Vouchers } from '~/orm/index'

import CustomError from '~/response/error/CustomError'

async function getOne({id=null}={}) {
	let response = await orm.query('select * from vouchers where vouchers.id = '+id, { type: orm.QueryTypes.SELECT});
    if (response == null || response == undefined) {
        return {};
    } else {
    	return response[0];
    }
}

async function get(filter, options) {
    options = options || {};
    let result_set;

    if (filter) {
        result_set = await Vouchers.findAll({where: filter});
    } else {
        result_set = await Vouchers.findAll();
    }

    if (!result_set || Object.keys(result_set).length == 0) {
        if (options.throwIfEmpty) {
            throw new CustomError(400, 'Data vouchers tidak di temukan');
        }
    }
    return result_set;
}

let obj = {}
obj.get = get
obj.getOne = getOne

export default obj