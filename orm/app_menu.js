import { orm } from '~/services/mariadb'
import Sequelize from 'sequelize'


const App_menu = orm.define('app_menu', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    type : Sequelize.STRING,
    title : Sequelize.STRING,
    inquiry : Sequelize.BOOLEAN,
    hint :Sequelize.STRING,
    enabled : Sequelize.BOOLEAN,
    scope : Sequelize.ENUM('kios','other'),
    role : Sequelize.STRING,
    icon : Sequelize.STRING,
}, {
    timestamps: false,
    freezeTableName: true,

    // define the table's name
    tableName: 'app_menu'
});

export default App_menu
