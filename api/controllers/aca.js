'use strict';
import _ from 'lodash'
import shortid from 'shortid'
import moment, { now } from 'moment'
import request from 'request'
import get from 'simple-get'
import fs from 'fs'
import path from 'path'
import base64 from 'base64topdf'
import { Logs } from '~/orm/index'

import api_backend from '~/external_api/api_backend'

import middleware from '~/api/controllers/auth'
import service_aca from '~/services/aca'
import service_user from '~/services/user'
import helper from '~/utils/helper'

import CustomError from '~/response/error/CustomError'
import { callbackify } from 'util';
import { resolve } from 'path';
import { rejects } from 'assert';

const acl_aca= ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];

const url_payment = 'http://192.168.12.2/api/v1/multi_vacc/trf'
const url_check_saldo = 'http://192.168.12.2/api/v1/vacc/inq'
// const url_api_aca = 'http://182.23.65.68/TestingACAMicroIntegration/api/ACAMicro/PostPolicy'
const url_api_aca = 'http://182.23.65.68/ACAMicroIntegration/api/ACAMicro/PostPolicy'
const url_aca = 'http://192.168.12.6:21002';


async function checkSaldoVa(req,res){
	const token = req.swagger.params.token.value;
	try{
            let auth_user = await middleware.verify({token: token, acl: acl_aca, is_allowed_access_token_static: false});
            request({
                    headers: {
                            'apigatekey': 'webKEY123456789'
                    },
                    url: url_check_saldo,
                    method: "POST",
                    formData: {
                            'vaccount' : auth_user.dataValues.valdo_account
                    }
            }, function (error, result, body){
                    let respon = JSON.parse(body)
                    let out = {}
                    out.data = respon.result
                    res.status(200).json(out);
            })
	}catch(err){
        if(err.code === undefined) {
                err.code = 400;
                err.message = 'Gagal : '+err;
        }
   err.message = (err.message === undefined) ? err : err.message;
       res.status(400).json({code: 400, message: err.message});
	}
}

function send_certificate(item){
	let PIN = item.PIN
	let path_dir = __dirname+'/../../static_file/aca_images/certificate/';
        
	try{
		request({
			headers:{},
			// url: "http://182.23.65.68/TestingACAMicroIntegration/api/ACAMicro/ReportPrintCertificate?pin="+PIN+"&SendEmail=false",
            url: "http://182.23.65.68/ACAMicroIntegration/api/ACAMicro/ReportPrintCertificate?pin="+PIN+"&SendEmail=false",
			method: "GET"
		}, function (error, result, body){
			let certificate = JSON.parse(body)
			fs.writeFile(path_dir+'CRT'+PIN+'.pdf', certificate.PdfBase64String, {encoding: 'base64'}, function(err) {
			});
		})
		service_aca.saveCertificate({certificate:'/aca_images/certificate/CRT'+PIN+'.pdf',PIN:PIN})
	}catch(err){
		return err
	}
}

async function createACACustomer(req,res){
	const token = req.swagger.params.token.value;
	const pin = req.swagger.params.pin.value;
	const Reff0 =  _.isUndefined(req.swagger.params.Reff0.value)?'':req.swagger.params.Reff0.value ;
	const Reff1  = _.isUndefined(req.swagger.params.Reff1.value)?'':req.swagger.params.Reff1.value ;
	const Reff2  = _.isUndefined(req.swagger.params.Reff2.value)?'':req.swagger.params.Reff2.value ;
	const Reff3  = _.isUndefined(req.swagger.params.Reff3.value)?'':req.swagger.params.Reff3.value ;
	const Reff4  = _.isUndefined(req.swagger.params.Reff4.value)?'':req.swagger.params.Reff4.value ;
	const Reff5  = _.isUndefined(req.swagger.params.Reff5.value)?'':req.swagger.params.Reff5.value ;
	const Reff6  = _.isUndefined(req.swagger.params.Reff6.value)?'':req.swagger.params.Reff6.value ;
	const Reff7  = _.isUndefined(req.swagger.params.Reff7.value)?'':req.swagger.params.Reff7.value ;
	const image = req.swagger.params.image.value;

	let id_KdProduk = req.swagger.params.id_KdProduk.value;
	let Nama = req.swagger.params.Nama.value;
	let KTP = req.swagger.params.KTP.value;
	let MSISDN = req.swagger.params.MSISDN.value;
	let Alamat = req.swagger.params.Alamat.value;
	let kodePos = req.swagger.params.kodePos.value;
	let tglLahir = req.swagger.params.tglLahir.value;
	let AhliWaris = req.swagger.params.AhliWaris.value;
	let hubunganPeserta = req.swagger.params.hubunganPeserta.value;
        let email_retailer = req.swagger.params.email_retailer.value;
	let Email = req.swagger.params.Email.value;
       
	try {
        let auth_user = await middleware.verify({token: token, acl: acl_aca, is_allowed_access_token_static: false});
	let actual_pin = await service_user.getHashedPassword(auth_user.dataValues.salt, pin);
        let expected_pin = auth_user.dataValues.pin;
        if (expected_pin != actual_pin) {
            throw new CustomError(400, 'PIN salah');
        }
		request({
			headers: {
				'token': token,
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			url: url_aca+'/insurance/create-new',
			method: "POST",
            form: {
			   "pin": pin,
			   "id_KdProduk": id_KdProduk,
			   "Nama": Nama,
			   "KTP": KTP,
			   "MSISDN": MSISDN,
			   "Alamat": Alamat,
			   "kodePos": kodePos,
			   "tglLahir": tglLahir,
			   "AhliWaris": AhliWaris,
			   "hubunganPeserta": hubunganPeserta,
			   "email_retailer": email_retailer,
			   "Email": Email,
			   "Reff0": Reff0,
			   "Reff1": Reff1,
			   "Reff2": Reff2,
			   "Reff3": Reff3,
			   "Reff4": Reff4,
			   "Reff5": Reff5,
			   "Reff6": Reff6,
			   "Reff7": Reff7,
			   "image": image
            }
		}, function (error, result, body){
            let response = JSON.parse(body);
			res.json({code:response.code, message:response.message})
		})
	} catch(err) {
		if(err.code === undefined) {
			 err.code = 400;
			 err.message = 'Insert Gagal : '+err;
		}
		err.message = (err.message === undefined) ? err : err.message;
        res.status(400).json({code: 400, message: err.message});
	}
}

async function getAllInsuranceData(req,res){
	
	const token = req.swagger.params.token.value;
	const page = _.isUndefined(req.swagger.params.page.value)?0:req.swagger.params.page.value ;
	const items_per_page = _.isUndefined(req.swagger.params.items_per_page.value)?15:req.swagger.params.items_per_page.value ;
	const param_sorting = req.swagger.params.sorting;
        const param_sortingBy = req.swagger.params.sortingBy;
	const param_pin = req.swagger.params.PIN;

	let PIN = _.isUndefined(param_pin.value) ? null : param_pin.value;
	let sorting = _.isUndefined(param_sorting.value) ? null : param_sorting.value;
	let sortingBy = _.isUndefined(param_sortingBy.value) ? null : param_sortingBy.value;
	try {
        Logs.create({action: 'get_all_customer_detail', description: helper.getSimpleSwaggerParams(req.swagger.params)});
        let api_response = await api_backend.getAllInsuranceData({page:page,items_per_page:items_per_page,token:token});
        res.json(api_response);
	} catch(err) {
		if(err.code === undefined) {
			err.code = 400;
			err.message = 'Gagal :'+err;
		}
		err.message = (err.message === undefined) ? err : err.message;
		res.status(400).json({code: 400, message: err.message});
	}
    }

async function resendCertificate(req,res){
	const token = req.swagger.params.token.value;
	const PIN = parseInt(req.swagger.params.PIN.value);
	let path_dir = '/aca_images/certificate/';

	try{
		Logs.create({action: 'resend_aca_certificate', description: "token: "+token+", PIN: "+PIN, created_at: Date.now(),updated_at: Date.now()});

		request({
			headers:{},
			// url: "http://182.23.65.68/TestingACAMicroIntegration/api/ACAMicro/ReportPrintCertificate?pin="+PIN+"&SendEmail=true",
            url: "http://182.23.65.68/ACAMicroIntegration/api/ACAMicro/ReportPrintCertificate?pin="+PIN+"&SendEmail=true",
			method: "GET"
		}, function (error, result, body){
			let certificate = JSON.parse(body)
			fs.writeFile(path_dir+'CRT'+PIN+'.pdf', certificate.PdfBase64String, {encoding: 'base64'}, function(err) {
			});
		})
		service_aca.saveCertificate({certificate:path_dir+'CRT'+PIN+'.pdf',PIN:PIN}) 
		res.status(200).json({code: 200, status: 'success'});
	}catch(err){
		if(err.code === undefined) {
			err.code = 400;
			err.message = 'Gagal : '+err;
	   	}
	   err.message = (err.message === undefined) ? err : err.message;
	   res.status(400).json({code: 400, message: err.message});
	}
}

async function fileDownload(req,res){
	const token = req.swagger.params.token.value;
	const PIN = parseInt(req.swagger.params.PIN.value);
	let path_dir = 'aca_images/certificate/';

	try{
		Logs.create({action: 'download_certificate', description: "token: "+token+", PIN: "+PIN});

		var filePath = path_dir+'CRT'+PIN+'.pdf'
    	var fileName = 'CRT'+PIN+'.pdf'

    	res.download(filePath, fileName);  
	}catch(err){
		if(err.code === undefined) {
			err.code = 400;
			err.message = 'Gagal : '+err;
	   	}
	   err.message = (err.message === undefined) ? err : err.message;
	   res.status(400).json({code: 400, message: err.message});
	}
}

export { checkSaldoVa }
export { createACACustomer }
export { getAllInsuranceData }
export { resendCertificate }
export { fileDownload }
