import moment from 'moment'

import { orm } from '~/services/mariadb'
import { FMCG } from '~/orm/index'
import { UserUniqueCode } from '~/orm/index'
import { Op } from '~/utils/helper'

import FmcgNotFoundError from '~/response/error/FmcgNotFoundError'

function add({name=null, limit_unique_code=null, url=null, fmcg_token=null, fmcg_icon=null}={}) {
    let args = {};
    args.name = name;
    args.limit_unique_code = limit_unique_code;
    args.url = url;
    args.fmcg_token = fmcg_token;
    args.fmcg_icon = "/fmcg-icon/"+fmcg_icon;
    args.createdAt = moment().format('YYYY-MM-DD HH:mm:ss');
    args.is_topex = 0;

    return FMCG.create(args);
}

async function get(filter, options) {
    options = options || {};
    let result_set = '';

    if(filter) {
        result_set = await FMCG.findAll({
            attributes: ['id', 'name', ['limit_unique_code', 'limit'], 'fmcg_icon'],
            include: [{
                model: UserUniqueCode,
                required: true,

            }],
            where: filter
        });
    } else {
        result_set = await FMCG.findAll({
            include: [{
                model: UserUniqueCode,
                required: true,

            }]
        });
    }

    if (!result_set || Object.keys(result_set).length == 0) {
        if (options.throwIfEmpty) {
            throw new FmcgNotFoundError();
        }
    }

    return result_set;
}

async function getOne({id=null}={}) {
    return FMCG.findOne({
        attributes: ['id', 'name', ['limit_unique_code', 'limit'], 'fmcg_icon'],
        where: {id: id, delete: 0}
    });
}

async function getOneFmcgWithFilter(filter, options) {

    options = options || {};
    let result_set = '';

    result_set = await FMCG.findOne({
        where: filter
    });

    if (!result_set || Object.keys(result_set).length == 0) {
        if (options.throwIfEmpty) {
            throw new UserNotFoundError();
        }
    }

    return result_set;
}

async function getFmcgOnly(filter, options) {
    options = options || {};
    let result_set = '';

    if(filter) {
        result_set = await FMCG.findAll({
            attributes: ['id', 'name', ['limit_unique_code', 'limit'], 'fmcg_icon'],
            where: filter
        });
    } else {
        result_set = await FMCG.findAll({
            attributes: ['id', 'name', ['limit_unique_code', 'limit'], 'fmcg_icon']
        });
    }

    if (!result_set || Object.keys(result_set).length == 0) {
        if (options.throwIfEmpty) {
            throw new FmcgNotFoundError();
        }
    }

    return result_set;
}

function getAvailableFmcgToUser({id=null}={}) {
    let whereQuery = `where id not in (${id})`;

    if(id.length == 0) {
        whereQuery = '';
    }

    return orm.query(`
    select id, name, limit_unique_code as 'limit' from fmcgs ${whereQuery}
    `, { type: orm.QueryTypes.SELECT});
}

let obj = {};
obj.get = get;
obj.getOne = getOne;
obj.getOneFmcgWithFilter = getOneFmcgWithFilter;
obj.add = add;
obj.getFmcgOnly = getFmcgOnly;
obj.getAvailableFmcgToUser = getAvailableFmcgToUser;

export default obj;