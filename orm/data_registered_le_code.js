import { orm } from '~/services/mariadb'
import Sequelize from 'sequelize'

const RegisteredLeCode = orm.define('data_registered_le_codes', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    fmcg_id: {
        type: Sequelize.STRING, allowNull:false
    },
    user_id: {
        type: Sequelize.STRING, allowNull:false
    },
    le_code: {
        type: Sequelize.STRING, allowNull:false
    },
    jenis: {
        type: Sequelize.STRING, allowNull:false
    },
    retailer: {
        type: Sequelize.STRING, allowNull:false
    },
    address: {
        type: Sequelize.STRING, allowNull:false
    },
    dt_code: {
        type: Sequelize.STRING, allowNull:false
    },
    createdAt: {
        type: 'TIMESTAMP', allowNull:false
    },
    updatedAt: {
        type: 'TIMESTAMP', allowNull:false
    }
});

export default RegisteredLeCode