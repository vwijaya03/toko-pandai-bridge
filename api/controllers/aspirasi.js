'use strict';
import _ from 'lodash'
import util from 'util'
import middleware from '~/api/controllers/auth'
import moment from 'moment'
import request from 'request'

import service_fmcg from '~/services/fmcg'
import service_user from '~/services/user'
import service_pelunasan_manual from '~/services/pelunasan_manual'
import service_user_unique_code from '~/services/user_unique_code'
import helper from '~/utils/helper'

import ApiTop from '~/external_api/api_top'
import ApiFmcg from '~/external_api/api_uli'
import ApiPohonDana from '~/external_api/api_top_pohon_dana'
import ApiAspirasi from '~/external_api/api_top_aspirasi'
import MoneyAccess from '~/external_api/moneyaccess'
import api_backend from '~/external_api/api_backend'

let apigatekey= 'webKEY123456789';
let url_trf = '192.168.12.2';
let aspirasi_url = "http://192.168.12.7:10020";
let multifintech_url = "http://192.168.12.6:21001"


import { Logs } from '~/orm/index'

import CustomError from '~/response/error/CustomError'

const acl_calculator = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];
const acl_apply_loan = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];
const acl_get_invoices = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];
const acl_repayment = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];


async function calculator(req, res) {
    const token = req.swagger.params.token.value;
    const param_amount = req.swagger.params.amount;
    const param_discount = req.swagger.params.discount;
    const param_tenor = req.swagger.params.tenor;
    const param_admin = req.swagger.params.admin;
    const iso8601format = "YYYY-MM-DD";

    let amount = _.isUndefined(param_amount.value) ? 0 : param_amount.value;
    let discount = _.isUndefined(param_discount.value) ? 0 : 0;
    let tenor = _.isUndefined(param_tenor.value) ? 0 : param_tenor.value;
    let admin = _.isUndefined(param_admin.value) ? 0 : param_admin.value;
    let data_unique_code = '';
    let fmcg_top = 23;
    
    
    try {
        Logs.create({action: 'calculator top', description: helper.getSimpleSwaggerParams(req.swagger.params)});
             
        let auth_user = await middleware.verify({token: token, acl: acl_calculator, is_allowed_access_token_static: false});
        let result_fmcg_top = await service_fmcg.getOneFmcgWithFilter({id: fmcg_top});   
        
        let check_user_le_code = await service_user_unique_code.get({user_id:auth_user.dataValues.user_id,fmcg_id:1})
        
        if(check_user_le_code.length == 0){
            throw new CustomError(400, 'Tambahkan icon limit pandai')
        }
        let invoice_args = {
            url: result_fmcg_top.dataValues.url,
            fmcg_token: result_fmcg_top.dataValues.fmcg_token,
            fmcg_name: result_fmcg_top.dataValues.name,
            amount: amount,
            tenor: tenor,
            discount: discount,
            admin: admin
        }
        let calculator = await ApiAspirasi.calculator(invoice_args);
        let week = {};
        if (calculator.status === 'success') {

//            let potongan = parseInt((amount/100)/4*tenor*0.25);
            let potongan = 0;
                if((((calculator["data"].loan_payback)-potongan)/calculator["data"].tenor % 1) !== 0)
                {
                    admin= parseInt((parseInt(calculator["data"].loan_payback)-potongan)/calculator["data"].tenor)+1;
                }
                else{
                    admin= parseInt((parseInt(calculator["data"].loan_payback)-potongan)/calculator["data"].tenor);
                }
                week = [{
                    "loan_amount": parseInt(calculator["data"].amount)+".0000",
                    "loan_payback": parseInt((calculator["data"].loan_payback)-potongan)+".0000",
                    "administrasi": parseInt(calculator["data"].administrasi)+".0000",
                    "loan_length": parseInt(calculator["data"].tenor),
                    "installment": parseInt((calculator["data"].installment)-(potongan/calculator["data"].tenor))+".0000",
                    "total_potongan": potongan+".0000",
               }]
        }else{
            week = calculator
        }
        let weeks = {}
        weeks.status = calculator.status;
        weeks.message = calculator.message;
        weeks.week = week;
    
        res.status(200).json(weeks)
        
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data calculator TOP, '+err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function apply_loan(req, res) {
    const token = req.swagger.params.token.value;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_le_code = req.swagger.params.le_code;
    const param_dt_code = req.swagger.params.dt_code;
    const param_amount = req.swagger.params.amount;
    const param_invoice_id = req.swagger.params.invoice_id;
    const param_tenor = req.swagger.params.tenor;
    const param_principal_code = req.swagger.params.principal_code;
    const param_total_potongan = req.swagger.params.total_potongan;
    const param_discount = req.swagger.params.discount;
    const param_admin = req.swagger.params.admin;
    const param_denda = req.swagger.params.denda;
    const param_pin = req.swagger.params.pin;


    const iso8601format = "YYYY-MM-DD";
    
    let pin_user = _.isUndefined(param_pin.value)?null:param_pin.value;
    let fmcg_id = _.isUndefined(param_fmcg_id.value)?null:param_fmcg_id.value;
    let dt_code = _.isUndefined(param_dt_code.value) ? null : param_dt_code.value;
    let le_code = _.isUndefined(param_le_code.value) ? null : param_le_code.value;
    let amount = _.isUndefined(param_amount.value) ? null : param_amount.value;
    let invoice_number = _.isUndefined(param_invoice_id.value) ? null : param_invoice_id.value;
    let tenor = _.isUndefined(param_tenor.value) ? null : param_tenor.value;
    let principal_code = _.isUndefined(param_principal_code.value) ? null : param_principal_code.value;
//    let total_potongan = _.isUndefined(param_total_potongan.value) ? null : param_total_potongan.value;
    let total_potongan = 0;
    let discount = _.isUndefined(param_discount.value) ? 0 : parseFloat(param_discount.value);
    let admin = _.isUndefined(param_admin.value) ? 0 : parseFloat(param_admin.value);
    let denda = _.isUndefined(param_denda.value) ? 0 : parseFloat(param_denda.value);
    let data_unique_code = '';

    try {
        Logs.create({action: 'apply_loan', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_apply_loan, is_allowed_access_token_static: false});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});

        if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal apply loan, data fmcg  tidak tersedia');
        }
        
        let result_fmcg_top = await service_fmcg.getOneFmcgWithFilter({id: 23});

        if(result_fmcg_top == null) {
            throw new CustomError(400, 'Gagal apply loan, data fintech tidak tersedia');
        }
        
        if(invoice_number.slice(-2) == "_R") {
            throw new CustomError(400, 'Invoice Rokok Tidak bisa memakai Limit');
        }
        
        let user_salt = auth_user.dataValues.salt;
        
        let newpin = await service_user.getHashedPassword(user_salt, pin_user.toString());
        if(auth_user.dataValues.pin != newpin) {
            throw new CustomError(400, 'Pin Salah');
        }

        let api_transact = await api_backend.cek_transactions({dt_code:dt_code,invoice_id:invoice_number,fmcg_id:fmcg_id});

        if (api_transact.totalTransaksi !== 0){
          throw new CustomError(400, 'Pengajuan sebagian ditolak');
        }

        if (amount < 100000){
            throw new CustomError(400, 'Invoice Minimal Rp. 100.000');
        }

        let check_loan = await ApiAspirasi.get_loan({
            url: result_fmcg_top.dataValues.url,
            fmcg_token : result_fmcg_top.dataValues.fmcg_token,
            fmcg_name : result_fmcg_top.dataValues.name,
            dt_code:dt_code,
            le_code:le_code,
            invoice_id:invoice_number});
        if(check_loan.length == 0){
            
            let args ={
                "le_code": le_code,
                "dt_code": dt_code,
                "invoice_number": invoice_number,
                "amount" : amount,
                "tenor":tenor,
                "discount": discount,
                "denda": denda,
                "admin": admin
            };
        
            request({
            headers: {
            'Content-Type': 'application/json'
            },
                url: aspirasi_url+"/pinjaman",
                method: "POST",
                json: true,
                body: args
            }, function (error, response, body){
                let loan_response = body;

                if(loan_response.status === "failed"){
                    res.status(400).json({code:400, message:body.message})
                }
                else if(loan_response.status === "success"){
                    let argss = {
                        url: result_fmcg_top.dataValues.url,
                        fmcg_token : result_fmcg_top.dataValues.fmcg_token,
                        fmcg_name : result_fmcg_top.dataValues.name,
                        user_id:auth_user.dataValues.user_id,
                        le_code:le_code,
                        dt_code:dt_code,
                        amount:amount,
                        total_amount_invoice:parseInt(amount),
                        invoice_id:invoice_number,
                        tenor:tenor,
                        loan_type:"Credit",
                        loan_by:"Aspirasi",
                        principal_code:fmcg_id,
                        total_potongan:total_potongan,
                        discount:discount,
                        admin:admin,
                        denda:denda
                      }
                      ApiAspirasi.insert_loan_history(argss)

                      let invoice_update = {};
                       invoice_update.le_code = le_code;
                       invoice_update.invoice_id = invoice_number;
                       invoice_update.amount = amount;
                       invoice_update.fmcg_id = fmcg_id;
                       invoice_update.paid_by = "Limit Pandai"

                       ApiPohonDana.update_payment_fmcg(invoice_update);

                       request({
                        headers: {
                            token : '2b3e6865-bdae-4881-ba71-ea9c5f123e8d'
                        },
                            url: multifintech_url+"/invoice/update-payment-fmcg",
                            method: "POST",
                            json: true,
                            body: invoice_update
                        }, function (error, response, body){
                        })
                       
                      res.status(200).json(loan_response);
                }
            })
        }else{
            res.status(400).json({code:400, message:'Pengajuan Limit Pandai sudah pernah dilakukan'})
        }
        
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan update invoice, '+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}



async function repayment(req, res) {
    const token = req.swagger.params.token.value;
    const pin = req.swagger.params.pin.value;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_tenor = req.swagger.params.tenor;
    const param_amount = req.swagger.params.amount;
    const param_invoice_number = req.swagger.params.invoice_id;
    const param_le_code = req.swagger.params.le_code;
    const param_dt_code = req.swagger.params.dt_code;
    const param_loan_id = req.swagger.params.loan_id;

    const iso8601format = "YYYY-MM-DD";

    let fmcg_id = _.isUndefined(param_fmcg_id.value)?null:param_fmcg_id.value;
    let amount = _.isUndefined(param_amount.value) ? null : param_amount.value;
    let tenor = _.isUndefined(param_tenor.value) ? null : param_tenor.value;
    let invoice_number = _.isUndefined(param_invoice_number.value) ? null : param_invoice_number.value;
    let le_code = _.isUndefined(param_le_code.value) ? null : param_le_code.value;
    let dt_code = _.isUndefined(param_dt_code.value) ? null : param_dt_code.value;
    let loan_id = _.isUndefined(param_loan_id.value) ? null : param_loan_id.value;

    let data_unique_code = '';
    let tanggal_mutasi = '';
    let vacc_aspirasi = '21000000602';
    let vacc_topan = '2010001162';
    let fmcg_top = 23;
    let out = 0;
    let index
    
    try {

        let auth_user = await middleware.verify({token: token, acl: acl_repayment, is_allowed_access_token_static: false});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
        if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal menampilkan data invoice, data fmcg tidak tersedia');
        }
        let result_fmcg_top = await service_fmcg.getOneFmcgWithFilter({id: fmcg_top});
        if(result_fmcg_top == null) {
            throw new CustomError(400, 'Gagal menampilkan data invoice, data fmcg tidak tersedia');
        }

        let actual_pin = await service_user.getHashedPassword(auth_user.dataValues.salt, pin);
        let expected_pin = auth_user.dataValues.pin;
        if (expected_pin != actual_pin) {
            throw new CustomError(400, 'PIN salah');
        }
        let vacc_toko = auth_user.dataValues.valdo_account
        let nama_toko = auth_user.dataValues.fullname

        let inv = {}
        inv.url = result_fmcg_top.dataValues.url;
        inv.fmcg_token = result_fmcg_top.dataValues.fmcg_token;
        inv.fmcg_name = result_fmcg_top.dataValues.name;
        inv.invoice_number = invoice_number;
        inv.invoice_payment_status = 'unpaid';

        let args ={}
        args.url = result_fmcg.dataValues.url;
        args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        args.fmcg_name = result_fmcg.dataValues.name;
        args.invoice_id = invoice_number;
        args.dt_code = dt_code

       
        let distributor = await service_pelunasan_manual.get_dt_unicode({dt_code:dt_code})
        let retailer = await service_pelunasan_manual.get_le_unicode({le_code:le_code})         
        let api_response = await MoneyAccess.inquiry({mobnum: vacc_toko, vaccount: vacc_toko, usertype: auth_user.dataValues.type});
        out = api_response;
        if(out.balance < amount){
              throw new CustomError(400, 'Saldo tidak cukup');
        }

        let get_top_invoice = await ApiAspirasi.get_all_tenor(inv)
        if(get_top_invoice["data"].length === 0){
            throw new CustomError(400,'Invoice tidak ditemukan atau sudah dibayar')
        }
        let unpaid_invoices = await get_top_invoice["data"].filter(function(item){
            return item.invoice_payment_status == "unpaid"
        })

        for(let i=0;i < unpaid_invoices.length;i++){
            if(Number(tenor) > Number(unpaid_invoices[i]["cash_memo_doc_no"].split("-")[1])){
                res.status(400).json({code:400,message:'Invoice cicilan ke - '+unpaid_invoices[i]["cash_memo_doc_no"].split("-")[1]+' Anda belum dibayar'}) 
            }
            else if(Number(tenor) === Number(unpaid_invoices[i]["cash_memo_doc_no"].split("-")[1])){
                if(get_top_invoice["data"].length === 0){
                    throw new CustomError(400,'Invoice tidak ditemukan atau sudah dibayar')
                }
            }

            if(unpaid_invoices[i].loan_id === loan_id){
                index = i
            }
        }
        let get_tenor = get_top_invoice.data[0].cash_memo_doc_no.split('-')
        
        let tenor = get_tenor[1]
        var ten = tenor.split(" ", 1);
       

        let loan_history = await ApiAspirasi.get_loan({
            url: result_fmcg_top.dataValues.url,
            fmcg_token : result_fmcg_top.dataValues.fmcg_token,
            fmcg_name : result_fmcg_top.dataValues.name,
            dt_code:dt_code,
            le_code:le_code,
            invoice_id:invoice_number});

        if(unpaid_invoices[index].overdue > 0){
            tanggal_mutasi = moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
        }
        else if(loan_history[0].tenor == 1 && unpaid_invoices[index].overdue <= 0){
           tanggal_mutasi = moment(new Date(loan_history[0].createdAt).setDate(new Date(loan_history[0].createdAt).getDate() + 7)).format('YYYY-MM-DD HH:mm:ss')
        }
        else if(loan_history[0].tenor == 2 && unpaid_invoices[index].overdue <= 0){
            tanggal_mutasi = moment(new Date(loan_history[0].createdAt).setDate(new Date(loan_history[0].createdAt).getDate() + 14)).format('YYYY-MM-DD HH:mm:ss')
        }
        else if(loan_history[0].tenor == 3 && unpaid_invoices[index].overdue <= 0){
            tanggal_mutasi = moment(new Date(loan_history[0].createdAt).setDate(new Date(loan_history[0].createdAt).getDate() + 21)).format('YYYY-MM-DD HH:mm:ss')
        }
        else if(loan_history[0].tenor == 4 && unpaid_invoices[index].overdue <= 0){
            tanggal_mutasi = moment(new Date(loan_history[0].createdAt).setDate(new Date(loan_history[0].createdAt).getDate() + 28)).format('YYYY-MM-DD HH:mm:ss')
        }

        if(Object.keys(get_top_invoice).length !== 0){
           
        let payment = {
            trans_type:"transfer",
            status:"success", 
                vacc_from: vacc_toko,
            vacc_to:[
                {vacc_number:vacc_aspirasi, amount: amount, desc:`Pembayaran invoice ${invoice_number}-${parseInt(tenor)} Rp. ${helper.IDRformat(amount)} dari ${nama_toko}-${le_code} (${vacc_toko}) ke Limit Pandai (${vacc_aspirasi})`}
            ],
            pin:pin,
            tanggal_mutasi: tanggal_mutasi
        }
            request({
                headers: {
                    'apigatekey': apigatekey,
                    'Content-Type': 'application/json'
                },
                url: "http://192.168.12.2/api/v1/multi_vacc_v2/trf",
                method: "POST",
                json: true,   
                body: payment
            }, async function (error, result, body){
                let top_money_access = body.result
                let top_money_access_response = {
                    code: body.status.code,
                    desc: body.result.desc
                }
                                               
                Logs.create({action: `api money access TOP`, description: 'response'+top_money_access});
                if(body.status.code === 200){

                let args = {
                    "le_code": le_code,
                    "dt_code": dt_code,
                    "invoice_number": invoice_number,
                    "cicilan_ke": tenor,
                    "amount": amount,
                    "loan_id":loan_id
                }
                request({
                headers: {
                    'Content-Type': 'application/json'                                   
                },
                url: "http://192.168.12.7:10020/repayment",
                method: "POST",
                json: true,
                body: args
                }, async function (error, response, body){
                    let res_repayment = body;
                    if(res_repayment.status !== 'success'){
                        res.status(400).json({code:400, message:res_repayment.message})
                    }
                    else if(body.status === 'success'){
                        let input_to_transactions = {};
                    input_to_transactions.url = result_fmcg_top.dataValues.url;
                    input_to_transactions.fmcg_token = result_fmcg_top.dataValues.fmcg_token;
                    input_to_transactions.fmcg_name = result_fmcg_top.dataValues.name;
                    input_to_transactions.fmcg_id = fmcg_id;
                    input_to_transactions.dt_code = dt_code;
                    input_to_transactions.invoice_id = invoice_number+'-'+tenor;
                    input_to_transactions.amount = amount;
                    input_to_transactions.invoice_code = dt_code+invoice_number+'-'+tenor;
                    input_to_transactions.valdo_tx_id = "TR"+invoice_number+moment().format('YYYYMMDDHHmmss');
                    input_to_transactions.distributor_id = 0;
                    input_to_transactions.retailer_id = 0;  
                    input_to_transactions.from_user_id = auth_user.dataValues.user_id;
                    input_to_transactions.to_user_id = 0;
                    
                    let insert_manual_transaction = await ApiAspirasi.insert_transactions(input_to_transactions)
                    
                    request({
                        headers: {
                            'token': '2b3e6865-bdae-4881-ba71-ea9c5f123e8d'                                  
                        },
                        url: "http://192.168.12.6:21001/invoice/get-updated-invoice",
                        method: "GET",
                        json: true,
                        body: {
                            "invoice_id": invoice_number
                        }
                        }, async function (error, response, body){
                    })

                    let repayment_response = {
                        message: res_repayment.message,
                        status: res_repayment.status
                    }
                    
                    let out = {
                        response : repayment_response,
                        top_money_access_desc : top_money_access_response,
                        update_transaction : insert_manual_transaction
                    } 
                    res.status(200).json({code:200, body:out})
                    }
                    });
                }
            })
    } 
    else{
        throw new CustomError(400, 'Invoice tidak sesuai!')
    }  
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data invoice, '+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}



async function manual_repayment(req, res) {
    const token = req.swagger.params.token.value;
    const param_loan_id = req.swagger.params.loan_id;

    const iso8601format = "YYYY-MM-DD";
    let loan_id = _.isUndefined(param_loan_id.value) ? null : param_loan_id.value;

    let data_unique_code = '';
    let vacc_aspirasi = '21000000602';
    let tanggal_mutasi = ''
    let vacc_topan = '2010001162';
    let fmcg_top = 23;
    let index 
    
    try {

        let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});
  
        let result_fmcg_top = await service_fmcg.getOneFmcgWithFilter({id: fmcg_top});
        if(result_fmcg_top == null) {
            throw new CustomError(400, 'Gagal menampilkan data invoice, data fmcg tidak tersedia');
        }


        let args ={}
        args.url = result_fmcg_top.dataValues.url;
        args.fmcg_token = result_fmcg_top.dataValues.fmcg_token;
        args.fmcg_name = result_fmcg_top.dataValues.name;
        args.loan_id = loan_id

        let invoice = await ApiAspirasi.one_invoice_loanid(args)
        if(invoice[0].invoice_payment_status === "unpaid"){

            let toko = await service_user.getOne({user_id:invoice[0].user_id})
            
            let api_response = await MoneyAccess.inquiry({mobnum: toko.dataValues.valdoAccount, vaccount: toko.dataValues.valdoAccount, usertype: toko.dataValues.type});
   
            if(api_response.balance < invoice[0].cash_memo_balance_amount){
                throw new CustomError(400, 'Saldo tidak cukup');
            }

            let inv = {}
            inv.url = result_fmcg_top.dataValues.url;
            inv.fmcg_token = result_fmcg_top.dataValues.fmcg_token;
            inv.fmcg_name = result_fmcg_top.dataValues.name;
            inv.invoice_number = invoice[0].invoice_id;
            inv.invoice_payment_status = 'unpaid';

            let get_top_invoice = await ApiAspirasi.get_all_tenor(inv)
            
            if(get_top_invoice["data"].length === 0){
                throw new CustomError(400,'Invoice tidak ditemukan atau sudah dibayar')
            }
            
            let unpaid_invoices = await get_top_invoice["data"].filter(function(item){
                return item.invoice_payment_status == "unpaid"
            })

            let tenor = invoice[0].cash_memo_doc_no.split('-')[1];
            
            for(let i=0;i < unpaid_invoices.length;i++){
            if(Number(tenor) > Number(unpaid_invoices[i]["cash_memo_doc_no"].split("-")[1])){
                res.status(400).json({code:400,message:'Invoice cicilan ke - '+unpaid_invoices[i]["cash_memo_doc_no"].split("-")[1]+' Anda belum dibayar'}) 
            }
            else if(Number(tenor) === Number(unpaid_invoices[i]["cash_memo_doc_no"].split("-")[1])){
                if(get_top_invoice["data"].length === 0){
                    throw new CustomError(400,'Invoice tidak ditemukan atau sudah dibayar')
                }
            }

            if(unpaid_invoices[i].loan_id === loan_id){
                index = i
            }
        }
            let loan_history = await ApiAspirasi.get_loan({
                url: result_fmcg_top.dataValues.url,
                fmcg_token : result_fmcg_top.dataValues.fmcg_token,
                fmcg_name : result_fmcg_top.dataValues.name,
                dt_code: invoice[0].dt_code,
                le_code: invoice[0].le_code,
                invoice_id: invoice[0].invoice_id
            });
            if(unpaid_invoices[index].overdue > 0){
                tanggal_mutasi = moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
            }
            else if(loan_history[0].tenor == 1 && unpaid_invoices[index].overdue <= 0){
                tanggal_mutasi = moment(new Date(loan_history[0].createdAt).setDate(new Date(loan_history[0].createdAt).getDate() + 7)).format('YYYY-MM-DD HH:mm:ss')
            }
            else if(loan_history[0].tenor == 2 && unpaid_invoices[index].overdue <= 0){
                tanggal_mutasi = moment(new Date(loan_history[0].createdAt).setDate(new Date(loan_history[0].createdAt).getDate() + 14)).format('YYYY-MM-DD HH:mm:ss')
            }
            else if(loan_history[0].tenor == 3 && unpaid_invoices[index].overdue <= 0){
                tanggal_mutasi = moment(new Date(loan_history[0].createdAt).setDate(new Date(loan_history[0].createdAt).getDate() + 21)).format('YYYY-MM-DD HH:mm:ss')
            }
            else if(loan_history[0].tenor == 4 && unpaid_invoices[index].overdue <= 0){
                tanggal_mutasi = moment(new Date(loan_history[0].createdAt).setDate(new Date(loan_history[0].createdAt).getDate() + 28)).format('YYYY-MM-DD HH:mm:ss')
            }
            if(Object.keys(get_top_invoice).length !== 0){

                let data = {
                    'trans_type':'payment_limit',
                    'dt_code': invoice[0].dt_code,
                    'le_code': invoice[0].le_code,
                    'amount': invoice[0].total_yg_harus_dibayar,
                    'vaccount_to': vacc_aspirasi,
                    'vaccount_from': toko.dataValues.valdoAccount,
                    'transid': "TR"+moment().format('YYYYMMDDHHmmss'),
                    'desc' : `Pembayaran invoice ${invoice[0].cash_memo_doc_no} (${invoice[0].le_code}) Rp. ${helper.IDRformat(invoice[0].total_yg_harus_dibayar)} dari ${toko.dataValues.fullname} (${toko.dataValues.valdoAccount}) ke Limit Pandai (${vacc_aspirasi})`,
                    'tanggal_mutasi': tanggal_mutasi
                }
                request({
                    headers: {
                        'apigatekey': apigatekey
                    },
                    url: 'http://'+url_trf+'/api/v1/admin/trf',
                    method: "POST",
                    json: true,   
                    formData: data
                }, function (error, result, body){
                    let top_money_access = body.result
                    let top_money_access_response = {
                        code: body.status.code,
                        desc: body.result.desc
                    }                               
                    Logs.create({action: `api money access TOP`, description: 'response'+top_money_access});
                    if(body.status.code === 200){
                        let args = {
                            "le_code": invoice[0].le_code,
                            "dt_code": invoice[0].dt_code,
                            "invoice_number": invoice[0].invoice_id,
                            "cicilan_ke": tenor,
                            "amount": invoice[0].total_yg_harus_dibayar,
                            "loan_id":loan_id
                        }
                        request({
                        headers: {
                            'Content-Type': 'application/json'                                   
                        },
                        url: "http://192.168.12.7:10020/repayment",
                        method: "POST",
                        json: true,
                        body: args
                        }, async function (error, response, body){
                            let res_repayment = body
                            
                            if(res_repayment.status !== 'success'){
                                res.status(400).json({code:400, message:res_repayment.message})
                            }
                            else if(body.status === 'success'){
                                let input_to_transactions = {};
                                input_to_transactions.url = result_fmcg_top.dataValues.url;
                                input_to_transactions.fmcg_token = result_fmcg_top.dataValues.fmcg_token;
                                input_to_transactions.fmcg_name = result_fmcg_top.dataValues.name;
                                input_to_transactions.fmcg_id = loan_history[0].principal_code;
                                input_to_transactions.dt_code = invoice[0].dt_code;
                                input_to_transactions.invoice_id = invoice[0].cash_memo_doc_no;
                                input_to_transactions.amount = invoice[0].total_yg_harus_dibayar;
                                input_to_transactions.invoice_code = invoice[0].dt_code+invoice[0].cash_memo_doc_no;
                                input_to_transactions.valdo_tx_id = "TR"+invoice[0].invoice_id+moment().format('YYYYMMDDHHmmss');
                                input_to_transactions.distributor_id = 0;
                                input_to_transactions.retailer_id = 0;  
                                input_to_transactions.from_user_id = invoice[0].user_id;
                                input_to_transactions.to_user_id = 0;
                                
                                let insert_manual_transaction = await ApiAspirasi.insert_transactions(input_to_transactions)
                                      
                                request({
                                    headers: {
                                        'token': '2b3e6865-bdae-4881-ba71-ea9c5f123e8d'                                  
                                    },
                                    url: "http://192.168.12.7:21001/invoice/get-updated-invoice",
                                    method: "GET",
                                    json: true,
                                    body: {
                                        "invoice_id": invoice[0].invoice_id
                                    }
                                        }, async function (error, response, body){
                                })
                  
                                let repayment_response = {
                                    message: res_repayment.message,
                                    status: res_repayment.status
                                }
                                      
                                let out = {
                                    response : repayment_response,
                                    top_money_access_desc : top_money_access_response,
                                    update_transaction : insert_manual_transaction
                                } 
                                res.status(200).json({code:200, body:out})
                            }
                        })
                    }
                })
            }
            
        }else{
            throw new CustomError(400, 'Invoice sudah lunas')
        }

    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data invoice, '+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function payment_distributor(req, res) {
    
    const token = req.swagger.params.token.value;
    const param_le_code = req.swagger.params.le_code;
    const param_dt_code = req.swagger.params.dt_code;
    const param_invoice_no = req.swagger.params.invoice_number;
    const param_principal_code = req.swagger.params.principal_code;

    const iso8601format = "YYYY-MM-DD";

    let dt_code = _.isUndefined(param_dt_code.value) ? null : param_dt_code.value;
    let le_code = _.isUndefined(param_le_code.value) ? null : param_le_code.value;
    let invoice_no = _.isUndefined(param_invoice_no.value) ? null : param_invoice_no.value;
    let principal_code = _.isUndefined(param_principal_code.value) ? null : param_principal_code.value;
    let fmcg_id = principal_code
    let fmcg_top = 23;
    let vacc_aspirasi = '21000000601';
    
    
    try {
        Logs.create({action: 'payment_distributor', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});
//        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
//        if(result_fmcg.dataValues == null) {
//            throw new CustomError(400, 'Gagal menampilkan data payment_distributor, data fmcg tidak tersedia');
//        }
        let result_fmcg_top = await service_fmcg.getOneFmcgWithFilter({id: fmcg_top});
        if(result_fmcg_top.dataValues == null) {
            throw new CustomError(400, 'Gagal menampilkan data payment_distributor, data fmcg tidak tersedia');
        }

        let invoice = {
            url: result_fmcg_top.dataValues.url,
            fmcg_token : result_fmcg_top.dataValues.fmcg_token,
            fmcg_name : result_fmcg_top.dataValues.name,
            le_code:le_code,
            dt_code:dt_code,
            invoice_id:invoice_no,
            invoice_code: dt_code+invoice_no
          }
    
        let loan_history = await ApiAspirasi.get_loan_history(invoice);
        if(loan_history.length == 1){

            let check_disburse = await ApiAspirasi.get_data_disburse({url: result_fmcg_top.dataValues.url,fmcg_token : result_fmcg_top.dataValues.fmcg_token,fmcg_name : result_fmcg_top.dataValues.name,le_code:le_code,dt_code:dt_code,invoice_no:invoice_no,invoice_code:dt_code+invoice_no,principal_code:principal_code})

            if(check_disburse.length == 0){
                let dist = await service_pelunasan_manual.get_dt_unicode({dt_code: dt_code})
                let toko = await service_pelunasan_manual.get_le_unicode({le_code: le_code})
                let toko_data = await service_pelunasan_manual.get_le_aditional({le_code: le_code})
                let nama_toko = '';
                if (toko_data.length === 0)
                {
                    nama_toko = 'Tidak ada nama';
                } else {
                    nama_toko = toko_data[0].value;
                }

                let payment = {
                    trans_type: "transfer",
                    status: "success",
                    vacc_from: vacc_aspirasi,
                    vacc_to: [
                        {vacc_number: dist[0].valdo_account, amount: parseInt(loan_history[0].amount), desc: `Pembayaran invoice ${invoice_no} (${nama_toko}-${le_code})  Rp. ${helper.IDRformat(loan_history[0].amount)} dari Limit Pandai (${vacc_aspirasi}) ke ${dist[0].fullname} (${dist[0].valdo_account})`}
                    ],
                    pin: '123456'
                }
                request({
                    headers: {
                        'apigatekey': apigatekey,
                        'Content-Type': 'application/json'
                    },
                    url: 'http://' + url_trf + '/api/v1/multi_vacc/trf',
                    method: "POST",
                    json: true,
                    body: payment
                }, async function (error, result, body) {  
                    if (body.status.code === 200) {

                        let input_to_transactions = {};
                        input_to_transactions.fmcg_id = fmcg_id;
                        input_to_transactions.dt_code = dt_code;
                        input_to_transactions.invoice_id = invoice_no;
                        input_to_transactions.amount = parseInt(loan_history[0].amount);
                        input_to_transactions.invoice_code = dt_code+invoice_no;
                        input_to_transactions.valdo_tx_id = "TR"+invoice_no+moment().format('YYYYMMDDHHmmss');
                        input_to_transactions.distributor_id = dist[0].user_id;
                        input_to_transactions.retailer_id = toko[0].user_id;  
                        input_to_transactions.from_user_id = 0;
                        input_to_transactions.to_user_id = dist[0].user_id;
       
                        let insert_manual_transaction = await ApiPohonDana.multifmcg_transactions(input_to_transactions)
                        let invoice_args = {};
                        invoice_args.url = result_fmcg_top.dataValues.url;
                        invoice_args.fmcg_name = result_fmcg_top.dataValues.name;
                        invoice_args.fmcg_token = result_fmcg_top.dataValues.fmcg_token;
                        invoice_args.fmcg_id = fmcg_id;
                        invoice_args.principal_code = fmcg_id
                        invoice_args.dt_code = dt_code;
                        invoice_args.le_code = le_code;
                        invoice_args.invoice_no = invoice_no;
                        invoice_args.tenor = loan_history[0].tenor;
                        invoice_args.amount = parseInt(loan_history[0].amount);
                        invoice_args.total_tagihan = parseInt(loan_history[0].total_amount_invoice);

                        let api_response = await ApiAspirasi.payment_distributor(invoice_args);
                        res.status(200).json({code:200,status:'success',message:api_response.body});
                   }
                   else{
                    res.status(400).json({code:400, message:'Gagal melakukan transfer pembayaran'})
                   }
                })

            }else if(check_disburse.length > 0){
                res.status(400).json({code:400, message:'Pengajuan telah di-disburse'})
            }

        }else if(loan_history == 0){
            res.status(400).json({code:400, message:'Data pengajuan tidak ditemukan'})
        }else if(loan_history > 1){
            res.status(400).json({code:400, message:'Terdapat 2 pengajuan invoice yang sama'})
        }
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal, '+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function new_repayment(req, res) {
    const token = req.swagger.params.token.value;
    const pin = req.swagger.params.pin.value;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_tenor = req.swagger.params.tenor;
    const param_amount = req.swagger.params.amount;
    const param_invoice_number = req.swagger.params.invoice_id;
    const param_le_code = req.swagger.params.le_code;
    const param_dt_code = req.swagger.params.dt_code;
    const param_loan_id = req.swagger.params.loan_id;

    const iso8601format = "YYYY-MM-DD";

    let fmcg_id = _.isUndefined(param_fmcg_id.value)?null:param_fmcg_id.value;
    let amount = _.isUndefined(param_amount.value) ? null : param_amount.value;
    let tenor = _.isUndefined(param_tenor.value) ? null : param_tenor.value;
    let invoice_number = _.isUndefined(param_invoice_number.value) ? null : param_invoice_number.value;
    let le_code = _.isUndefined(param_le_code.value) ? null : param_le_code.value;
    let dt_code = _.isUndefined(param_dt_code.value) ? null : param_dt_code.value;
    let loan_id = _.isUndefined(param_loan_id.value) ? null : param_loan_id.value;

    let data_unique_code = '';
    let tanggal_mutasi = '';
    let vacc_aspirasi = '21000000602';
    let vacc_topan = '2010001162';
    let fmcg_top = 23;
    let out = 0;
    let index
    
    try {

        let auth_user = await middleware.verify({token: token, acl: acl_repayment, is_allowed_access_token_static: false});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
        if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal menampilkan data invoice, data fmcg tidak tersedia');
        }
        let result_fmcg_top = await service_fmcg.getOneFmcgWithFilter({id: fmcg_top});
        if(result_fmcg_top == null) {
            throw new CustomError(400, 'Gagal menampilkan data invoice, data fmcg tidak tersedia');
        }

        let actual_pin = await service_user.getHashedPassword(auth_user.dataValues.salt, pin);
        let expected_pin = auth_user.dataValues.pin;
        if (expected_pin != actual_pin) {
            throw new CustomError(400, 'PIN salah');
        }
        
        let vacc_toko = auth_user.dataValues.valdo_account
        let nama_toko = auth_user.dataValues.fullname

        let inv = {}
        inv.url = result_fmcg_top.dataValues.url;
        inv.fmcg_token = result_fmcg_top.dataValues.fmcg_token;
        inv.fmcg_name = result_fmcg_top.dataValues.name;
        inv.invoice_number = invoice_number;
        inv.invoice_payment_status = 'unpaid';

        let args ={}
        args.url = result_fmcg.dataValues.url;
        args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        args.fmcg_name = result_fmcg.dataValues.name;
        args.invoice_id = invoice_number;
        args.dt_code = dt_code

       
        let distributor = await service_pelunasan_manual.get_dt_unicode({dt_code:dt_code})
        let retailer = await service_pelunasan_manual.get_le_unicode({le_code:le_code})  
        let api_response = await MoneyAccess.inquiry({mobnum: vacc_toko, vaccount: vacc_toko, usertype: auth_user.dataValues.type});
        out = api_response;
        if(out.balance < amount){
              throw new CustomError(400, 'Saldo tidak cukup');
        }

        let get_top_invoice = await ApiAspirasi.get_all_tenor(inv)
        if(get_top_invoice["data"].length === 0){
            throw new CustomError(400,'Invoice tidak ditemukan atau sudah dibayar')
        }
        let unpaid_invoices = await get_top_invoice["data"].filter(function(item){
            return item.invoice_payment_status == "unpaid"
        })
        
        for(let i=0;i < unpaid_invoices.length;i++){
            if(Number(tenor) > Number(unpaid_invoices[i]["cash_memo_doc_no"].split("-")[1])){
                res.status(400).json({code:400,message:'Invoice cicilan ke - '+unpaid_invoices[i]["cash_memo_doc_no"].split("-")[1]+' Anda belum dibayar'}) 
            }
            else if(Number(tenor) === Number(unpaid_invoices[i]["cash_memo_doc_no"].split("-")[1])){
                if(get_top_invoice["data"].length === 0){
                    throw new CustomError(400,'Invoice tidak ditemukan atau sudah dibayar')
                }
            }

            if(unpaid_invoices[i].loan_id === loan_id){
                index = i
            }
        }

        let get_tenor = get_top_invoice.data[0].cash_memo_doc_no.split('-')
        
        let tenor = get_tenor[1]
        var ten = tenor.split(" ", 1);
       

        let loan_history = await ApiAspirasi.get_loan({
            url: result_fmcg_top.dataValues.url,
            fmcg_token : result_fmcg_top.dataValues.fmcg_token,
            fmcg_name : result_fmcg_top.dataValues.name,
            dt_code:dt_code,
            le_code:le_code,
            invoice_id:invoice_number});

        if(unpaid_invoices[index].overdue > 0){
            tanggal_mutasi = moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
        }
        else if(loan_history[0].tenor == 1 && unpaid_invoices[index].overdue <= 0){
           tanggal_mutasi = moment(new Date(loan_history[0].createdAt).setDate(new Date(loan_history[0].createdAt).getDate() + 7)).format('YYYY-MM-DD HH:mm:ss')
        }
        else if(loan_history[0].tenor == 2 && unpaid_invoices[index].overdue <= 0){
            tanggal_mutasi = moment(new Date(loan_history[0].createdAt).setDate(new Date(loan_history[0].createdAt).getDate() + 14)).format('YYYY-MM-DD HH:mm:ss')
        }
        else if(loan_history[0].tenor == 3 && unpaid_invoices[index].overdue <= 0){
            tanggal_mutasi = moment(new Date(loan_history[0].createdAt).setDate(new Date(loan_history[0].createdAt).getDate() + 21)).format('YYYY-MM-DD HH:mm:ss')
        }
        else if(loan_history[0].tenor == 4 && unpaid_invoices[index].overdue <= 0){
            tanggal_mutasi = moment(new Date(loan_history[0].createdAt).setDate(new Date(loan_history[0].createdAt).getDate() + 28)).format('YYYY-MM-DD HH:mm:ss')
        }

        if(Object.keys(get_top_invoice).length !== 0){
           
        let payment = {
            trans_type:"payment_limit",
            status:"success", 
                vacc_from: vacc_toko,
            vacc_to:[
                {vacc_number:vacc_aspirasi, amount: amount, desc:`Pembayaran invoice ${invoice_number}-${parseInt(tenor)} Rp. ${helper.IDRformat(amount)} dari ${nama_toko}-${le_code} (${vacc_toko}) ke Limit Pandai (${vacc_aspirasi})`}
            ],
            pin:pin,
            tanggal_mutasi: tanggal_mutasi
        }
            request({
                headers: {
                    'apigatekey': apigatekey,
                    'Content-Type': 'application/json'
                },
                url: "http://192.168.12.2/api/v1/multi_vacc_v2/trf",
                method: "POST",
                json: true,   
                body: payment
            }, async function (error, result, body){
                let top_money_access = body.result
                let top_money_access_response = {
                    code: body.status.code,
                    desc: body.result.desc
                }
                                               
                Logs.create({action: `api money access TOP`, description: 'response'+top_money_access});
                if(body.status.code === 200){

                let args = {
                    "le_code": le_code,
                    "dt_code": dt_code,
                    "invoice_number": invoice_number,
                    "cicilan_ke": tenor,
                    "amount": amount,
                    "loan_id":loan_id
                }
                request({
                headers: {
                    'Content-Type': 'application/json'                                   
                },
                url: "http://192.168.12.7:10020/repayment",
                method: "POST",
                json: true,
                body: args
                }, async function (error, response, body){
                    let res_repayment = body
                    if(res_repayment.status !== 'success'){
                        res.status(400).json({code:400, message:res_repayment.message})
                    }
                    else if(body.status === 'success'){
                    let input_to_transactions = {};
                    input_to_transactions.url = result_fmcg_top.dataValues.url;
                    input_to_transactions.fmcg_token = result_fmcg_top.dataValues.fmcg_token;
                    input_to_transactions.fmcg_name = result_fmcg_top.dataValues.name;
                    input_to_transactions.fmcg_id = fmcg_id;
                    input_to_transactions.dt_code = dt_code;
                    input_to_transactions.invoice_id = invoice_number+'-'+tenor;
                    input_to_transactions.amount = amount;
                    input_to_transactions.invoice_code = dt_code+invoice_number+'-'+tenor;
                    input_to_transactions.valdo_tx_id = "TR"+invoice_number+moment().format('YYYYMMDDHHmmss');
                    input_to_transactions.distributor_id = 0;
                    input_to_transactions.retailer_id = 0;  
                    input_to_transactions.from_user_id = auth_user.dataValues.user_id;
                    input_to_transactions.to_user_id = 0;
                    
                    let insert_manual_transaction = await ApiAspirasi.insert_transactions(input_to_transactions)
                    
                    request({
                        headers: {
                            'token': '2b3e6865-bdae-4881-ba71-ea9c5f123e8d'                                  
                        },
                        url: "http://192.168.12.6:21001/invoice/get-updated-invoice",
                        method: "GET",
                        json: true,
                        body: {
                            "invoice_id": invoice_number
                        }
                        }, async function (error, response, body){
                    })
                    let repayment_response = {
                        message: res_repayment.message,
                        status: res_repayment.status
                    }
                    let out = {
                        response : repayment_response,
                        top_money_access_desc : top_money_access_response,
                        update_transaction : insert_manual_transaction
                    } 
                    res.status(200).json({code:200, body:out})
                    }
                    });
                }
            })
    } 
    else{
        throw new CustomError(400, 'Invoice tidak sesuai!')
    }  
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data invoice, '+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}


export { calculator }
export { apply_loan }
export { repayment }
export { payment_distributor}
export { manual_repayment }
export { new_repayment }