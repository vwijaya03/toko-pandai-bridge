import { orm } from '~/services/mariadb'
import Sequelize from 'sequelize'

const AcaProduct = orm.define('aca_products', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: Sequelize.STRING, allowNull:true
    },
    KdProduk: {
        type: Sequelize.STRING, allowNull:true
    },
    jmlhPremi: {
        type: Sequelize.INTEGER, allowNull:true
    },
    ProductDesc: {
        type: Sequelize.STRING, allowNull:true
    },
    panduan: {
        type: Sequelize.STRING, allowNull:true
    },
    icon: {
        type: Sequelize.STRING, allowNull:true
    },
    hide_status: {
        type: Sequelize.INTEGER, allowNull:true
    },
    createdAt: {
        type: 'TIMESTAMP', allowNull:true
    },
    updatedAt: {
        type: 'TIMESTAMP', allowNull:true
    }
});

export default AcaProduct
