import { orm } from '~/services/mariadb'
import Sequelize from 'sequelize'
// import CustomDataTypes from '~/utils/sequelize-custom-datatypes';

const Bfi = orm.define('bfi_leasing', {
    id: {
        type: Sequelize.INTEGER, field: 'id',
        primaryKey: true,
        autoIncrement: true
    },
    customer_name: {
        type: Sequelize.STRING, allowNull:true
    },
    address: {
        type: Sequelize.STRING, allowNull:true
    },
    mobile_phone: {
        type: Sequelize.STRING, allowNull:true
    },
    asset_name: {
        type: Sequelize.STRING, allowNull:true
    },
    manufacturing_year: {
        type: Sequelize.STRING, allowNull:true
    },
    funding_amount: {
        type: Sequelize.STRING, allowNull:true
    },
    installment: {
        type: Sequelize.STRING, allowNull:true
    },
    tenor: {
        type: Sequelize.STRING, allowNull:true
    },
    cust_appid: {
        type: Sequelize.STRING, allowNull:true
    },
    fundingamount: {
        type: Sequelize.STRING, allowNull:true
    },
    telestatus: {
        type: Sequelize.STRING, allowNull:true
    },
    teledatetime: {
        type: Sequelize.STRING, allowNull:true
    },
    telereason: {
        type: Sequelize.STRING, allowNull:true
    },
    surveydscheduledatetime: {
        type: Sequelize.STRING, allowNull:true
    },
    surveyrescheduledatetime: {
        type: Sequelize.STRING, allowNull:true
    },
    surveystatus: {
        type: Sequelize.STRING, allowNull:true
    },
    surveydatetime: {
        type: Sequelize.STRING, allowNull:true
    },
    surveyreason: {
        type: Sequelize.STRING, allowNull:true
    },
    approvalstatus: {
        type: Sequelize.STRING, allowNull:true
    },
    approvaldatetime: {
        type: Sequelize.STRING, allowNull:true
    },
    golivestatus: {
        type: Sequelize.STRING, allowNull:true
    },
    golivedatetime: {
        type: Sequelize.STRING, allowNull:true
    },
    result: {
        type: Sequelize.STRING, allowNull:true
    },
    notes: {
        type: Sequelize.STRING, allowNull:true
    },
    create_at: {
        type: Sequelize.STRING, allowNull:true
    },
    user_id: {
        type: Sequelize.STRING, allowNull:true
    }
}, {
    freezeTableName: true, // Model tableName will be the same as the model name
    timestamps: false
});

export default Bfi
