'use strict';

import helper from '~/utils/helper'

import { Bank } from '~/orm/index'

import BankNotFoundError from '~/response/error/BankNotFoundError'

async function get(filter, options) {
    options = options || {};
    let result_set = '';

    if(filter) {
        result_set = await Bank.findAll({where: filter});
    } else {
        result_set = await Bank.findAll();
    }

    if (!result_set || Object.keys(result_set).length == 0) {
        if (options.throwIfEmpty) {
            throw new BankNotFoundError();
        }
    }

    return result_set;
}

function getOne({id=null}={}) {
    return Bank.findOne({
        where: {
            id: id
        }
    });
}

function add({name=null}={}) {
    let args = {};
    args.name = name;
    
    return Bank.create(args);
}

let obj = {}
obj.add = add
obj.get = get
obj.getOne = getOne

export default obj