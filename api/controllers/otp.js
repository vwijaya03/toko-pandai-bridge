import _ from 'lodash'
import util from 'util'
import request from 'request'
import moment from 'moment'

import middleware from '~/api/controllers/auth'
import service_otp from '~/services/otp'
import service_user from '~/services/user'
import helper from '~/utils/helper'

import { Logs } from '~/orm/index'

const acl_otp = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];

async function generate_otp(req, res) {

    const token = req.swagger.params.token.value? req.swagger.params.token.value:'xxx';
    const to = req.swagger.params.to.value;
    const type = req.swagger.params.type.value;
    const purpose = req.swagger.params.purpose.value;

    let _to = '';

    try {
        Logs.create({action: 'generate_otp', description: helper.getSimpleSwaggerParams(req.swagger.params)});

    	let auth_user = await middleware.verify({token: token, acl: acl_otp, is_allowed_access_token_static: true});
        let users = await service_user.getOneByPhone({phone: to });
        var today = new Date();   
        
        let time_setting = moment(today).format('YYYY:MM:DD');
        let time_user = moment(users.join_date).format('YYYY:MM:DD');
        
        let dformat = helper.formatCurrentDateTime();
        let args = {};

        args.token_user = token;
        switch (purpose) {
            case 2: {

            } break;
            default: {
                args.to = ((auth_user)? (((type == "email")? auth_user.dataValues.email:((type == "sms")? auth_user.dataValues.phone:""))):to);
            } break;
        }
        args.type = type;
        args.purpose = purpose;
        args.ipaddress =  req.connection.remoteAddress ||  req.socket.remoteAddress || req.headers['x-forwarded-for'];
        args.createdAt = dformat;
        args.updatedAt = dformat;

        _to = args.to;

        let response = await service_otp.addOtp(args);
        let no = to;
        if((to.slice(0, 1))==0){
            no = to.replace(/^.{1}/g, '62');
            
        }
        let message = "";
        if(time_setting == time_user){
          message = 'OTP+anda+adalah+'+response.otp+' Ini+Adalah+Otp+anda+gunakan+sebelum+5+menit'
        }else{
          message = 'OTP+anda+adalah+'+response.otp.slice(0, 3)+'***+(Tiga+digit+***+Hubungi+IT+Support)+untuk+5+menit'
        }
        
        request.post({
            headers: {'content-type' : 'application/x-www-form-urlencoded'},
            url: 'http://ginsms.gratika.id:8100/sendSms.do?username=tokopandai&password=TokoPandai1234&senderid=TOKOPANDAI&pesan='+message+'&msisdn='+no+'',
          }, function(error, response, body){
          });
        res.json({
            otp: response.otp,
            type: type,
            to: _to,
            createdAt: response.createdAt
        });
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Generate otp gagal, '+err;
        }

        err.message = (err.message === undefined) ? err : err.message;
        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function verify_otp(req, res) {

	const token = _.isUndefined(req.swagger.params.token.value) ? '' : req.swagger.params.token.value;
    const otp = _.isUndefined(req.swagger.params.otp.value) ? '' : req.swagger.params.otp.value;
    const to = req.swagger.params.to.value;
    const purpose = req.swagger.params.purpose.value;

    try {
        Logs.create({action: 'verify_otp', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_otp, is_allowed_access_token_static: true});

    	let response = await service_otp.verifyOtp(token, otp, to, purpose);
    	let out = {};
    	
    	if(Object.keys(response).length > 0) {
    		out.result = {"OTP":"Valid"};
    	} else {
    		out.result = {"OTP":"Invalid"};
    	}
    	
    	res.json(out);

    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Verifikasi otp gagal, '+err;
        }

        err.message = (err.message === undefined) ? err : err.message;
        res.status(err.code).json({code: err.code, message: err.message});
    }
}

export { generate_otp }
export { verify_otp }