import { orm } from '~/services/mariadb'
import Sequelize from 'sequelize'

const BankAccount = orm.define('users_banking', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    user_id: {
        type: Sequelize.INTEGER, allowNull:false
    },
    name: {
        type: Sequelize.STRING, allowNull:true
    },
    no_rekening: {
        type: Sequelize.STRING, allowNull:false
    },
    bank_id: {
        type: Sequelize.STRING, allowNull:true
    }
});

export default BankAccount