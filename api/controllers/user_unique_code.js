import _ from 'lodash'
import moment from 'moment'
import util from 'util'

import middleware from '~/api/controllers/auth'

import helper from '~/utils/helper'

import service_fmcg from '~/services/fmcg'
import service_user from '~/services/user'
import service_user_unique_code from '~/services/user_unique_code'
import service_register_lecode from '~/services/register_lecode'

import { Logs } from '~/orm/index'
import { RegisteredLeCode } from '~/orm/index'

import FmcgNotFoundError from '~/response/error/FmcgNotFoundError'
import CustomError from '~/response/error/CustomError'
import CustomSuccess from '~/response/success/CustomSuccess'

const acl_add_unique_code = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center','finance'];
const acl_delete_unique_code = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center','finance'];
const acl_update_unique_code = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center','finance'];

async function add_unique_code(req, res) {
	const token = req.swagger.params.token.value;
	const fmcg_id = req.swagger.params.fmcg_id.value;
    const unique_code = req.swagger.params.unique_code.value;
	const description = req.swagger.params.description.value;
    const position = req.swagger.params.position.value;

    let user_unique_code = '';

	try {
        Logs.create({action: 'add_unique_code', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_add_unique_code, is_allowed_access_token_static: false});
        let response = await service_fmcg.getOne({id: fmcg_id});

        if(response == null || response.length == 0) {
        	throw new FmcgNotFoundError();
        }

	   if(auth_user.dataValues.type ===0){
            let exist_le_codes = await service_user_unique_code.getOne({fmcg_id: fmcg_id, unique_code: unique_code});
            if(exist_le_codes !== null) {
                throw new CustomError(400, 'Gagal, unique code dan fmcg sudah didaftarkan');
            }
        }
        
        
        let exist_le_code = await service_register_lecode.getOne({fmcg_id: fmcg_id,  le_code: unique_code});
        if(exist_le_code == null) {
            throw new CustomError(400, 'Gagal menambahkan unique code, unique code tidak valid');
        }

        

        let current_total_unique_code_response = await service_user_unique_code.get({user_id: auth_user.dataValues.user_id, fmcg_id: fmcg_id});

        if(auth_user.dataValues.type == 1 || auth_user.dataValues.type == 3) {
            if(current_total_unique_code_response.length > 1) {
                throw new CustomError(400, 'Gagal menambahkan unique code, distributor dan distributor manager hanya boleh memiliki 1 unique code');
            }
        } else {
            if(current_total_unique_code_response.length >= response.dataValues.limit) {
                throw new CustomError(400, 'Gagal menambahkan unique code, melebihi jumlah limit');
            }
        }

        let unique_code_response = '';

        if(auth_user.dataValues.type == 3) {
            unique_code_response = await service_user_unique_code.get({fmcg_id: fmcg_id, unique_code: unique_code});
            
            if(unique_code_response == null || unique_code_response.length == 0) {
                // service_user_unique_code.add({user_id: auth_user.dataValues.user_id, fmcg_id: fmcg_id, unique_code: unique_code, description: description});
                throw new CustomError(400, 'Distributor belum mendaftarkan kode distributor');
            } else {
                let user = await service_user.getOne({user_id: unique_code_response[0].dataValues.user_id});
                
                if(user == null) {
                    throw new CustomError(400, 'Distributor tidak ditemukan');
                }

                if(user.dataValues.type != 1) {
                    throw new CustomError(400, 'Kode distributor tidak di daftarkan oleh distributor, silahkan hubungi admin');
                }

                if(user.dataValues.valdoAccount == null || user.dataValues.valdoAccount.length == 0) {
                    throw new CustomError(400, 'Distributor belum melengkapi data diri');
                }

                unique_code_response = await service_user_unique_code.get({user_id: auth_user.dataValues.user_id, fmcg_id: fmcg_id, unique_code: unique_code});

                if(unique_code_response == null || unique_code_response.length == 0) {
                    user_unique_code = await service_user_unique_code.add({user_id: auth_user.dataValues.user_id, fmcg_id: fmcg_id, unique_code: unique_code, description: description});
                    
                    service_user.updateVA({user_id: auth_user.dataValues.user_id, valdo_account: user.dataValues.valdoAccount});
                }
            }
            
        } else {
            unique_code_response = await service_user_unique_code.get({user_id: auth_user.dataValues.user_id, fmcg_id: fmcg_id, unique_code: unique_code});

            if(unique_code_response == null || unique_code_response.length == 0) {
                user_unique_code = await service_user_unique_code.add({user_id: auth_user.dataValues.user_id, fmcg_id: fmcg_id, unique_code: unique_code, description: description,position:position});
            }
        }

        let result = {};
        result.id = user_unique_code.id;
        result.fmcg_id = fmcg_id;
        result.unique_code = unique_code;
        result.description = description;

        res.json(helper.customSuccessResponse(new CustomSuccess(200, 'Unique code berhasil di tambahkan'), 'result', result));
    } catch(err) {
        
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal menambahkan unique code, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function delete_unique_code(req, res) { 
	const token = req.swagger.params.token.value;
    const id = req.swagger.params.id.value;
    
    try {
        Logs.create({action: 'delete_unique_code', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_delete_unique_code, is_allowed_access_token_static: false});
        let response = await service_user_unique_code.deleteById(id,auth_user.dataValues.user_id);

        let result = {};
        result.id = id;

        res.json(helper.customSuccessResponse(new CustomSuccess(200, 'Data berhasil di hapus'), 'result', result));
    } catch(err) {
        if(err.code === undefined) { 
            err.code = 400;
            err.message = 'Gagal menghapus data user unique code, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
    
}

async function modify_unique_code(req, res) { 
    const token = req.swagger.params.token.value;
    const id = req.swagger.params.id.value;
	const fmcg_id = req.swagger.params.fmcg_id.value;
    const unique_code = req.swagger.params.unique_code.value;
	const description = req.swagger.params.description.value;
    
    try {
        Logs.create({action: 'modify_unique_code', description: `param req token: ${token}, id: ${id}, fmcg_id: ${fmcg_id}, unique_code: ${unique_code}, description: ${description}`});

        let auth_user = await middleware.verify({token: token, acl: acl_update_unique_code, is_allowed_access_token_static: false});
        
        let response = await service_fmcg.getOne({id: fmcg_id});

        if(response == null || response.length == 0) {
        	throw new FmcgNotFoundError();
        }
        
        let response_old = await service_user_unique_code.getOne({id: id});
        
        if(response_old.fmcg_id != fmcg_id){
            let current_total_unique_code_response = await service_user_unique_code.get({user_id: auth_user.dataValues.user_id, fmcg_id: fmcg_id});

            if(current_total_unique_code_response.length >= response.dataValues.limit) {
                throw new CustomError(400, 'Gagal mengubah unique code, melebihi jumlah limit');
            }
        }

        await service_user_unique_code.update({id: id, fmcg_id: fmcg_id, unique_code: unique_code, description: description});

        let updated_data = await service_user_unique_code.get({user_id: auth_user.dataValues.user_id, fmcg_id: fmcg_id, unique_code: unique_code});
        
        let out = {};
        out.oldcode = response_old;
        out.newcode = updated_data[0];

        res.json(helper.customSuccessResponse(new CustomSuccess(200, 'Data berhasil di ubah'), 'result', out));
    } catch(err) {
        if(err.code === undefined) { 
            err.code = 400;
            err.message = 'Gagal mengubah data user unique code, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
    
}

export { delete_unique_code }
export { add_unique_code }
export { modify_unique_code }
