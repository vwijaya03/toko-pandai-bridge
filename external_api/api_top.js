import request from 'request'
import util from 'util'

import { Logs } from '~/orm/index'

import CustomError from '~/response/error/CustomError'
import FmcgGetInvoicesError from '~/response/error/FmcgGetInvoicesError'
import FmcgGetReturError from '~/response/error/FmcgGetReturError'
import FmcgGetTotalUnpaidAmountError from '~/response/error/FmcgGetTotalUnpaidAmountError'
import FmcgCheckProcessInvoiceError from '~/response/error/FmcgCheckProcessInvoiceError'
import FmcgTaskListError from '~/response/error/FmcgTaskListError'
import FmcgMakePaymentError from '~/response/error/FmcgMakePaymentError'
import FmcgProcessInvoiceError from '~/response/error/FmcgProcessInvoiceError'
import FmcgUploadDataPotonganError from '~/response/error/FmcgUploadDataPotonganError'
import FmcgUploadDataPembatalanPotonganError from '~/response/error/FmcgUploadDataPembatalanPotonganError'
import FmcgTaskListPotonganError from '~/response/error/FmcgTaskListPotonganError'
import FmcgTaskListPembatalanPotonganError from '~/response/error/FmcgTaskListPembatalanPotonganError'

const api_path_get_invoice = '/top-ext/get-invoice';
const api_path_get_one_invoice = '/top-ext/get-one-invoice';
const api_path_cek_saldo = '/top-ext/cek-saldo';
const api_path_calculator = '/top-ext/calculator';
const api_path_apply_loan = '/top-ext/apply-loan';
const api_path_repayment= '/top-ext/repayment';
const api_path_payment_distributor = '/top-ext/payment-distributor';
const api_path_payment_pembiayaan = '/updatePaymentPembiayaan';
const api_path_payment_status_distributor = '/top-ext/payment-status-distributor';
const api_path_insert_manual_transaction = '/payment/manual-transaction';
const api_path_repayment_history= '/top-ext/repayment-history';
const api_path_get_manual_invoice= '/payment/get-invoice';
const api_path_get_one_distributor_uli = '/get-one-distributor';
const api_path_get_invoice_all_tenor = '/top-ext/get-invoice-all-tenor';
const api_path_cek_le_code =  "/top-ext/cek-le-code"
const api_path_invoice_loan_history = '/top-ext/insert-invoice-loan-history';
const api_path_get_invoice_loan_history = '/top-ext/get-invoice-loan-history';
const api_path_get_unpaid_invoices = '/top-ext/get-unpaid-invoices-distributor';
const api_path_get_one_invoice_loan = '/top-ext/get-one-invoice-loan';
const api_path_get_all_le_code = '/top-ext/get-all-le-code';
const api_path_get_all_tenor = '/top-ext/get-all-tenor';
const api_path_get_one_transactions = '/get-one-transaction';
const api_path_get_payment_distributor = '/top-ext/get-payment-distributor';
const api_path_admin_get_invoice = '/top-ext/admin-get-invoice';
const api_path_get_top_user = '/get-top-user';
const api_path_get_manual_inv_loan_history = '/top-ext/get-manual-invoice-loan-history';
const api_path_manual_insert_top_payment_distributor = '/top-ext/manual-insert-top-payment-distributor';
const api_path_create_retur = '/retur-potongan';
const api_path_update_invoice = '/top-ext/get-inv-insert';
const api_path_cek_aktivasi_top =  "/top-ext/cek-aktivasi-top";
const api_path_get_all_data_akuisisi=  "/top-ext/get-all-data-akuisisi";
const api_path_update_loan_history = '/top-ext/update-invoice-loan-history';
const api_path_cek_fintech =  "/top-ext/cek-fintech";
const api_path_get_invoice_fit =  "/top-ext/get-inv-fit";


function commonHeaders(token) {
    return {
        'Cache-Control': 'no-cache',
        'token': token
    };
}

function handleResponse(rawJsonResponse, errorClass, resolve, reject, requestError, function_name, fmcg_name) {

	try {
		let successJsonResp = JSON.parse(rawJsonResponse);

		if(requestError != null) {
			reject(new errorClass(requestError));
		}

		if(successJsonResp.code == 400 || successJsonResp.code == 401) {
			reject(new errorClass(successJsonResp.message));
		} else {
			resolve(successJsonResp);
		}
	} catch(err) {
		Logs.create({action: `error ${function_name} ${fmcg_name}`, description: util.inspect(err), additional_description: util.inspect(rawJsonResponse)});
		reject(new errorClass('terjadi kesalahan silahkan coba lagi'));
	}

}

function handleResponseError(errorClass, resolve, reject, err, function_name, fmcg_name) {
	Logs.create({action: `error ${function_name} ${fmcg_name}`, description: util.inspect(err)});
	reject(new errorClass(err));
}

function cek_fintech({le_code=null}={}) {
	let invoice_args = {};
        let url='http://localhost:30111';
        let fmcg_token='2b3e6865-bdae-4881-ba71-ea9c5f123e8d';
        let fmcg_name='TopExt';

	invoice_args.le_code = le_code;

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_cek_le_code = url+api_path_cek_fintech;
	    request.get( {url:api_cek_le_code, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}

function get_invoice({url=null, fmcg_token=null, le_code=null,fmcg_name=null,user_id=null,invoice_payment_status=null,
	invoice_due_date=null,overdue=null,dt_code=null,role_type=null,page=null,items_per_page=null,sorting=null, dist_dt_code=null}={}) {

	let invoice_args = {};

	invoice_args.url = url;
	invoice_args.fmcg_token = fmcg_token;
	invoice_args.fmcg_name = fmcg_name;
	invoice_args.le_code = le_code;
	invoice_args.user_id = user_id;
	invoice_args.dt_code = dt_code;
	invoice_args.dist_dt_code = dist_dt_code;
	invoice_args.role_type = role_type;
	invoice_args.page= page;
        invoice_args.overdue = overdue;
	invoice_args.items_per_page = items_per_page;
	invoice_args.sorting = sorting;
	invoice_args.invoice_payment_status = invoice_payment_status;
	invoice_args.invoice_due_date = invoice_due_date;
	invoice_args.overdue = overdue;

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_invoice = url+api_path_get_invoice;
	    request.get( {url:api_invoice, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}

function get_manual_invoice({url=null, fmcg_token=null, le_code=null,fmcg_name=null,invoice_id=null}={}) {
	let invoice_args = {};

	invoice_args.url = url;
	invoice_args.fmcg_token = fmcg_token;
	invoice_args.fmcg_name = fmcg_name;
	invoice_args.invoice_id = invoice_id;

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_get_manual_invoice = url+api_path_get_manual_invoice;
	    request.get( {url:api_get_manual_invoice, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}

function get_one_invoice({url=null,tenor=null, fmcg_token=null, invoice_id=null, dt_code=null, 
	le_code=null,fmcg_name=null, user_id=null}={}) {
	let invoice_args = {};

	invoice_args.url = url;
	invoice_args.fmcg_token = fmcg_token;
	invoice_args.le_code = le_code;
        invoice_args.dt_code = dt_code;
	invoice_args.invoice_id = invoice_id;
	invoice_args.tenor = tenor;
	invoice_args.user_id = user_id;
        

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_one_invoice = url+api_path_get_one_invoice;
	    request.get( {url:api_one_invoice, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}

function get_one_invoice_offline({url=null,tenor=null, fmcg_token=null, invoice_id=null, dt_code=null, 
	le_code=null,fmcg_name=null, user_id=null}={}) {
	let invoice_args = {};

	invoice_args.url = url;
	invoice_args.fmcg_token = fmcg_token;
	invoice_args.le_code = le_code;
        invoice_args.dt_code = dt_code;
	invoice_args.invoice_id = invoice_id;
	invoice_args.tenor = tenor;
	invoice_args.user_id = user_id
        

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_one_invoice = url+api_path_get_one_invoice;
	    request.get( {url:api_one_invoice, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}

function get_invoice_all_tenor({url=null,fmcg_token=null,invoice_number=null,dt_code=null, 
	le_code=null,fmcg_name=null}={}) {
	let input = {};
	input.url = url;
	input.fmcg_token = fmcg_token;
	input.fmcg_name = fmcg_name;
	input.le_code = le_code;
  input.dt_code = dt_code;
	input.invoice_number = invoice_number;

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_invoice_all_tenor = url+api_path_get_invoice_all_tenor;
	    request.get( {url:api_invoice_all_tenor, headers:headers, qs:input}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}


function cek_saldo({le_code=null}={}) {
	let invoice_args = {};
        let url='http://localhost:30111';
        let fmcg_token='2b3e6865-bdae-4881-ba71-ea9c5f123e8d';
        let fmcg_name='TopExt';

	invoice_args.le_code = le_code;

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_cek_saldo = url+api_path_cek_saldo;
	    request.get( {url:api_cek_saldo, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}

function cek_le_code({le_code=null}={}) {
	let invoice_args = {};
        let url='http://localhost:30111';
        let fmcg_token='2b3e6865-bdae-4881-ba71-ea9c5f123e8d';
        let fmcg_name='TopExt';

	invoice_args.le_code = le_code;

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_cek_le_code = url+api_path_cek_le_code;
	    request.get( {url:api_cek_le_code, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}


function calculator({url=null, fmcg_token=null, amount=null,fmcg_name=null,discount=null}={}) {
	let invoice_args = {};

	invoice_args.url = url;
	invoice_args.fmcg_token = fmcg_token;
	invoice_args.amount = amount;
	invoice_args.discount = discount;

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_calculator = url+api_path_calculator;
	    request.get( {url:api_calculator, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}

function apply_loan({url=null,fmcg_token=null,dt_code =null,le_code =null,invoice_number=null,tenor=null , amount=null, principal_code =null,discount=null,fmcg_name=null}={}) {
	let invoice_args = {};

	invoice_args.url = url;
	invoice_args.fmcg_token = fmcg_token;
	invoice_args.dt_code = dt_code;
	invoice_args.le_code = le_code;
	invoice_args.invoice_number = invoice_number;
	invoice_args.tenor = tenor;
	invoice_args.amount = amount;
	invoice_args.principal_code = principal_code;
	invoice_args.discount = discount;
	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_apply_loan = url+api_path_apply_loan;
	    request.post( {url:api_apply_loan, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}


function repayment({url=null,fmcg_id=null,vaccount=null, fmcg_token=null, amount=null,fmcg_name=null,invoice_number=null,le_code=null,dt_code=null}={}) {
	let invoice_args = {};

	invoice_args.url = url;
	invoice_args.fmcg_token = fmcg_token;
        invoice_args.fmcg_id = fmcg_id;
	invoice_args.amount = amount;
	invoice_args.invoice_number = invoice_number;
        invoice_args.le_code = le_code;
	invoice_args.dt_code = dt_code;

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_repayment = url+api_path_repayment;
	    request.post( {url:api_repayment, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}

function payment_distributor({fmcg_id=null,le_code=null,dt_code=null,invoice_no=null,total_amount=null,principal_code=null,
	tenor=null,fmcg_name=null,fmcg_token=null,url=null,amount_invoice=null}={}) {
	let invoice_args = {};
	invoice_args.fmcg_token=fmcg_token;
	invoice_args.fmcg_name=fmcg_name;
	invoice_args.url = url
	invoice_args.fmcg_id = fmcg_id;
	invoice_args.tenor = tenor;
	invoice_args.le_code = le_code;
	invoice_args.dt_code = dt_code;
	invoice_args.invoice_no = invoice_no;
	invoice_args.total_amount = total_amount;
	invoice_args.amount_invoice = amount_invoice;
	invoice_args.principal_code = principal_code;
        

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_payment_distributor = url+api_path_payment_distributor;
	    request.post( {url:api_payment_distributor, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}

function update_payment_status_distributor({invoice_code=null,payment_status=null,fmcg_name=null,fmcg_token=null,url=null}={}){
		let update_data = {};
		update_data.fmcg_token=fmcg_token;
		update_data.fmcg_name=fmcg_name;
		update_data.url = url;
		update_data.invoice_code = invoice_code;
		update_data.payment_status = payment_status;

		return new Promise(function(resolve, reject) {
					let headers = commonHeaders(fmcg_token);
					let api_payment_status_distributor = url+api_path_payment_status_distributor;
				request.post( {url:api_payment_status_distributor, headers:headers, qs:update_data}, function(err, httpResponse, body) {
					if(err) {
						return err;
					}
					handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
				}).on('error', function(err) {
				handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
	});
}

function updatePaymentPembiayaan({url=null, fmcg_token=null,fmcg_name=null, paid_by=null,le_code=null, dt_code=null,invoice_id=null,amount=null}={}) {
	let invoice_args = {};

        invoice_args.url = url;
        invoice_args.fmcg_token = fmcg_token;
        invoice_args.le_code = le_code;
        invoice_args.dt_code = dt_code;
        invoice_args.invoice_id = invoice_id;
        invoice_args.amount = amount;
        invoice_args.paid_by = paid_by;

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_payment_pembiayaan = url+api_path_payment_pembiayaan;
	      request.post({url:api_payment_pembiayaan, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}


//function insert_manual_transaction({valdo_tx_id=null,transact_date=null,currency_code=null,type=null,distributor_id=null,retailer_id=null,from_user_id=null,to_user_id=null,invoice_code=null,url=null, fmcg_token=null,fmcg_name=null, le_code=null, dt_code=null,invoice_id=null,amount=null}={}) {
//    
//    let invoice_args = {};
//
//    invoice_args.url = url;
//    invoice_args.fmcg_token = fmcg_token;
//    invoice_args.le_code = le_code;
//    invoice_args.dt_code = dt_code;
//    invoice_args.invoice_id = invoice_id;
//    invoice_args.invoice_code = invoice_id+dt_code;
//    invoice_args.amount = amount;
//    invoice_args.valdo_tx_id = valdo_tx_id;
//    invoice_args.transact_date = transact_date;
//    invoice_args.currency_code = currency_code;
//    invoice_args.type = type;
//    invoice_args.distributor_id = distributor_id;
//    invoice_args.retailer_id = retailer_id;  
//    invoice_args.from_user_id = from_user_id;
//    invoice_args.to_user_id = to_user_id;
//    
//
//
//	return new Promise(function(resolve, reject) {
//        let headers = commonHeaders(fmcg_token);
//        let api_payment_manual = url+api_path_insert_manual_transaction;
//	      request.post({url:api_payment_manual, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
//	    	if(err) {
//	    		return err;
//	    	}
//	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
//	    }).on('error', function(err) {
//			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
//		});
//    });
//}

function updatePaymentPembiayaan({url=null, fmcg_token=null,fmcg_name=null, paid_by=null,le_code=null, dt_code=null,invoice_id=null,amount=null}={}) {
	let invoice_args = {};

        invoice_args.url = url;
        invoice_args.fmcg_token = fmcg_token;
        invoice_args.le_code = le_code;
        invoice_args.dt_code = dt_code;
        invoice_args.invoice_id = invoice_id;
        invoice_args.amount = amount;
        invoice_args.paid_by = paid_by;

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_payment_pembiayaan = url+api_path_payment_pembiayaan;
	      request.post({url:api_payment_pembiayaan, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}

function insert_manual_transaction({valdo_tx_id=null,transact_date=null,currency_code=null,type=null,distributor_id=null,retailer_id=null,from_user_id=null,to_user_id=null,invoice_code=null,url=null, fmcg_token=null,fmcg_name=null, le_code=null, dt_code=null,invoice_id=null,amount=null}={}) {
    
    let invoice_args = {};

    invoice_args.url = url;
    invoice_args.fmcg_token = fmcg_token;
    invoice_args.le_code = le_code;
    invoice_args.dt_code = dt_code;
    invoice_args.invoice_id = invoice_id;
    invoice_args.invoice_code = invoice_id+dt_code;
    invoice_args.amount = amount;
    invoice_args.valdo_tx_id = valdo_tx_id;
    invoice_args.transact_date = transact_date;
    invoice_args.currency_code = currency_code;
    invoice_args.type = type;
    invoice_args.distributor_id = distributor_id;
    invoice_args.retailer_id = retailer_id;  
    invoice_args.from_user_id = from_user_id;
    invoice_args.to_user_id = to_user_id;
    


	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_payment_manual = url+api_path_insert_manual_transaction;
	      request.post({url:api_payment_manual, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}

function repayment_history({url=null,fmcg_token=null,fmcg_name=null}={}) {
	let invoice_args = {};

	invoice_args.url = url;
	invoice_args.fmcg_token = fmcg_token;
        invoice_args.fmcg_name = fmcg_name;

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_repayment_history = url+api_path_repayment_history;
	    request.post( {url:api_repayment_history, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}

function get_one_distributor({dt_code=null,url=null,fmcg_name=null,fmcg_token=null}={}){
	let invoice_args = {};

	invoice_args.url = url;
	invoice_args.fmcg_token = fmcg_token;
        invoice_args.dt_code = dt_code;
        
        

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_one_invoice = url+api_path_get_one_distributor_uli;
	    request.get( {url:api_one_invoice, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}

function insert_loan_history({url=null,fmcg_token=null,fmcg_name=null,user_id=null,le_code=null,dt_code=null,
	amount=null,invoice_id=null,total_amount_invoice=null,tenor=null,principal_code=null,total_potongan=null,loan_type=null,loan_by=null}={}){

	let args = {}
	args.url = url;
	args.fmcg_token = fmcg_token;
	args.fmcg_name = fmcg_name;
	args.user_id = user_id;
	args.le_code = le_code;
	args.dt_code = dt_code;
        args.total_amount_invoice=total_amount_invoice;
	args.amount = amount;
	args.invoice_id = invoice_id;
	args.tenor = tenor;
	args.principal_code = principal_code;
	args.total_potongan = total_potongan;
        args.loan_type = loan_type;
	args.loan_by = loan_by;

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_invoice_loan_history = url+api_path_invoice_loan_history;
	    request.post( {url:api_invoice_loan_history, headers:headers, qs:args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'loan history', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'loan history', fmcg_name);
		});
    });
}

function get_loan_history({url=null,fmcg_token=null,fmcg_name=null,le_code=null,dt_code=null,invoice_id=null}={}){

	let invoice = {}
	invoice.url = url;
	invoice.fmcg_token = fmcg_token;
	invoice.fmcg_name = fmcg_name;
	invoice.le_code = le_code;
	invoice.dt_code = dt_code;
	invoice.invoice_id = invoice_id;
	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_get_invoice_loan_history = url+api_path_get_invoice_loan_history;
	    request.get( {url:api_get_invoice_loan_history, headers:headers, qs:invoice}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'loan history', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'loan history', fmcg_name);
		});
    });
}

function get_unpaid_invoices({fmcg_name=null,fmcg_token=null,url=null}={}){
	let args = {};
	args.fmcg_token=fmcg_token;
	args.fmcg_name=fmcg_name;
	args.url = url

	return new Promise(function(resolve, reject) {
				let headers = commonHeaders(fmcg_token);
				let api_get_unpaid_invoices = url+api_path_get_unpaid_invoices;
			request.post( {url:api_get_unpaid_invoices, headers:headers, qs:args}, function(err, httpResponse, body) {
				if(err) {
					return err;
				}
				handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
			}).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	});
});
}

function cek_fit({url=null,fmcg_name=null}={}){
	let args = {};
	args.url = url;
        args.fmcg_name=fmcg_name;

	return new Promise(function(resolve, reject) {
                                 let headers = commonHeaders();
				let api_get_unpaid_invoices = url;
			request.post( {url:api_get_unpaid_invoices, headers:headers, qs:args}, function(err, httpResponse, body) {
				if(err) {
					return err;
				}
				handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
			}).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	});
});
}

function get_all_le_code({fmcg_name=null,fmcg_token=null,url=null,dist_dt_code=null}){
	let data_dist = {}
		data_dist.url = url;
		data_dist.fmcg_token = fmcg_token;
		data_dist.fmcg_name = fmcg_name
		data_dist.dist_dt_code = dist_dt_code;

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
		let api_get_all_le_code = url+api_path_get_all_le_code;
		request.get( {url:api_get_all_le_code, headers:headers, qs:data_dist}, function(err, httpResponse, body) {
			if(err) {
				return err;
			}
			handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		}).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
	});

}

function get_all_tenor({fmcg_name=null,fmcg_token=null,url=null,invoice_number=null,invoice_payment_status=null}={}){
	let data = {};
	data.url = url;
	data.fmcg_name=fmcg_name;
	data.fmcg_token=fmcg_token;
	data.invoice_number=invoice_number;
	data.invoice_payment_status=invoice_payment_status;

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
		let api_get_all_tenor = url+api_path_get_all_tenor;
		request.get( {url:api_get_all_tenor, headers:headers, qs:data}, function(err, httpResponse, body) {
			if(err) {
				return err;
			}
			handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		}).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
	});
}


function get_one_transaction({url=null, fmcg_token=null, fmcg_name=null, invoice_id=null, dt_code=null}={}) {
	return new Promise(function(resolve, reject) {

        let headers = commonHeaders(fmcg_token);
        let url_get_one_invoice = url+api_path_get_one_transactions+'/'+encodeURIComponent(invoice_id)+'/'+dt_code;
	    request.get( {url:url_get_one_invoice, headers:headers}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get one invoice', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get one invoice', fmcg_name);
		});
    });
}

function get_one_loan_invoices({url=null,fmcg_name=null,fmcg_token=null,dt_code=null,invoice_id=null}={}){
	let args = {};
	args.url='http://localhost:30111';
        args.fmcg_token='2b3e6865-bdae-4881-ba71-ea9c5f123e8d';
        args.fmcg_name='TopExt';
        args.dt_code = dt_code;
        args.invoice_id = invoice_id;
        
        
	return new Promise(function(resolve, reject) {
                        let headers = commonHeaders(fmcg_token);
                        let api_path_get_one_invoice_loan = url+api_path_get_one_invoice_loan;
			request.post( {url:api_path_get_one_invoice_loan, headers:headers, qs:args}, function(err, httpResponse, body) {
                        if(err) {
                                return err;
                        }
                        handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
			}).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	});
});
}

function get_payment_distributor({fmcg_name=null,fmcg_token=null,url=null,dt_code=null,invoice_no=null}){
	let args = {}
	args.url = url
	args.fmcg_name = fmcg_name
	args.fmcg_token = fmcg_token
	args.dt_code = dt_code
	args.invoice_no = invoice_no
	

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
		let api_get_payment_distributor = url+api_path_get_payment_distributor;
		request.get( {url:api_get_payment_distributor, headers:headers, qs:args}, function(err, httpResponse, body) {
			if(err) {
				return err;
			}
			handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		}).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
	});
}

function admin_get_invoice({url=null,fmcg_token=null,fmcg_name=null,invoice_payment_status=null}){
	let args = {}
	args.url = url;
	args.fmcg_token = fmcg_token;
	args.fmcg_name = fmcg_name;
	args.invoice_payment_status = invoice_payment_status;

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
		let api_admin_get_invoice = url+api_path_admin_get_invoice;
		request.get( {url:api_admin_get_invoice, headers:headers, qs:args}, function(err, httpResponse, body) {
			if(err) {
				return err;
			}
			handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		}).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
	});
}

function get_top_user({url=null, fmcg_token=null, fmcg_name=null, le_code=null}={}) {
  let args = {}
  args.url = url;
  args.fmcg_token = fmcg_token;
  args.fmcg_name = fmcg_name;
  args.le_code = le_code
	return new Promise(function(resolve, reject) {
    let headers = commonHeaders(fmcg_token);
    let api_get_top_user = url+api_path_get_top_user;
    request.get({url:api_get_top_user, headers:headers, qs:args}, function(err, httpResponse, body) {
    if(err) {
      return err;
    }
    handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
  }).on('error', function(err) {
  handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
});
});
}


function get_manual_invoice_loan_history({url=null,fmcg_token=null,fmcg_name=null}){
	let args = {}
	args.url = url;
	args.fmcg_token = fmcg_token;
	args.fmcg_name = fmcg_name;
	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
		let api_get_manual_inv_loan_history = url+api_path_get_manual_inv_loan_history;
		request.get( {url:api_get_manual_inv_loan_history, headers:headers, qs:args}, function(err, httpResponse, body) {
			if(err) {
				return err;
			}
			handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		}).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
	});
}

function manual_insert_top_payment_distributor({url=null,fmcg_token=null,fmcg_name=null,le_code=null,dt_code=null,invoice_no=null,
	amount=null,tenor=null,fmcg_id=null,principal_code=null,user_id=null}){
	let invoice_args = {}
	invoice_args.url = url;
	invoice_args.fmcg_name = fmcg_name;
	invoice_args.fmcg_token = fmcg_token;
	invoice_args.le_code = le_code;
	invoice_args.dt_code = dt_code;
	invoice_args.invoice_no = invoice_no;
	invoice_args.amount = amount;
	invoice_args.tenor = tenor;
	invoice_args.fmcg_id = fmcg_id;
	invoice_args.principal_code = principal_code;
	invoice_args.user_id = user_id;
	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
		let api_manual_insert_top_payment_distributor = url+api_path_manual_insert_top_payment_distributor;
		request.post( {url:api_manual_insert_top_payment_distributor, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
			if(err) {
				return err;
			}
			handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		}).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
	});
}

function create_retur({url=null,fmcg_token=null,fmcg_name=null,le_code=null,dt_code=null,invoice_id=null,amount_potongan=null,remark_potongan=null}={}) {
  let args = {}
  args.url = url;
  args.fmcg_token = fmcg_token;
  args.fmcg_name = fmcg_name;
  args.le_code = le_code;
  args.dt_code = dt_code;
  args.invoice_id = invoice_id;
  args.amount_potongan = amount_potongan; 
  args.remark_potongan = remark_potongan; 
	return new Promise(function(resolve, reject) {
    let headers = commonHeaders(fmcg_token);
    let api_create_retur = url+api_path_create_retur;
    request.post({url:api_create_retur, headers:headers, qs:args}, function(err, httpResponse, body) {
    if(err) {
      return err;
    }
    handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
  }).on('error', function(err) {
  handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
});
});
}

function get_inv_insert({url=null,fmcg_token=null,fmcg_name=null}={}) {
  let args = {}
  args.url = url;
  args.fmcg_token = fmcg_token;
  args.fmcg_name = fmcg_name;
  
    return new Promise(function(resolve, reject) {
    let headers = commonHeaders(fmcg_token);
    let api_update_invoice = url+api_path_update_invoice;
    request.get({url:api_update_invoice, headers:headers, qs:args}, function(err, httpResponse, body) {
    if(err) {
      return err;
    }
    handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
  }).on('error', function(err) {
  handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
});
});
}

function cek_aktivasi_top({le_code=null}={}) {
	let invoice_args = {};
        let url='http://localhost:30111';
        let fmcg_token='2b3e6865-bdae-4881-ba71-ea9c5f123e8d';
        let fmcg_name='TopExt';

	invoice_args.le_code = le_code;

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_cek_le_code = url+api_path_cek_aktivasi_top;
	    request.get( {url:api_cek_le_code, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}

function get_all_data_akuisisi({le_code=null}={}) {
	let invoice_args = {};
        let url='http://localhost:30111';
        let fmcg_token='2b3e6865-bdae-4881-ba71-ea9c5f123e8d';
        let fmcg_name='TopExt';

	invoice_args.le_code = le_code;

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_cek_le_code = url+api_path_get_all_data_akuisisi;
	    request.get( {url:api_cek_le_code, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}

function update_loan_history({url=null,fmcg_token=null,fmcg_name=null,id=null,le_code=null,dt_code=null,invoice_id=null,loan_response=null}={}){

	let args = {}
	args.url = url;
	args.fmcg_token = fmcg_token;
	args.fmcg_name = fmcg_name;
	args.id = id;
	args.le_code = le_code;
	args.dt_code = dt_code;
	args.invoice_id = invoice_id;
	args.loan_response = loan_response;

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_update_loan_history = url+api_path_update_loan_history;
	    request.post( {url:api_update_loan_history, headers:headers, qs:args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'loan history', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'loan history', fmcg_name);
		});
    });
}

function cek_fintech({le_code=null}={}) {
	let invoice_args = {};
        let url='http://localhost:30111';
        let fmcg_token='2b3e6865-bdae-4881-ba71-ea9c5f123e8d';
        let fmcg_name='TopExt';

	invoice_args.le_code = le_code;

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_cek_le_code = url+api_path_cek_fintech;
	    request.get( {url:api_cek_le_code, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}

function get_inv_fit({le_code=null,dt_code=null,invoice_number=null,tenor=null}={}) {
	let invoice_args = {};
        let url='http://localhost:30111';
        let fmcg_token='2b3e6865-bdae-4881-ba71-ea9c5f123e8d';
        let fmcg_name='TopExt';

	invoice_args.le_code = le_code;
        invoice_args.dt_code = dt_code;
        invoice_args.invoice_number = invoice_number;
        invoice_args.tenor = tenor;

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_cek_le_code = url+api_path_get_invoice_fit;
	    request.get( {url:api_cek_le_code, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}


let obj = {}

obj.get_invoice = get_invoice;
obj.get_one_invoice = get_one_invoice;
obj.cek_saldo = cek_saldo;
obj.calculator = calculator;
obj.apply_loan = apply_loan;
obj.repayment = repayment;
obj.payment_distributor = payment_distributor;
obj.update_payment_status_distributor = update_payment_status_distributor;
obj.updatePaymentPembiayaan = updatePaymentPembiayaan;
obj.insert_manual_transaction =insert_manual_transaction;
obj.repayment_history = repayment_history;
obj.get_manual_invoice =get_manual_invoice;
obj.get_one_distributor = get_one_distributor;
obj.get_invoice_all_tenor = get_invoice_all_tenor;
obj.cek_le_code =cek_le_code;
obj.insert_loan_history = insert_loan_history;
obj.get_loan_history = get_loan_history;
obj.get_unpaid_invoices = get_unpaid_invoices;
obj.cek_fit =cek_fit;
obj.get_one_invoice_offline =get_one_invoice_offline;
obj.get_one_loan_invoices=get_one_loan_invoices;
obj.get_all_le_code =get_all_le_code;
obj.get_all_tenor =get_all_tenor;
obj.get_one_transaction =get_one_transaction;
obj.get_payment_distributor = get_payment_distributor;
obj.admin_get_invoice = admin_get_invoice;
obj.get_top_user = get_top_user;
obj.get_manual_invoice_loan_history = get_manual_invoice_loan_history;
obj.manual_insert_top_payment_distributor = manual_insert_top_payment_distributor;
obj.create_retur = create_retur;
obj.get_inv_insert = get_inv_insert;
obj.cek_aktivasi_top = cek_aktivasi_top;
obj.get_all_data_akuisisi = get_all_data_akuisisi
obj.update_loan_history = update_loan_history
obj.cek_fintech = cek_fintech;
obj.get_inv_fit = get_inv_fit
export default obj

