import { orm } from '~/services/mariadb'
import Sequelize from 'sequelize'

const Vouchers = orm.define('vouchers', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    title: Sequelize.STRING,
    description: Sequelize.STRING,
    issuer: Sequelize.STRING,
    image_url: Sequelize.STRING,
    amount: Sequelize.DECIMAL,
    expiry: Sequelize.DATE
}, {
    timestamps: false,
    freezeTableName: true,

  // define the table's name
  tableName: 'vouchers'

});

export default Vouchers
