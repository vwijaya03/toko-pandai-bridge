import { orm } from '~/services/mariadb'
import { App_menu } from '~/orm/index'

async function get(filter, options) {
    options = options || {};
    let result_set;

    if (filter) {
        result_set = await App_menu.findAll({where: filter});
    } else {
        result_set = await App_menu.findAll();
    }

    if (!result_set || Object.keys(result_set).length == 0) {
        if (options.throwIfEmpty) {
            throw new CustomError(400, 'Data informasi dan promosi tidak di temukan');
        }
    }
    return result_set;
}

function getbyid({id=null}){
    try{
        return orm.query(`select id,type,title,icon,(CASE WHEN inquiry=0 THEN "false" ELSE "true" END) AS inquiry,hint,(CASE WHEN enabled=0 THEN "false" ELSE "true" END) as enabled,scope,role FROM app_menu where id in ("1","7","6","3","2","26")`, { type: orm.QueryTypes.SELECT })
    }
    catch(err) {
    }
}

function getbyids({id=null}){
    try{
        return orm.query(`select id,type,title,icon,(CASE WHEN inquiry=0 THEN "false" ELSE "true" END) AS inquiry,hint,(CASE WHEN enabled=0 THEN "false" ELSE "true" END) as enabled,scope,role FROM app_menu where id in ("1","7","6","3","2","22","23","24","25")`, { type: orm.QueryTypes.SELECT })
    }
    catch(err) {
    }
}


let obj = {}
obj.get = get
obj.getbyid = getbyid
obj.getbyids = getbyids

export default obj