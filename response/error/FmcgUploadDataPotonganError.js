class FmcgUploadDataPotonganError extends Error {
	constructor(message) {
        super(message);
        this.code = 400;
        this.err_code = 4300;
        this.message = 'Gagal upload data potongan, '+message;
    }
}

export default FmcgUploadDataPotonganError