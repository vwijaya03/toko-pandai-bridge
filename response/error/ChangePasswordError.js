class ChangePasswordError extends Error {
	constructor(code, message) {
        super(message);
        this.code = 400;
        this.message = 'Password lama anda salah';
    }
}

export default ChangePasswordError