import fs from 'fs'
import path from 'path'
import cheerio from 'cheerio'
import util from 'util'

import middleware from '~/api/controllers/auth'

const acl_faq = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];

async function get_faqs(req, res) {
	const token = req.swagger.params.token.value;

	try {
        let auth_user = await middleware.verify({token: token, acl: acl_faq, is_allowed_access_token_static: true});
        let faq_dir = path.join(__dirname, "..", "..", "static_file", "faq")
        let result = []

        fs.readdirSync(faq_dir).forEach(file => {
            let faq_file = path.join(faq_dir, file)

            let content = fs.readFileSync(faq_file).toString();
            let $ = cheerio.load(content);
            let faq = {
                title: $('title').text(),
                id: $('order').text(),
                url: "/faq/"+file
            }
            result.push(faq)

        })
        result = result.sort(function(a,b) {return (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0);} );

        let out = {};
        out.faqs = result;

        res.json(out);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal mendapatkan data faq, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

export { get_faqs }