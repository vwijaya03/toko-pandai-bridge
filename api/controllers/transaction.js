import _ from 'lodash'
import util from 'util'
import request from 'request'
import fs from 'fs'
import moment from 'moment'

import middleware from '~/api/controllers/auth'

import service_fmcg from '~/services/fmcg'
import service_user from '~/services/user'
import service_user_unique_code from '~/services/user_unique_code'
import service_data from '~/services/user_additional_data'
import service_pelunasan_manual from '~/services/pelunasan_manual'

import helper from '~/utils/helper'
import TransactionAdapter from '~/adapter/TransactionAdapter'

import MoneyAccess from '~/external_api/moneyaccess'
import ApiFmcg from '~/external_api/api_fmcg'
import ApiUli from '~/external_api/api_uli'
import ApiPohonDana from '~/external_api/api_top_pohon_dana'



import { User } from '~/orm/index'
import { Logs } from '~/orm/index'

import CustomError from '~/response/error/CustomError'
import CustomSuccess from '~/response/success/CustomSuccess'

const acl_get_transactions = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center','finance'];
const acl_get_summary = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center','finance'];

async function get_transactions(req, res) {
    const token = req.swagger.params.token.value;
    const param_user_id = req.swagger.params.user_id;
    const param_email = req.swagger.params.email;
    const param_type = req.swagger.params.type;
    const param_star_date = req.swagger.params.start_date;
    const param_end_date = req.swagger.params.end_date;
    const param_page = req.swagger.params.page;
    const param_item_per_page = req.swagger.params.item_per_page;

    let user_id = _.isUndefined(param_user_id.value)?null:param_user_id.value;
    let email = _.isUndefined(param_email.value)?null:param_email.value;
    let type = _.isUndefined(param_type.value)?null:param_type.value;
    let start_date = _.isUndefined(param_star_date.value)?null:param_star_date.value;
    let end_date = _.isUndefined(param_end_date.value)?null:param_end_date.value;
    let page = _.isUndefined(param_page.value)?1:param_page.value;
    let item_per_page = _.isUndefined(param_item_per_page.value)?15:param_item_per_page.value;

    let vacc_number, user = '';
    let d = new Date();
    d.setDate(d.getDate() - 30);

    page = (page < 1 ) ? 1 : page;

    try {
        Logs.create({action: 'get_transactions', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_get_transactions, is_allowed_access_token_static: false});

        if(user_id) {
            user = await service_user.getOne({user_id: user_id});

            if(user == null) {
                throw new CustomError(400, 'Data user tidak di temukan');
            }

            vacc_number = user.dataValues.valdoAccount
        } else if(email) {
            
            user = await service_user.getOne({email: email});

            if(user == null) {
                throw new CustomError(400, 'Data user tidak di temukan');
            }

            vacc_number = user.dataValues.valdoAccount
        } else {
            vacc_number = auth_user.dataValues.valdo_account
        }

        if(!start_date) {
            start_date = helper.formatDate(d);
        }

        if(!end_date) {
            end_date = helper.formatCurrentDate();
        }
        if(auth_user.dataValues.type == 2){
          item_per_page =300;   
        }

         let args = {};
        args.vacc_number = vacc_number;
        args.type = helper.transactionType(type);
        args.start_date = start_date;
        args.end_date = end_date;
        args.pagenum = page;
        args.item_per_page = item_per_page;
        args.usertype = auth_user.dataValues.type;
        let api_response= await MoneyAccess.history(args);
        Logs.create({action: 'api response money access get_transactions history', description: util.inspect(api_response)});
        let transactions = TransactionAdapter.parseHistory({response: api_response});
        
        let out = {};
        out.size = transactions['itempage'];
        out.page = transactions['pagenumber'];
        out.items_per_page = item_per_page;
        out.total_pages = transactions['totalpage'];
        out.total_items = transactions['totalitem'];
        out.transactions = transactions;

        res.json(out);
        // }
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal menampilkan data transaksi, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function get_summary(req, res) {
    const token = req.swagger.params.token.value;
    const param_user_id = req.swagger.params.user_id;
    const param_fmcg_id = req.swagger.params.fmcg_id;

    let user_id = _.isUndefined(param_user_id.value)?null:param_user_id.value;
    let fmcg_id = _.isUndefined(param_fmcg_id.value)?null:param_fmcg_id.value;
    let query_user_id, vacc_number, join_date = '';

    try {
        Logs.create({action: 'get_summary', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_get_summary, is_allowed_access_token_static: false});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});

        if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal menampilkan saldo dan jumlah invoice yang belum di bayar, data fmcg tidak tersedia');
        }

        if(user_id) {
            let user = await service_user.getOne({user_id: user_id});

            if(user == null) {
                throw new CustomError(400, 'Gagal menampilkan saldo dan jumlah invoice yang belum di bayar, Data tidak di temukan');
            } else if(user.dataValues.valdoAccount == null || user.dataValues.valdoAccount.length < 5) {
                throw new CustomError(400, 'Silahkan lengkapi data profile anda terlebih dahulu');
            }

            vacc_number = user.dataValues.valdoAccount
            query_user_id = user_id;
            join_date = user.dataValues.join_date;
        } else {
            vacc_number = auth_user.dataValues.valdo_account
            query_user_id = auth_user.dataValues.user_id;
            join_date = auth_user.dataValues.join_date;
        }

        let result_unique_code = await service_user_unique_code.get({user_id: query_user_id});
        let data_unique_code = '';

        await result_unique_code.map( (val, index) => {
            if(index == 0) {
                data_unique_code += val.unique_code;
            } else {
                data_unique_code += ','+val.unique_code;
            }
        });

        let args = {};
        args.url = result_fmcg.dataValues.url;
        args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        args.fmcg_name = result_fmcg.dataValues.name;
        args.le_code = data_unique_code;
        args.join_date = helper.formatDateTime(join_date);
        
        let api_response_moneyaccess = await MoneyAccess.inquiry({mobnum: vacc_number, vaccount: vacc_number});

        Logs.create({action: 'api response money access get_summary inquiry', description: util.inspect(api_response_moneyaccess)});

        let api_response_fmcg = await ApiUli.get_total_unpaid_amount(args);

        Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} get_total_unpaid_amount get_summary`, description: util.inspect(api_response_fmcg)});

        let out = {};
        out.balance = (isNaN(api_response_moneyaccess.balance)) ? 0 : Number(api_response_moneyaccess.balance);
        out.unpaid_total = (isNaN(api_response_fmcg[0].unpaid_total)) ? 0 : Number(api_response_fmcg[0].unpaid_total);

        res.json(out); 
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal menampilkan saldo dan jumlah invoice yang belum di bayar, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function make_payment(req, res) {
    const token = req.swagger.params.token.value;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_invoice_id = req.swagger.params.invoice_id;
    const param_dt_code = req.swagger.params.dt_code;
    const param_pin = req.swagger.params.pin;
    const param_amount = req.swagger.params.amount;
    const param_total_potongan = req.swagger.params.total_potongan;
    const param_total_retur = req.swagger.params.total_retur;

    let fmcg_id = _.isUndefined(param_fmcg_id.value)?null:param_fmcg_id.value;
    let invoice_id = _.isUndefined(param_invoice_id.value)?null:param_invoice_id.value;
    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
    let pin = _.isUndefined(param_pin.value)?null:param_pin.value;
    let amount = _.isUndefined(param_amount.value)?null:param_amount.value;
    let total_potongan = _.isUndefined(param_total_potongan.value)?null:param_total_potongan.value;
    let total_retur = _.isUndefined(param_total_retur.value)?0:param_total_retur.value;

    try {
        Logs.create({action: 'make_payment', description: helper.getSimpleSwaggerParams(req.swagger.params)});
        
        await helper.requestLimit({accessToken: token, lastRequestDate: helper.formatCustomCurrentDateTime(), limitRequestDate: helper.formatCustomCurrentDateTimeWithParam(helper.formatCustomCurrentDateTime(), null, null, null, null, 10)});
        let auth_user = await middleware.verify({token: token, acl: acl_get_transactions, is_allowed_access_token_static: false});

        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});

        if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal melakukan pembayaran invoice, data fmcg tidak tersedia');
        }

        let expected_pin = await service_user.getHashedPin(auth_user.dataValues.salt, pin);

        if(expected_pin != auth_user.dataValues.pin) {
            throw new CustomError(400, 'Pin salah');
        }

        let user_unique_code = await service_user_unique_code.getOne({unique_code: dt_code});

        if(user_unique_code == null) {
            throw new CustomError(400, 'Gagal melakukan pembayaran invoice, distributor tidak di temukan');
        }

        let toUser = await service_user.getOne({user_id: user_unique_code.dataValues.user_id});

        if(toUser == null) {
            throw new CustomError(400, 'Gagal melakukan pembayaran invoice, distributor tidak di temukan');
        }

        let balance_args = {};
        balance_args.mobum = auth_user.dataValues.valdo_account;
        balance_args.vaccount = auth_user.dataValues.valdo_account;
        balance_args.usertype = auth_user.dataValues.type;

        Logs.create({action: 'make_payment balance args', description: util.inspect(balance_args)});

        let api_response_inquiry = await MoneyAccess.inquiry(balance_args);

        Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} make_payment inquiry`, description: util.inspect(api_response_inquiry)});

        if(Number(amount) > Number(api_response_inquiry.balance)) {
            throw new CustomError(400, `Saldo tidak cukup, sisa saldo ${api_response_inquiry.balance}`);
        }
        
        let detail_toko =await service_data.get({user_id:auth_user.dataValues.user_id});
        let moneyaccess_args = {};
        moneyaccess_args.mobnum = auth_user.dataValues.valdo_account;
        moneyaccess_args.from = auth_user.dataValues.valdo_account;
        moneyaccess_args.to = toUser.dataValues.valdoAccount;
        moneyaccess_args.amount = amount;
        moneyaccess_args.pin = pin;
        moneyaccess_args.usertype = auth_user.dataValues.type;
        moneyaccess_args.transId = invoice_id.replace(/\//g, '')+helper.currentMilisecondsTime();
        if(fmcg_id ==24){
             moneyaccess_args.desc = `Top Up ${invoice_id} Rp. ${helper.IDRformat(amount)} dari ${auth_user.dataValues.fullname}(${detail_toko[0].dataValues.value}) (${auth_user.dataValues.valdo_account}) ke ${toUser.dataValues.fullname} (${toUser.dataValues.valdoAccount})`;
        }else{
             moneyaccess_args.desc = `Pembayaran invoice ${invoice_id} Rp. ${helper.IDRformat(amount)} dari ${auth_user.dataValues.fullname}(${detail_toko[0].dataValues.value}) (${auth_user.dataValues.valdo_account}) ke ${toUser.dataValues.fullname} (${toUser.dataValues.valdoAccount})`;
        }
        Logs.create({action: 'make_payment moneyaccess args', description: util.inspect(moneyaccess_args)});

        let payment_args = {};
        payment_args.url = result_fmcg.dataValues.url;
        payment_args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        payment_args.fmcg_name = result_fmcg.dataValues.name;
        payment_args.dt_code = dt_code;
        payment_args.invoice_id = invoice_id;
        payment_args.amount = amount;
        payment_args.total_potongan = total_potongan;
        payment_args.total_retur = total_retur;
        payment_args.fromUserId = auth_user.dataValues.user_id;
        payment_args.toUserId = toUser.dataValues.user_id;
        payment_args.transact_date = helper.formatCurrentDateTime();
        payment_args.valdo_tx_id = invoice_id + helper.formatCurrentDateTimeWithoutSeparator();
        payment_args.currency_code = 'IDR';
        
        Logs.create({action: 'make_payment payment args', description: util.inspect(payment_args)});

        let api_response_fmcg = await ApiUli.make_payment(payment_args);

        Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} make_payment`, description: util.inspect(api_response_fmcg)});

        let api_response_transfer = await MoneyAccess.transfer(moneyaccess_args);
        
        Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} make_payment transfer`, description: util.inspect(api_response_transfer)});
        
        res.json({code: 200, message: 'Pembayaran invoice berhasil'});
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal menampilkan data transaksi, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}
async function make_payment_ordering(req, res) {
    const token = req.swagger.params.token.value;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_invoice_id = req.swagger.params.invoice_id;
    const param_dt_code = req.swagger.params.dt_code;
    const param_pin = req.swagger.params.pin;
    const param_amount = req.swagger.params.amount;
    const param_total_potongan = req.swagger.params.total_potongan;
    const param_total_retur = req.swagger.params.total_retur;
    const param_vacc_to = req.swagger.params.vacc_to;

    let fmcg_id = _.isUndefined(param_fmcg_id.value)?null:param_fmcg_id.value;
    let invoice_id = _.isUndefined(param_invoice_id.value)?null:param_invoice_id.value;
    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
    let pin = _.isUndefined(param_pin.value)?null:param_pin.value;
    let amount = _.isUndefined(param_amount.value)?null:param_amount.value;
    let total_potongan = _.isUndefined(param_total_potongan.value)?null:param_total_potongan.value;
    let total_retur = _.isUndefined(param_total_retur.value)?0:param_total_retur.value;
    let vacc_to = _.isUndefined(param_vacc_to.value)?null:param_vacc_to.value;

    try {
        Logs.create({action: 'make_payment_ordering', description: helper.getSimpleSwaggerParams(req.swagger.params)});
        let auth_user = await middleware.verify({token: token, acl: acl_get_transactions, is_allowed_access_token_static: false});
        
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
        
        if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal melakukan pembayaran invoice, data fmcg tidak tersedia');
        }

        let expected_pin = await service_user.getHashedPin(auth_user.dataValues.salt, pin);

        if(expected_pin != auth_user.dataValues.pin) {
            throw new CustomError(400, 'Pin salah');
        }

        let balance_args = {};
        balance_args.mobum = auth_user.dataValues.valdo_account;
        balance_args.vaccount = auth_user.dataValues.valdo_account;
        balance_args.usertype = auth_user.dataValues.type;

        Logs.create({action: 'make_payment_ordering balance args', description: util.inspect(balance_args)});

        let api_response_inquiry = await MoneyAccess.inquiry(balance_args);

        Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} make_payment inquiry`, description: util.inspect(api_response_inquiry)});

        if(Number(amount) > Number(api_response_inquiry.balance)) {
            throw new CustomError(400, `Saldo tidak cukup, sisa saldo ${api_response_inquiry.balance}`);
        }
        
        let detail_toko =await service_data.get({user_id:auth_user.dataValues.user_id});
//        let moneyaccess_args = {};
//        moneyaccess_args.trans_type = result_fmcg.dataValues.payment_type;
//        moneyaccess_args.vacc_from = auth_user.dataValues.valdo_account;
//        moneyaccess_args.vacc_to = vacc_to;
//        moneyaccess_args.amount = amount;
//        moneyaccess_args.pin = pin;
//        moneyaccess_args.transId = invoice_id.replace(/\//g, '')+helper.currentMilisecondsTime();
//        moneyaccess_args.desc = `Pembayaran invoice ${invoice_id} Rp. ${helper.IDRformat(amount)} dari ${auth_user.dataValues.fullname}(${detail_toko[0].dataValues.value}) (${auth_user.dataValues.valdo_account}) ke Toko Pandai (${vacc_to})`;

       
//        let api_response_transfer = await MoneyAccess.new_payment(moneyaccess_args);
        
        let moneyaccess_args = {
            trans_type:result_fmcg.dataValues.payment_type,
            status:"success", 
            vacc_from: auth_user.dataValues.valdo_account,
            vacc_to:[
                {vacc_number:vacc_to, amount: parseInt(amount), desc:`Pembayaran invoice ${invoice_id} Rp. ${helper.IDRformat(amount)} dari ${auth_user.dataValues.fullname}(${detail_toko[0].dataValues.value}) (${auth_user.dataValues.valdo_account}) ke Toko Pandai (${vacc_to})`,transid:invoice_id.replace(/\//g, '')+helper.currentMilisecondsTime()}],        
            pin:pin,
            tanggal_mutasi: ''
        }

        request({
            headers: {
                'apigatekey': 'webKEY123456789',
                'Content-Type': 'application/json'
            },
            url: 'http://192.168.12.2/api/v1/multi_vacc_v3/trf',
            method: "POST",
            json: true,
            body: moneyaccess_args
        }, async function (error, result, body) { 
            if(body.status.code == 200){
                 Logs.create({action: 'make_payment_ordering moneyaccess args', description: util.inspect(moneyaccess_args)});
                 res.json({code: 200, message: 'Pembayaran invoice berhasil'});
            }else
            {
                res.status(400).json({code: 400, message: 'Pembayaran invoice Gagal'});
            }
        
        })
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal menampilkan data transaksi, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function cod_payment(req, res) {

    const token = req.swagger.params.token.value;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_invoice_id = req.swagger.params.invoice_id;
    const param_le_code = req.swagger.params.le_code;
    const param_dt_code = req.swagger.params.dt_code;
    const param_phone = req.swagger.params.no_hp_driver;
    const param_otp = req.swagger.params.otp;
    const param_amount = req.swagger.params.amount;
    const param_nama_petugas = req.swagger.params.nama_petugas;
    const param_image = req.swagger.params.image;
    const param_pin = req.swagger.params.pin;

    let fmcg_id = _.isUndefined(param_fmcg_id.value)?null:param_fmcg_id.value;
    let invoice_id = _.isUndefined(param_invoice_id.value)?null:param_invoice_id.value;
    let le_code = _.isUndefined(param_le_code.value)?null:param_le_code.value;
    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
    let phone = _.isUndefined(param_phone.value)?null:param_phone.value;
    let otp = _.isUndefined(param_otp.value)?null:param_otp.value;
    let amount = _.isUndefined(param_amount.value)?null:param_amount.value;
    let nama_petugas = _.isUndefined(param_nama_petugas.value)?null:param_nama_petugas.value;
    let image = _.isUndefined(param_image.value)?null:param_image.value;
    let pin = _.isUndefined(param_pin.value)?null:param_pin.value;
    

    try {

        let path_dir_image = __dirname+'/../../static_file/img_cod/'
        let image_name = 'https://charlie.tokopandai.id/toko-pandai-api/v1/img_cod/'+dt_code+invoice_id+'_'+Date.now()+'.jpg'
        if(image !== null || image !== undefined){
            fs.writeFile(path_dir_image+dt_code+invoice_id+'_'+Date.now()+'.jpg', image, {encoding: 'base64'}, function(err) {
            });
        }
        let auth_user = await middleware.verify({token: token, acl: acl_get_transactions, is_allowed_access_token_static: false});
        let detail_toko = await service_data.get({user_id: auth_user.dataValues.user_id});
        
        let get_dist = await service_user_unique_code.getOne({unique_code: dt_code});
        let detail_dist = await service_user.get({user_id: get_dist.dataValues.user_id});
        
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
        if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal melakukan pembayaran invoice, data fmcg tidak tersedia');
        }

        let desc_payment = `Pembayaran invoice ${invoice_id} Rp. ${helper.IDRformat(amount)} dari ${detail_toko[0].dataValues.value}(${auth_user.dataValues.fullname}) (${auth_user.dataValues.valdo_account}) ke ${detail_dist[0].dataValues.fullname} (${detail_dist[0].dataValues.valdoAccount})`

        let expected_pin = await service_user.getHashedPin(auth_user.dataValues.salt, pin);

        if(expected_pin != auth_user.dataValues.pin) {
            throw new CustomError(400, 'Pin salah');
        }
        if(otp.length>0){
            let args = {}
            args.url = result_fmcg.dataValues.url;
            args.fmcg_token = result_fmcg.dataValues.fmcg_token;
            args.fmcg_name = result_fmcg.dataValues.name;
            args.otp = otp;
            args.no_hp_driver = phone;

            let otp_code = await ApiUli.get_otp_code(args)

            let inv = {}
            inv.url = result_fmcg.dataValues.url;
            inv.fmcg_token = result_fmcg.dataValues.fmcg_token;
            inv.fmcg_name = result_fmcg.dataValues.name;
            inv.invoice_id = invoice_id;
            inv.dt_code = dt_code;

            let invoice = await ApiUli.get_one_invoice(inv)

            if(Number(invoice.balance_amount == 0)){
                throw new CustomError(400, 'Invoice sudah lunas');
            }
            else if(Number(amount) > Number(invoice.balance_amount)){
                throw new CustomError(400, 'Jumlah pembayaran yang anda masukkan lebih besar dari invoice');
            }
            else{

                let invoice_update = {};
                invoice_update.le_code = le_code;
                invoice_update.invoice_id = invoice_id;
                invoice_update.amount = 0;
                invoice_update.fmcg_id = fmcg_id;
                invoice_update.paid_by = "COD";

                await ApiPohonDana.update_payment_fmcg(invoice_update);

                let cod_detail = {}
                cod_detail.url = result_fmcg.dataValues.url;
                cod_detail.fmcg_token = result_fmcg.dataValues.fmcg_token;
                cod_detail.fmcg_name = result_fmcg.dataValues.name;
                cod_detail.nama_petugas = nama_petugas;
                cod_detail.image = image_name;
                cod_detail.invoice_id = invoice_id;
                cod_detail.le_code = le_code;
                cod_detail.dt_code = dt_code;
                cod_detail.amount = amount;
                cod_detail.no_hp_driver = phone;
                cod_detail.invoice_status = 'pending'
                cod_detail.otp = otp;
                cod_detail.android_status = false
                cod_detail.desc_payment = desc_payment
                await ApiUli.cod_payment(cod_detail)

                res.json({code: 200, message: 'success'});
                
            }

        }
        else{

            let inv = {}
            inv.url = result_fmcg.dataValues.url;
            inv.fmcg_token = result_fmcg.dataValues.fmcg_token;
            inv.fmcg_name = result_fmcg.dataValues.name;
            inv.invoice_id = invoice_id;
            inv.dt_code = dt_code;

            let invoice = await ApiUli.get_one_invoice(inv)

            if(Number(invoice.balance_amount == 0)){
                throw new CustomError(400, 'Invoice sudah lunas');
            }
            else if(Number(amount) > Number(invoice.balance_amount)){
                throw new CustomError(400, 'Jumlah pembayaran yang anda masukkan lebih besar dari invoice');
            }
            else{

                let invoice_update = {};
                invoice_update.le_code = le_code;
                invoice_update.invoice_id = invoice_id;
                invoice_update.amount = 0;
                invoice_update.fmcg_id = fmcg_id;
                invoice_update.paid_by = "COD";

                await ApiPohonDana.update_payment_fmcg(invoice_update);

                let cod_detail = {}
                cod_detail.url = result_fmcg.dataValues.url;
                cod_detail.fmcg_token = result_fmcg.dataValues.fmcg_token;
                cod_detail.fmcg_name = result_fmcg.dataValues.name;
                cod_detail.nama_petugas = nama_petugas;
                cod_detail.image = image_name;
                cod_detail.invoice_id = invoice_id;
                cod_detail.le_code = le_code;
                cod_detail.dt_code = dt_code;
                cod_detail.amount = amount;
                cod_detail.no_hp_driver = phone;
                cod_detail.invoice_status = 'pending'
                cod_detail.otp = otp;
                cod_detail.android_status = true
                cod_detail.desc_payment = desc_payment

                await ApiUli.cod_payment(cod_detail)

                res.json({code: 200, message: 'success'});
            }
        }
    
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal menampilkan data transaksi, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function confirm_cod_payment(req, res) {
    const token = req.swagger.params.token.value;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_invoice_id = req.swagger.params.invoice_id;
    const param_le_code = req.swagger.params.le_code;
    const param_dt_code = req.swagger.params.dt_code;
     const param_amount = req.swagger.params.amount;

    let fmcg_id = _.isUndefined(param_fmcg_id.value)?null:param_fmcg_id.value;
    let invoice_id = _.isUndefined(param_invoice_id.value)?null:param_invoice_id.value;
    let le_code = _.isUndefined(param_le_code.value)?null:param_le_code.value;
    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
    let amount = _.isUndefined(param_amount.value)?null:param_amount.value;
    

    try {

        Logs.create({action: 'confirm_cod_payment', description: helper.getSimpleSwaggerParams(req.swagger.params)});
        
        await middleware.verify({token: token, acl: "", is_allowed_access_token_static: true});
        
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
        
        if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal melakukan pembayaran invoice, data fmcg tidak tersedia');
        }

        let args = {}
        args.url = result_fmcg.dataValues.url;
        args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        args.fmcg_name = result_fmcg.dataValues.name;
        args.invoice_id = invoice_id;
        args.dt_code = dt_code;
        args.le_code = le_code;

        let cod_trans = await ApiUli.get_cod_payment(args)
        
        if(cod_trans[0].invoice_status == 'success'){
            throw new CustomError(400, 'Status COD Payment sudah terkonfirmasi');
        }
        else{

            let update = {}
            update.url = result_fmcg.dataValues.url;
            update.fmcg_token = result_fmcg.dataValues.fmcg_token;
            update.fmcg_name = result_fmcg.dataValues.name;
            update.invoice_id = invoice_id;
            update.dt_code = dt_code;
            update.le_code = le_code;
            update.status = 1;
            update.invoice_status = 'success'

            await ApiUli.update_cod_payment(update)

            let invoice_update = {};
            invoice_update.le_code = le_code;
            invoice_update.invoice_id = invoice_id;
            invoice_update.amount = cod_trans[0].amount;
            invoice_update.fmcg_id = fmcg_id;
            invoice_update.paid_by = "COD"

            await ApiPohonDana.update_payment_fmcg(invoice_update);
            let input_to_transactions = {};
            input_to_transactions.fmcg_id = fmcg_id;
            input_to_transactions.dt_code = dt_code;
            input_to_transactions.invoice_id = invoice_id;
            input_to_transactions.amount = parseInt(cod_trans[0].amount);
            input_to_transactions.invoice_code = dt_code+invoice_id;
            input_to_transactions.valdo_tx_id = "COD"+invoice_id+moment().format('YYYYMMDDHHmmss');
            input_to_transactions.distributor_id = 0;
            input_to_transactions.retailer_id = 0;  
            input_to_transactions.from_user_id = 0;
            input_to_transactions.to_user_id = 0;

            await ApiPohonDana.multifmcg_transactions(input_to_transactions)

            res.json({code: 200, message: 'success'});
        }
        
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal menampilkan data transaksi, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function make_repayment_ordering(req, res) {
    const token = req.swagger.params.token.value;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_invoice_id = req.swagger.params.invoice_id;
    const param_dt_code = req.swagger.params.dt_code;
    const param_pin = req.swagger.params.pin;
    const param_amount = req.swagger.params.amount;
    const param_total_potongan = req.swagger.params.total_potongan;
    const param_total_retur = req.swagger.params.total_retur;
    const param_vacc_from = req.swagger.params.vacc_from;
    const param_vacc_to = req.swagger.params.vacc_to;

    let fmcg_id = _.isUndefined(param_fmcg_id.value)?null:param_fmcg_id.value;
    let invoice_id = _.isUndefined(param_invoice_id.value)?null:param_invoice_id.value;
    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
    let pin = _.isUndefined(param_pin.value)?null:param_pin.value;
    let amount = _.isUndefined(param_amount.value)?null:param_amount.value;
    let total_potongan = _.isUndefined(param_total_potongan.value)?null:param_total_potongan.value;
    let total_retur = _.isUndefined(param_total_retur.value)?0:param_total_retur.value;
    let vacc_to = _.isUndefined(param_vacc_to.value)?null:param_vacc_to.value;
    let vacc_from = _.isUndefined(param_vacc_from.value)?null:param_vacc_from.value;
    try {
        Logs.create({action: 'make_payment_ordering', description: helper.getSimpleSwaggerParams(req.swagger.params)});
        let auth_user = await middleware.verify({token: token, acl: acl_get_transactions, is_allowed_access_token_static: false});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
        
        if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal melakukan pembayaran invoice, data fmcg tidak tersedia');
        }
        
        let user_unique_code = await service_pelunasan_manual.get_dt_unicode({dt_code: dt_code});
        console.log(user_unique_code,"+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
        if(user_unique_code == null) {
            throw new CustomError(400, 'Gagal melakukan pembayaran invoice, distributor tidak di temukan');
        }

        let balance_args = {};
        balance_args.mobum = auth_user.dataValues.valdo_account;
        balance_args.vaccount = auth_user.dataValues.valdo_account;
        balance_args.usertype = auth_user.dataValues.type;

        Logs.create({action: 'make_payment_ordering balance args', description: util.inspect(balance_args)});

        let api_response_inquiry = await MoneyAccess.inquiry(balance_args);

        Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} make_payment inquiry`, description: util.inspect(api_response_inquiry)});

//        if(Number(amount) > Number(api_response_inquiry.balance)) {
//            throw new CustomError(400, `Saldo tidak cukup, sisa saldo ${api_response_inquiry.balance}`);
//        }
        
        let detail_toko =await service_data.get({user_id:auth_user.dataValues.user_id});

        let moneyaccess_args = {};
        moneyaccess_args.mobnum = vacc_from;
        moneyaccess_args.from = vacc_from;
        moneyaccess_args.to = vacc_to;
        moneyaccess_args.amount = amount;
        moneyaccess_args.pin = pin;
        moneyaccess_args.usertype = auth_user.dataValues.type;
        moneyaccess_args.transId = invoice_id.replace(/\//g, '')+helper.currentMilisecondsTime();
        
        moneyaccess_args.desc = `Pembayaran invoice ${invoice_id} Rp. ${helper.IDRformat(amount)} dari ${auth_user.dataValues.fullname}(${detail_toko[0].dataValues.value}) (${auth_user.dataValues.valdo_account}) ke ${user_unique_code[0].fullname} (${user_unique_code[0].valdo_account})`;
        Logs.create({action: 'make_payment_ordering moneyaccess args', description: util.inspect(moneyaccess_args)});
        let api_response_transfer = await MoneyAccess.transfer(moneyaccess_args);
        
        let input_to_transactions = {};
        input_to_transactions.fmcg_id = fmcg_id;
        input_to_transactions.dt_code = dt_code;
        input_to_transactions.invoice_id = invoice_id;
        input_to_transactions.amount = amount;
        input_to_transactions.invoice_code = dt_code+invoice_id;
        input_to_transactions.valdo_tx_id = "GP"+invoice_id+moment().format('YYYYMMDDHHmmss');
        input_to_transactions.distributor_id = 0;
        input_to_transactions.retailer_id = 0;  
        input_to_transactions.from_user_id = 0;
        input_to_transactions.to_user_id = 0;

        await ApiPohonDana.multifmcg_transactions(input_to_transactions)
        
        Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} make_payment transfer`, description: util.inspect(api_response_transfer)});
        res.json({code: 200, message: 'Pembayaran invoice berhasil'});
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal menampilkan data transaksi, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function make_payment_ordering_cancel(req, res) {
    const token = req.swagger.params.token.value;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_invoice_id = req.swagger.params.invoice_id;
    const param_dt_code = req.swagger.params.dt_code;
    const param_pin = req.swagger.params.pin;
    const param_amount = req.swagger.params.amount;
    const param_total_potongan = req.swagger.params.total_potongan;
    const param_total_retur = req.swagger.params.total_retur;
    const param_vacc_from = req.swagger.params.vacc_from;
    const param_vacc_to = req.swagger.params.vacc_to;

    let fmcg_id = _.isUndefined(param_fmcg_id.value)?null:param_fmcg_id.value;
    let invoice_id = _.isUndefined(param_invoice_id.value)?null:param_invoice_id.value;
    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
    let pin = _.isUndefined(param_pin.value)?null:param_pin.value;
    let amount = _.isUndefined(param_amount.value)?null:param_amount.value;
    let total_potongan = _.isUndefined(param_total_potongan.value)?null:param_total_potongan.value;
    let total_retur = _.isUndefined(param_total_retur.value)?0:param_total_retur.value;
    let vacc_to = _.isUndefined(param_vacc_to.value)?null:param_vacc_to.value;
    let vacc_from = _.isUndefined(param_vacc_from.value)?null:param_vacc_from.value;
    try {
        Logs.create({action: 'make_payment_ordering', description: helper.getSimpleSwaggerParams(req.swagger.params)});
        let auth_user = await middleware.verify({token: token, acl: acl_get_transactions, is_allowed_access_token_static: false});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
        
        if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal melakukan pembayaran invoice, data fmcg tidak tersedia');
        }

//        let expected_pin = await service_user.getHashedPin(auth_user.dataValues.salt, pin);
//
//        if(expected_pin != auth_user.dataValues.pin) {
//            throw new CustomError(400, 'Pin salah');
//        }

        let user_unique_code = await service_user_unique_code.getOne({unique_code: dt_code});

        if(user_unique_code == null) {
            throw new CustomError(400, 'Gagal melakukan pembayaran invoice, distributor tidak di temukan');
        }

        let toUser = await service_user.getOne({user_id: user_unique_code.dataValues.user_id});

        if(toUser == null) {
            throw new CustomError(400, 'Gagal melakukan pembayaran invoice, distributor tidak di temukan');
        }

        let balance_args = {};
        balance_args.mobum = auth_user.dataValues.valdo_account;
        balance_args.vaccount = auth_user.dataValues.valdo_account;
        balance_args.usertype = auth_user.dataValues.type;

        Logs.create({action: 'make_payment_ordering balance args', description: util.inspect(balance_args)});

        let api_response_inquiry = await MoneyAccess.inquiry(balance_args);

        Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} make_payment inquiry`, description: util.inspect(api_response_inquiry)});

        if(Number(amount) > Number(api_response_inquiry.balance)) {
            throw new CustomError(400, `Saldo tidak cukup, sisa saldo ${api_response_inquiry.balance}`);
        }
        
        let detail_toko =await service_data.get({user_id:auth_user.dataValues.user_id});

        let moneyaccess_args = {};
        moneyaccess_args.mobnum = vacc_from;
        moneyaccess_args.from = vacc_from;
        moneyaccess_args.to = vacc_to;
        moneyaccess_args.amount = amount;
        moneyaccess_args.pin = pin;
        moneyaccess_args.usertype = auth_user.dataValues.type;
        moneyaccess_args.transId = invoice_id.replace(/\//g, '')+helper.currentMilisecondsTime();
        
        moneyaccess_args.desc = `Cancel Pembayaran invoice ${invoice_id} Rp. ${helper.IDRformat(amount)} dari ${auth_user.dataValues.fullname}(${detail_toko[0].dataValues.value}) (${auth_user.dataValues.valdo_account}) ke ${toUser.dataValues.fullname} (${toUser.dataValues.valdoAccount})`;
        Logs.create({action: 'make_payment_ordering moneyaccess args', description: util.inspect(moneyaccess_args)});
        let api_response_transfer = await MoneyAccess.transfer(moneyaccess_args);
        Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} make_payment transfer`, description: util.inspect(api_response_transfer)});
        res.json({code: 200, message: 'Pembayaran invoice berhasil'});
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal menampilkan data transaksi, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function new_get_transactions(req, res) {
    const token = req.swagger.params.token.value;
    const param_user_id = req.swagger.params.user_id;
    const param_email = req.swagger.params.email;
    const param_type = req.swagger.params.type;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_star_date = req.swagger.params.start_date;
    const param_end_date = req.swagger.params.end_date;
    const param_page = req.swagger.params.page;
    const param_item_per_page = req.swagger.params.item_per_page;

    let user_id = _.isUndefined(param_user_id.value)?null:param_user_id.value;
    let email = _.isUndefined(param_email.value)?null:param_email.value;
    let type = _.isUndefined(param_type.value)?null:param_type.value;
    let fmcg_id = _.isUndefined(param_fmcg_id.value)?null:param_fmcg_id.value;
    let start_date = _.isUndefined(param_star_date.value)?null:param_star_date.value;
    let end_date = _.isUndefined(param_end_date.value)?null:param_end_date.value;
    let page = _.isUndefined(param_page.value)?1:param_page.value;
    let item_per_page = _.isUndefined(param_item_per_page.value)?15:param_item_per_page.value;
    let vacc_number, user = '';
    let d = new Date();
    d.setDate(d.getDate() - 30);

    page = (page < 1 ) ? 1 : page;

    try {
        Logs.create({action: 'get_transactions', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_get_transactions, is_allowed_access_token_static: false});

        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id:fmcg_id});

        if(user_id) {
            user = await service_user.getOne({user_id: user_id});

            if(user == null) {
                throw new CustomError(400, 'Data user tidak di temukan');
            }

            vacc_number = user.dataValues.valdoAccount
        } else if(email) {
            
            user = await service_user.getOne({email: email});

            if(user == null) {
                throw new CustomError(400, 'Data user tidak di temukan');
            }

            vacc_number = user.dataValues.valdoAccount
        } else {
            vacc_number = auth_user.dataValues.valdo_account
        }

        if(!start_date) {
            start_date = helper.formatDate(d);
        }

        if(!end_date) {
            end_date = helper.formatCurrentDate();
        }

         let args = {};
        args.vacc_number = vacc_number;
        if(fmcg_id !== 0){args.type = result_fmcg.dataValues.payment_type;}
        args.start_date = start_date;
        args.end_date = end_date;
        args.pagenum = page;
        args.item_per_page = item_per_page;
        args.usertype = auth_user.dataValues.type;
        let api_response= await MoneyAccess.history(args);
        Logs.create({action: 'api response money access get_transactions history', description: util.inspect(api_response)});
        let transactions = TransactionAdapter.parseHistory({response: api_response});
        let out = {};
        out.size = transactions['itempage'];
        out.page = transactions['pagenumber'];
        out.items_per_page = item_per_page;
        out.total_pages = transactions['totalpage'];
        out.total_items = transactions['totalitem'];
        out.transactions = transactions;

        res.json(out);
        // }
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal menampilkan data transaksi, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function new_make_payment(req, res) {
    const token = req.swagger.params.token.value;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_invoice_id = req.swagger.params.invoice_id;
    const param_dt_code = req.swagger.params.dt_code;
    const param_pin = req.swagger.params.pin;
    const param_amount = req.swagger.params.amount;
    const param_total_potongan = req.swagger.params.total_potongan;
    const param_total_retur = req.swagger.params.total_retur;
    const param_retur = req.swagger.params.total_retur_detail;

    let fmcg_id = _.isUndefined(param_fmcg_id.value)?null:param_fmcg_id.value;
    let invoice_id = _.isUndefined(param_invoice_id.value)?null:param_invoice_id.value;
    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
    let pin = _.isUndefined(param_pin.value)?null:param_pin.value;
    let amount = _.isUndefined(param_amount.value)?null:param_amount.value;
    let total_potongan = _.isUndefined(param_total_potongan.value)?null:param_total_potongan.value;
    let total_retur = _.isUndefined(param_total_retur.value)?0:param_total_retur.value;
    let retur = _.isUndefined(param_retur.value)?null:param_retur.value;

    try {
        Logs.create({action: 'make_payment', description: helper.getSimpleSwaggerParams(req.swagger.params)});
        let auth_user = await middleware.verify({token: token, acl: acl_get_transactions, is_allowed_access_token_static: false});
        
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
        
        if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal melakukan pembayaran invoice, data fmcg tidak tersedia');
        }

        let expected_pin = await service_user.getHashedPin(auth_user.dataValues.salt, pin);

        if(expected_pin != auth_user.dataValues.pin) {
            throw new CustomError(400, 'Pin salah');
        }

        let user_unique_code = await service_user_unique_code.getOne({unique_code: dt_code});

        if(user_unique_code == null) {
            throw new CustomError(400, 'Gagal melakukan pembayaran invoice, distributor tidak di temukan');
        }

        let toUser = await service_user.getOne({user_id: user_unique_code.dataValues.user_id});

        if(toUser == null) {
            throw new CustomError(400, 'Gagal melakukan pembayaran invoice, distributor tidak di temukan');
        }

        let balance_args = {};
        balance_args.mobum = auth_user.dataValues.valdo_account;
        balance_args.vaccount = auth_user.dataValues.valdo_account;
        balance_args.usertype = auth_user.dataValues.type;

        Logs.create({action: 'make_payment balance args', description: util.inspect(balance_args)});

        let api_response_inquiry = await MoneyAccess.inquiry(balance_args);

        Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} make_payment inquiry`, description: util.inspect(api_response_inquiry)});

        if(Number(amount) > Number(api_response_inquiry.balance)) {
            throw new CustomError(400, `Saldo tidak cukup, sisa saldo ${api_response_inquiry.balance}`);
        }
        
        let detail_toko =await service_data.get({user_id:auth_user.dataValues.user_id});
        let desc
        let trans_type
        let transId = invoice_id.replace(/\//g, '')+helper.currentMilisecondsTime();

        if(fmcg_id ==24){
            desc = `Top UP ${invoice_id} Rp. ${helper.IDRformat(amount)} dari ${auth_user.dataValues.fullname}(${detail_toko[0].dataValues.value}) (${auth_user.dataValues.valdo_account}) ke ${toUser.dataValues.fullname} (${toUser.dataValues.valdoAccount})`;
        }else{
            desc = `Pembayaran invoice ${invoice_id} Rp. ${helper.IDRformat(amount)} dari ${auth_user.dataValues.fullname}(${detail_toko[0].dataValues.value}) (${auth_user.dataValues.valdo_account}) ke ${toUser.dataValues.fullname} (${toUser.dataValues.valdoAccount})`;
        }

        if(auth_user.dataValues.type == 2){
            trans_type = 'pickup'
        }else{
            trans_type = result_fmcg.dataValues.payment_type
        }

        let moneyaccess_args = {
            trans_type:trans_type,
            status:"success", 
            vacc_from: auth_user.dataValues.valdo_account,
            vacc_to:[
                {vacc_number:toUser.dataValues.valdoAccount, amount: parseInt(amount), desc:desc,transid:transId}],        
            pin:pin,
            tanggal_mutasi: ''
        }

        request({
            headers: {
                'apigatekey': 'webKEY123456789',
                'Content-Type': 'application/json'
            },
            url: 'http://192.168.12.2/api/v1/multi_vacc_v3/trf',
            method: "POST",
            json: true,
            body: moneyaccess_args
        }, async function (error, result, body) { 
            if(body.status.code == 200){
                
                Logs.create({action: 'make_payment moneyaccess args', description: util.inspect(moneyaccess_args)});
        
                let payment_args = {};
                payment_args.url = result_fmcg.dataValues.url;
                payment_args.fmcg_token = result_fmcg.dataValues.fmcg_token;
                payment_args.fmcg_name = result_fmcg.dataValues.name;
                payment_args.dt_code = dt_code;
                payment_args.invoice_id = invoice_id;
                payment_args.amount = amount;
                payment_args.total_potongan = total_potongan;
                payment_args.total_retur = total_retur;
                payment_args.fromUserId = auth_user.dataValues.user_id;
                payment_args.toUserId = toUser.dataValues.user_id;
                payment_args.transact_date = helper.formatCurrentDateTime();
                payment_args.valdo_tx_id = invoice_id + helper.formatCurrentDateTimeWithoutSeparator();
                payment_args.currency_code = 'IDR';
                payment_args.fmcg_id = fmcg_id;
                payment_args.retur = retur;
                Logs.create({action: 'make_payment payment args', description: util.inspect(payment_args)});

                let api_response_fmcg = await ApiUli.make_payment(payment_args);
                Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} make_payment`, description: util.inspect(api_response_fmcg)});
                
                res.json({code: 200, message: 'Pembayaran invoice berhasil'});
            }else{
                res.status(400).json({code: 400, message: body.status.desc});
            }
        })
        
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal menampilkan data transaksi, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function cod_check_phone(req, res) {
    const token = req.swagger.params.token.value;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_phone = req.swagger.params.no_hp_driver;

    let phone = _.isUndefined(param_phone.value)?null:param_phone.value;
    let fmcg_id = _.isUndefined(param_fmcg_id.value)?null:param_fmcg_id.value;

    try {

        await middleware.verify({token: token, acl: acl_get_transactions, is_allowed_access_token_static: false});

        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
        if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal melakukan pembayaran invoice, data fmcg tidak tersedia');
        }
        
        request({
            headers: {
                'x-api-key': '$2a$10$QNB/3KKnXvzSRQMd/stp1eDEHbtZHlAaKfeTKKJ9R5.OtUnEgnrA6'
            },
            host:'http://192.168.13.5',
            port: '9002',
            uri: 'http://192.168.13.5:9002/api_agenttokopandai/v2/check_va_hp/'+phone,
            method: "GET",
        }, async function (error, result, body) {
            let response = JSON.parse(body)
            let out = {}

            if(response.status == 200){
                
                if(response.android == false){
                    let otp = Math.floor(Math.random()*1000000)
                    if(otp.length !== 6){
                        otp = Math.floor(Math.random()*1000000)
                    }

                    let msisdn = phone.split('')
                    msisdn[0] = '62'
                    msisdn = msisdn.join('')

                    let message = `Gunakan kode OTP ${otp} ini untuk request pembayaran Cash on Delivery (COD).`

                    request({
                        headers: {},
                        url: 'http://ginsms.gratika.id:8100/sendSms.do?username=tokopandai&password=TokoPandai1234&senderid=TOKOPANDAI&pesan='+message+'&msisdn='+msisdn+'',
                        method: "POST",
                    }, async function (error, result, body) {
                    })

                    let args = {}
                    args.url = result_fmcg.dataValues.url;
                    args.fmcg_token = result_fmcg.dataValues.fmcg_token;
                    args.fmcg_name = result_fmcg.dataValues.name;
                    args.otp = otp;
                    args.no_hp_driver = phone;
                    args.android_status = response.android;

                    await ApiUli.insert_cod_otp(args)

                }

                out.code = response.status
                out.message = response.message
                out.android = response.android
                res.status(200).json(out)

            }else{

                out.code = response.status
                out.message = response.message
                res.status(400).json(out)
            }
        })

    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal menampilkan data transaksi, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function other_payment(req, res) {
    const token = req.swagger.params.token.value;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_pin = req.swagger.params.pin;
    const param_invoice_id = req.swagger.params.invoice_id;
    const param_dt_code = req.swagger.params.dt_code;
    const param_le_code = req.swagger.params.le_code;
    const param_amount = req.swagger.params.amount;
    const param_approvalcode = req.swagger.params.approvalcode;
    const param_bank = req.swagger.params.bank;
    const param_tanggal_cair = req.swagger.params.tanggal_cair;
    const param_payment_method = req.swagger.params.payment_method;

    let fmcg_id = _.isUndefined(param_fmcg_id.value)?null:param_fmcg_id.value;
    let pin = _.isUndefined(param_pin.value)?null:param_pin.value;
    let invoice_id = _.isUndefined(param_invoice_id.value)?"":param_invoice_id.value;
    let dt_code = _.isUndefined(param_dt_code.value)?"":param_dt_code.value;
    let le_code = _.isUndefined(param_le_code.value)?"":param_le_code.value;
    let amount = _.isUndefined(param_amount.value)?"":param_amount.value;
    let approvalcode = _.isUndefined(param_approvalcode.value)?"0":param_approvalcode.value;
    let tanggal_cair = _.isUndefined(param_tanggal_cair.value)?"0":param_tanggal_cair.value;
    let bank = _.isUndefined(param_bank.value)?"":param_bank.value;
    let payment_method = _.isUndefined(param_payment_method.value)?"":param_payment_method.value;

    try {
        Logs.create({action: 'other_payment', description: helper.getSimpleSwaggerParams(req.swagger.params)});
        let auth_user = await middleware.verify({token: token, acl: acl_get_transactions, is_allowed_access_token_static: false});
        
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
        
        if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal melakukan pembayaran invoice, data fmcg tidak tersedia');
        }
        if(fmcg_id !== 19){
            throw new CustomError(400, `Tidak dapat melakukan pembayaran invoice ${result_fmcg.dataValues.name} dengan ${payment_method}`)
        }

        let expected_pin = await service_user.getHashedPin(auth_user.dataValues.salt, pin);

        if(expected_pin != auth_user.dataValues.pin) {
            throw new CustomError(400, 'Pin salah');
        }
        let payment_args = {
            invoice_id: invoice_id,
            dt_code: dt_code,
            le_code: le_code,
            amount: amount,
            tanggal_cair: tanggal_cair,
            bank: bank,
            approvalcode: approvalcode,
            payment_method: payment_method
        }

        request({
            headers: {
                'token': '1f8l4ckp1nk1ny0ur4r34th3n91rlS93n3r4t10nm4k3y0urf33lth3h34tth1S1SSh0S13y0ukn0w'
            },
            url: 'http://localhost:10016/other-payment',
            method: "POST",
            json: true,
            body: payment_args
        },async function (error, result, body){
            
            let inv = {}
            inv.url = result_fmcg.dataValues.url;
            inv.fmcg_token = result_fmcg.dataValues.fmcg_token;
            inv.fmcg_name = result_fmcg.dataValues.name;
            inv.invoice_id = invoice_id;
            inv.dt_code = dt_code;

            let hasil = await ApiUli.get_one_invoice(inv)
            if(body.status === 'success'){
                res.status(200).json({code:200, message: 'Pembayaran invoice berhasil'})
            }
            else{
                res.status(400).json({code:400, message: body.message})
            }
        })
            
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal menampilkan data transaksi, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function update_other_payment(req, res) {
    const token = req.swagger.params.token.value;
    const param_invoice_id = req.swagger.params.invoice_id;
    const param_dt_code = req.swagger.params.dt_code;

    let invoice_id = _.isUndefined(param_invoice_id.value)?"":param_invoice_id.value;
    let dt_code = _.isUndefined(param_dt_code.value)?"":param_dt_code.value;

    try {
        Logs.create({action: 'other_payment', description: helper.getSimpleSwaggerParams(req.swagger.params)});
        await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});

        let args = {
            invoice_id: invoice_id,
            dt_code: dt_code
        }

        request({
            headers: {
                'token': '1f8l4ckp1nk1ny0ur4r34th3n91rlS93n3r4t10nm4k3y0urf33lth3h34tth1S1SSh0S13y0ukn0w'
            },
            url: 'http://192.168.12.6:10016/other-payment/update',
            method: "POST",
            json: true,
            body: args
        }, function (error, result, body){
            if(body.status === 'success'){
                res.status(200).json({code:200, message: 'Success'})
            }
            else{
                res.status(400).json({code:400, message: body.message})
            }
        })
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal menampilkan data transaksi, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function new_ondemand_payment(req, res) {
    const token = req.swagger.params.token.value;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_invoice_id = req.swagger.params.invoice_id;
    const param_dt_code = req.swagger.params.dt_code;
    const param_pin = req.swagger.params.pin;
    const param_amount = req.swagger.params.amount;
    const param_total_potongan = req.swagger.params.total_potongan;
    const param_total_retur = req.swagger.params.total_retur;
    const param_retur = req.swagger.params.total_retur_detail;

    let fmcg_id = _.isUndefined(param_fmcg_id.value)?null:param_fmcg_id.value;
    let invoice_id = _.isUndefined(param_invoice_id.value)?null:param_invoice_id.value;
    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
    let pin = _.isUndefined(param_pin.value)?null:param_pin.value;
    let amount = _.isUndefined(param_amount.value)?null:param_amount.value;
    let total_potongan = _.isUndefined(param_total_potongan.value)?null:param_total_potongan.value;
    let total_retur = _.isUndefined(param_total_retur.value)?0:param_total_retur.value;
    let retur = _.isUndefined(param_retur.value)?null:param_retur.value;

    try {
        Logs.create({action: 'make_payment', description: helper.getSimpleSwaggerParams(req.swagger.params)});
        let auth_user = await middleware.verify({token: token, acl: acl_get_transactions, is_allowed_access_token_static: false});
        
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
        
        if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal melakukan pembayaran invoice, data fmcg tidak tersedia');
        }

        let expected_pin = await service_user.getHashedPin(auth_user.dataValues.salt, pin);

        if(expected_pin != auth_user.dataValues.pin) {
            throw new CustomError(400, 'Pin salah');
        }

        let user_unique_code = await service_user_unique_code.getOne({unique_code: dt_code});

        if(user_unique_code == null) {
            throw new CustomError(400, 'Gagal melakukan pembayaran invoice, distributor tidak di temukan');
        }

        let toUser = await service_user.getOne({user_id: user_unique_code.dataValues.user_id});

        if(toUser == null) {
            throw new CustomError(400, 'Gagal melakukan pembayaran invoice, distributor tidak di temukan');
        }

        let balance_args = {};
        balance_args.mobum = auth_user.dataValues.valdo_account;
        balance_args.vaccount = auth_user.dataValues.valdo_account;
        balance_args.usertype = auth_user.dataValues.type;

        Logs.create({action: 'make_payment balance args', description: util.inspect(balance_args)});

        let api_response_inquiry = await MoneyAccess.inquiry(balance_args);

        Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} make_payment inquiry`, description: util.inspect(api_response_inquiry)});

        if(Number(amount) > Number(api_response_inquiry.balance)) {
            throw new CustomError(400, `Saldo tidak cukup, sisa saldo ${api_response_inquiry.balance}`);
        }
        
        let detail_toko =await service_data.get({user_id:auth_user.dataValues.user_id});
        let desc
        let trans_type
        let transId = invoice_id.replace(/\//g, '')+helper.currentMilisecondsTime();

        if(fmcg_id ==24){
            desc = `Top UP ${invoice_id} Rp. ${helper.IDRformat(amount)} dari ${auth_user.dataValues.fullname}(${detail_toko[0].dataValues.value}) (${auth_user.dataValues.valdo_account}) ke ${toUser.dataValues.fullname} (${toUser.dataValues.valdoAccount})`;
        }else{
            desc = `Pembayaran invoice ${invoice_id} Rp. ${helper.IDRformat(amount)} dari ${auth_user.dataValues.fullname}(${detail_toko[0].dataValues.value}) (${auth_user.dataValues.valdo_account}) ke ${toUser.dataValues.fullname} (${toUser.dataValues.valdoAccount})`;
        }

        if(auth_user.dataValues.type == 2){
            trans_type = 'pickup'
        }else{
            trans_type = result_fmcg.dataValues.payment_type
        }

        let moneyaccess_args = {
            trans_type:trans_type,
            status:"success", 
            vacc_from: auth_user.dataValues.valdo_account,
            vacc_to:[
                {vacc_number:toUser.dataValues.valdoAccount, amount: parseInt(amount), desc:desc,transid:transId}],        
            pin:pin,
            tanggal_mutasi: ''
        }
        
        let payment_args = {};
        payment_args.url = result_fmcg.dataValues.url;
        payment_args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        payment_args.fmcg_name = result_fmcg.dataValues.name;
        payment_args.dt_code = dt_code;
        payment_args.invoice_id = invoice_id;
        payment_args.amount = amount;
        payment_args.total_potongan = total_potongan;
        payment_args.total_retur = total_retur;
        payment_args.fromUserId = auth_user.dataValues.user_id;
        payment_args.toUserId = toUser.dataValues.user_id;
        payment_args.transact_date = helper.formatCurrentDateTime();
        payment_args.valdo_tx_id = invoice_id + helper.formatCurrentDateTimeWithoutSeparator();
        payment_args.currency_code = 'IDR';
        payment_args.fmcg_id = fmcg_id;
        payment_args.retur = retur;
        Logs.create({action: 'make_payment payment args', description: util.inspect(payment_args)});

        let api_response_fmcg = await ApiUli.make_payment(payment_args);
        
        if(api_response_fmcg.code === 400){
            throw new CustomError(400, `${api_response_fmcg.message}`);
        }
        request({
            headers: {
                'apigatekey': 'webKEY123456789',
                'Content-Type': 'application/json'
            },
            url: 'http://192.168.12.2/api/v1/multi_vacc_v3/trf',
            method: "POST",
            json: true,
            body: moneyaccess_args
        }, async function (error, result, body) { 
            if(body.status.code == 200){
                
                Logs.create({action: 'make_payment moneyaccess args', description: util.inspect(moneyaccess_args)});
        
                Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} make_payment`, description: util.inspect(api_response_fmcg)});
                
                res.json({code: 200, message: 'Pembayaran invoice berhasil'});
            }else{
                res.status(400).json({code: 400, message: body.status.desc});
            }
        })
        
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal menampilkan data transaksi, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

export { get_transactions }
export { new_get_transactions }
export { get_summary }
export { make_payment }
export { new_make_payment }
export { make_payment_ordering }
export { cod_payment }
export { cod_check_phone }
export { confirm_cod_payment }
export { make_repayment_ordering }
export { make_payment_ordering_cancel }
export { other_payment }
export { update_other_payment }
export { new_ondemand_payment }