import _ from 'lodash'
import util from 'util'

import middleware from '~/api/controllers/auth'

import helper from '~/utils/helper'

import service_fmcg from '~/services/fmcg'
import service_user_additional_data from '~/services/user_additional_data'

import { Logs } from '~/orm/index'

import FmcgNotFoundError from '~/response/error/FmcgNotFoundError'
import CustomError from '~/response/error/CustomError'
import CustomSuccess from '~/response/success/CustomSuccess'

const acl_add_additional_data = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center','finance'];
const acl_edit_additional_data = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center','finance'];

async function add_additional_data(req, res) {
    const token = req.swagger.params.token.value;
    const field = req.swagger.params.field.value;
    const value = req.swagger.params.value.value;

    try {
        Logs.create({action: 'add_additional_data', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_add_additional_data, is_allowed_access_token_static: false});
        let args = {};
        args.user_id = auth_user.dataValues.user_id;
        args.field = field;
        args.value = value;

        service_user_additional_data.add(args);

        res.json(new CustomSuccess(200, 'Data tambahan user berhasil di tambahkan'));
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal menambahkan data tambahan user, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function edit_additional_data(req, res) {
    const token = req.swagger.params.token.value;
    const field = req.swagger.params.field.value;
    const value = req.swagger.params.value.value;

    try {
        Logs.create({action: 'edit_additional_data', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_edit_additional_data, is_allowed_access_token_static: false});

        if(field.length != value.length) {
            throw new CustomError(400, 'Jumlah field dan value tidak sama');
        }

        for (let i = 0; i < field.length; i++) {
            service_user_additional_data.edit({user_id: auth_user.dataValues.user_id, field: field[i], value: value[i]});
        }

        res.json(new CustomSuccess(200, 'Data tambahan user berhasil di ubah'));
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal mengubah data tambahan user, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

export { add_additional_data }
export { edit_additional_data }