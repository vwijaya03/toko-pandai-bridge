class FmcgNotFoundError extends Error {
	constructor(code, message) {
        super(message);
        this.code = 400;
        this.message = 'FMCG tidak di temukan';
    }
}

export default FmcgNotFoundError