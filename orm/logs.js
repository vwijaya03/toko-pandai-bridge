import { orm } from '~/services/mariadb'
import Sequelize from 'sequelize'
import moment from 'moment'

const Logs = orm.define('logs', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    action: {
        type: Sequelize.STRING, allowNull:false, field:'action'
    },
    description: {
        type: Sequelize.STRING, allowNull:false, field:'description'
    },
    additional_description: {
        type: Sequelize.STRING, allowNull:true, field:'additional_description'
    },
    created_at: {
        type: Sequelize.DATE, allowNull:true, field:'created_at'
    },
    updated_at: {
        type: Sequelize.DATE, allowNull:true, field:'updated_at'
    }
}, {
    freezeTableName: true, // Model tableName will be the same as the model name
    timestamps: false
});

export default Logs