const fs = require('fs');
const path = require('path');

//require('source-map-support').install();
var xl = require('excel4node');


async function generateExcel(data_json,header,footer){
    let data1 = JSON.parse(JSON.stringify(data_json));
    let data2 = JSON.stringify(data1);
    let obj_value = JSON.parse(data2);

    var wb = new xl.Workbook({
        defaultFont: {
            name: 'Verdana',
            size: 12
        },
        dateFormat: 'mm/dd/yyyy hh:mm:ss',
        logLevel: 1,
        workbookView: {
            windowWidth: 28800,
            windowHeight: 17620,
            xWindow: 240,
            yWindow: 480,
        },
        author: 'Workbook Author Name'
    });
    var multiLineStyle = wb.createStyle({
        alignment: {
            wrapText: true,
            vertical: 'top'
        }
    });
    var largeText = wb.createStyle({
        font: {
            name: 'Cambria',
            size: 20
        }
    });
    var medText = wb.createStyle({
        font: {
            name: 'Cambria',
            size: 14,
            color: '#D4762C'
        },
        alignment: {
            vertical: 'center'
        }
    });    
    var currencyStyle = wb.createStyle({
        numberFormat: '$##0.00; [Red]($##0.00); $0.00'
    });    
    var invoiceWS = wb.addWorksheet('Invoice', {
        pageSetup: {
            fitToWidth: 1
        },
        headerFooter: {
            oddHeader: header,
            oddFooter: footer
        }
    });
    let total_cash_memo_total_amount = 0;
    let total_cash_memo_balance_amount = 0;
    let total_invoice_payment_status_paid = 0;
    let total_invoice_payment_status_unpaid = 0;

    invoiceWS.cell(1, 1).string('id');
    invoiceWS.cell(1, 2).string('session_id');
    invoiceWS.cell(1, 3).string('file_name');
    invoiceWS.cell(1, 4).string('dt_code');
    invoiceWS.cell(1, 5).string('le_code');
    invoiceWS.cell(1, 6).string('outlet_code');
    invoiceWS.cell(1, 7).string('cash_memo_total_amount');
    invoiceWS.cell(1, 8).string('cash_memo_balance_amount');
    invoiceWS.cell(1, 9).string('cashmemo_type');
    invoiceWS.cell(1, 10).string('invoice_id');
    invoiceWS.cell(1, 11).string('invoice_sales_date');
    invoiceWS.cell(1, 12).string('invoice_due_date');
    invoiceWS.cell(1, 13).string('invoice_payment_status_paid');
    invoiceWS.cell(1, 14).string('invoice_payment_status_unpaid');
    invoiceWS.cell(1, 15).string('invoice_details');
    invoiceWS.cell(1, 16).string('approval_date');
    invoiceWS.cell(1, 17).string('status');
    
    for (var i = 2; i <= obj_value.length+1; i++) {
        try{ invoiceWS.cell(i, 1).string(obj_value[i-2].id.toString());}
        catch(err){ invoiceWS.cell(i, 1).string(''); }
        try{ invoiceWS.cell(i, 2).string(obj_value[i-2].session_id.toString()); }
        catch(err){ invoiceWS.cell(i, 2).string(''); }
        try{ invoiceWS.cell(i, 3).string(obj_value[i-2].file_name.toString()); }
        catch(err){ invoiceWS.cell(i, 3).string(''); }
        try{ invoiceWS.cell(i, 4).string(obj_value[i-2].dt_code.toString()); }
        catch(err){ invoiceWS.cell(i, 4).string(''); }
        try{ invoiceWS.cell(i, 5).string(obj_value[i-2].le_code.toString()); }
        catch(err){ invoiceWS.cell(i, 5).string(''); }
        try{ invoiceWS.cell(i, 6).string(obj_value[i-2].outlet_code.toString()); }
        catch(err){ invoiceWS.cell(i, 6).string(''); }
        try{ invoiceWS.cell(i, 7).string(obj_value[i-2].cash_memo_total_amount.toString()); 
            total_cash_memo_total_amount=total_cash_memo_total_amount+parseFloat(obj_value[i-2].cash_memo_total_amount);}
        catch(err){ invoiceWS.cell(i, 7).string(''); }
        try{ invoiceWS.cell(i, 8).string(obj_value[i-2].cash_memo_balance_amount.toString()); 
            total_cash_memo_balance_amount=total_cash_memo_balance_amount+parseFloat(obj_value[i-2].cash_memo_balance_amount);}
        catch(err){ invoiceWS.cell(i, 8).string(''); }
        try{ invoiceWS.cell(i, 9).string(obj_value[i-2].cashmemo_type.toString()); }
        catch(err){ invoiceWS.cell(i, 9).string(''); }
        try{  invoiceWS.cell(i, 10).string(obj_value[i-2].invoice_id.toString()); }
        catch(err){ invoiceWS.cell(i, 10).string(''); }
        try{ invoiceWS.cell(i, 11).string(obj_value[i-2].invoice_sales_date.toString()); }
        catch(err){ invoiceWS.cell(i, 11).string(''); }
        try{ invoiceWS.cell(i, 12).string(obj_value[i-2].invoice_due_date.toString()); }
        catch(err){ invoiceWS.cell(i, 12).string(''); }
        try{ invoiceWS.cell(i, 13).string(obj_value[i-2].invoice_payment_status_paid.toString()); 
            total_invoice_payment_status_paid=total_invoice_payment_status_paid+parseFloat(obj_value[i-2].invoice_payment_status_paid);}
        catch(err){ invoiceWS.cell(i, 13).string(''); }
        try{ invoiceWS.cell(i, 14).string(obj_value[i-2].invoice_payment_status_unpaid.toString()); 
            total_invoice_payment_status_unpaid=total_invoice_payment_status_unpaid+parseFloat(obj_value[i-2].invoice_payment_status_unpaid);}
        catch(err){ invoiceWS.cell(i, 14).string(''); }
        try{ invoiceWS.cell(i, 15).string(obj_value[i-2].invoice_details.toString()); }
        catch(err){ invoiceWS.cell(i, 15).string(''); }
        try{ invoiceWS.cell(i, 16).string(obj_value[i-2].approval_date.toString()); }
        catch(err){ invoiceWS.cell(i, 16).string(''); }
        try{ invoiceWS.cell(i, 17).string(obj_value[i-2].status.toString()); }
        catch(err){ invoiceWS.cell(i, 17).string(''); }   
    }
    try{ invoiceWS.cell(i, 1).string('Total : '); }
    catch(err){ invoiceWS.cell(i, 7).string(''); } 
    try{ invoiceWS.cell(i, 7).string(total_cash_memo_total_amount.toString()); }
    catch(err){ invoiceWS.cell(i, 7).string(''); } 
    try{ invoiceWS.cell(i, 8).string(total_cash_memo_balance_amount.toString()); }
    catch(err){ invoiceWS.cell(i, 8).string(''); } 
    try{ invoiceWS.cell(i, 13).string(total_invoice_payment_status_paid.toString()); }
    catch(err){ invoiceWS.cell(i, 13).string(''); } 
    try{ invoiceWS.cell(i, 14).string(total_invoice_payment_status_unpaid.toString()); }
    catch(err){ invoiceWS.cell(i, 14).string(''); } 
    return wb;
}

let obj = {}
obj.generateExcel = generateExcel

export default obj