import _ from 'lodash'

function get({entity=null, where=null, order=null, limit=null, offset=null, include=null, options=null, attributes=null}={}){
    if (!entity) {
        return Promise.resolve(null);
    }

    options = options || {};

    return Promise.resolve()
    .then(() => {
        let args = {};
        if (order) {
            args.order = order;
        }
        if (limit) {
            args.limit = limit;
        }
        if (offset) {
            args.offset = offset;
        }
        if (attributes && _.isArray(attributes)) {
            args.attributes = attributes;
        }
        if (include) {
            args.include = include;
        }
        if (where) {
            args.where = where;
        }
        if (options && options.transaction) {
            args.transaction = options.transaction;
        }
        if (options && options.lock) {
            args.lock = options.lock;
        }
        return {args:args};
    })
    .then(p_result => {
        let {args:args} = p_result;
        return entity.count({where:args.where}).then(total => {
            p_result.total = total;
            return p_result;
        });
    })
    .then(p_result => {
        let {args:args} = p_result;
        return entity.findAll(args).then(result_list => {
            p_result.result_list = result_list;
            return p_result;
        });
    })
    .then(p_result => {
        let {args:args, total:total, result_list:result_list} = p_result;
        
        if (! (_.isArray(result_list))) {
            result_list = [result_list];
        }
        if (! result_list || result_list.length == 0) {
            if (options.throwIfEmpty) {
                throw new ResourceNotFoundError();
            }
        }
        return {total:total, result_list:result_list};
    });
}

let obj = {}
obj.get = get

export default obj