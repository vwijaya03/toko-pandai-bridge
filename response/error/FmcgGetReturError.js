class FmcgGetReturError extends Error {
	constructor(message) {
        super(message);
        this.code = 400;
        this.err_code = 4300;
        this.message = 'Gagal menampilkan data retur, '+message;
    }
}

export default FmcgGetReturError