import request from 'request'
import util from 'util'

import { Logs } from '~/orm/index'

import CustomError from '~/response/error/CustomError'
import FmcgGetInvoicesError from '~/response/error/FmcgGetInvoicesError'
import FmcgGetReturError from '~/response/error/FmcgGetReturError'
import FmcgGetTotalUnpaidAmountError from '~/response/error/FmcgGetTotalUnpaidAmountError'
import FmcgCheckProcessInvoiceError from '~/response/error/FmcgCheckProcessInvoiceError'
import FmcgTaskListError from '~/response/error/FmcgTaskListError'
import FmcgMakePaymentError from '~/response/error/FmcgMakePaymentError'
import FmcgProcessInvoiceError from '~/response/error/FmcgProcessInvoiceError'
import FmcgUploadDataPotonganError from '~/response/error/FmcgUploadDataPotonganError'
import FmcgUploadDataPembatalanPotonganError from '~/response/error/FmcgUploadDataPembatalanPotonganError'
import FmcgUploadDataPembatalanInvoiceError from '~/response/error/FmcgUploadDataPembatalanInvoiceError'
import FmcgTaskListPotonganError from '~/response/error/FmcgTaskListPotonganError'
import FmcgTaskListPembatalanPotonganError from '~/response/error/FmcgTaskListPembatalanPotonganError'

const api_path_invoices = '/invoice';
const api_path_one_invoice = '/invoice';
const api_path_check_process_invoice = '/check-process-invoice';
const api_path_check_process_invoice_potongan = '/check-process-invoice-potongan';
const api_path_retur = '/retur';
const api_path_unbound_retur = '/unbound-retur';
const api_path_task_list = '/task-list';
const api_path_task_list_potongan = '/task-potongan';
const api_path_task_list_pembatalan_potongan = '/task-pembatalan-potongan';
const api_path_task_list_pembatalan_invoice = '/task-pembatalan-invoice';
const api_path_total_unpaid_amount = '/total-unpaid-amount';
const api_path_payment = '/payment';
const api_path_process_invoice = '/process-invoice';
const api_path_upload_data_potongan = '/upload-data-potongan';
const api_path_upload_data_pembatalan_potongan = '/upload-pembatalan-potongan';
const api_path_upload_data_pembatalan_invoice = '/upload-pembatalan-invoice';
const api_path_cod_payment = '/payment-cod'
const api_path_insert_cod_otp = '/insert-cod-otp'
const api_path_get_cod_otp = '/get-cod-otp'
const api_path_get_cod_payment = '/get-cod-payment'
const api_path_update_cod_payment = '/update-cod-payment'

function commonHeaders(token) {
    return {
        'Cache-Control': 'no-cache',
        'token': token
    };
}

function handleResponse(rawJsonResponse, errorClass, resolve, reject, requestError, function_name, fmcg_name) {

	try {
		let successJsonResp = JSON.parse(rawJsonResponse);
	
		if(requestError != null) {
			reject(new errorClass(requestError));
		}

		if(successJsonResp.code == 400 || successJsonResp.code == 401) {
			reject(new errorClass(successJsonResp.message));
		} else {
			resolve(successJsonResp);
		}
	} catch(err) {
		Logs.create({action: `error ${function_name} ${fmcg_name}`, description: util.inspect(err), additional_description: util.inspect(rawJsonResponse)});

		reject(new errorClass('terjadi kesalahan silahkan coba lagi'));
	}
	
}

function handleResponseError(errorClass, resolve, reject, err, function_name, fmcg_name) {
	Logs.create({action: `error ${function_name} ${fmcg_name}`, description: util.inspect(err)});
	
	reject(new errorClass(err));
}

function get_invoices({url=null, fmcg_token=null, fmcg_name=null, platform=null, usertype=null, outlet_code=null, distributor_code=null, le_code=null, unique_code=null, paid=null,paid_by=null, unpaid=null, page=1, items_per_page=15, sorting=null, sortingBy=null, filter_by=null, filter1=null, filter2=null, join_date=null,}={}) {
	let args = {};
    args.platform = platform;    
    args.usertype = usertype;    
    args.unique_code = unique_code;
    args.outlet_code = outlet_code;
    args.dt_code = distributor_code;
    args.paid_by = paid_by;
    args.le_code = le_code;
    args.paid = paid;
    args.unpaid = unpaid;
    args.filter1 = filter1;
    args.filter2 = filter2;
    args.join_date = join_date;
    args.page = page;
    args.items_per_page = items_per_page;
    args.sorting = sorting;
    args.sortingBy = sortingBy;
    args.filter_by = filter_by;
    
	return new Promise(function(resolve, reject) {
        
        let headers = commonHeaders(fmcg_token);
        let url_get_invoices = url+api_path_invoices;

	    request.get( {url:url_get_invoices, headers:headers, qs:args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}

	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}

function get_one_invoice({url=null, fmcg_token=null, fmcg_name=null, invoice_id=null, dt_code=null}={}) {
    return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        // let url_get_one_invoice = url+api_path_one_invoice+'/'+invoice_id+'/'+dt_code;
        let url_get_one_invoice = url+api_path_one_invoice+'/'+encodeURIComponent(invoice_id)+'/'+dt_code;

        request.get( {url:url_get_one_invoice, headers:headers}, function(err, httpResponse, body) {
            if(err) {
                return err;
            }

            handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get one invoice', fmcg_name);
        }).on('error', function(err) {
            handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get one invoice', fmcg_name);
        });
    });
}

function get_unbound_retur({url=null, fmcg_token=null, fmcg_name=null, le_code=null}={}) {
    let args = {};
    args.le_code = le_code;

    return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let url_get_unbound_retur = url+api_path_unbound_retur;

        request.get( {url:url_get_unbound_retur, headers:headers, qs: args}, function(err, httpResponse, body) {
            if(err) {
                return err;
            }

            handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get unbound retur', fmcg_name);
        }).on('error', function(err) {
            handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get unbound retur', fmcg_name);
        });
    });
}

function get_retur({url=null, fmcg_token=null, fmcg_name=null, platform=null, usertype=null, outlet_code=null, distributor_code=null, le_code=null, unique_code=null, page=1, items_per_page=15, sorting=null, sortingBy=null, filter_by=null, start_date=null, end_date=null, join_date=null}={}) {
	let args = {};
    args.platform = platform;    
    args.usertype = usertype;    
    args.unique_code = unique_code;
    args.outlet_code = outlet_code;
    args.dt_code = distributor_code;
    args.le_code = le_code;
    args.start_date = start_date;
	args.end_date = end_date;
	args.join_date = join_date;
    args.page = page;
    args.items_per_page = items_per_page;
    args.sorting = sorting;
    args.sortingBy = sortingBy;
    args.filter_by = filter_by;
    
	return new Promise(function(resolve, reject) {
        
        let headers = commonHeaders(fmcg_token);
        let url_get_retur = url+api_path_retur;

	    request.get( {url:url_get_retur, headers:headers, qs:args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}

	    	handleResponse(body, FmcgGetReturError, resolve, reject, err, 'get retur', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetReturError, resolve, reject, err, 'get retur', fmcg_name);
		});
    });
}

function get_total_unpaid_amount({url=null, fmcg_token=null, fmcg_name=null, le_code=null, join_date=null}={}) {
	let args = {};
    args.le_code = le_code;
    args.join_date = join_date;
    
	return new Promise(function(resolve, reject) {
        
        let headers = commonHeaders(fmcg_token);
        let url_get_total_unpaid_amount = url+api_path_total_unpaid_amount;

	    request.get( {url:url_get_total_unpaid_amount, headers:headers, qs:args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}

	    	handleResponse(body, FmcgGetTotalUnpaidAmountError, resolve, reject, err, 'get total unpaid amount', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetTotalUnpaidAmountError, resolve, reject, err, 'get total unpaid amount', fmcg_name);
		});
    });
}

function check_process_invoice({url=null, fmcg_token=null, fmcg_name=null, dt_code=null}={}) {
	let args = {};
    args.dt_code = dt_code;
    
	return new Promise(function(resolve, reject) {
        
        let headers = commonHeaders(fmcg_token);
        let url_check_process_invoice = url+api_path_check_process_invoice;

	    request.post( {url:url_check_process_invoice, headers:headers, form:args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}

	    	handleResponse(body, FmcgCheckProcessInvoiceError, resolve, reject, err, 'post check process invoice', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgCheckProcessInvoiceError, resolve, reject, err, 'post check process invoice', fmcg_name);
		});
    });
}

function check_process_invoice_potongan({url=null, fmcg_token=null, fmcg_name=null, dt_code=null}={}) {
	let args = {};
    args.dt_code = dt_code;
    
	return new Promise(function(resolve, reject) {
        
        let headers = commonHeaders(fmcg_token);
        let url_check_process_invoice_potongan = url+api_path_check_process_invoice_potongan;

	    request.post( {url:url_check_process_invoice_potongan, headers:headers, form:args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}

	    	handleResponse(body, FmcgCheckProcessInvoiceError, resolve, reject, err, 'post check process invoice', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgCheckProcessInvoiceError, resolve, reject, err, 'post check process invoice', fmcg_name);
		});
    });
}

function get_task_list({url=null, fmcg_token=null, fmcg_name=null, dt_code=null, email=null, page=null, item_per_page=null, sorting=null, sortingBy=null, usertype=null}={}) {
	let args = {};
    args.url = url;
    args.fmcg_token = fmcg_token;
    args.fmcg_name = fmcg_name;
    args.dt_code = dt_code;
    args.email = email;
    args.page = page;
    args.item_per_page = item_per_page;
    args.sorting = sorting;
    args.sortingBy = sortingBy;
    args.usertype = usertype;

    return new Promise(function(resolve, reject) {
        
        let headers = commonHeaders(fmcg_token);
        let url_task_list = url+api_path_task_list;

	    request.post( {url:url_task_list, headers:headers, form:args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}

	    	handleResponse(body, FmcgTaskListError, resolve, reject, err, 'post task list', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgTaskListError, resolve, reject, err, 'post task list', fmcg_name);
		});
    });
}

function make_payment({url=null, fmcg_token=null, fmcg_name=null, dt_code=null, invoice_id=null, amount=null, total_potongan=null, total_retur=null, fromUserId=null, toUserId=null, transact_date=null, valdo_tx_id=null, currency_code=null}={}) {
	  let args = {};
    args.url = url;
    args.fmcg_token = fmcg_token;
    args.fmcg_name = fmcg_name;
    args.dt_code = dt_code;
    args.invoice_id = invoice_id;
    args.amount = amount;
    args.total_potongan = total_potongan;
    args.total_retur = total_retur;
    args.fromUserId = fromUserId;
    args.toUserId = toUserId;
    args.transact_date = transact_date;
    args.valdo_tx_id = valdo_tx_id;
    args.currency_code = currency_code;
    
    return new Promise(function(resolve, reject) {
        
        let headers = commonHeaders(fmcg_token);
        let url_make_payment = url+api_path_payment;

        request.post( {url:url_make_payment, headers:headers, form:args}, function(err, httpResponse, body) {
            if(err) {
                return err;
            }

            handleResponse(body, FmcgMakePaymentError, resolve, reject, err, 'post make payment', fmcg_name);
        })
        .on('error', function(err) {
            handleResponseError(FmcgMakePaymentError, resolve, reject, err, 'post make payment', fmcg_name);
        });
    });
}

function process_invoice({url=null, fmcg_token=null, fmcg_name=null, filename=null, dt_code=null, user_id=null}={}) {
	let args = {};
    args.url = url;
    args.fmcg_token = fmcg_token;
    args.fmcg_name = fmcg_name;
    args.filename = filename;
    args.dt_code = dt_code;
    args.user_id = user_id;

    return new Promise(function(resolve, reject) {
        
        let headers = commonHeaders(fmcg_token);
        let url_process_invoice = url+api_path_process_invoice;

	    request.post( {url:url_process_invoice, headers:headers, form:args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}

	    	handleResponse(body, FmcgProcessInvoiceError, resolve, reject, err, 'post process invoice', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgProcessInvoiceError, resolve, reject, err, 'post process invoice', fmcg_name);
		});
    });
}

function upload_data_potongan({url=null, fmcg_token=null, fmcg_name=null, filename=null, dt_code=null, user_id=null}={}) {
	let args = {};
    args.url = url;
    args.fmcg_token = fmcg_token;
    args.fmcg_name = fmcg_name;
    args.filename = filename;
    args.dt_code = dt_code;
    args.user_id = user_id;

    return new Promise(function(resolve, reject) {
        
        let headers = commonHeaders(fmcg_token);
        let url_upload_data_potongan = url+api_path_upload_data_potongan;

	    request.post( {url:url_upload_data_potongan, headers:headers, form:args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}

	    	handleResponse(body, FmcgUploadDataPotonganError, resolve, reject, err, 'post upload data potongan', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgUploadDataPotonganError, resolve, reject, err, 'post upload data potongan', fmcg_name);
		});
    });
}

function upload_data_pembatalan_potongan({url=null, fmcg_token=null, fmcg_name=null, filename=null, dt_code=null, user_id=null}={}) {
	let args = {};
    args.url = url;
    args.fmcg_token = fmcg_token;
    args.fmcg_name = fmcg_name;
    args.filename = filename;
    args.dt_code = dt_code;
    args.user_id = user_id;

    return new Promise(function(resolve, reject) {
        
        let headers = commonHeaders(fmcg_token);
        let url_upload_data_pembatalan_potongan = url+api_path_upload_data_pembatalan_potongan;

	    request.post( {url:url_upload_data_pembatalan_potongan, headers:headers, form:args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}

	    	handleResponse(body, FmcgUploadDataPembatalanPotonganError, resolve, reject, err, 'post upload data pembatalan potongan', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgUploadDataPembatalanPotonganError, resolve, reject, err, 'post upload data pembatalan potongan', fmcg_name);
		});
    });
}

function upload_data_pembatalan_invoice({url=null, fmcg_token=null, fmcg_name=null, filename=null, dt_code=null, user_id=null}={}) {
	let args = {};
    args.url = url;
    args.fmcg_token = fmcg_token;
    args.fmcg_name = fmcg_name;
    args.filename = filename;
    args.dt_code = dt_code;
    args.user_id = user_id;

    return new Promise(function(resolve, reject) {
        
        let headers = commonHeaders(fmcg_token);
        let url_upload_data_pembatalan_potongan = url+api_path_upload_data_pembatalan_invoice;

	    request.post( {url:url_upload_data_pembatalan_invoice, headers:headers, form:args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}

	    	handleResponse(body, FmcgUploadDataPembatalanInvoiceError, resolve, reject, err, 'post upload data pembatalan invoice', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgUploadDataInvoiceError, resolve, reject, err, 'post upload data pembatalan invoice', fmcg_name);
		});
    });
}

function get_task_list_potongan({url=null, fmcg_token=null, fmcg_name=null, dt_code=null, email=null, page=null, item_per_page=null, sorting=null, sortingBy=null, usertype=null}={}) {
	let args = {};
    args.url = url;
    args.fmcg_token = fmcg_token;
    args.fmcg_name = fmcg_name;
    args.dt_code = dt_code;
    args.email = email;
    args.page = page;
    args.item_per_page = item_per_page;
    args.sorting = sorting;
    args.sortingBy = sortingBy;
    args.usertype = usertype;

    return new Promise(function(resolve, reject) {
        
        let headers = commonHeaders(fmcg_token);
        let url_task_list_potongan = url+api_path_task_list_potongan;

	    request.post( {url:url_task_list_potongan, headers:headers, form:args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}

	    	handleResponse(body, FmcgTaskListPotonganError, resolve, reject, err, 'post task list potongan', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgTaskListPotonganError, resolve, reject, err, 'post task list potongan', fmcg_name);
		});
    });
}

function get_task_list_pembatalan_potongan({url=null, fmcg_token=null, fmcg_name=null, dt_code=null, email=null, page=null, item_per_page=null, sorting=null, sortingBy=null, usertype=null}={}) {
	let args = {};
    args.url = url;
    args.fmcg_token = fmcg_token;
    args.fmcg_name = fmcg_name;
    args.dt_code = dt_code;
    args.email = email;
    args.page = page;
    args.item_per_page = item_per_page;
    args.sorting = sorting;
    args.sortingBy = sortingBy;
    args.usertype = usertype;

    return new Promise(function(resolve, reject) {
        
        let headers = commonHeaders(fmcg_token);
        let url_task_list_pembatalan_potongan = url+api_path_task_list_pembatalan_potongan;

	    request.post( {url:url_task_list_pembatalan_potongan, headers:headers, form:args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}

	    	handleResponse(body, FmcgTaskListPembatalanPotonganError, resolve, reject, err, 'post task list pembatalan potongan', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgTaskListPembatalanPotonganError, resolve, reject, err, 'post task list pembatalan potongan', fmcg_name);
		});
    });
}

function get_task_list_pembatalan_invoice({url=null, fmcg_token=null, fmcg_name=null, dt_code=null, email=null, page=null, item_per_page=null, sorting=null, sortingBy=null, usertype=null}={}) {
	let args = {};
    args.url = url;
    args.fmcg_token = fmcg_token;
    args.fmcg_name = fmcg_name;
    args.dt_code = dt_code;
    args.email = email;
    args.page = page;
    args.item_per_page = item_per_page;
    args.sorting = sorting;
    args.sortingBy = sortingBy;
    args.usertype = usertype;

    return new Promise(function(resolve, reject) {
        
        let headers = commonHeaders(fmcg_token);
        let url_task_list_pembatalan_invoice = url+api_path_task_list_pembatalan_potongan;

	    request.post( {url:url_task_list_pembatalan_invoice, headers:headers, form:args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}

	    	handleResponse(body, FmcgTaskListPembatalanInvoiceError, resolve, reject, err, 'post task list pembatalan invoice', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgTaskListPembatalanInvoiceError, resolve, reject, err, 'post task list pembatalan invoice', fmcg_name);
		});
    });
}

function cod_payment({url=null, fmcg_token=null, fmcg_name=null, nama_petugas=null, image=null, no_hp_driver=null, otp=null, le_code=null, dt_code=null, invoice_id=null, amount=null, android_status=null,invoice_status=null,desc_payment=null}={}) {
  
  let cod_detail = {}
  cod_detail.url = url;
  cod_detail.fmcg_token = fmcg_token;
  cod_detail.fmcg_name = fmcg_name;
  cod_detail.nama_petugas = nama_petugas;
  cod_detail.image = image;
  cod_detail.invoice_id = invoice_id;
  cod_detail.le_code = le_code;
  cod_detail.dt_code = dt_code;
  cod_detail.amount = amount;
  cod_detail.no_hp_driver = no_hp_driver;
  cod_detail.otp = otp;
  cod_detail.android_status = android_status;
  cod_detail.invoice_status = invoice_status;
  cod_detail.desc_payment = desc_payment;

  return new Promise(function(resolve, reject) {
      let headers = commonHeaders(fmcg_token);
      let url_cod_payment = url+api_path_cod_payment;

    request.post( {url:url_cod_payment, headers:headers, form:cod_detail}, function(err, httpResponse, body) {
      if(err) {
        return err;
      }

      handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'cod_payment', fmcg_name);
    }).on('error', function(err) {
      handleResponseError(FmcgProcessInvoiceError, resolve, reject, err, 'post process invoice', fmcg_name);
    });
  });
}

function insert_cod_otp({url=null, fmcg_token=null, fmcg_name=null, no_hp_driver=null, otp=null, android_status=null}={}) {
  
  let otp_detail = {}
  otp_detail.url = url;
  otp_detail.fmcg_token = fmcg_token;
  otp_detail.fmcg_name = fmcg_name;
  otp_detail.no_hp_driver = no_hp_driver;
  otp_detail.otp = otp;
  otp_detail.android_status = android_status;

  return new Promise(function(resolve, reject) {
      let headers = commonHeaders(fmcg_token);
      let url_cod_otp = url+api_path_insert_cod_otp;

    request.post( {url:url_cod_otp, headers:headers, form:otp_detail}, function(err, httpResponse, body) {
      if(err) {
        return err;
      }

      handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'cod_payment', fmcg_name);
    }).on('error', function(err) {
      handleResponseError(FmcgProcessInvoiceError, resolve, reject, err, 'post process invoice', fmcg_name);
    });
  });
}

function get_otp_code({url=null, fmcg_token=null, fmcg_name=null, no_hp_driver=null, otp=null}={}) {
  
  let otp_detail = {}
  otp_detail.url = url;
  otp_detail.fmcg_token = fmcg_token;
  otp_detail.fmcg_name = fmcg_name;
  otp_detail.no_hp_driver = no_hp_driver;
  otp_detail.otp = otp;

  return new Promise(function(resolve, reject) {
      let headers = commonHeaders(fmcg_token);
      let url_cod_otp = url+api_path_get_cod_otp;

    request.get( {url:url_cod_otp, headers:headers, form:otp_detail}, function(err, httpResponse, body) {
      if(err) {
        return err;
      }

      handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'cod_payment', fmcg_name);
    }).on('error', function(err) {
      handleResponseError(FmcgProcessInvoiceError, resolve, reject, err, 'post process invoice', fmcg_name);
    });
  });
}

function get_cod_payment({url=null, fmcg_token=null, fmcg_name=null, le_code=null, dt_code=null, invoice_id=null}={}) {
  let cod_detail = {}
  cod_detail.url = url
  cod_detail.fmcg_token = fmcg_token
  cod_detail.fmcg_name = fmcg_name
  cod_detail.invoice_id = invoice_id;
  cod_detail.le_code = le_code;
  cod_detail.dt_code = dt_code;

  return new Promise(function(resolve, reject) {
      
      let headers = commonHeaders(fmcg_token);
      let url_get_cod_payment = url+api_path_get_cod_payment;

    request.get( {url:url_get_cod_payment, headers:headers, form:cod_detail}, function(err, httpResponse, body) {
      if(err) {
        return err;
      }

      handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'cod_payment', fmcg_name);
    }).on('error', function(err) {
      handleResponseError(FmcgProcessInvoiceError, resolve, reject, err, 'post process invoice', fmcg_name);
  });
  });
}

function update_cod_payment({url=null, fmcg_token=null, fmcg_name=null, le_code=null, dt_code=null, invoice_id=null, status=null, invoice_status=null}={}) {
  let detail = {}
  detail.url = url
  detail.fmcg_token = fmcg_token
  detail.fmcg_name = fmcg_name
  detail.invoice_id = invoice_id;
  detail.le_code = le_code;
  detail.dt_code = dt_code;
  detail.status = status;
  detail.invoice_status = invoice_status

    return new Promise(function(resolve, reject) {
        
        let headers = commonHeaders(fmcg_token);
        let url_update_cod_payment = url+api_path_update_cod_payment;

	    request.post( {url:url_update_cod_payment, headers:headers, form:detail}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}

	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'cod_payment', fmcg_name);
	    }).on('error', function(err) {
        handleResponseError(FmcgProcessInvoiceError, resolve, reject, err, 'post process invoice', fmcg_name);
		});
    });
}

let obj = {}
obj.get_invoices = get_invoices
obj.get_one_invoice = get_one_invoice
obj.check_process_invoice = check_process_invoice
obj.check_process_invoice_potongan = check_process_invoice_potongan
obj.get_retur = get_retur
obj.get_unbound_retur = get_unbound_retur
obj.get_total_unpaid_amount = get_total_unpaid_amount
obj.get_task_list = get_task_list
obj.get_task_list_potongan = get_task_list_potongan
obj.get_task_list_pembatalan_potongan = get_task_list_pembatalan_potongan
obj.get_task_list_pembatalan_invoice = get_task_list_pembatalan_invoice
obj.make_payment = make_payment
obj.process_invoice = process_invoice
obj.upload_data_potongan = upload_data_potongan
obj.upload_data_pembatalan_potongan = upload_data_pembatalan_potongan
obj.upload_data_pembatalan_invoice = upload_data_pembatalan_invoice
obj.cod_payment = cod_payment
obj.insert_cod_otp = insert_cod_otp
obj.get_otp_code = get_otp_code
obj.get_cod_payment = get_cod_payment
obj.update_cod_payment = update_cod_payment

export default obj
