'use strict';

const { OAuth2Client } = require('google-auth-library');
import _ from 'lodash'
import util from 'util'

import middleware from '~/api/controllers/auth'
import base_repo from '~/api/controllers/base'

import service_bank_account from '~/services/bank_account'
import service_fmcg from '~/services/fmcg'
import service_otp from '~/services/otp'
import service_user from '~/services/user'
import service_user_additional_data from '~/services/user_additional_data'
import service_user_unique_code from '~/services/user_unique_code'
import service_formatter from '~/services/formatter'
import service_register_lecode from '~/services/register_lecode'


import helper from '~/utils/helper'

import MoneyAccess from '~/external_api/moneyaccess'
import ApiTop from '~/external_api/api_top'
import ApiPd from '~/external_api/api_top_pohon_dana'
import ApiBackend from '~/external_api/api_backend'

import { User } from '~/orm/index'
import { Logs } from '~/orm/index'
import { UserUniqueCode } from '~/orm/index'
import { allowed_user_status } from '~/properties'
import { ACCESS_TOKEN_INTERNAL_DEV } from '~/properties'
import { USERTYPES } from '~/properties'
import { UserQuery } from '~/orm/user'
import { Op } from '~/utils/helper'

import SigninError from '~/response/error/SigninError'
import UnauthorizedRequestError from '~/response/error/UnauthorizedRequestError'
import ChangePasswordError from '~/response/error/ChangePasswordError'
import CustomError from '~/response/error/CustomError'
import CustomSuccess from '~/response/success/CustomSuccess'

const acl_get_users = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center','finance'];
const acl_logout = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center','finance'];
const acl_changepassword = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center','finance'];
const acl_resetpin = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center','finance'];
const acl_changepin = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center','finance'];
const acl_addpin = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center','finance'];
const acl_register_user = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center','finance'];
const acl_edit_user = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center','finance'];
const acl_get_one_user = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center','finance'];
const acl_get_balance = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center','finance'];
const acl_get_one_user_by_vaccount = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center','finance'];

async function signin(req, res) {
    const email = req.swagger.params.email.value;
    const password = req.swagger.params.password.value;
    const param_imei = req.swagger.params.imei;

    let imei = _.isUndefined(param_imei.value)?'':param_imei.value;
    let out = {};
    let token = '';
   
    //let saldo_fit = 0;
    //let fit;

    try {

        Logs.create({action: 'signin', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let users1 = await service_user.getOneByEmail({email: email});
        let users2 = await service_user.getOneByPhone({phone: email });
        
        let user = users1 === null?users2:users1;

        if(user == null) {
            throw new SigninError();
        }

        let actual_password = await service_user.getHashedPassword(user.dataValues.salt, password);
        let expected_password = user.dataValues.password;

        if (expected_password != actual_password) {
            throw new SigninError();
        }
        
        if(imei.length != 0) {
            if(user.dataValues.imei != imei) {
                caller_logout(user.dataValues.user_id);
                return res.status(400).json({code: 4001, message: 'Login gagal, IMEI berbeda', phone: user.dataValues.phone});
            }
            if(user.dataValues.imei == null || user.dataValues.imei == "") {
                User.update({imei: imei}, {where: {user_id: user.dataValues.user_id}});
            }
        }

        // regenerate access token if nearing expired
        let is_check_token_expiry = true;

        if (is_check_token_expiry) {
            let delta_days_in_seconds = 259200; // 3 days

            if (user) {

                // expiry is seconds
                let expiry_ts = await helper.modifiedSequelizeTimestampStandart(user.dataValues.access_token_expiry).getTime();

                if(isNaN(expiry_ts)) {
                    expiry_ts = 1463312000;
                }

                const now_seconds = new Date().getTime();

                if (now_seconds > (expiry_ts - delta_days_in_seconds)) {
                    let new_access_token = await service_user.renewAccessToken({user:user});
                    token = new_access_token;
                } else {
                    token = user.dataValues.access_token;
                }

                delete user.dataValues.salt;
                delete user.dataValues.password;

                let userUniqueCodeTableName = UserUniqueCode.getTableName();
                let where_key = `$${userUniqueCodeTableName}.user_id$`;
                let filter = {};
                filter[where_key] = user.dataValues.user_id;
                


             // let getOne = await service_user_unique_code.getOne({user_id:user.dataValues.user_id,fmcg_id:13})
                
       //          if(getOne == null || getOne == '') {
                    
       //               saldo_fit =0;
                     
       //          }else{
       //               let fit_data = await ApiTop.cek_saldo({le_code:getOne.dataValues.unique_code});
       //              if(fit_data.saldo===0 || fit_data.saldo.status =="failed"){
       //                  saldo_fit =0
       //              } else{
       //              if(fit_data.saldo.status == "not_found"){
       //                   saldo_fit =0
       //               }
       //               else{
       //               saldo_fit= fit_data.saldo.data.plafond;
       //           }
       //           }
       //          }

                let role_name = await middleware.getUSERTYPESKey(USERTYPES, user.dataValues.type);
                let additional_data_user = await service_user_additional_data.get({user_id: user.dataValues.user_id});
                let bank_accounts = await service_bank_account.get({user_id: user.dataValues.user_id});
                let user_fmcg = await service_fmcg.get(filter);
                
                // for(let i=0; i < user_fmcg.length; i++){
                //     if(user_fmcg[i].dataValues.id === 1){
                //         user_fmcg[i].dataValues.balance = saldo_fit,
                //         user_fmcg[i].dataValues.is_topex = true
                //     }
                //     else{
                //         user_fmcg[i].dataValues.is_topex = false
                //     }
                // }
               
                let name = helper.splitString(user.dataValues.fullname);

                user.dataValues.firstname = "";
                user.dataValues.lastname = "";
                user.dataValues.pin_exist = false;

                if(Array.isArray(name)) {
                    if(name[0]) {
                        user.dataValues.firstname = name[0];
                        user.dataValues.lastname = name[1];
                    } else {
                        user.dataValues.firstname = name[1];
                        user.dataValues.lastname = '';
                    }
                }
                
                if(user.dataValues.pin != null) {
                    user.dataValues.pin_exist = true;
                }

                delete user.dataValues.pin;
                
                user.dataValues.access_token_expiry = helper.formatDateTime(user.dataValues.access_token_expiry);
                user.dataValues.join_date = helper.formatDateTime(user.dataValues.join_date);
                user.dataValues.role = {
                    id: user.dataValues.type,
                    name: role_name,
                    data: additional_data_user
                };
//                let i = 0;
//                do {
//                  bank_accounts[i].dataValues.icon = bank_accounts[i].dataValues.bank.dataValues.icon;
//                  i++;
//                } while (i < bank_accounts.length);

                for(let i=0; i < bank_accounts.length; i++){
                   bank_accounts[i].dataValues.icon = bank_accounts[i].dataValues.bank.dataValues.icon;
               }
                user.dataValues.banks = bank_accounts;
                user.dataValues.fmcg = user_fmcg;

                let data = helper.removeNullFromObject(user.dataValues);
               
                
                out.Token = {
                    token: token,
                    expired: data.access_token_expiry
                };

                delete data.access_token;
                delete data.access_token_expiry;
                out.User = data;
               
                out.message = "";
                out.status = "";
                out.code = 200;
                
                
            }
        }

        res.json(out);

    } catch (err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Signin gagal, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function logout(req, res) {
    const token = req.swagger.params.token.value;

    try {
        Logs.create({action: 'logout', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_logout, is_allowed_access_token_static: false});

        caller_logout(auth_user.dataValues.user_id);

        res.json(new CustomSuccess(200, 'Logout berhasil'));
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Logout gagal, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

function caller_logout(user_id)
{
    User.update({fcm_id: "", accessTokenExpiry: "1970-01-18 05:28:32"}, {where: {user_id: user_id}});
}

async function google_auth(req, res) {
    const token = req.swagger.params.token.value;
    const idToken = req.swagger.params.idToken.value; //android phone id
    const WEB_CLIENT = "205092347191-6605qvma0alrl48e9pi00e32l48s3ked.apps.googleusercontent.com";
    const CLIENTS = ["205092347191-cjrm9qh5egk1c1ctgpatm9slbr2ouutr.apps.googleusercontent.com", "205092347191-fv1kvq7k10qa7vujho59deha12uol416.apps.googleusercontent.com", "205092347191-qcs9gfh3l36vr4rljod9jm9gibo2hr3s.apps.googleusercontent.com", WEB_CLIENT];

    let client = new OAuth2Client(WEB_CLIENT);
    let access_token = ""; 
    let delta_days_in_seconds = 259200;
    let saldo_fit = '';
    let out = {};

    try {
        Logs.create({action: 'google sign in', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        if(token != ACCESS_TOKEN_INTERNAL_DEV) {
            throw new UnauthorizedRequestError();
        } else {
            const ticket = await client.verifyIdToken({
                idToken: idToken,
                audience: CLIENTS,
            });

            const payload = ticket.getPayload();
            let user = await service_user.getOneByEmail({email: payload['email']});
            
            if (user) {
                // expiry is seconds
                let expiry_ts = await helper.modifiedSequelizeTimestampStandart(user.dataValues.access_token_expiry).getTime();

                if(isNaN(expiry_ts)) {
                    expiry_ts = 1463312000;
                }

                const now_seconds = new Date().getTime();

                if (now_seconds > (expiry_ts - delta_days_in_seconds)) {
                    let new_access_token = await service_user.renewAccessToken({user:user});
                    access_token = new_access_token;
                } else {
                    access_token = user.dataValues.access_token;
                }

                delete user.dataValues.salt;
                delete user.dataValues.password;
                
                let userUniqueCodeTableName = UserUniqueCode.getTableName();
                let where_key = `$${userUniqueCodeTableName}.user_id$`;
                let filter = {};
                filter[where_key] = user.dataValues.user_id;
                
                // let getOne = await service_user_unique_code.getOne({user_id:user.dataValues.user_id, fmcg_id:13})
                
                // if(getOne == null || getOne == '') {
                //     saldo_fit = 0; 
                // } else {
                //     let fit_data = await ApiTop.cek_saldo({le_code:getOne.dataValues.unique_code});
                    
                //     if(fit_data.saldo.status == "not_found") {
                //         saldo_fit =""
                //     } else {
                //         saldo_fit = fit_data.saldo.data.plafond; 
                //     }
                // }

                let role_name = await middleware.getUSERTYPESKey(USERTYPES, user.dataValues.type);
                let additional_data_user = await service_user_additional_data.get({user_id: user.dataValues.user_id});
                let bank_accounts = await service_bank_account.get({user_id: user.dataValues.user_id});
                let user_fmcg = await service_fmcg.get(filter);
                
                // for(let i=0; i < user_fmcg.length; i++){
                //     if(user_fmcg[i].dataValues.id === 1){
                //         user_fmcg[i].dataValues.balance = saldo_fit,
                //         user_fmcg[i].dataValues.is_topex = true
                //     }
                //     else{
                //         user_fmcg[i].dataValues.is_topex = false
                //     }
                // }
            
                let name = helper.splitString(user.dataValues.fullname);

                user.dataValues.firstname = "";
                user.dataValues.lastname = "";
                user.dataValues.pin_exist = false;

                if(Array.isArray(name)) {
                    if(name[0]) {
                        user.dataValues.firstname = name[0];
                        user.dataValues.lastname = name[1];
                    } else {
                        user.dataValues.firstname = name[1];
                        user.dataValues.lastname = '';
                    }
                }
                
                if(user.dataValues.pin != null) {
                    user.dataValues.pin_exist = true;
                }

                delete user.dataValues.pin;
                
                user.dataValues.access_token_expiry = helper.formatDateTime(user.dataValues.access_token_expiry);
                user.dataValues.join_date = helper.formatDateTime(user.dataValues.join_date);
                user.dataValues.role = {
                    id: user.dataValues.type,
                    name: role_name,
                    data: additional_data_user
                };
                user.dataValues.banks = bank_accounts;
                user.dataValues.fmcg = user_fmcg;

                let data = helper.removeNullFromObject(user.dataValues);
            
                
                out.Token = {
                    token: access_token,
                    expired: data.access_token_expiry
                };

                delete data.access_token;
                delete data.access_token_expiry;
                
                //out.new = datas;
                out.User = data;
                out.message = "";
                out.status = "";
                out.code = 200;

                res.json(out);
            } else {
                // tell client to register, with given email and name
                let fname = payload['name'];
                let names = fname.split(/\s/gi);
                let lname = "";
                
                if (names.length > 1) {
                    fname = names[0];
                    names.shift();
                    lname = names.join(" ");
                }

                let out = {
                    mustRegister: true,
                    email: payload['email'],
                    fname: fname,
                    lname: lname
                };

                res.json(out);
            }
        }
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal login menggunakan akun google, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function user_changepassword(req, res) {
    const token = req.swagger.params.token.value;
    const oldpassword = _.isUndefined(req.swagger.params.oldpassword.value)?'':req.swagger.params.oldpassword.value;
    const newpassword = _.isUndefined(req.swagger.params.newpassword.value)?'':req.swagger.params.newpassword.value;

    try {
        Logs.create({action: 'user_changepassword', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_changepassword, is_allowed_access_token_static: false});
        let expected_oldpassword = await service_user.getHashedPassword(auth_user.dataValues.salt, oldpassword);

        if(expected_oldpassword != auth_user.dataValues.password) {
            throw new ChangePasswordError();
        }

        let args = {};
        args.user_id = auth_user.dataValues.user_id;
        args.user_salt = auth_user.dataValues.salt;
        args.newpassword = newpassword;

        service_user.changepassword(args);

        res.json(new CustomSuccess(200, 'Password berhasil di ubah'));

    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Password gagal di ubah, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function user_resetpassword(req, res) {
    const email = req.swagger.params.email.value;

    try {
        Logs.create({action: 'user_resetpassword', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let user = await service_user.getOneByEmail({email: email});

        if(user == null) {
            throw new CustomError(400, 'Data tidak di temukan');
        } else {
            Logs.create({action: 'generate otp', description: helper.getSimpleSwaggerParams(req.swagger.params)});
                
//            let dformat = helper.formatCurrentDateTime();
//            let args = {};
//    
//            args.token_user = '';
//            args.to = email;
//            args.type = 'email';
//            args.purpose = 2;
//            args.ipaddress =  req.connection.remoteAddress ||  req.socket.remoteAddress || req.headers['x-forwarded-for'];
//            args.createdAt = dformat;
//            args.updatedAt = dformat;
//    
//            let _to = args.to;
            let response = await service_user.konfirmasi_reset_password({from_email: email});
            
            if (response) {
                res.json(new CustomSuccess(200, 'Konfirmasi perubahan password telah dikirim, silahkan periksa email anda'));
            } else{
                //throw new CustomError(400, 'Password baru saja di ubah');
        res.json(new CustomSuccess(200, 'Konfirmasi perubahan password telah dikirim, silahkan periksa email anda'));
            }
        }
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal mereset password, '+err;
        }

        err.message = (err.message === undefined) ? err : err.message;
        res.status(err.code).json({code: err.code, message: err.message});
    }
     
}

async function user_konfirmasi_resetpassword(req, res) {
    const from_email = decodeURIComponent(req.swagger.params.from.value);
    const otp = req.swagger.params.otp.value;

    try {
        Logs.create({action: 'user_konfirmasi_resetpassword', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        await service_user.konfirmasi_reset_password({from_email: from_email, otp: otp});

        res.send('Silahkan cek email untuk mendapatkan password baru', 200);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Konfirmasi reset password gagal, '+err;
        }

        res.send('Konfirmasi reset password gagal', 400);
    }
}

async function user_addpin(req, res) {
    const token = req.swagger.params.token.value;
    const pin = req.swagger.params.pin.value;

    let fullname, firstname, lastname = '';

    try {
        Logs.create({action: 'user_addpin', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_addpin, is_allowed_access_token_static: false});

        if(auth_user.dataValues.pin != null) {
            throw new CustomError(400, 'Pin sudah di tambahkan');
        }

        if(auth_user.dataValues.type == 3 || auth_user.dataValues.type == 10 || auth_user.dataValues.type == 11) {
            throw new CustomError(400, 'Hak akses anda tidak memerlukan pin');
        } else {

            let va_number = helper.createVANumber(auth_user.dataValues.user_id, auth_user.dataValues.type);
            let user_additional_data = await service_user_additional_data.get({user_id: auth_user.dataValues.user_id});

            if(auth_user.dataValues.type == 0) {
                for (let i = 0; i < user_additional_data.length; i++) {
                    if(user_additional_data[i].field == 'Nama Toko') {
                        if(user_additional_data[i].value == null || user_additional_data[i].value == '') {
                            throw new CustomError(400, 'Silahkan lengkapi data profile anda terlebih dahulu');
                        }
                    }
                }
                
//                let i = 0;
//                do {
//                  if(user_additional_data[i].field == 'Nama Toko') {
//                        if(user_additional_data[i].value == null || user_additional_data[i].value == '') {
//                            throw new CustomError(400, 'Silahkan lengkapi data profile anda terlebih dahulu');
//                        }
//                    }
//                    i++;
//                } while (i < user_additional_data.length);

                if(
                    user_additional_data == null || user_additional_data.length == 0 ||
                    auth_user.dataValues.fullname == null || auth_user.dataValues.fullname.length < 3 ||
                    auth_user.dataValues.tanggal_lahir == null || auth_user.dataValues.tanggal_lahir.length < 3 ||
                    auth_user.dataValues.tempat_lahir == null || auth_user.dataValues.tempat_lahir.length < 3 ||
                    auth_user.dataValues.ktp == null || auth_user.dataValues.ktp.length < 3
                ) {
                    throw new CustomError(400, 'Silahkan lengkapi data profile anda terlebih dahulu');
                } else {
                    if(auth_user.dataValues.valdo_account == null || auth_user.dataValues.valdo_account.length < 6) {
                        fullname = auth_user.dataValues.fullname;
                        firstname = fullname.substr(0, fullname.indexOf(' '));
                        lastname = fullname.substr(fullname.indexOf(' ')+1);

                        if(!firstname) {
                            firstname = fullname;
                            lastname = '';
                        }

                        let args = {};
                        args.mobnum = va_number;
                        args.fname = firstname;
                        args.lname = lastname;
                        args.dob = auth_user.dataValues.tanggal_lahir;
                        args.pob = auth_user.dataValues.tempat_lahir;
                        args.email = auth_user.dataValues.email;
                        args.pin = pin;
                        args.usertype = auth_user.dataValues.type;

                        await service_user.updateVA({user_id: auth_user.dataValues.user_id, valdo_account: va_number});
                        let api_response = await MoneyAccess.register(args);

                        Logs.create({action: 'api response moneyaccess register', description: util.inspect(api_response)});
                    }
                }
            } else {
               if(
                    auth_user.dataValues.fullname == null || auth_user.dataValues.fullname.length < 3 ||
                    auth_user.dataValues.tanggal_lahir == null || auth_user.dataValues.tanggal_lahir.length < 3 ||
                    auth_user.dataValues.tempat_lahir == null || auth_user.dataValues.tempat_lahir.length < 3 ||
                    auth_user.dataValues.ktp == null || auth_user.dataValues.ktp.length < 3
                ) {
                    throw new CustomError(400, 'Silahkan lengkapi data profile anda terlebih dahulu');
               } else {
                    if(auth_user.dataValues.valdo_account == null || auth_user.dataValues.valdo_account.length < 6) {
                        fullname = auth_user.dataValues.fullname;
                        firstname = fullname.substr(0, fullname.indexOf(' '));
                        lastname = fullname.substr(fullname.indexOf(' ')+1);

                        if(!firstname) {
                            firstname = fullname;
                            lastname = '';
                        }

                        let args = {};
                        args.mobnum = va_number;
                        args.fname = firstname;
                        args.lname = lastname;
                        args.dob = auth_user.dataValues.tanggal_lahir;
                        args.pob = auth_user.dataValues.tempat_lahir;
                        args.email = auth_user.dataValues.email;
                        args.pin = pin;
                        args.usertype = auth_user.dataValues.type;

                        let api_response = await MoneyAccess.register(args);
                        await service_user.updateVA({user_id: auth_user.dataValues.user_id, valdo_account: va_number});

                        Logs.create({action: 'api response moneyaccess register', description: util.inspect(api_response)});
                    }
               }
            }
        }

        let args = {};
        args.user_id = auth_user.dataValues.user_id;
        args.user_salt = auth_user.dataValues.salt;
        args.pin = pin;

        await service_user.changepin(args);

        auth_user = await middleware.verify({token: token, acl: acl_edit_user, is_allowed_access_token_static: false});

        delete auth_user.dataValues.salt;
        delete auth_user.dataValues.password;

        let userUniqueCodeTableName = UserUniqueCode.getTableName();
        let where_key = `$${userUniqueCodeTableName}.user_id$`;
        let filter = {};
        filter[where_key] = auth_user.dataValues.user_id;

        let role_name = await middleware.getUSERTYPESKey(USERTYPES, auth_user.dataValues.type);
        let additional_data_user = await service_user_additional_data.get({user_id: auth_user.dataValues.user_id});
        let bank_accounts = await service_bank_account.get({user_id: auth_user.dataValues.user_id});
        let user_fmcg = await service_fmcg.get(filter);
        let name = helper.splitString(auth_user.dataValues.fullname);

        auth_user.dataValues.firstname = "";
        auth_user.dataValues.lastname = "";
        auth_user.dataValues.pin_exist = false;

        if(Array.isArray(name)) {
            if(name[0]) {
                auth_user.dataValues.firstname = name[0];
                auth_user.dataValues.lastname = name[1];
            } else {
                auth_user.dataValues.firstname = name[1];
                auth_user.dataValues.lastname = '';
            }
        }

        if(auth_user.dataValues.pin != null) {
            auth_user.dataValues.pin_exist = true;
        }

        delete auth_user.dataValues.pin;

        auth_user.dataValues.access_token_expiry = helper.formatDateTime(auth_user.dataValues.access_token_expiry);
        auth_user.dataValues.join_date = helper.formatDateTime(auth_user.dataValues.join_date);
        auth_user.dataValues.role = {
            id: auth_user.dataValues.type,
            name: role_name,
            data: additional_data_user
        };
        auth_user.dataValues.banks = bank_accounts;
        auth_user.dataValues.fmcg = user_fmcg;

        let data = helper.removeNullFromObject(auth_user.dataValues);
        let out = {};
        out.Token = {
            token: data.access_token,
            expired: data.access_token_expiry
        };

        delete data.access_token;
        delete data.access_token_expiry;

        out.User = data;
        out.message = "";
        out.status = "";
        out.code = 200;

        res.json(out);

    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal menambahkan pin, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function user_resetpin(req, res) {

    const token = req.swagger.params.token.value;

    try {
        Logs.create({action: 'user_resetpin', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_resetpin, is_allowed_access_token_static: false});

        if(auth_user.dataValues.valdo_account == null) {
            throw new CustomError(400, 'VAccount kosong, silahkan registrasi VAccount terlebih dahulu');
        }

        let moneyaccess_args = {
            vaccount:auth_user.dataValues.valdo_account,
            usertype:auth_user.dataValues.type
        };

        let moneyAccessResponse = await MoneyAccess.resetpin(moneyaccess_args);

        Logs.create({action: 'api response moneyaccess reset pin', description: util.inspect(moneyAccessResponse)});

        let args = {};

        args.pin = moneyAccessResponse.userinfo.new_pin;
        args.user_salt = auth_user.dataValues.salt;
        args.user_id = auth_user.dataValues.user_id;
        args.email = auth_user.dataValues.email;
        console.log(args)
        service_user.resetpin(args);

        res.json(new CustomSuccess(200, 'Reset pin berhasil'));

    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Reset pin gagal, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function user_changepin(req, res) {
    const token = req.swagger.params.token.value;
    const old_pin = req.swagger.params.old_pin.value;
    const new_pin = req.swagger.params.new_pin.value;
    const retype_new_pin = req.swagger.params.retype_new_pin.value;

    try {
        Logs.create({action: 'user_changepin', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_changepin, is_allowed_access_token_static: false});

        if(auth_user.dataValues.pin.length < 7) {
            throw new CustomError(400, 'Pin belum di tambahkan');
        }

        if(auth_user.dataValues.valdo_account == null) {
            throw new CustomError(400, 'VAccount kosong, silahkan registrasi VAccount terlebih dahulu');
        }

        let moneyaccess_args = {
            vaccount: auth_user.dataValues.valdo_account,
            old_pin: old_pin,
            new_pin: new_pin,
            retype_new_pin: retype_new_pin,
            usertype:auth_user.dataValues.type
        };

        let moneyAccessResponse = await MoneyAccess.changepin(moneyaccess_args);

        Logs.create({action: 'api response moneyaccess user_changepin changepin', description: util.inspect(moneyAccessResponse)});

        let args = {};

        args.pin = moneyAccessResponse.userinfo.new_pin;
        args.user_salt = auth_user.dataValues.salt;
        args.user_id = auth_user.dataValues.user_id;

        service_user.changepin(args);

        res.json(new CustomSuccess(200, 'Pin berhasil di ubah'));

    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Pin gagal di ubah, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function user_changeimei(req, res) {
    const phone = req.swagger.params.phone.value;
    const imei = req.swagger.params.imei.value;
    const otp = req.swagger.params.otp.value;

    try {
        Logs.create({action: 'user_changeimei', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let result = await service_otp.verifyOtp(null, otp, phone, 1);
        let out = {};

        if(_.size(result) > 0) {
            User.update({imei: imei}, {where: {phone: phone}});

            out.code = 200;
            out.message = "IMEI berhasil di ubah";
        } else {
            throw new CustomError(400, 'IMEI gagal di ubah');
        }

        res.status(out.code).json(out);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'IMEI gagal di ubah, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function register(req, res) {
    const param_imei = req.swagger.params.imei;
    const param_email = req.swagger.params.email;
    const param_password = req.swagger.params.password;
    const param_phone = req.swagger.params.phone;
    const param_usertype = req.swagger.params.usertype;

    let imei = _.isUndefined(param_imei.value)?helper.currentMilisecondsTime():param_imei.value;
    let email = _.isUndefined(param_email.value)?'':param_email.value;
    let password = _.isUndefined(param_password.value)?'':param_password.value;
    let phone = _.isUndefined(param_phone.value)?'':param_phone.value;
    let usertype = _.isUndefined(param_usertype.value)?'':param_usertype.value;

    try {
        Logs.create({action: 'register', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let exist_imei = await service_user.get({imei: imei});
        let exist_email = await service_user.get({email: email});
        let exist_mobnum = await service_user.get({phone: phone});

        if(!helper.isPhoneNumberOnly(phone)) {
            throw new CustomError(400, 'Format nomor telepon salah');
        }
//        if(exist_imei.length > 0) {
//            throw new CustomError(400, 'IMEI sudah di gunakan');
//        }
        if(exist_email.length > 0) {
            throw new CustomError(400, 'E-mail sudah di gunakan');
        }
        if(exist_mobnum.length > 0) {
            throw new CustomError(400, 'Nomor hp sudah di gunakan');
        }

        let args = {};
        args.imei = imei;
        args.email = email;
        args.password = password;
        args.phone = phone;
        args.usertype = usertype;

        let register_user_response = await service_user.register(args);

        if(usertype == 0) {
            await service_user_additional_data.add({user_id: register_user_response.user_id, field: 'Nama Toko', value: ''});
        }

        let user = await service_user.getOne({user_id: register_user_response.user_id});

        delete user.dataValues.salt;
        delete user.dataValues.password;
        delete user.dataValues.pin;
        delete user.dataValues.access_token_expiry;

        let userUniqueCodeTableName = UserUniqueCode.getTableName();
        let where_key = `$${userUniqueCodeTableName}.user_id$`;
        let filter = {};
        filter[where_key] = user.dataValues.user_id;

        let role_name = await middleware.getUSERTYPESKey(USERTYPES, user.dataValues.type);
        let additional_data_user = await service_user_additional_data.get({user_id: user.dataValues.user_id});
        let bank_accounts = await service_bank_account.get({user_id: user.dataValues.user_id});
        let user_fmcg = await service_fmcg.get(filter);

        user.dataValues.firstname = "";
        user.dataValues.lastname = "";
        user.dataValues.pin_exist = false;
        user.dataValues.accessTokenExpiry = helper.formatDateTime(user.dataValues.accessTokenExpiry);
        user.dataValues.join_date = helper.formatDateTime(user.dataValues.join_date);
        user.dataValues.role = {
            id: user.dataValues.type,
            name: role_name,
            data: additional_data_user
        };
        user.dataValues.banks = bank_accounts;
        user.dataValues.fmcg = user_fmcg;

        let data = helper.removeNullFromObject(user.dataValues);
        let out = {};

        out.Token = {
            token: data.accessToken,
            expired: data.accessTokenExpiry
        };

        delete data.accessToken;
        delete data.accessTokenExpiry;

        if(usertype == 0) {
            await User.update({imei: null, access_token: null}, {where: {user_id: register_user_response.user_id}});
        }

        out.User = data;
        out.message = "";
        out.status = "";
        out.code = 200;

        res.json(out);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Register gagal, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function edit_user(req, res) {
    const token = req.swagger.params.token.value;
    const fullnames = _.isUndefined(req.swagger.params.fullname.value)?'':req.swagger.params.fullname.value;
    const tanggal_lahir = _.isUndefined(req.swagger.params.tanggal_lahir.value)?'':req.swagger.params.tanggal_lahir.value;
    const tempat_lahir = _.isUndefined(req.swagger.params.tempat_lahir.value)?'':req.swagger.params.tempat_lahir.value;
    const ktp = _.isUndefined(req.swagger.params.ktp.value)?'':req.swagger.params.ktp.value;
    const role_data_field = _.isUndefined(req.swagger.params.role_data_field.value)?[]:req.swagger.params.role_data_field.value;
    const role_data_value = _.isUndefined(req.swagger.params.role_data_value.value)?[]:req.swagger.params.role_data_value.value;

    try {
        Logs.create({action: 'edit_user', description: helper.getSimpleSwaggerParams(req.swagger.params)});
        let fullname= fullnames.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
       
        let auth_user = await middleware.verify({token: token, acl: acl_edit_user, is_allowed_access_token_static: false});
        let firstname = fullname.substr(0, fullname.indexOf(' '));
        let lastname = fullname.substr(fullname.indexOf(' ')+1);
        let combine_name = '';

        if(auth_user.dataValues.type == 3 || auth_user.dataValues.type == 10 || auth_user.dataValues.type == 11) {
            if(!firstname) {
                combine_name = lastname;
            } else {
                combine_name = firstname+' '+lastname;
            }

            let args = {};
            args.user_id = auth_user.dataValues.user_id;
            args.fullname = combine_name;
            args.tanggal_lahir = tanggal_lahir;
            args.tempat_lahir = tempat_lahir;
            args.ktp = ktp;

            await service_user.update(args);
        } else {

            if(auth_user.dataValues.type == 0) {
                if(role_data_field.length != role_data_value.length) {
                    throw new CustomError(400, 'Data tambahan user tidak valid');
                }

                for (let i = 0; i < role_data_field.length; i++) {
                    await service_user_additional_data.edit({user_id: auth_user.dataValues.user_id, field: role_data_field[i], value: role_data_value[i]});
                }
            }

            if(!firstname) {
                combine_name = lastname;
            } else {
                combine_name = firstname+' '+lastname;
            }

            let args = {};
            args.user_id = auth_user.dataValues.user_id;
            args.fullname = combine_name;
            args.tanggal_lahir = tanggal_lahir;
            args.tempat_lahir = tempat_lahir;
            args.ktp = ktp;

            await service_user.update(args);
        }

        auth_user = await middleware.verify({token: token, acl: acl_edit_user, is_allowed_access_token_static: false});

        delete auth_user.dataValues.salt;
        delete auth_user.dataValues.password;

        let userUniqueCodeTableName = UserUniqueCode.getTableName();
        let where_key = `$${userUniqueCodeTableName}.user_id$`;
        let filter = {};
        filter[where_key] = auth_user.dataValues.user_id;

        let role_name = await middleware.getUSERTYPESKey(USERTYPES, auth_user.dataValues.type);
        let additional_data_user = await service_user_additional_data.get({user_id: auth_user.dataValues.user_id});
        let bank_accounts = await service_bank_account.get({user_id: auth_user.dataValues.user_id});
        let user_fmcg = await service_fmcg.get(filter);
        let name = helper.splitString(auth_user.dataValues.fullname);

        auth_user.dataValues.firstname = "";
        auth_user.dataValues.lastname = "";
        auth_user.dataValues.pin_exist = false;

        if(Array.isArray(name)) {
            if(name[0]) {
                auth_user.dataValues.firstname = name[0];
                auth_user.dataValues.lastname = name[1];
            } else {
                auth_user.dataValues.firstname = name[1];
                auth_user.dataValues.lastname = '';
            }
        }

        if(auth_user.dataValues.pin != null) {
            auth_user.dataValues.pin_exist = true;
        }

        delete auth_user.dataValues.pin;

        auth_user.dataValues.access_token_expiry = helper.formatDateTime(auth_user.dataValues.access_token_expiry);
        auth_user.dataValues.join_date = helper.formatDateTime(auth_user.dataValues.join_date);
        auth_user.dataValues.role = {
            id: auth_user.dataValues.type,
            name: role_name,
            data: additional_data_user
        };
        auth_user.dataValues.banks = bank_accounts;
        auth_user.dataValues.fmcg = user_fmcg;

        let data = helper.removeNullFromObject(auth_user.dataValues);
        let out = {};
        out.Token = {
            token: data.access_token,
            expired: data.access_token_expiry
        };

        delete data.access_token;
        delete data.access_token_expiry;

        out.User = data;
        out.message = "";
        out.status = "";
        out.code = 200;

        res.json(out);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Data user gagal di ubah, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function get_one_user(req, res) {
    const token = req.swagger.params.token.value;
    const id = req.swagger.params.id.value;
    const param_imei = req.swagger.params.imei;

    let imei = _.isUndefined(param_imei.value)?'':param_imei.value;

    try {
        Logs.create({action: 'get_one_user', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_get_one_user, is_allowed_access_token_static: false});

        if(imei.length != 0) {
            if(auth_user.dataValues.imei != imei) {
                caller_logout(auth_user.dataValues.user_id);
                return res.status(400).json({code: 4001, message: 'Login gagal, IMEI berbeda', phone: auth_user.dataValues.phone});
            }

            if(auth_user.dataValues.imei == null || auth_user.dataValues.imei == "") {
                User.update({imei: imei}, {where: {user_id: auth_user.dataValues.user_id}});
            }
        }

        delete auth_user.dataValues.salt;
        delete auth_user.dataValues.password;

        let userUniqueCodeTableName = UserUniqueCode.getTableName();
        let where_key = `$${userUniqueCodeTableName}.user_id$`;
        let filter = {};
        filter[where_key] = auth_user.dataValues.user_id;

        let role_name = await middleware.getUSERTYPESKey(USERTYPES, auth_user.dataValues.type);
        let additional_data_user = await service_user_additional_data.get({user_id: auth_user.dataValues.user_id});
        let bank_accounts = await service_bank_account.get({user_id: auth_user.dataValues.user_id});
        let user_fmcg = await service_fmcg.get(filter);
        let name = helper.splitString(auth_user.dataValues.fullname);

        auth_user.dataValues.firstname = "";
        auth_user.dataValues.lastname = "";
        auth_user.dataValues.pin_exist = false;

        if(Array.isArray(name)) {
            if(name[0]) {
                auth_user.dataValues.firstname = name[0];
                auth_user.dataValues.lastname = name[1];
            } else {
                auth_user.dataValues.firstname = name[1];
                auth_user.dataValues.lastname = '';
            }
        }

        if(auth_user.dataValues.pin != null) {
            auth_user.dataValues.pin_exist = true;
        }

        delete auth_user.dataValues.pin;

        auth_user.dataValues.access_token_expiry = helper.formatDateTime(auth_user.dataValues.access_token_expiry);
        auth_user.dataValues.join_date = helper.formatDateTime(auth_user.dataValues.join_date);
        auth_user.dataValues.role = {
            id: auth_user.dataValues.type,
            name: role_name,
            data: additional_data_user
        };

        for(let i=0; i < bank_accounts.length; i++){
           bank_accounts[i].dataValues.icon = bank_accounts[i].dataValues.bank.dataValues.icon;
        }
        auth_user.dataValues.banks = bank_accounts;
        auth_user.dataValues.fmcg = user_fmcg;

        let data = helper.removeNullFromObject(auth_user.dataValues);
        let out = {};
        out.Token = {
            token: data.access_token,
            expired: data.access_token_expiry
        };

        delete data.access_token;
        delete data.access_token_expiry;

        out.User = data;
        out.message = "";
        out.status = "";
        out.code = 200;

        res.json(out);

    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal mendapatkan data user, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function renew_token(req, res) {
    const token = req.swagger.params.token.value;

    let out = {};
    let new_token = '';

    try {
        Logs.create({action: 'renew_token', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let user = await service_user.authUser({token: token});

        if(user == null) {
            throw new CustomError(400, 'Token tidak valid');
        }

        // regenerate access token if nearing expired
        let is_check_token_expiry = true;

        if (is_check_token_expiry) {
            let delta_days_in_seconds = 259200; // 3 days

            if (user) {

                // expiry is seconds
                let expiry_ts = await helper.modifiedSequelizeTimestampStandart(user.dataValues.access_token_expiry).getTime();

                if(isNaN(expiry_ts)) {
                    expiry_ts = 1463312000;
                }

                const now_seconds = new Date().getTime();

                if (now_seconds > (expiry_ts - delta_days_in_seconds)) {
                    // renew access token and update db
                    let new_access_token = await service_user.renewAccessToken({user:user});
                    new_token = new_access_token;
                } else {
                    new_token = user.dataValues.access_token;
                }

                out.token = new_token;
            }
        }

        res.json(out);

    } catch (err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal memperbarui token, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function update_fcm(req, res) {
    const token = req.swagger.params.token.value;
    const fcm_id = req.swagger.params.fcm_id.value;

    try {
        Logs.create({action: 'update_fcm', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_get_users, is_allowed_access_token_static: false});
        await service_user.updateFcm({user_id: auth_user.dataValues.user_id, fcm_id: fcm_id});

        res.json(new CustomSuccess(200, 'Fcm berhasil di ubah'));
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal mengubah fcm id, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function get_balance(req,res){
    const token = req.swagger.params.token.value;
    let out = '';
    let out_top = '';
    let fit_response = 0;
    let saldo ={"saldo_fit":0};

    try {
        Logs.create({action: 'get_balance', description: helper.getSimpleSwaggerParams(req.swagger.params)});
       
        let auth_user = await middleware.verify({token: token, acl: acl_get_balance, is_allowed_access_token_static: false});

        // let getOne = await service_user_unique_code.getOne({user_id: auth_user.dataValues.user_id,fmcg_id:13})
        // if(getOne == null || getOne == '') {
        //      out = 0;
        //      out_top = 0;
        //      fit_response =0;
        // }else{

        //     fit_response = await ApiTop.cek_saldo({le_code:getOne.dataValues.unique_code});
        // }
        if(auth_user.dataValues.valdo_account == null || auth_user.dataValues.valdo_account == '') {
             out = 0;
             out_top = 0;
        } else {
            let api_response = await MoneyAccess.inquiry({mobnum: auth_user.dataValues.valdo_account, vaccount: auth_user.dataValues.valdo_account, usertype: auth_user.dataValues.type});
            Logs.create({action: 'api response moneyaccess get_balance inquiry', description: util.inspect(api_response)});

            delete api_response.userid;
            delete api_response.vacc_number;
            out = api_response.balance;
            if(auth_user.dataValues.type ===0){
                   if(fit_response===0 || fit_response.saldo.status =="failed"){
                        saldo =0
                    } else{
            if(fit_response.saldo.status == "success"){
               saldo ={"saldo_fit":fit_response.saldo.data.plafond};
           }
            else{
                 saldo ={"saldo_fit":0};
            }
                }
            }
        }
         
        res.json({balance: out, server_time: helper.formatCurrentDateTime(), saldo_fmcg: [saldo]});
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal mendapatkan informasi saldo, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}


async function get_user_fmcg(req, res) {
  const token = req.swagger.params.token.value;

    try {
        Logs.create({action: 'get_user_fmcg', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_get_balance, is_allowed_access_token_static: false});
        let out = {};
        let saldo_fit = 0;
        let status_fit = false;
        let outlet_type = "Credit";
        let new_retur = false;
        
        let saldo_fit2 = 0;
        let status_fit2 = false;
        let outlet_type2 = "Credit";
        let new_retur2 = false;
        let logo ="";
        let paid_by = "fit";
        
        let cek_fintech = "fit";
        let cek_fintech2 = "fit";

        let text="";
        let text2="";
        let textojk="";
        let url_pohondana ="";
        let perjanjian = "";
        let term_and_service_url = "";
        let status_topex=false;

         let ordering = false;
        let url_ordering = "";
        let status_ordering = false;
        let url_ordering_list = "";
        
        let getdatalecode =[];
        
        let fit ="";
        let user_has_fmcg = [];
        
        let userUniqueCodeTableName = UserUniqueCode.getTableName();
        let where_key = `$${userUniqueCodeTableName}.user_id$`;
        let filter = {};
        filter[where_key] = auth_user.dataValues.user_id;
        let user_fmcg = await service_fmcg.get(filter);
//        let i = 1;
//        do {
        for(let i=0; i < user_fmcg.length; i++){
            if(user_fmcg[i].dataValues.id === 1){
                let getOne = await service_user_unique_code.getOne({user_id:auth_user.dataValues.user_id,fmcg_id:user_fmcg[i].dataValues.id})
                if(getOne == null || getOne == '') {
                    outlet_type = "Credit";
                    new_retur = false;
                    saldo_fit =0;
                    status_fit=false;
                    logo ="";
                    paid_by ="fit";
                }else{
                    getdatalecode = await ApiBackend.getdatalecode({le_code:getOne.dataValues.unique_code});
                    outlet_type = getdatalecode.fintech.toko; 
                    new_retur = getdatalecode.fintech.toko === "Cash" ? true : false;
                    saldo_fit = getdatalecode.fintech.fintech === "fit" ? getdatalecode.fintech.plafond[0].plafond : getdatalecode.fintech.plafond[1].plafond;;
                    logo = getdatalecode.fintech.fintech === "pohondana" ? "/fmcg-icon/pohondana.png":"";
                    
                    if(getdatalecode.fintech.fintech === "fit"){
                        if(getdatalecode.fintech.plafond[0].status === "success" && getdatalecode.fintech.plafond[1].status === "success"){status_topex=true}else{status_topex=false;}
                    }else{
                        logo = getdatalecode.fintech.fintech === "pohondana" ? "" : "/fmcg-icon/pohondana.png";
                        if(getdatalecode.fintech.plafond[1].status != "success" && getdatalecode.fintech.plafond[1].status === "success"){status_topex=true}else{status_topex=false;}
                    }
                    paid_by = getdatalecode.fintech.fintech;
                    status_fit= getdatalecode.fintech.is_active;

                }
                user_fmcg[i].dataValues.is_topex = true
                user_fmcg[i].dataValues.status_fit = status_fit;
                user_fmcg[i].dataValues.balance = 0;
                user_fmcg[i].dataValues.topex_name = "Limit Pandai";
                user_fmcg[i].dataValues.paid_by = paid_by;
                user_fmcg[i].dataValues.week =
                  [{"loan_length": 1}]
            }else if(user_fmcg[i].dataValues.id === 13){
                let getOne = await service_user_unique_code.getOne({user_id:auth_user.dataValues.user_id,fmcg_id:user_fmcg[i].dataValues.id})
               for(let j=0; j < user_fmcg[i].dataValues.users_unique_codes.length; j++){
                    user_fmcg[i].dataValues.users_unique_codes[j].dataValues.ordering='1';
                    let getlecode = await service_user_unique_code.getOne({user_id:auth_user.dataValues.user_id,fmcg_id:user_fmcg[i].dataValues.id})
                    getdatalecode = await ApiBackend.getdatalecode({le_code:getOne.dataValues.unique_code});
                    if(getdatalecode.ordering.status === true){
                        ordering = true;url_ordering = getdatalecode.ordering.redirect_to_store;
                        status_ordering = getdatalecode.ordering.status;
                        url_ordering_list = getdatalecode.ordering.redirect_to_order_list;
                    }
                    else{
                        url_ordering_list = "";ordering = false;url_ordering = "";status_ordering = getdatalecode.ordering.status;
                    }
                    user_fmcg[i].dataValues.users_unique_codes[j].dataValues.status_ordering = status_ordering;
                    user_fmcg[i].dataValues.users_unique_codes[j].dataValues.ordering = ordering;

                    
                    let pembelian  ={};
                    pembelian.name  = "Pembelian";
                    pembelian.description = "Beli Semua Produk Unilever Disini";
                    pembelian.url_ordering = url_ordering; 
                    pembelian.icon="/fmcg-icon/unilever-icon.png"
                  
                    let list_order ={};
                    list_order.name = "Riwayat Pembelian";
                    list_order.description = "Lihat Aktivitas Pembelian";
                    list_order.url_ordering = url_ordering_list;
                    list_order.icon="/fmcg-icon/umum.png"
                    let menu_ordering  =[pembelian,list_order];
                    user_fmcg[i].dataValues.users_unique_codes[j].dataValues.menu_ordering = menu_ordering;
                  
              
                }
                
                if(getOne == null || getOne == '') {
                    ordering = false;
                    url_ordering = "";
                    status_ordering = false;
                    outlet_type = "Credit";
                    new_retur = false;
                    saldo_fit =0;
                    status_fit=false;
                    logo ="";
                    paid_by ="fit";
                }else{
                    if(auth_user.dataValues.type===0){
                    getdatalecode = await ApiBackend.getdatalecode({le_code:getOne.dataValues.unique_code});
       
                    outlet_type = getdatalecode.fintech.toko; 
                    new_retur = getdatalecode.fintech.toko === "Cash" ? true : false;
                     if(getdatalecode.fintech.fintech == "fit"){
                        saldo_fit=getdatalecode.fintech.plafond[0].plafond;
                        textojk="Layanan ini diselenggarakan oleh PT Finansial Integrasi Teknologi yang terdaftar dan diawasi oleh Otoritas Jasa Keuangan";
                    }else if(getdatalecode.fintech.fintech == "pohondana"){
                        saldo_fit=getdatalecode.fintech.plafond[1].plafond;
                    }else if(getdatalecode.fintech.fintech == "Aspirasi"){
                        saldo_fit=getdatalecode.fintech.plafond[2].plafond;
                        textojk = "Layanan ini diselenggarakan oleh PT Julo Jeknologi Indonesia yang terdaftar dan diawasi oleh Otoritas Jasa Keuangan";
                    
                    }else {
                        saldo_fit=0;
                    }
                    logo = getdatalecode.fintech.fintech === "pohondana" ? "/fmcg-icon/pohondana.png":"";
                    if(getdatalecode.ordering.status === true){
                        ordering = true;url_ordering = getdatalecode.ordering.redirect_to_store;
                        status_ordering = getdatalecode.ordering.status;
                    }
                    else{
                        ordering = false;url_ordering = "";status_ordering = getdatalecode.ordering.status;
                    }
                    if(getdatalecode.fintech.fintech === "fit"){
                    if(getdatalecode.fintech.plafond[0].status === "success" && getdatalecode.fintech.plafond[1].status === "success"){status_topex=true}else{status_topex=false;}
                    }else{
                        if(getdatalecode.fintech.plafond[1].status != "success" && getdatalecode.fintech.plafond[1].status === "success"){status_topex=true}else{status_topex=false;}
                    }
                    paid_by = getdatalecode.fintech.fintech;
                    status_fit= getdatalecode.fintech.is_active;
                    textojk = "Layanan ini diselenggarakan oleh PT Julo Jeknologi Indonesia yang terdaftar dan diawasi oleh Otoritas Jasa Keuangan"
                    //textojk = getdatalecode.fintech.fintech === "fit" ? "Layanan ini diselenggarakan oleh PT Finansial Integrasi Teknologi yang terdaftar dan diawasi oleh Otoritas Jasa Keuangan" : "Layanan ini diselenggarakan oleh PT Pohon Dana Indonesia yang terdaftar dan diawasi oleh Otoritas Jasa Keuangan";

                    }else{
                        outlet_type = "Credit";
                        new_retur = false;
                        saldo_fit =0;
                        status_fit=false;
                        logo ="";
                        paid_by ="fit";
                    }
                }
                user_fmcg[i].dataValues.is_topex = false;
                user_fmcg[i].dataValues.status_fit = status_fit;
                user_fmcg[i].dataValues.outlet_type = outlet_type;
                user_fmcg[i].dataValues.topex_name = "Limit Pandai Uli";
                user_fmcg[i].dataValues.balance = getdatalecode.fintech.plafond[2].plafond;
                user_fmcg[i].dataValues.url_logo_fintech= logo;
                user_fmcg[i].dataValues.allow_new_retur = new_retur;
                user_fmcg[i].dataValues.paid_by = paid_by;
                user_fmcg[i].dataValues.perjanjian = "/faq/perjanjian_aspirasi.html";
                user_fmcg[i].dataValues.term_and_service_url = "/faq/perjanjian_aspirasi.html";
                user_fmcg[i].dataValues.url_ojk = "/fmcg-icon/logo_ojk.png";
                user_fmcg[i].dataValues.url_text_ojk =textojk
                user_fmcg[i].dataValues.position =getOne.dataValues.position
                user_fmcg[i].dataValues.week =
                     [{"loan_length": 1}]
            }
            else if(user_fmcg[i].dataValues.id === 10){
                let getOne = await service_user_unique_code.getOne({user_id:auth_user.dataValues.user_id,fmcg_id:user_fmcg[i].dataValues.id})
               for(let j=0; j < user_fmcg[i].dataValues.users_unique_codes.length; j++){
                    user_fmcg[i].dataValues.users_unique_codes[j].dataValues.ordering='1';
                    let getlecode = await service_user_unique_code.getOne({user_id:auth_user.dataValues.user_id,fmcg_id:user_fmcg[i].dataValues.id})
                    getdatalecode = await ApiBackend.getdatalecode({le_code:getOne.dataValues.unique_code});
                    if(getdatalecode.ordering.status === true){
                        ordering = true;url_ordering = getdatalecode.ordering.redirect_to_store;
                        status_ordering = getdatalecode.ordering.status;
                        url_ordering_list = getdatalecode.ordering.redirect_to_order_list;
                    }
                    else{
                        url_ordering_list = "";ordering = false;url_ordering = "";status_ordering = getdatalecode.ordering.status;
                    }
                    user_fmcg[i].dataValues.users_unique_codes[j].dataValues.status_ordering = status_ordering;
                    user_fmcg[i].dataValues.users_unique_codes[j].dataValues.ordering = ordering;

                    
                    let pembelian  ={};
                    pembelian.name  = "Pembelian";
                    pembelian.description = "Beli Semua Produk Unilever Disini";
                    pembelian.url_ordering = url_ordering; 
                    pembelian.icon="/fmcg-icon/unilever-icon.png"
                  
                    let list_order ={};
                    list_order.name = "Riwayat Pembelian";
                    list_order.description = "Lihat Aktivitas Pembelian";
                    list_order.url_ordering = url_ordering_list;
                    list_order.icon="/fmcg-icon/umum.png"
                    let menu_ordering  =[pembelian,list_order];
                    user_fmcg[i].dataValues.users_unique_codes[j].dataValues.menu_ordering = menu_ordering;
                  
              
                }
                
                if(getOne == null || getOne == '') {
                    ordering = false;
                    url_ordering = "";
                    status_ordering = false;
                    outlet_type = "Credit";
                    new_retur = false;
                    saldo_fit =0;
                    status_fit=false;
                    logo ="";
                    paid_by ="fit";
                }else{
                    if(auth_user.dataValues.type===0){
                    getdatalecode = await ApiBackend.getdatalecode({le_code:getOne.dataValues.unique_code});
       
                    outlet_type = getdatalecode.fintech.toko; 
                    new_retur = getdatalecode.fintech.toko === "Cash" ? true : false;
                     if(getdatalecode.fintech.fintech == "fit"){
                        saldo_fit=getdatalecode.fintech.plafond[0].plafond;
                        textojk="Layanan ini diselenggarakan oleh PT Finansial Integrasi Teknologi yang terdaftar dan diawasi oleh Otoritas Jasa Keuangan";
                    }else if(getdatalecode.fintech.fintech == "pohondana"){
                        saldo_fit=getdatalecode.fintech.plafond[1].plafond;
                    }else if(getdatalecode.fintech.fintech == "Aspirasi"){
                        saldo_fit=getdatalecode.fintech.plafond[2].plafond;
                        textojk = "Layanan ini diselenggarakan oleh PT Julo Jeknologi Indonesia yang terdaftar dan diawasi oleh Otoritas Jasa Keuangan";
                    
                    }else {
                        saldo_fit=0;
                    }
                    logo = getdatalecode.fintech.fintech === "pohondana" ? "/fmcg-icon/pohondana.png":"";
                    if(getdatalecode.ordering.status === true){
                        ordering = true;url_ordering = getdatalecode.ordering.redirect_to_store;
                        status_ordering = getdatalecode.ordering.status;
                    }
                    else{
                        ordering = false;url_ordering = "";status_ordering = getdatalecode.ordering.status;
                    }
                    if(getdatalecode.fintech.fintech === "fit"){
                    if(getdatalecode.fintech.plafond[0].status === "success" && getdatalecode.fintech.plafond[1].status === "success"){status_topex=true}else{status_topex=false;}
                    }else{
                        if(getdatalecode.fintech.plafond[1].status != "success" && getdatalecode.fintech.plafond[1].status === "success"){status_topex=true}else{status_topex=false;}
                    }
                    paid_by = getdatalecode.fintech.fintech;
                    status_fit= getdatalecode.fintech.is_active;
                    textojk = "Layanan ini diselenggarakan oleh PT Julo Jeknologi Indonesia yang terdaftar dan diawasi oleh Otoritas Jasa Keuangan"
                    //textojk = getdatalecode.fintech.fintech === "fit" ? "Layanan ini diselenggarakan oleh PT Finansial Integrasi Teknologi yang terdaftar dan diawasi oleh Otoritas Jasa Keuangan" : "Layanan ini diselenggarakan oleh PT Pohon Dana Indonesia yang terdaftar dan diawasi oleh Otoritas Jasa Keuangan";

                    }else{
                        outlet_type = "Credit";
                        new_retur = false;
                        saldo_fit =0;
                        status_fit=false;
                        logo ="";
                        paid_by ="fit";
                    }
                }
                user_fmcg[i].dataValues.is_topex = false;
                user_fmcg[i].dataValues.status_fit = status_fit;
                user_fmcg[i].dataValues.outlet_type = outlet_type;
                user_fmcg[i].dataValues.topex_name = "Limit Pandai Bekraf";
                user_fmcg[i].dataValues.balance = getdatalecode.fintech.plafond[2].plafond;
                user_fmcg[i].dataValues.url_logo_fintech= logo;
                user_fmcg[i].dataValues.allow_new_retur = new_retur;
                user_fmcg[i].dataValues.paid_by = paid_by;
                user_fmcg[i].dataValues.perjanjian = "/faq/perjanjian_aspirasi.html";
                user_fmcg[i].dataValues.term_and_service_url = "/faq/perjanjian_aspirasi.html";
                user_fmcg[i].dataValues.url_ojk = "/fmcg-icon/logo_ojk.png";
                user_fmcg[i].dataValues.url_text_ojk =textojk
                user_fmcg[i].dataValues.position =getOne.dataValues.position
                user_fmcg[i].dataValues.week =
                     [{"loan_length": 1}]
            }
            else if(user_fmcg[i].dataValues.id === 2){
                let getOne = await service_user_unique_code.getOne({user_id:auth_user.dataValues.user_id,fmcg_id:user_fmcg[i].dataValues.id})
                if(getOne == null || getOne == '') {
                    outlet_type = "Credit";
                    new_retur = false;
                    saldo_fit =0;
                    status_fit=false;
                    logo ="";
                    paid_by = "";
                }else{
                   if(auth_user.dataValues.type===0){
                    getdatalecode = await ApiBackend.getdatalecode({le_code:getOne.dataValues.unique_code});
                    outlet_type = getdatalecode.fintech.toko; 
                    new_retur = getdatalecode.fintech.toko === "Cash" ? true : false;
                    saldo_fit = getdatalecode.fintech.fintech === "fit" ? getdatalecode.fintech.plafond[0].plafond : getdatalecode.fintech.plafond[1].plafond;;
                    logo = getdatalecode.fintech.fintech === "pohondana" ? "/fmcg-icon/pohondana.png":"";
                    perjanjian = getdatalecode.fintech.fintech === "pohondana" ? "/faq/faq_perjanjian_pohondana.html":"/faq/faq_perjanjian_pinjam_modal.html";
                    term_and_service_url = getdatalecode.fintech.fintech === "pohondana" ? "/faq/tncpohondana.html":"/faq/tncpinjammodal.html";
                    
                    if(getdatalecode.fintech.fintech === "fit"){
                        if(getdatalecode.fintech.plafond[0].status === "success" && getdatalecode.fintech.plafond[1].status === "success"){status_topex=true}else{status_topex=false;}
                    }else{
                        if(getdatalecode.fintech.plafond[1].status != "success" && getdatalecode.fintech.plafond[1].status === "success"){status_topex=true}else{status_topex=false;}
                    }
                    paid_by = getdatalecode.fintech.fintech;
                    status_fit= getdatalecode.fintech.is_active;
                    textojk = getdatalecode.fintech.fintech === "fit" ? "Layanan ini diselenggarakan oleh PT Finansial Integrasi Teknologi yang terdaftar dan diawasi oleh Otoritas Jasa Keuangan" : "Layanan ini diselenggarakan oleh PT Pohon Dana Indonesia yang terdaftar dan diawasi oleh Otoritas Jasa Keuangan";
                    }else{
                        outlet_type = "Credit";
                        new_retur = false;
                        saldo_fit =0;
                        status_fit=false;
                        logo ="";
                        paid_by ="fit";
                    }
                }
                user_fmcg[i].dataValues.is_topex = false;
                user_fmcg[i].dataValues.status_fit = status_fit;
                user_fmcg[i].dataValues.outlet_type = outlet_type;
                user_fmcg[i].dataValues.topex_name = "Limit Pandai Walls";
                user_fmcg[i].dataValues.balance = saldo_fit;
                user_fmcg[i].dataValues.url_logo_fintech= logo;
                user_fmcg[i].dataValues.allow_new_retur = new_retur;
                user_fmcg[i].dataValues.paid_by = paid_by;
                user_fmcg[i].dataValues.perjanjian = perjanjian;
                user_fmcg[i].dataValues.term_and_service_url = term_and_service_url;
                user_fmcg[i].dataValues.url_ojk = "/fmcg-icon/logo_ojk.png";
                user_fmcg[i].dataValues.url_text_ojk = textojk
                user_fmcg[i].dataValues.week =
                  [{"loan_length": 1}]
            }
            else if(user_fmcg[i].dataValues.id === 14){
                let getOne = await service_user_unique_code.getOne({user_id:auth_user.dataValues.user_id,fmcg_id:user_fmcg[i].dataValues.id})
                if(getOne == null || getOne == '') {
                    outlet_type = "Credit";
                    new_retur = false;
                    saldo_fit =0;
                    status_fit=false;
                    logo ="";
                    paid_by = "";
                }else{
                    getdatalecode = await ApiBackend.getdatalecode({le_code:getOne.dataValues.unique_code});
                    outlet_type = getdatalecode.fintech.toko; 
                    new_retur = getdatalecode.fintech.toko === "Cash" ? true : false;
                    saldo_fit = getdatalecode.fintech.fintech === "fit" ? getdatalecode.fintech.plafond[0].plafond : getdatalecode.fintech.plafond[1].plafond;;
                    logo = getdatalecode.fintech.fintech === "pohondana" ? "/fmcg-icon/pohondana.png":"";
                    perjanjian = getdatalecode.fintech.fintech === "pohondana" ? "/faq/faq_perjanjian_pohondana.html":"/faq/faq_perjanjian_pinjam_modal.html";
                    term_and_service_url = getdatalecode.fintech.fintech === "pohondana" ? "/faq/tncpohondana.html":"/faq/tncpinjammodal.html";
                    
                    if(getdatalecode.fintech.fintech === "fit"){
                        if(getdatalecode.fintech.plafond[0].status === "success" && getdatalecode.fintech.plafond[1].status === "success"){status_topex=true}else{status_topex=false;}
                    }else{
                        if(getdatalecode.fintech.plafond[1].status != "success" && getdatalecode.fintech.plafond[1].status === "success"){status_topex=true}else{status_topex=false;}
                    }
                    status_fit= getdatalecode.fintech.is_active;
                    paid_by = getdatalecode.fintech.fintech;
                    textojk = getdatalecode.fintech.fintech === "fit" ? "Layanan ini diselenggarakan oleh PT Finansial Integrasi Teknologi yang terdaftar dan diawasi oleh Otoritas Jasa Keuangan" : "Layanan ini diselenggarakan oleh PT Pohon Dana Indonesia yang terdaftar dan diawasi oleh Otoritas Jasa Keuangan";
               
                }
                user_fmcg[i].dataValues.is_topex = false;
                user_fmcg[i].dataValues.status_fit = status_fit;
                user_fmcg[i].dataValues.outlet_type = outlet_type;
                user_fmcg[i].dataValues.topex_name = "Limit Pandai Godrej";
                user_fmcg[i].dataValues.balance = saldo_fit;
                user_fmcg[i].dataValues.url_logo_fintech= logo;
                user_fmcg[i].dataValues.allow_new_retur = new_retur;
                user_fmcg[i].dataValues.paid_by = paid_by;
                user_fmcg[i].dataValues.perjanjian = perjanjian;
                user_fmcg[i].dataValues.term_and_service_url = term_and_service_url;
                user_fmcg[i].dataValues.url_ojk = "/fmcg-icon/logo_ojk.png";
                user_fmcg[i].dataValues.url_text_ojk = textojk;
                user_fmcg[i].dataValues.week =
                  [{"loan_length": 1}]
            }
            else if(user_fmcg[i].dataValues.id === 15){
                let getOne = await service_user_unique_code.getOne({user_id:auth_user.dataValues.user_id,fmcg_id:user_fmcg[i].dataValues.id})
                if(getOne == null || getOne == '') {
                    outlet_type = "Credit";
                    new_retur = false;
                    saldo_fit =0;
                    status_fit=false;
                    logo ="";
                    paid_by = "";
                }else{
                    if(auth_user.dataValues.type===0){
                    getdatalecode = await ApiBackend.getdatalecode({le_code:getOne.dataValues.unique_code});
                    outlet_type = getdatalecode.fintech.toko; 
                    new_retur = getdatalecode.fintech.toko === "Cash" ? true : false;
                    saldo_fit = getdatalecode.fintech.fintech === "fit" ? getdatalecode.fintech.plafond[0].plafond : getdatalecode.fintech.plafond[1].plafond;;       
                    logo = getdatalecode.fintech.fintech === "pohondana" ? "/fmcg-icon/pohondana.png":"";
                    perjanjian = getdatalecode.fintech.fintech === "pohondana" ? "/faq/faq_perjanjian_pohondana.html":"/faq/faq_perjanjian_pinjam_modal.html";
                    term_and_service_url = getdatalecode.fintech.fintech === "pohondana" ? "/faq/tncpohondana.html":"/faq/tncpinjammodal.html";
                    
                    if(getdatalecode.fintech.fintech === "fit"){
                        if(getdatalecode.fintech.plafond[0].status === "success" && getdatalecode.fintech.plafond[1].status === "success"){status_topex=true}else{status_topex=false;}
                    }else{
                        if(getdatalecode.fintech.plafond[1].status != "success" && getdatalecode.fintech.plafond[1].status === "success"){status_topex=true}else{status_topex=false;}
                    }
                    paid_by = getdatalecode.fintech.fintech;
                    status_fit= getdatalecode.fintech.is_active;
                    textojk = getdatalecode.fintech.fintech === "fit" ? "Layanan ini diselenggarakan oleh PT Finansial Integrasi Teknologi yang terdaftar dan diawasi oleh Otoritas Jasa Keuangan" : "Layanan ini diselenggarakan oleh PT Pohon Dana Indonesia yang terdaftar dan diawasi oleh Otoritas Jasa Keuangan";
                    }else{
                        outlet_type = "Credit";
                        new_retur = false;
                        saldo_fit =0;
                        status_fit=false;
                        logo ="";
                        paid_by ="fit";
                    }
                }
                user_fmcg[i].dataValues.is_topex = false;
                user_fmcg[i].dataValues.status_fit = status_fit;
                user_fmcg[i].dataValues.outlet_type = outlet_type;
                user_fmcg[i].dataValues.topex_name = "Limit Pandai lakme";
                user_fmcg[i].dataValues.balance = saldo_fit;
                user_fmcg[i].dataValues.url_logo_fintech= logo;
                user_fmcg[i].dataValues.allow_new_retur = new_retur;
                user_fmcg[i].dataValues.paid_by = paid_by;
                user_fmcg[i].dataValues.perjanjian = perjanjian;
                user_fmcg[i].dataValues.term_and_service_url = term_and_service_url;
                user_fmcg[i].dataValues.url_ojk = "/fmcg-icon/logo_ojk.png";
                user_fmcg[i].dataValues.url_text_ojk = textojk;
                user_fmcg[i].dataValues.week =
                [{"loan_length": 1}]
            }
            else if(user_fmcg[i].dataValues.id === 3){
                //let ppob = await MoneyAccess.inquiry({mobnum: auth_user.dataValues.valdo_account, vaccount: auth_user.dataValues.valdo_account, usertype: auth_user.dataValues.type});
                let getOne = await service_user_unique_code.getOne({user_id:auth_user.dataValues.user_id,fmcg_id:user_fmcg[i].dataValues.id})
                //let api_response = await MoneyAccess.inquiry({mobnum: auth_user.dataValues.valdo_account, vaccount: auth_user.dataValues.valdo_account, usertype: auth_user.dataValues.type});
                getdatalecode = await ApiBackend.getdatalecode({le_code:getOne.dataValues.unique_code});
                    
                let aktif = false
                if (parseInt(getdatalecode.store.va_detail.is_ppob_limit) == 1){aktif = true}else{aktif = false}
                user_fmcg[i].dataValues.is_topex = true;
                user_fmcg[i].dataValues.is_active =  aktif;
                user_fmcg[i].dataValues.is_quota_ppob = true;
                user_fmcg[i].dataValues.paid_by = "kuota";
                user_fmcg[i].dataValues.topex_name = "Quota PPOB";
                user_fmcg[i].dataValues.balance= getdatalecode.store.va_detail.current_ppob_limit;
                user_fmcg[i].dataValues.total_limit = getdatalecode.store.va_detail.ppob_limit;
            }else if(user_fmcg[i].dataValues.id === 16){
                let getOne = await service_user_unique_code.getOne({user_id:auth_user.dataValues.user_id,fmcg_id:user_fmcg[i].dataValues.id})

                if(getOne == null || getOne == '') {
                    outlet_type = "Credit";
                    new_retur = false;
                    saldo_fit =0;
                    status_fit=false;
                    logo ="";
                    paid_by ="fit";
                }else{
                    if(auth_user.dataValues.type===0){
                    getdatalecode = await ApiBackend.getdatalecode({le_code:getOne.dataValues.unique_code});
                    outlet_type = getdatalecode.fintech.toko; 
                    new_retur = getdatalecode.fintech.toko === "Cash" ? true : false;
                    saldo_fit = getdatalecode.fintech.fintech === "fit" ? getdatalecode.fintech.plafond[0].plafond : getdatalecode.fintech.plafond[1].plafond;;
                    logo = getdatalecode.fintech.fintech === "pohondana" ? "/fmcg-icon/pohondana.png":"";
                    perjanjian = getdatalecode.fintech.fintech === "pohondana" ? "/faq/faq_perjanjian_pohondana.html":"/faq/faq_perjanjian_pinjam_modal.html";
                    term_and_service_url = getdatalecode.fintech.fintech === "pohondana" ? "/faq/tncpohondana.html":"/faq/tncpinjammodal.html";
                    
                    if(getdatalecode.fintech.fintech === "fit"){
                        if(getdatalecode.fintech.plafond[0].status === "success" && getdatalecode.fintech.plafond[1].status === "success"){status_topex=true}else{status_topex=false;}
                    }else{
                        if(getdatalecode.fintech.plafond[1].status != "success" && getdatalecode.fintech.plafond[1].status === "success"){status_topex=true}else{status_topex=false;}
                    }
                    paid_by = getdatalecode.fintech.fintech;
                    status_fit= getdatalecode.fintech.is_active;
                    textojk = getdatalecode.fintech.fintech === "fit" ? "Layanan ini diselenggarakan oleh PT Finansial Integrasi Teknologi yang terdaftar dan diawasi oleh Otoritas Jasa Keuangan" : "Layanan ini diselenggarakan oleh PT Pohon Dana Indonesia yang terdaftar dan diawasi oleh Otoritas Jasa Keuangan";
                    }else{
                        outlet_type = "Credit";
                        new_retur = false;
                        saldo_fit =0;
                        status_fit=false;
                        logo ="";
                        paid_by ="fit";
                    }
                }
                user_fmcg[i].dataValues.is_topex = false;
                user_fmcg[i].dataValues.status_fit = status_fit;
                user_fmcg[i].dataValues.outlet_type = outlet_type;
                user_fmcg[i].dataValues.topex_name = "Limit Frisian Flag";
                user_fmcg[i].dataValues.balance = saldo_fit;
                user_fmcg[i].dataValues.url_logo_fintech= logo;
                user_fmcg[i].dataValues.allow_new_retur = new_retur;
                user_fmcg[i].dataValues.paid_by = paid_by;
                user_fmcg[i].dataValues.perjanjian = perjanjian;
                user_fmcg[i].dataValues.term_and_service_url = term_and_service_url;
                user_fmcg[i].dataValues.url_ojk = "/fmcg-icon/logo_ojk.png";
                user_fmcg[i].dataValues.url_text_ojk =textojk
                user_fmcg[i].dataValues.week =
                  [{"loan_length": 1}]
            }else if(user_fmcg[i].dataValues.id === 21){
                let getOne = await service_user_unique_code.getOne({user_id:auth_user.dataValues.user_id,fmcg_id:user_fmcg[i].dataValues.id})
               for(let j=0; j < user_fmcg[i].dataValues.users_unique_codes.length; j++){
                   user_fmcg[i].dataValues.users_unique_codes[j].dataValues.ordering='1';
                    let getlecode = await service_user_unique_code.getOne({user_id:auth_user.dataValues.user_id,fmcg_id:user_fmcg[i].dataValues.id})
                    getdatalecode = await ApiBackend.getdatalecode({le_code:getOne.dataValues.unique_code});
                    if(getdatalecode.ordering.status === true){
                        ordering = true;url_ordering = getdatalecode.ordering.redirect_to_store;
                        status_ordering = getdatalecode.ordering.status;
                        url_ordering_list = getdatalecode.ordering.redirect_to_order_list;
                    }
                    else{
                        url_ordering_list = "";ordering = false;url_ordering = "";status_ordering = getdatalecode.ordering.status;
                    }
                    user_fmcg[i].dataValues.users_unique_codes[j].dataValues.status_ordering = status_ordering;
                    user_fmcg[i].dataValues.users_unique_codes[j].dataValues.ordering = ordering;

                    
                    let pembelian  ={};
                    pembelian.name  = "Pembelian";
                    pembelian.description = "Beli Semua Produk Borwita Disini";
                    pembelian.url_ordering = url_ordering; 
                    pembelian.icon="/fmcg-icon/borwita.png"
                    //user_fmcg[i].dataValues.users_unique_codes[j].dataValues.pembelian = pembelian;
                  
                    let list_order ={};
                    list_order.name = "Riwayat Pembelian";
                    list_order.description = "Lihat Aktivitas Pembelian";
                    list_order.url_ordering = url_ordering_list;
                    list_order.icon="/fmcg-icon/umum.png"
                    let menu_ordering  =[pembelian,list_order];
                    user_fmcg[i].dataValues.users_unique_codes[j].dataValues.menu_ordering = menu_ordering;
                  
              
                }
                
                if(getOne == null || getOne == '') {
                    ordering = false;
                    url_ordering = "";
                    status_ordering = false;
                    outlet_type = "Credit";
                    new_retur = false;
                    saldo_fit =0;
                    status_fit=false;
                    logo ="";
                    paid_by ="fit";
                }else{
                    if(auth_user.dataValues.type===0){
                    getdatalecode = await ApiBackend.getdatalecode({le_code:getOne.dataValues.unique_code});
       
                    outlet_type = getdatalecode.fintech.toko; 
                    new_retur = getdatalecode.fintech.toko === "Cash" ? true : false;
                    saldo_fit = getdatalecode.fintech.fintech === "fit" ? getdatalecode.fintech.plafond[0].plafond : getdatalecode.fintech.plafond[1].plafond;
                    logo = getdatalecode.fintech.fintech === "pohondana" ? "/fmcg-icon/pohondana.png":"";
                    if(getdatalecode.ordering.status === true){
                        ordering = true;url_ordering = getdatalecode.ordering.redirect_to_store;
                        status_ordering = getdatalecode.ordering.status;
                    }
                    else{
                        ordering = false;url_ordering = "";status_ordering = getdatalecode.ordering.status;
                    }
                    if(getdatalecode.fintech.fintech === "fit"){
                    if(getdatalecode.fintech.plafond[0].status === "success" && getdatalecode.fintech.plafond[1].status === "success"){status_topex=true}else{status_topex=false;}
                    }else{
                        if(getdatalecode.fintech.plafond[1].status != "success" && getdatalecode.fintech.plafond[1].status === "success"){status_topex=true}else{status_topex=false;}
                    }
                    paid_by = getdatalecode.fintech.fintech;
                    status_fit= getdatalecode.fintech.is_active;
                    textojk = getdatalecode.fintech.fintech === "fit" ? "Layanan ini diselenggarakan oleh PT Finansial Integrasi Teknologi yang terdaftar dan diawasi oleh Otoritas Jasa Keuangan" : "Layanan ini diselenggarakan oleh PT Pohon Dana Indonesia yang terdaftar dan diawasi oleh Otoritas Jasa Keuangan";
                    }else{
                        outlet_type = "Credit";
                        new_retur = false;
                        saldo_fit =0;
                        status_fit=false;
                        logo ="";
                        paid_by ="fit";
                    }
                }
                user_fmcg[i].dataValues.is_topex = false;
                user_fmcg[i].dataValues.status_fit = status_fit;
                user_fmcg[i].dataValues.outlet_type = outlet_type;
                user_fmcg[i].dataValues.topex_name = "Limit Pandai Uli";
                user_fmcg[i].dataValues.balance = saldo_fit;
                user_fmcg[i].dataValues.url_logo_fintech= logo;
                user_fmcg[i].dataValues.allow_new_retur = new_retur;
                user_fmcg[i].dataValues.paid_by = paid_by;
                user_fmcg[i].dataValues.perjanjian = "/faq/faq_perjanjian_pinjam_modal.html";
                user_fmcg[i].dataValues.term_and_service_url = "/faq/tncpinjammodal.html";
                user_fmcg[i].dataValues.url_ojk = "/fmcg-icon/logo_ojk.png";
                user_fmcg[i].dataValues.url_text_ojk =textojk;
                user_fmcg[i].dataValues.position =getOne.dataValues.position;
                user_fmcg[i].dataValues.week =
                     [{"loan_length": 1}]
            }
            else if(user_fmcg[i].dataValues.id === 25){
                let getOne = await service_user_unique_code.getOne({user_id:auth_user.dataValues.user_id,fmcg_id:user_fmcg[i].dataValues.id})
               for(let j=0; j < user_fmcg[i].dataValues.users_unique_codes.length; j++){
                    user_fmcg[i].dataValues.users_unique_codes[j].dataValues.ordering='1';
                    let getlecode = await service_user_unique_code.getOne({user_id:auth_user.dataValues.user_id,fmcg_id:user_fmcg[i].dataValues.id})
                    getdatalecode = await ApiBackend.getdatalecode({le_code:getOne.dataValues.unique_code});
                    if(getdatalecode.ordering.status === true){
                        ordering = true;url_ordering = getdatalecode.ordering.redirect_to_store;
                        status_ordering = getdatalecode.ordering.status;
                        url_ordering_list = getdatalecode.ordering.redirect_to_order_list;
                    }
                    else{
                        url_ordering_list = "";ordering = false;url_ordering = "";status_ordering = getdatalecode.ordering.status;
                    }
                    user_fmcg[i].dataValues.users_unique_codes[j].dataValues.status_ordering = status_ordering;
                    user_fmcg[i].dataValues.users_unique_codes[j].dataValues.ordering = ordering;

                    
                    let pembelian  ={};
                    pembelian.name  = "Pembelian";
                    pembelian.description = "Beli Semua Produk Horeca Disini";
                    pembelian.url_ordering = url_ordering; 
                    pembelian.icon="/fmcg-icon/horeca.png"
                    //user_fmcg[i].dataValues.users_unique_codes[j].dataValues.pembelian = pembelian;
                  
                    let list_order ={};
                    list_order.name = "Riwayat Pembelian";
                    list_order.description = "Lihat Aktivitas Pembelian";
                    list_order.url_ordering = url_ordering_list;
                    list_order.icon="/fmcg-icon/umum.png"
                    let menu_ordering  =[pembelian,list_order];
                    user_fmcg[i].dataValues.users_unique_codes[j].dataValues.menu_ordering = menu_ordering;
                  
              
                }
                
                if(getOne == null || getOne == '') {
                    ordering = false;
                    url_ordering = "";
                    status_ordering = false;
                    outlet_type = "Credit";
                    new_retur = false;
                    saldo_fit =0;
                    status_fit=false;
                    logo ="";
                    paid_by ="fit";
                }else{
                    if(auth_user.dataValues.type===0){
                    getdatalecode = await ApiBackend.getdatalecode({le_code:getOne.dataValues.unique_code});
                    outlet_type = getdatalecode.fintech.toko; 
                    new_retur = getdatalecode.fintech.toko === "Cash" ? true : false;
                    saldo_fit = getdatalecode.fintech.fintech === "fit" ? getdatalecode.fintech.plafond[0].plafond : getdatalecode.fintech.plafond[1].plafond;
                    logo = getdatalecode.fintech.fintech === "pohondana" ? "/fmcg-icon/pohondana.png":"";
                    if(getdatalecode.ordering.status === true){
                        ordering = true;url_ordering = getdatalecode.ordering.redirect_to_store;
                        status_ordering = getdatalecode.ordering.status;
                    }
                    else{
                        ordering = false;url_ordering = "";status_ordering = getdatalecode.ordering.status;
                    }
                    if(getdatalecode.fintech.fintech === "fit"){
                    if(getdatalecode.fintech.plafond[0].status === "success" && getdatalecode.fintech.plafond[1].status === "success"){status_topex=true}else{status_topex=false;}
                    }else{
                        if(getdatalecode.fintech.plafond[1].status != "success" && getdatalecode.fintech.plafond[1].status === "success"){status_topex=true}else{status_topex=false;}
                    }
                    paid_by = getdatalecode.fintech.fintech;
                    status_fit= getdatalecode.fintech.is_active;
                    textojk = getdatalecode.fintech.fintech === "fit" ? "Layanan ini diselenggarakan oleh PT Finansial Integrasi Teknologi yang terdaftar dan diawasi oleh Otoritas Jasa Keuangan" : "Layanan ini diselenggarakan oleh PT Pohon Dana Indonesia yang terdaftar dan diawasi oleh Otoritas Jasa Keuangan";
                    }else{
                        outlet_type = "Credit";
                        new_retur = false;
                        saldo_fit =0;
                        status_fit=false;
                        logo ="";
                        paid_by ="fit";
                    }
                }
                user_fmcg[i].dataValues.is_topex = false;
                user_fmcg[i].dataValues.status_fit = status_fit;
                user_fmcg[i].dataValues.outlet_type = outlet_type;
                user_fmcg[i].dataValues.topex_name = "Limit Pandai Horeca";
                user_fmcg[i].dataValues.balance = saldo_fit;
                user_fmcg[i].dataValues.url_logo_fintech= logo;
                user_fmcg[i].dataValues.allow_new_retur = new_retur;
                user_fmcg[i].dataValues.paid_by = paid_by;
                user_fmcg[i].dataValues.perjanjian = "/faq/faq_perjanjian_pinjam_modal.html";
                user_fmcg[i].dataValues.term_and_service_url = "/faq/tncpinjammodal.html";
                user_fmcg[i].dataValues.url_ojk = "/fmcg-icon/logo_ojk.png";
                user_fmcg[i].dataValues.url_text_ojk =textojk
                user_fmcg[i].dataValues.position =getOne.dataValues.position
//                user_fmcg[i].dataValues.ordering =ordering;
//                user_fmcg[i].dataValues.url_ordering=url_ordering;
//                user_fmcg[i].dataValues.status_ordering=status_ordering;

                user_fmcg[i].dataValues.week =
                     [{"loan_length": 1}]
            }
            else if(user_fmcg[i].dataValues.id === 27){
                let getOne = await service_user_unique_code.getOne({user_id:auth_user.dataValues.user_id,fmcg_id:user_fmcg[i].dataValues.id})
               for(let j=0; j < user_fmcg[i].dataValues.users_unique_codes.length; j++){
                    user_fmcg[i].dataValues.users_unique_codes[j].dataValues.ordering='1';
                    let getlecode = await service_user_unique_code.getOne({user_id:auth_user.dataValues.user_id,fmcg_id:user_fmcg[i].dataValues.id})
                    getdatalecode = await ApiBackend.getdatalecode({le_code:getOne.dataValues.unique_code});
                    if(getdatalecode.ordering.status === true){
                        ordering = true;url_ordering = getdatalecode.ordering.redirect_to_store;
                        status_ordering = getdatalecode.ordering.status;
                        url_ordering_list = getdatalecode.ordering.redirect_to_order_list;
                    }
                    else{
                        url_ordering_list = "";ordering = false;url_ordering = "";status_ordering = getdatalecode.ordering.status;
                    }
                    user_fmcg[i].dataValues.users_unique_codes[j].dataValues.status_ordering = status_ordering;
                    user_fmcg[i].dataValues.users_unique_codes[j].dataValues.ordering = ordering;

                    
                    let pembelian  ={};
                    pembelian.name  = "Pembelian";
                    pembelian.description = "Beli Produk Kedai Sayur Disini";
                    pembelian.url_ordering = url_ordering; 
                    pembelian.icon="/fmcg-icon/kedai_sayur.png"
                    //user_fmcg[i].dataValues.users_unique_codes[j].dataValues.pembelian = pembelian;
                  
                    let list_order ={};
                    list_order.name = "Riwayat Pembelian";
                    list_order.description = "Lihat Aktivitas Pembelian";
                    list_order.url_ordering = url_ordering_list;
                    list_order.icon="/fmcg-icon/umum.png"
                    let menu_ordering  =[pembelian,list_order];
                    user_fmcg[i].dataValues.users_unique_codes[j].dataValues.menu_ordering = menu_ordering;
                  
              
                }
                
                if(getOne == null || getOne == '') {
                    ordering = false;
                    url_ordering = "";
                    status_ordering = false;
                    outlet_type = "Credit";
                    new_retur = false;
                    saldo_fit =0;
                    status_fit=false;
                    logo ="";
                    paid_by ="fit";
                }else{
                    if(auth_user.dataValues.type===0){
                    getdatalecode = await ApiBackend.getdatalecode({le_code:getOne.dataValues.unique_code});
                    outlet_type = getdatalecode.fintech.toko; 
                    new_retur = getdatalecode.fintech.toko === "Cash" ? true : false;
                    saldo_fit = getdatalecode.fintech.fintech === "fit" ? getdatalecode.fintech.plafond[0].plafond : getdatalecode.fintech.plafond[1].plafond;
                    logo = getdatalecode.fintech.fintech === "pohondana" ? "/fmcg-icon/pohondana.png":"";
                    if(getdatalecode.ordering.status === true){
                        ordering = true;url_ordering = getdatalecode.ordering.redirect_to_store;
                        status_ordering = getdatalecode.ordering.status;
                    }
                    else{
                        ordering = false;url_ordering = "";status_ordering = getdatalecode.ordering.status;
                    }
                    if(getdatalecode.fintech.fintech === "fit"){
                    if(getdatalecode.fintech.plafond[0].status === "success" && getdatalecode.fintech.plafond[1].status === "success"){status_topex=true}else{status_topex=false;}
                    }else{
                        if(getdatalecode.fintech.plafond[1].status != "success" && getdatalecode.fintech.plafond[1].status === "success"){status_topex=true}else{status_topex=false;}
                    }
                    paid_by = getdatalecode.fintech.fintech;
                    status_fit= getdatalecode.fintech.is_active;
                    textojk = getdatalecode.fintech.fintech === "fit" ? "Layanan ini diselenggarakan oleh PT Finansial Integrasi Teknologi yang terdaftar dan diawasi oleh Otoritas Jasa Keuangan" : "Layanan ini diselenggarakan oleh PT Pohon Dana Indonesia yang terdaftar dan diawasi oleh Otoritas Jasa Keuangan";
                    }else{
                        outlet_type = "Credit";
                        new_retur = false;
                        saldo_fit =0;
                        status_fit=false;
                        logo ="";
                        paid_by ="fit";
                    }
                }
                user_fmcg[i].dataValues.is_topex = false;
                user_fmcg[i].dataValues.status_fit = status_fit;
                user_fmcg[i].dataValues.outlet_type = outlet_type;
                user_fmcg[i].dataValues.topex_name = "Limit Kedai Sayur";
                user_fmcg[i].dataValues.balance = saldo_fit;
                user_fmcg[i].dataValues.url_logo_fintech= logo;
                user_fmcg[i].dataValues.allow_new_retur = new_retur;
                user_fmcg[i].dataValues.paid_by = paid_by;
                user_fmcg[i].dataValues.perjanjian = "/faq/faq_perjanjian_pinjam_modal.html";
                user_fmcg[i].dataValues.term_and_service_url = "/faq/tncpinjammodal.html";
                user_fmcg[i].dataValues.url_ojk = "/fmcg-icon/logo_ojk.png";
                user_fmcg[i].dataValues.url_text_ojk =textojk
                user_fmcg[i].dataValues.position =getOne.dataValues.position
//                user_fmcg[i].dataValues.ordering =ordering;
//                user_fmcg[i].dataValues.url_ordering=url_ordering;
//                user_fmcg[i].dataValues.status_ordering=status_ordering;

                user_fmcg[i].dataValues.week =
                     [{"loan_length": 1}]
            }else if(user_fmcg[i].dataValues.id === 28){
                let getOne = await service_user_unique_code.getOne({user_id:auth_user.dataValues.user_id,fmcg_id:user_fmcg[i].dataValues.id})
               for(let j=0; j < user_fmcg[i].dataValues.users_unique_codes.length; j++){
                    user_fmcg[i].dataValues.users_unique_codes[j].dataValues.ordering='1';
                    let getlecode = await service_user_unique_code.getOne({user_id:auth_user.dataValues.user_id,fmcg_id:user_fmcg[i].dataValues.id})
                    getdatalecode = await ApiBackend.getdatalecode({le_code:getOne.dataValues.unique_code});
                    if(getdatalecode.ordering.status === true){
                        ordering = true;url_ordering = getdatalecode.ordering.redirect_to_store;
                        status_ordering = getdatalecode.ordering.status;
                        url_ordering_list = getdatalecode.ordering.redirect_to_order_list;
                    }
                    else{
                        url_ordering_list = "";ordering = false;url_ordering = "";status_ordering = getdatalecode.ordering.status;
                    }
                    user_fmcg[i].dataValues.users_unique_codes[j].dataValues.status_ordering = status_ordering;
                    user_fmcg[i].dataValues.users_unique_codes[j].dataValues.ordering = ordering;

                    
                    let pembelian  ={};
                    pembelian.name  = "Pembelian";
                    pembelian.description = "Beli Produk Grosir Disini";
                    pembelian.url_ordering = url_ordering; 
                    pembelian.icon="/fmcg-icon/grosirpandai.png"
                    //user_fmcg[i].dataValues.users_unique_codes[j].dataValues.pembelian = pembelian;
                  
                    let list_order ={};
                    list_order.name = "Riwayat Pembelian";
                    list_order.description = "Lihat Aktivitas Pembelian";
                    list_order.url_ordering = url_ordering_list;
                    list_order.icon="/fmcg-icon/umum.png"
                    let menu_ordering  =[pembelian,list_order];
                    user_fmcg[i].dataValues.users_unique_codes[j].dataValues.menu_ordering = menu_ordering;
                  
              
                }
                
                if(getOne == null || getOne == '') {
                    ordering = false;
                    url_ordering = "";
                    status_ordering = false;
                    outlet_type = "Credit";
                    new_retur = false;
                    saldo_fit =0;
                    status_fit=false;
                    logo ="";
                    paid_by ="fit";
                }else{
                    if(auth_user.dataValues.type===0){
                    getdatalecode = await ApiBackend.getdatalecode({le_code:getOne.dataValues.unique_code});
                    outlet_type = getdatalecode.fintech.toko; 
                    new_retur = getdatalecode.fintech.toko === "Cash" ? true : false;
                    saldo_fit = getdatalecode.fintech.fintech === "fit" ? getdatalecode.fintech.plafond[0].plafond : getdatalecode.fintech.plafond[1].plafond;
                    logo = getdatalecode.fintech.fintech === "pohondana" ? "/fmcg-icon/pohondana.png":"";
                    if(getdatalecode.ordering.status === true){
                        ordering = true;url_ordering = getdatalecode.ordering.redirect_to_store;
                        status_ordering = getdatalecode.ordering.status;
                    }
                    else{
                        ordering = false;url_ordering = "";status_ordering = getdatalecode.ordering.status;
                    }
                    if(getdatalecode.fintech.fintech === "fit"){
                    if(getdatalecode.fintech.plafond[0].status === "success" && getdatalecode.fintech.plafond[1].status === "success"){status_topex=true}else{status_topex=false;}
                    }else{
                        if(getdatalecode.fintech.plafond[1].status != "success" && getdatalecode.fintech.plafond[1].status === "success"){status_topex=true}else{status_topex=false;}
                    }
                    paid_by = getdatalecode.fintech.fintech;
                    status_fit= getdatalecode.fintech.is_active;
                    textojk = getdatalecode.fintech.fintech === "fit" ? "Layanan ini diselenggarakan oleh PT Finansial Integrasi Teknologi yang terdaftar dan diawasi oleh Otoritas Jasa Keuangan" : "Layanan ini diselenggarakan oleh PT Pohon Dana Indonesia yang terdaftar dan diawasi oleh Otoritas Jasa Keuangan";
                    }else{
                        outlet_type = "Credit";
                        new_retur = false;
                        saldo_fit =0;
                        status_fit=false;
                        logo ="";
                        paid_by ="fit";
                    }
                }
                user_fmcg[i].dataValues.is_topex = false;
                user_fmcg[i].dataValues.status_fit = status_fit;
                user_fmcg[i].dataValues.outlet_type = outlet_type;
                user_fmcg[i].dataValues.topex_name = "Limit Grosir pandai";
                user_fmcg[i].dataValues.balance = saldo_fit;
                user_fmcg[i].dataValues.url_logo_fintech= logo;
                user_fmcg[i].dataValues.allow_new_retur = new_retur;
                user_fmcg[i].dataValues.paid_by = paid_by;
                user_fmcg[i].dataValues.perjanjian = "/faq/faq_perjanjian_pinjam_modal.html";
                user_fmcg[i].dataValues.term_and_service_url = "/faq/tncpinjammodal.html";
                user_fmcg[i].dataValues.url_ojk = "/fmcg-icon/logo_ojk.png";
                user_fmcg[i].dataValues.url_text_ojk =textojk
                user_fmcg[i].dataValues.position =getOne.dataValues.position
//                user_fmcg[i].dataValues.ordering =ordering;
//                user_fmcg[i].dataValues.url_ordering=url_ordering;
//                user_fmcg[i].dataValues.status_ordering=status_ordering;

                user_fmcg[i].dataValues.week =
                     [{"loan_length": 1}]
            }else if(user_fmcg[i].dataValues.id === 22){
                let getOne = await service_user_unique_code.getOne({user_id:auth_user.dataValues.user_id,fmcg_id:user_fmcg[i].dataValues.id})
               for(let j=0; j < user_fmcg[i].dataValues.users_unique_codes.length; j++){
                    user_fmcg[i].dataValues.users_unique_codes[j].dataValues.ordering='1';
                    let getlecode = await service_user_unique_code.getOne({user_id:auth_user.dataValues.user_id,fmcg_id:user_fmcg[i].dataValues.id})
                    getdatalecode = await ApiBackend.getdatalecode({le_code:getOne.dataValues.unique_code});
                    if(getdatalecode.ordering.status === true){
                        ordering = true;url_ordering = getdatalecode.ordering.redirect_to_store;
                        status_ordering = getdatalecode.ordering.status;
                        url_ordering_list = getdatalecode.ordering.redirect_to_order_list;
                    }
                    else{
                        url_ordering_list = "";ordering = false;url_ordering = "";status_ordering = getdatalecode.ordering.status;
                    }
                    user_fmcg[i].dataValues.users_unique_codes[j].dataValues.status_ordering = status_ordering;
                    user_fmcg[i].dataValues.users_unique_codes[j].dataValues.ordering = ordering;

                    
                    let pembelian  ={};
                    pembelian.name  = "Pembelian";
                    pembelian.description = "Beli Semua Produk Ondemand Disini";
                    pembelian.url_ordering = url_ordering; 
                    pembelian.icon="/fmcg-icon/umum.png"
                    //user_fmcg[i].dataValues.users_unique_codes[j].dataValues.pembelian = pembelian;
                  
                    let list_order ={};
                    list_order.name = "Riwayat Pembelian";
                    list_order.description = "Lihat Aktivitas Pembelian";
                    list_order.url_ordering = url_ordering_list;
                    list_order.icon="/fmcg-icon/umum.png"
                    let menu_ordering  =[pembelian,list_order];
                    user_fmcg[i].dataValues.users_unique_codes[j].dataValues.menu_ordering = menu_ordering;
                  
              
                }
                
                if(getOne == null || getOne == '') {
                    ordering = false;
                    url_ordering = "";
                    status_ordering = false;
                    outlet_type = "Credit";
                    new_retur = false;
                    saldo_fit =0;
                    status_fit=false;
                    logo ="";
                    paid_by ="fit";
                }else{
                    if(auth_user.dataValues.type===0){
                    getdatalecode = await ApiBackend.getdatalecode({le_code:getOne.dataValues.unique_code});
       
                    console.log(getdatalecode.ordering.response);
                    outlet_type = getdatalecode.fintech.toko; 
                    new_retur = getdatalecode.fintech.toko === "Cash" ? true : false;
                    saldo_fit = getdatalecode.fintech.fintech === "fit" ? getdatalecode.fintech.plafond[0].plafond : getdatalecode.fintech.plafond[1].plafond;
                    logo = getdatalecode.fintech.fintech === "pohondana" ? "/fmcg-icon/pohondana.png":"";
                    if(getdatalecode.ordering.status === true){
                        ordering = true;url_ordering = getdatalecode.ordering.redirect_to_store;
                        status_ordering = getdatalecode.ordering.status;
                    }
                    else{
                        ordering = false;url_ordering = "";status_ordering = getdatalecode.ordering.status;
                    }
                    if(getdatalecode.fintech.fintech === "fit"){
                    if(getdatalecode.fintech.plafond[0].status === "success" && getdatalecode.fintech.plafond[1].status === "success"){status_topex=true}else{status_topex=false;}
                    }else{
                        if(getdatalecode.fintech.plafond[1].status != "success" && getdatalecode.fintech.plafond[1].status === "success"){status_topex=true}else{status_topex=false;}
                    }
                    paid_by = getdatalecode.fintech.fintech;
                    status_fit= getdatalecode.fintech.is_active;
                    textojk = getdatalecode.fintech.fintech === "fit" ? "Layanan ini diselenggarakan oleh PT Finansial Integrasi Teknologi yang terdaftar dan diawasi oleh Otoritas Jasa Keuangan" : "Layanan ini diselenggarakan oleh PT Pohon Dana Indonesia yang terdaftar dan diawasi oleh Otoritas Jasa Keuangan";
                    }else{
                        outlet_type = "Credit";
                        new_retur = false;
                        saldo_fit =0;
                        status_fit=false;
                        logo ="";
                        paid_by ="fit";
                    }
                }
                user_fmcg[i].dataValues.is_topex = false;
                user_fmcg[i].dataValues.status_fit = status_fit;
                user_fmcg[i].dataValues.outlet_type = outlet_type;
                user_fmcg[i].dataValues.topex_name = "Limit Pandai Uli";
                user_fmcg[i].dataValues.balance = saldo_fit;
                user_fmcg[i].dataValues.url_logo_fintech= logo;
                user_fmcg[i].dataValues.allow_new_retur = new_retur;
                user_fmcg[i].dataValues.paid_by = paid_by;
                user_fmcg[i].dataValues.perjanjian = "/faq/faq_perjanjian_pinjam_modal.html";
                user_fmcg[i].dataValues.term_and_service_url = "/faq/tncpinjammodal.html";
                user_fmcg[i].dataValues.url_ojk = "/fmcg-icon/logo_ojk.png";
                user_fmcg[i].dataValues.url_text_ojk =textojk;
                user_fmcg[i].dataValues.position =getOne.dataValues.position;
                user_fmcg[i].dataValues.week =
                     [{"loan_length": 1}]
            }
            else{
                user_fmcg[i].dataValues.is_topex = false;
            }
//        i++;
        }
//        while (i < user_fmcg.length);
        
        let available_fmcg = await service_fmcg.getAvailableFmcgToUser({id: user_has_fmcg});
        
        out.has = user_fmcg;
        out.available = available_fmcg;

        res.json(out);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal mendapatkan data FMCG user, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
 }

async function get_one_user_by_vaccount(req, res) {
    const token = req.swagger.params.token.value;
    const vaccount = req.swagger.params.va.value;

    try {
        Logs.create({action: 'get_one_user_by_vaccount', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_get_one_user_by_vaccount, is_allowed_access_token_static: false});
        let user = await service_user.getOneByVA({vaccount: vaccount});

        if(user == null || !user) {
            throw new CustomError(400, 'Data nama toko tidak di temukan');
        }

        let additional_data = await service_user_additional_data.get({user_id: user.user_id});

        res.json(helper.customSuccessResponse(new CustomSuccess(200, 'Data berhasil di tampilkan'), 'datas', additional_data));
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal mendapatkan data nama toko, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function get_users(req, res) {
    const token = req.swagger.params.token.value;

    const param_types = req.swagger.params.types;
    const param_fullname = req.swagger.params.fullname;
    const param_page = req.swagger.params.page;
    const param_items_per_page = req.swagger.params.items_per_page;

    let typesNameList = _.isUndefined(param_types.value)?[]:param_types.value.split(',');
    let startPage = _.isUndefined(param_page.value)?0:param_page.value;
    let itemsPerPage = _.isUndefined(param_items_per_page.value)?15:param_items_per_page.value;
    let fullname = _.isUndefined(param_fullname.value)?'':param_fullname.value;

    startPage = (startPage < 1 ) ? 1 : startPage;

    const limit = itemsPerPage;
    const offset = ( startPage - 1 ) * itemsPerPage;

    try {
        Logs.create({action: 'get_users', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_get_users, is_allowed_access_token_static: false});

        let where = {};
        let typesList;

        if (typesNameList && typesNameList.length > 0) {
            typesList = typesNameList.map(type => {
                return USERTYPES[type];
            });
       
            where = {fullname: {[Op.like]: '%'+fullname+'%'}};            
            UserQuery.types({filter:where, typesList:typesList});
        }

        if(fullname && fullname.length > 0)
        {
            where = {fullname: {[Op.like]: '%'+fullname+'%'}};            
            UserQuery.types({filter:where, typesList:typesList});
        }

        let result_obj = await base_repo.get({entity:User, where:where, limit:limit, offset:offset, attributes: ['user_id', 'email', 'fullname', 'nama_toko', 'phone', 'tanggal_lahir', 'tempat_lahir', 'ktp', 'type', 'imei', 'valdo_account', 'access_token', 'access_token_expiry', 'fcm_id', 'join_date', 'status']});

        const {total:total, result_list:result_list} = result_obj;

        let out = service_formatter.format({obj:result_list});
        let paging = {};
        let total_pages = Math.ceil(total / itemsPerPage);

        out.map( (val, index) => {
            if(val.dataValues.type == 0)
                val.dataValues.role = 'retailer';
            else if(val.dataValues.type == 1)
                val.dataValues.role = 'distributor';
            else if(val.dataValues.type == 2)
                val.dataValues.role = 'pickup';
            else if(val.dataValues.type == 3)
                val.dataValues.role = 'distributor manager';
            else if(val.dataValues.type == 4)
                val.dataValues.role = 'sales';
            else if(val.dataValues.type == 9)
                val.dataValues.role = 'unilever admin';
            else if(val.dataValues.type == 10)
                val.dataValues.role = 'superadmin';
            else if(val.dataValues.type == 11)
                val.dataValues.role = 'call center';
             else if(val.dataValues.type == 12)
                val.dataValues.role = 'finance';
        });

        out = Object.assign(out, paging);

        paging.page = startPage;
        paging.total_pages = total_pages;
        paging.total_items = total;
        paging.items_per_page = itemsPerPage;
        paging.size = out.length;
        paging.users = out;

        res.json(paging);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal menampilkan data user, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function get_user_types(req, res) {
    const token = req.swagger.params.token.value;

    try {
        Logs.create({action: 'get_user_types', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_get_users, is_allowed_access_token_static: false});
        
        res.json(USERTYPES);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal mendapatkan data user types, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function register_manual_toko(req, res) {
    let token = req.swagger.params.token.value;
    let le_code = req.swagger.params.le_code.value;
    let name = req.swagger.params.name.value;
    let imei = helper.currentMilisecondsTime();
    let email = le_code+'_'+name.replace(/[^a-zA-Z0-9s]/g, "")+'@tokopandai.id';
    let password = 123456;
    let pin = 123456;
    let phone = helper.currentMilisecondsTime();
    let usertype = 0;
    let fmcg_id =req.swagger.params.fmcg_id.value;

    try {
        let auth_user = await middleware.verify({token: token, acl: "", is_allowed_access_token_static: true});
        let exist_email = await service_user.get({email: email});
        let exist_le_codes = await service_user_unique_code.getOne({fmcg_id: fmcg_id, unique_code: le_code});
        let exist_le_code = await service_register_lecode.getOne({fmcg_id: fmcg_id,  le_code: le_code});
        if(exist_le_code == null) {
            throw new CustomError(400, 'Gagal menambahkan le code, le code belum didaftarkan di master outlet');
        }
        if(exist_le_codes !== null) {
            throw new CustomError(400, 'Gagal, Le code sudah didaftarkan');
        }
        if(exist_email.length > 0) {
            throw new CustomError(400, 'E-mail sudah di gunakan');
        }
        let args = {};
        args.fullname = name;
        args.nama_toko = name;
        args.imei = helper.currentMilisecondsTime();
        args.email = email;
        args.pin=pin;
        args.password = "";
        args.phone = "00000000";
        args.usertype = usertype;
        args.register_type = "register_manual";
        let register_user_response = await service_user.register_manual(args);
        
        await service_user_additional_data.add({user_id: register_user_response.user_id, field: 'Nama Toko', value: name});
        let va_number = helper.createVANumber(register_user_response.user_id, 0);
        let argss = {};
        argss.mobnum = va_number;
        argss.fname = name;
        argss.lname = name;
        argss.dob = "1990-01-01";
        argss.pob = "FMCG";
        argss.email = register_user_response.email;
        argss.pin = pin;
        argss.usertype = register_user_response.user_id;
        let user_unique_code = await service_user_unique_code.add({user_id: register_user_response.user_id, fmcg_id: fmcg_id, unique_code: le_code, description: "LE_MANUAL",position:0});
        await service_user.updateVA({user_id: register_user_response.user_id, valdo_account: va_number});
        await MoneyAccess.register(argss);
      

        let userUniqueCodeTableName = UserUniqueCode.getTableName();
        let where_key = `$${userUniqueCodeTableName}.user_id$`;
        let filter = {};
        filter[where_key] = register_user_response.user_id;

        let role_name = await middleware.getUSERTYPESKey(USERTYPES, usertype);
        let additional_data_user = await service_user_additional_data.get({user_id: register_user_response.user_id});
        let bank_accounts = await service_bank_account.get({user_id: register_user_response.user_id});
        let user_fmcg = await service_fmcg.get(filter);
        let user = await service_user.getOne({user_id: register_user_response.user_id});
        user.dataValues.firstname = "";
        user.dataValues.lastname = "";
        user.dataValues.pin_exist = false;
        user.dataValues.accessTokenExpiry = helper.formatDateTime(register_user_response.accessTokenExpiry);
        user.dataValues.join_date = helper.formatDateTime(register_user_response.join_date);
        user.dataValues.role = {
            id: usertype,
            name: role_name,
            data: additional_data_user
        };
        user.dataValues.banks = bank_accounts;
        user.dataValues.fmcg = user_fmcg;

        let data = helper.removeNullFromObject(register_user_response.dataValues);
        let out = {};

        out.Token = {
            token: data.accessToken,
            expired: data.accessTokenExpiry
        };

        delete data.accessToken;
        delete data.accessTokenExpiry;

        if(usertype == 0) {
            await User.update({imei: null, access_token: null}, {where: {user_id: register_user_response.user_id}});
        }
        
        
        

        out.User = data;
        out.message = "Pendaftaran Manual Berhasil";
        out.status = "Success";
        out.code = 200;

        res.json(out);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Register gagal, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}


async function new_register(req, res) {
    let imei = req.swagger.params.imei.value;
    let le_code = req.swagger.params.le_code.value;
    let name = req.swagger.params.name.value;
    let address = req.swagger.params.address.value;
    let email = req.swagger.params.email.value;
    let password = req.swagger.params.password.value;
    let pin = req.swagger.params.pin.value;
    let phone = req.swagger.params.phone.value;
    let fmcg_id =req.swagger.params.fmcg_id.value;
    
    let usertype = 0;

    let dt_code = 'GP01';
    try {
        
        let exist_email = await service_user.get({email: email});
        let exist_le_codes = await service_user_unique_code.getOne({fmcg_id: fmcg_id, unique_code: le_code});
        let exist_le_code = await service_register_lecode.getOne({fmcg_id: fmcg_id,  le_code: le_code});
        if(exist_le_code == null) {
            throw new CustomError(400, 'Gagal menambahkan le code, le code belum didaftarkan di master outlet');
        }
        if(exist_le_codes !== null) {
            throw new CustomError(400, 'Gagal, Le code sudah didaftarkan');
        }
        if(exist_email.length > 0) {
            throw new CustomError(400, 'E-mail sudah di gunakan');
        }
        let args = {};
        args.imei = imei;
        args.fullname = name;
        args.nama_toko = name;
        args.email = email;
        args.password = password.toString();
        args.phone = phone.toString();
        args.usertype = usertype;
        args.pin = pin;
        args.register_type = "register_manual";
        let register_user_response = await service_user.register_manual(args);
        
        await service_user_additional_data.add({user_id: register_user_response.user_id, field: 'Nama Toko', value: name});
        let va_number = helper.createVANumber(register_user_response.user_id, 0);
        let argss = {};
        argss.mobnum = va_number;
        argss.fname = name;
        argss.lname = name;
        argss.dob = "1990-01-01";
        argss.pob = "Jakarta";
        argss.email = register_user_response.email;
        argss.pin = pin;
        argss.usertype = register_user_response.user_id;
        let user_unique_code = await service_user_unique_code.add({user_id: register_user_response.user_id, fmcg_id: fmcg_id, unique_code: le_code, description: "LE_MANUAL_ES",position:0});
        await service_user.updateVA({user_id: register_user_response.user_id, valdo_account: va_number});
        await MoneyAccess.register(argss);
        let bank = {};
        bank.user_id = register_user_response.user_id;
        bank.no_rekening = "00000000";
        bank.bank_id = 2;
        bank.name = "tokopandai";
        await service_bank_account.createBankAccount(bank);
        let akusisi = await ApiBackend.post_lecode_akuisisi({le_code:le_code,nama:name,alamat:address});
        if(fmcg_id == 28 || fmcg_id == 27 || fmcg_id == 22){
            await ApiBackend.post_toko_grosir({le_code: le_code,dt_code: dt_code, fmcg_id: fmcg_id,email: email,firstname: name,lastname: name,address: address,phone: phone});
        }
        let userUniqueCodeTableName = UserUniqueCode.getTableName();
        let where_key = `$${userUniqueCodeTableName}.user_id$`;
        let filter = {};
        filter[where_key] = register_user_response.user_id;

        let role_name = await middleware.getUSERTYPESKey(USERTYPES, usertype);
        let additional_data_user = await service_user_additional_data.get({user_id: register_user_response.user_id});
        let bank_accounts = await service_bank_account.get({user_id: register_user_response.user_id});
        let user_fmcg = await service_fmcg.get(filter);
        let user = await service_user.getOne({user_id: register_user_response.user_id});
        
        user.dataValues.firstname = "";
        user.dataValues.lastname = "";
        user.dataValues.pin_exist = false;
        user.dataValues.accessTokenExpiry = helper.formatDateTime(register_user_response.accessTokenExpiry);
        user.dataValues.join_date = helper.formatDateTime(register_user_response.join_date);
        user.dataValues.role = {
            id: usertype,
            name: role_name,
            data: additional_data_user
        };
        user.dataValues.banks = bank_accounts;
        user.dataValues.fmcg = user_fmcg;

        let data = helper.removeNullFromObject(register_user_response.dataValues);
        let out = {};

        out.Token = {
            token: data.accessToken,
            expired: data.accessTokenExpiry
        };
        delete data.accessToken;
        delete data.accessTokenExpiry;

        if(usertype == 0) {
            await User.update({imei: null, access_token: null}, {where: {user_id: register_user_response.user_id}});
        }
        out.User = data;
        out.message = "Pendaftaran Manual Berhasil";
        out.status = "Success";
        out.code = 200;

        res.json(out);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Register gagal, '+err;
        }
        res.status(err.code).json({code: err.code, message: err.message});
    }
}

export { signin }
export { logout }
export { register }
export { edit_user }
export { google_auth }
export { user_changepassword }
export { user_resetpassword }
export { user_konfirmasi_resetpassword }
export { user_resetpin }
export { user_changepin }
export { user_addpin }
export { user_changeimei }
export { get_one_user }
export { renew_token }
export { update_fcm }
export { get_balance }
export { get_user_fmcg }
export { get_one_user_by_vaccount }
export { get_users }
export { get_user_types }
export { register_manual_toko }
export { new_register }