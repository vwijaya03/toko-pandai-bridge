'use strict';

import _ from 'lodash'
import util from 'util'
import fs from 'fs'
import middleware from '~/api/controllers/auth'
import moment from 'moment'
import request from 'request'

import helper from '~/utils/helper'
import service_fmcg from '~/services/fmcg'
import ApiPPOB from '~/external_api/api_ppob_limit'
import ApiPPOBLama from '~/external_api/api_ppob'
import service_user from '~/services/user'


import { Logs } from '~/orm/index'

const acl_ppob_limit = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];

async function get_invoice(req,res){

    const token = req.swagger.params.token.value;
    const param_paid_unpaid_status = req.swagger.params.paid_unpaid_status;
    const param_invoice_id = req.swagger.params.invoice_id;
    const param_filter1 = req.swagger.params.filter1;
    const param_filter2 = req.swagger.params.filter2;
    const param_page = req.swagger.params.page;
    const param_items_per_page = req.swagger.params.items_per_page;
    const param_sorting = req.swagger.params.sorting;

    let paid_unpaid_status = _.isUndefined(param_paid_unpaid_status.value)?'':param_paid_unpaid_status.value;
    let invoice_id = _.isUndefined(param_invoice_id.value)? '' : param_invoice_id.value;
    let filter1 = _.isUndefined(param_filter1.value)? '' : param_filter1.value;
    let filter2 = _.isUndefined(param_filter2.value)? '' : param_filter2.value;
    let page = _.isUndefined(param_page.value)? 0 : param_page.value;
    let items_per_page = _.isUndefined(param_items_per_page.value)? 15 : param_items_per_page.value;
    let sorting = _.isUndefined(param_sorting.value)? '' :param_sorting;

    let fmcg_id = 3;
    let status;
    
    if(paid_unpaid_status === 'paid'){
        status = 1
    }else if(paid_unpaid_status === 'unpaid'){
        status = 0
    }else{
        status = null
    }

    try{

        let auth_user = await middleware.verify({token: token, acl: acl_ppob_limit, is_allowed_access_token_static: false});

        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
        if(result_fmcg.dataValues === null) {
            throw new CustomError(400, 'Gagal memproses data invoice, data fmcg tidak tersedia');
        }

        let args = {}
        args.url = result_fmcg.dataValues.url;
        args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        args.fmcg_name = result_fmcg.dataValues.name;
        args.user_id = auth_user.dataValues.user_id;
        args.vaccount = auth_user.dataValues.valdo_account;
        args.paid_unpaid_status = status;
        args.invoice_id = invoice_id;
        args.filter1 = filter1;
        args.filter2 = filter2;
        args.page = page;
        args.items_per_page = items_per_page;
        args.sorting = sorting;

        let get_all_invoices = await ApiPPOB.get_invoice(args)
        res.status(200).json(get_all_invoices)

    }catch(err){
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data invoice, '+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function get_one_invoice(req,res){

    const token = req.swagger.params.token.value;
    const invoice_id = req.swagger.params.invoice_id.value;

    let fmcg_id = 3;

    try{

        let auth_user = await middleware.verify({token: token, acl: acl_ppob_limit, is_allowed_access_token_static: false});

        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
        if(result_fmcg.dataValues === null) {
            throw new CustomError(400, 'Gagal memproses data invoice, data fmcg tidak tersedia');
        }

        let args = {}
        args.url = result_fmcg.dataValues.url;
        args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        args.fmcg_name = result_fmcg.dataValues.name;
        args.user_id = auth_user.dataValues.user_id;
        args.invoice_id = invoice_id;
     
        let get_invoice = await ApiPPOB.get_one_invoice(args)
        res.status(200).json(get_invoice)

    }catch(err){
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data invoice, '+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}



async function post_ppob_payment_service(req, res) {
    const log_action = "post_ppob_payment_service";

    const token = req.swagger.params.token.value;
    const param_type = req.swagger.params.type;
    const param_product_code = req.swagger.params.product_code;
    const param_no = req.swagger.params.no;
    const param_data = req.swagger.params.data;
    const param_nominal = req.swagger.params.nominal;
    const param_ref_number = req.swagger.params.refnumber;
    const param_pin = req.swagger.params.pin;

    let type = _.isUndefined(param_type.value)?'':param_type.value;
    let product_code = _.isUndefined(param_product_code.value)?'':param_product_code.value;
    let no = _.isUndefined(param_no.value)?'':param_no.value;
    let data = _.isUndefined(param_data.value)?'':param_data.value;
    let nominal = _.isUndefined(param_nominal.value)?'':param_nominal.value;
    let refnumber = _.isUndefined(param_ref_number.value)?'':param_ref_number.value;
    let pin = _.isUndefined(param_pin.value)?'':param_pin.value;

    try {
        Logs.create({action: log_action, description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_ppob_limit, is_allowed_access_token_static: false});
        let actual_pin = await service_user.getHashedPassword(auth_user.dataValues.salt, pin);
        let expected_pin = auth_user.dataValues.pin;
        if (expected_pin != actual_pin) {
            res.status(400).json("PIN salah")
        }

        let cus1, cus2, cus3, misc = '';

        switch(type) {
            case 'telkom':
                cus1 = data;
                cus2 = no;
                break;
            case 'pln_prabayar':
                cus1 = no;
                misc = Number(data.replace(/\D/gi, ""));
                break;
            case 'bpjs_kesehatan':
                cus1 = no;
                misc = data;
                break;
            default:
                cus1 = no;
                break;
        }

        if(cus1 == undefined)
            cus1 == ' ';
        if(cus2 == undefined)
            cus2 == ' ';
        if(cus3 == undefined)
            cus3 == ' ';
        if(misc == undefined)
            misc == ' ';

        if(type == 'pulsa' || type == 'data' || type == 'game' || type == 'voucher') {

            Logs.create({action: "post_ppob_payment_service ppob_topup_service", description: `param req model: ${type}, product_code: ${product_code}, customer_number1: ${cus1}, vaccount: ${auth_user.dataValues.valdo_account}, pin: ${pin}`});

            let api_response = await ApiPPOB.ppob_topup_service({model:type, product_code:product_code, customer_number1:cus1, vaccount:auth_user.dataValues.valdo_account, pin:pin});

            Logs.create({action: "api response ppob post_ppob_payment_service ppob_topup_service ", description: JSON.stringify(api_response)});
            if(api_response == false){
                res.status(400).json({code: 400, message: "Pembelian Gagal"});
            }
            else{
                res.json({code: 200, message: 'Pembelian berhasil', historyid: api_response.historyid,refnumber: api_response.RefNumber});
            }
//            res.json({code: 200, message: 'Pembelian berhasil', historyid: api_response.result.historyid,refnumber: api_response.RefNumber});
        } else {
            Logs.create({action: "post_ppob_payment_service ppob_payment_service", description: `param req model: ${type}, product_code: ${product_code}, customer_number1: ${cus1}, customer_number2: ${cus2}, customer_number3: ${cus3}, nominal: ${nominal}, refnumber: ${refnumber}, misc: ${misc}, vaccount: ${auth_user.dataValues.valdo_account}, pin: ${pin}`});

            let api_response = await ApiPPOB.ppob_payment_service({product_code:product_code, customer_number1:cus1, customer_number2:cus2, customer_number3:cus3, nominal:nominal, ref_number:refnumber, misc:misc, vaccount:auth_user.dataValues.valdo_account, pin:pin});

            Logs.create({action: "api response ppob post_ppob_payment_service ppob_payment_service", description: JSON.stringify(api_response)});
            
            res.json({code: 200, message: 'Pembayaran berhasil', historyid: api_response.result.historyid,refnumber: api_response.RefNumber});
        }
        let args = {}
        args.url = result_fmcg.dataValues.url;
        args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        args.fmcg_name = result_fmcg.dataValues.name;
        args.user_id = auth_user.dataValues.user_id;
        args.vaccount = auth_user.dataValues.valdo_account;
        args.model = type;
        args.product_code = product_code;
        args.customer_number = cus1;
        args.pin = pin;
        

        let top_up_response = await ApiPPOB.top_up_service(args)
        res.json(top_up_response)
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal melakukan pembelian / pembayaran, '+err;
        }

//        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function top_up_service(req,res){

    const token = req.swagger.params.token.value;
    const model = req.swagger.params.model.value;
    const product_code = req.swagger.params.product_code.value;
    const customer_number = req.swagger.params.customer_number.value;
    const pin = req.swagger.params.pin.value;

    let fmcg_id = 3;

    try{

        let auth_user = await middleware.verify({token: token, acl: acl_ppob_limit, is_allowed_access_token_static: false});

        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
        if(result_fmcg.dataValues === null) {
            throw new CustomError(400, 'Gagal memproses data invoice, data fmcg tidak tersedia');
        }

        let args = {}
        args.url = result_fmcg.dataValues.url;
        args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        args.fmcg_name = result_fmcg.dataValues.name;
        args.user_id = auth_user.dataValues.user_id;
        args.vaccount = auth_user.dataValues.valdo_account;
        args.model = model;
        args.product_code = product_code;
        args.customer_number = customer_number;
        args.pin = pin;

        let top_up_response = await ApiPPOB.top_up_service(args)
        
        res.status(200).json(top_up_response)

    }catch(err){
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data ppob, '+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function repayment(req,res){

    const token = req.swagger.params.token.value;
    const invoice_id = req.swagger.params.invoice_id.value;

    let fmcg_id = 3;

    try{

        let auth_user = await middleware.verify({token: token, acl: acl_ppob_limit, is_allowed_access_token_static: false});

        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
        if(result_fmcg.dataValues === null) {
            throw new CustomError(400, 'Gagal memproses data invoice, data fmcg tidak tersedia');
        }
        let payment = {}
        payment.url = result_fmcg.dataValues.url;
        payment.fmcg_token = result_fmcg.dataValues.fmcg_token;
        payment.fmcg_name = result_fmcg.dataValues.name;
        payment.user_id = auth_user.dataValues.user_id;
        payment.vaccount = auth_user.dataValues.valdo_account;
        payment.invoice_id = invoice_id;
        let repayment_response = await ApiPPOB.repayment(payment)

        let args = {}
        args.url = result_fmcg.dataValues.url;
        args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        args.fmcg_name = result_fmcg.dataValues.name;
        args.user_id = auth_user.dataValues.user_id;
        args.vaccount = auth_user.dataValues.valdo_account;
        args.paid_unpaid_status = "";
        args.invoice_id = invoice_id;
        args.filter1 = "";
        args.filter2 = "";
        args.page = "";
        args.items_per_page = "";
        args.sorting = "";

        let get_all_invoices = await ApiPPOB.get_invoice(args)
        
        res.status(200).json(repayment_response)

    }catch(err){
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data ppob, '+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function get_ppob_receipt(req, res) {
    const log_action = "get_ppob_receipt";

    const token = req.swagger.params.token.value;
    const historyid = req.swagger.params.historyid.value;

    try {
        Logs.create({action: log_action, description: `param req historyid: ${historyid}`});

        let auth_user = await middleware.verify({token: token, acl: acl_ppob_limit, is_allowed_access_token_static: false});
        let api_response = await ApiPPOBLama.ppob_receipt({historyid: historyid});

        Logs.create({action: 'api response ppob get_ppob_receipt ppob_receipt', description: JSON.stringify(api_response)});

        let result = [];
        let readableDate = helper.formatMilisecondToReadableDate(Number(api_response.Time));
        let readableTime = helper.formatMilisecondToReadableHour(Number(api_response.Time));
        let compact =[];

        if(api_response.product_type == 'pulsa' || api_response.product_type == 'data' || api_response.product_type == 'game' || api_response.product_type == 'voucher') {
            
            result.push(helper.createReceiptField("No Ref", (api_response.RefNumber != null) ? api_response.RefNumber : "", "string","NO REF"));
            result.push(helper.createReceiptField("No Pelanggan", (api_response.CustomerNumber1 != null) ? api_response.CustomerNumber1 : "", "string","NO PELANGGAN"));
            result.push(helper.createReceiptField("Produk", (api_response.Model != null) ? api_response.Model : "", "string","PRODUK"));
            result.push(helper.createReceiptField("Item",  (api_response.product_name != null) ? api_response.product_name : "", "string","ITEM"));
            result.push(helper.createReceiptField("Total Bayar", Number((api_response.total_bayar != null) ? api_response.total_bayar : ""), "currency","TOTAL BAYAR"));
            //result.push(helper.createReceiptField("Waktu Transaksi", convertMilisecondToDateString(Number(api_response.Time)), "string",""));

        } else if(api_response.product_type == 'multi_finance') {

            result.push(helper.createReceiptField("No Ref", (api_response.RefNumber != null) ? api_response.RefNumber : "", "string","NO REF"));
            result.push(helper.createReceiptField("No Pelanggan", (api_response.CustomerNumber1 != null) ? api_response.CustomerNumber1 : "", "string","NO PELANGGAN"));
            result.push(helper.createReceiptField("Nama Konsumen", api_response.CustomerName, "string","NAMA KONSUMEN"));
            result.push(helper.createReceiptField("Leasing", api_response.PtName, "string","LEASING"));
            result.push(helper.createReceiptField("No Pol", api_response.CarNumber, "string", "NO POL"));
            result.push(helper.createReceiptField("Tanggal Jatuh Tempo", api_response.LastPaidDueDate, "string","TANGGAL JATUH TEMPO"));
            result.push(helper.createReceiptField("Angsuran Ke", Number(api_response.LastPaidPeriode)+1, "number","ANGSURAN KE"));
            result.push(helper.createReceiptField("Tagihan", Number(api_response.Nominal), "currency","TAGIHAN"));
            result.push(helper.createReceiptField("Denda", Number(api_response.OdPenaltyFee), "currency", "DENDA"));
            result.push(helper.createReceiptField("Biaya Admin", Number(api_response.AdminCharge), "currency","BIAYA ADMIN"));
            result.push(helper.createReceiptField("Total Bayar", Number((api_response.total_bayar != null) ? api_response.total_bayar : ""), "currency","TOTAL BAYAR"));
            //result.push(helper.createReceiptField("Waktu Transaksi", convertMilisecondToDateString(Number(api_response.Time)), "string"));

            //compact.push(helper.createReceiptField("Waktu Transaksi", convertMilisecondToDateString(Number(api_response.Time)), "string"));

        } else if(api_response.product_type == 'pdam') {
            result.push(helper.createReceiptField("No Ref", (api_response.RefNumber != null) ? api_response.RefNumber : "", "string","NO REF"));
            result.push(helper.createReceiptField("No Pelanggan", (api_response.CustomerNumber1 != null) ? api_response.CustomerNumber1 : "", "string","NO PELANGGAN"));
            result.push(helper.createReceiptField("Nama Konsumen", api_response.CustomerName, "string", "NAMA KONSUMEN"));
            result.push(helper.createReceiptField("Alamat", api_response.CustomerAddress, "string","ALAMAT"));
            result.push(helper.createReceiptField("PDAM",  (api_response.product_name != null) ? api_response.product_name : "", "string","PDAM"));

            let keys = [];
            let index = 0;
            let regex = /^MonthPeriod(\d+)$/;
            let matched_index = 0;
            let denda = 0;

            _.forOwn(api_response, (v, k) => {
                keys.push(k);
            });

            _.forOwn(api_response, (v, k) => {
                let matched = k.match(regex);
                if (matched != null && v.length > 0) {
                    let period = Number(matched[1]);
                    matched_index = index;

                    result.push(helper.createReceiptField("", "", "blank"));

                    result.push(helper.createReceiptField(`Periode ${period}`, "", "blank"));

                    result.push(helper.createReceiptField(`Periode`, api_response[keys[index]] +" - "+ api_response[keys[index+1]], "string","PERIODE"));
                    result.push(helper.createReceiptField(`Stan Meter`, api_response[keys[index+2]] +" sd "+ api_response[keys[index+3]], "string","STAN METER"));
                    result.push(helper.createReceiptField(`Pemakaian`, String(Number(api_response[keys[index+3]]) - Number(api_response[keys[index+2]])) + " M3", "string","PEMAKAIAN"));
                    result.push(helper.createReceiptField(`Tagihan`, Number(api_response[keys[index+5]]), "currency","TAGIHAN"));
                    result.push(helper.createReceiptField(`Denda`, Number(api_response[keys[index+4]]), "currency","DENDA"));

                    denda += Number(api_response[keys[index+4]]);
                }
                index++;
            });

            result.push(helper.createReceiptField("", "", "blank"));

            result.push(helper.createReceiptField("Total Tagihan", Number(api_response.TotalBillAmount), "currency","TOTAL TAGIHAN"));
            result.push(helper.createReceiptField("Total Denda", Number(denda), "currency", "TOTAL DENDA"));
            result.push(helper.createReceiptField("Biaya Admin", Number(api_response.AdminCharge), "currency","BIAYA ADMIN"));
            result.push(helper.createReceiptField("Total Bayar", Number((api_response.total_bayar != null) ? api_response.total_bayar : ""), "currency","TOTAL BAYAR"));
            //result.push(helper.createReceiptField("Waktu Transaksi", convertMilisecondToDateString(Number(api_response.Time)), "string"));

            // result.push(helper.createReceiptField("Tanggal Jatuh Tempo", api_response.LastPaidDueDate, "string"));
            // result.push(helper.createReceiptField("Angsuran Ke", Number(api_response.LastPaidPeriode)+1, "number"));
            // result.push(helper.createReceiptField("Tagihan", Number(api_response.Nominal), "currency"));
            // result.push(helper.createReceiptField("Denda", Number(api_response.OdPenaltyFee), "currency"));
            // result.push(helper.createReceiptField("Biaya Admin", Number(api_response.AdminCharge), "currency"));
            // result.push(helper.createReceiptField("Total Bayar", Number(api_response.Nominal), "currency"));
            // result.push(helper.createReceiptField("Waktu Transaksi", convertMilisecondToDateString(Number(api_response.Time)), "string"));
        } else if(api_response.product_type == 'telkom') {
            result.push(helper.createReceiptField("No Ref", (api_response.RefNumber != null) ? api_response.RefNumber : "", "string", "NO REF"));
            result.push(helper.createReceiptField("No Pelanggan", (api_response.CustomerNumber1 != null) ? api_response.CustomerNumber1 : "", "string", "NO PELANGGAN"));
            result.push(helper.createReceiptField("Nama Konsumen", api_response.CustomerName, "string","NAMA KONSUMEN"));
            result.push(helper.createReceiptField("Tarif", Number(api_response.Nominal), "currency","TARIF"));

            let keys = [];
            let index = 0;
            let regex = /^RpTag(\d+)$/;
            let matched_index = 0;
            let denda = 0;

            _.forOwn(api_response, (v, k) => {
                keys.push(k);
            });
            _.forOwn(api_response, (v, k) => {
                let matched = k.match(regex);
                if (matched != null && v.length > 0) {
                    let period = Number(matched[1]);
                    matched_index = index;

                    result.push(helper.createReceiptField("", "", "blank"));

                    result.push(helper.createReceiptField(`Tagihan ${period}`, "", "blank"));

                    result.push(helper.createReceiptField(`Tagihan ${period}`, Number(api_response[keys[index]]), "currency","TAGIHAN"));

                }
                index++;
            });
            result.push(helper.createReceiptField("", "", "blank"));

            result.push(helper.createReceiptField("Total Tagihan", Number(api_response.BillAmount), "currency","TOTAL TAGIHAN"));
            result.push(helper.createReceiptField("Biaya Admin", Number(api_response.AdminCharge), "currency","BIAYA ADMIN"));
            result.push(helper.createReceiptField("Total Bayar", Number((api_response.total_bayar != null) ? api_response.total_bayar : ""), "currency", "TOTAL BAYAR"));
            //result.push(helper.createReceiptField("Waktu Transaksi", convertMilisecondToDateString(Number(api_response.Time)), "string"));
        } else if(api_response.product_type == 'pln_pascabayar') {
            result.push(helper.createReceiptField("No Ref", (api_response.RefNumber != null) ? api_response.RefNumber : "", "string","NO REF"));
            result.push(helper.createReceiptField("No Pelanggan", (api_response.CustomerNumber1 != null) ? api_response.CustomerNumber1 : "", "string","NO PELANGGAN"));
            result.push(helper.createReceiptField("Nama Konsumen", api_response.SubscriberName, "string","NAMA KONSUMEN"));
            result.push(helper.createReceiptField("Tarif/Daya", api_response.SubscriberSegmentation, "string","TARIF/DAYA"));

            let keys = [];
            let index = 0;
            let regex = /^Blth(\d+)$/;
            let matched_index = 0;
            let denda = 0;

            _.forOwn(api_response, (v, k) => {
                keys.push(k);
            });
            _.forOwn(api_response, (v, k) => {
                let matched = k.match(regex);
                if (matched != null && v.length > 0) {
                    let period = Number(matched[1]);
                    matched_index = index;

                    result.push(helper.createReceiptField("", "", "blank"));

                    //result.push(helper.createReceiptField(`Blth ${period}`, "", "blank"));

                    result.push(helper.createReceiptField(`Blth`, api_response[keys[index]], "string","BLTH"));
                    //result.push(helper.createReceiptField(`Tagihan`, Number(api_response[keys[index+3]]), "currency","TAGIHAN"));
                    //result.push(helper.createReceiptField(`Denda`, Number(api_response[keys[index+6]]), "currency","DENDA"));

                    denda += Number(api_response[keys[index+6]]);
                }
                index++;
            });
            result.push(helper.createReceiptField("", "", "blank"));

            result.push(helper.createReceiptField("Total Tagihan", Number(api_response.TotalRpTag), "currency","TOTAL TAGIHAN"));
            result.push(helper.createReceiptField("Total Denda", Number(denda), "currency", "TOTAL DENDA"));
            result.push(helper.createReceiptField("Biaya Admin", Number(api_response.AdminCharge), "currency","BIAYA ADMIN"));
            result.push(helper.createReceiptField("Total Bayar", Number((api_response.total_bayar != null) ? api_response.total_bayar : ""), "currency", "TOTAL BAYAR"));
            //result.push(helper.createReceiptField("Waktu Transaksi", convertMilisecondToDateString(Number(api_response.Time)), "string"));
        } else if(api_response.product_type == 'pulsa_pascabayar') {
            result.push(helper.createReceiptField("No Ref", (api_response.RefNumber != null) ? api_response.RefNumber : "", "string", "NO REF"));
            result.push(helper.createReceiptField("No Pelanggan", (api_response.CustomerNumber1 != null) ? api_response.CustomerNumber1 : "", "string", "NOMOR PELANGGAN"));
            result.push(helper.createReceiptField("Nama Konsumen", api_response.CustomerName, "string", "NAMA KONSUMEN" ));
            result.push(helper.createReceiptField("Provider",api_response.ProviderName, "string", "PROVIDER"));

            let keys = [];
            let index = 0;
            let regex = /^Periode(\d+)$/;
            let matched_index = 0;
            let denda = 0;

            _.forOwn(api_response, (v, k) => {
              keys.push(k);
            });
            _.forOwn(api_response, (v, k) => {
                let matched = k.match(regex);
                if (matched != null && v.length > 0) {
                    let period = Number(matched[1]);
                    matched_index = index;

                    result.push(helper.createReceiptField("", "", "blank"));

                    result.push(helper.createReceiptField(`Periode ${period}`, "", "blank"));

                    result.push(helper.createReceiptField(`Periode`, api_response[keys[index]]+" - "+ api_response[keys[index+1]], "string", "PERIODE"));
                    result.push(helper.createReceiptField(`Tagihan`, Number(api_response[keys[index+3]]), "currency", "TAGIHAN"));
                    result.push(helper.createReceiptField(`Denda`, Number(api_response[keys[index+2]]), "currency", "DENDA"));

                    denda += Number(api_response[keys[index+2]]);
                }
                index++;
            });
            result.push(helper.createReceiptField("", "", "blank"));

            result.push(helper.createReceiptField("Total Tagihan", Number(api_response.TotalBillAmount), "currency", "TOTAL TAGIHAN"));
            result.push(helper.createReceiptField("Total Denda", Number(denda), "currency", "TOTAL DENDA"));
            result.push(helper.createReceiptField("Biaya Admin", Number(api_response.AdminCharge), "currency", "BIAYA ADMIN"));
            result.push(helper.createReceiptField("Total Bayar", Number((api_response.total_bayar != null) ? api_response.total_bayar : ""), "currency", "TOTAL BAYAR"));
            
        } else if(api_response.product_type == 'pln_prabayar') {
            result.push(helper.createReceiptField("No Ref", (api_response.RefNumber != null) ? api_response.RefNumber : "", "string", "NO REF"));
            result.push(helper.createReceiptField("No Pelanggan", (api_response.CustomerNumber1 != null) ? api_response.CustomerNumber1 : "", "string","NO PELANGGAN"));
            result.push(helper.createReceiptField("Nama Konsumen", api_response.SubscriberName, "string","NAMA KONSUMEN"));
            result.push(helper.createReceiptField("Tarif/Daya", api_response.SubscriberSegmentation , "string","TARIF/DAYA"));
            result.push(helper.createReceiptField("Nominal", (api_response.Nominal != null) ? Number(api_response.Nominal) : 0, "currency","BIAYA ADMIN"));

            result.push(helper.createReceiptField("Biaya Admin", (api_response.AdminCharge != null) ? Number(api_response.AdminCharge) : 0, "currency","BIAYA ADMIN"));
            result.push(helper.createReceiptField("Total Bayar", Number((api_response.total_bayar != null) ? api_response.total_bayar : ""), "currency","TOTAL BAYAR"));
            result.push(helper.createReceiptField("Jumlah KWH", (api_response.MaxKwhLimit != null) ? Number(api_response.MaxKwhLimit.replace(',', '.')) : 0, "number","JUMLAH KWH"));
            result.push(helper.createReceiptField("TOKEN PLN", api_response.TokenPln, "string", "TOKEN PLN"));
            //result.push(helper.createReceiptField("Waktu Transaksi", convertMilisecondToDateString(Number(api_response.Time)), "string"));
        } else if(api_response.product_type == 'tv_cable') {
            result.push(helper.createReceiptField("No Ref", (api_response.RefNumber != null) ? api_response.RefNumber : "", "string","NO REF"));
            result.push(helper.createReceiptField("No Pelanggan", (api_response.CustomerNumber1 != null) ? api_response.CustomerNumber1 : "", "string","NO PELANGGAN"));
            result.push(helper.createReceiptField("Nama Konsumen", api_response.CustomerName, "string","NAMA KONSUMEN"));
            result.push(helper.createReceiptField("TV", Number(api_response.PtName), "currency","TV"));
            result.push(helper.createReceiptField("Tagihan", Number(api_response.Nominal), "currency","TAGIHAN"));
            result.push(helper.createReceiptField("Denda", Number(api_response.Penalty), "currency","DENDA"));
            result.push(helper.createReceiptField("Biaya Admin", Number(api_response.AdminCharge), "currency","BIAYA ADMIN"));
            result.push(helper.createReceiptField("TOTAL BAYAR", Number((api_response.total_bayar != null) ? api_response.total_bayar : ""), "currency","TOTAL BAYAR"));
            //result.push(helper.createReceiptField("Waktu Transaksi", convertMilisecondToDateString(Number(api_response.Time)), "string"));

            //compact.push(helper.createReceiptField("Waktu Transaksi", convertMilisecondToDateString(Number(api_response.Time)), "string"));

        } else if(api_response.product_type == 'bpjs_kesehatan') {
            result.push(helper.createReceiptField("No Ref", (api_response.RefNumber != null) ? api_response.RefNumber : "", "string","NO REF"));
            result.push(helper.createReceiptField("No Pelanggan", (api_response.CustomerNumber1 != null) ? api_response.CustomerNumber1 : "", "string","NO PELANGGAN"));
            result.push(helper.createReceiptField("Nama Konsumen", api_response.CustomerName, "string", "NAMA KONSUMEN"));
            result.push(helper.createReceiptField("Jumlah Peserta", Number(api_response.BillQuantity), "number","JUMLAH PESERTA"));
            result.push(helper.createReceiptField("BPJS", api_response.PtName, "string","BPJS"));
            result.push(helper.createReceiptField("Periode", api_response.misc, "string","PERIODE"));
            result.push(helper.createReceiptField("Kode Cabang", api_response.BillerCode, "string","KODE CABANG"));
            result.push(helper.createReceiptField("Tagihan", Number(api_response.Nominal), "currency", "TAGIHAN"));
            result.push(helper.createReceiptField("Denda", Number(api_response.Penalty), "currency","DENDA"));
            result.push(helper.createReceiptField("Biaya Admin", Number(api_response.AdminCharge), "currency","BIAYA ADMIN"));
            result.push(helper.createReceiptField("Total Bayar", Number((api_response.total_bayar != null) ? api_response.total_bayar : ""), "currency","TOTAL BAYAR"));
            //result.push(helper.createReceiptField("Waktu Transaksi", convertMilisecondToDateString(Number(api_response.Time)), "string"));
        }

        res.json({type:api_response.product_type,provider:api_response.product_name,time:Date.now(),result:result, readableDate: readableDate, readableTime: readableTime});
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal mengambil data transaksi, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function get_ppob_purchase_check_price(req, res) {
    const log_action = "get_ppob_purchase_check_price";

    const token = req.swagger.params.token.value;
    const param_type = req.swagger.params.type;
    const param_no = req.swagger.params.no;
    const param_provider = req.swagger.params.provider;

    let type = _.isUndefined(param_type.value)?'':param_type.value;
    let provider = _.isUndefined(param_provider.value)?'':param_provider.value;
    let no = _.isUndefined(param_no.value)?'':param_no.value;

    try {
        Logs.create({action: log_action, description: "param req type: "+type+", no: "+no});

        let auth_user = await middleware.verify({token: token, acl: acl_ppob_limit, is_allowed_access_token_static: false});

        if(type == 'pulsa') {
            type = type.trim().toLowerCase();
            no = no.trim().toLowerCase();

            no = no.replace(/^\+62/, '0');

            let operators = {
                TELKOMSEL: ["0812","0813","0821","0822","0852","0853","0823","0851"],
                INDOSAT: ["0814","0815","0816","0855","0856","0857","0858"],
                THREE: ["0895","0896","0897","0898","0899"],
                SMARTFREN: ["0881","0882","0883","0884","0885","0886","0887","0888","0889"],
                XL: ["0817","0818","0819","0859","0877","0878"],
                AXIS: ["0838","0831","0832","0833"]
            };

            let no_hp = no.slice(0, 4);

            _.find(operators, (operator, operator_name) => {
                if(_.includes(operator, no_hp)) {
                    provider = operator_name;
                    return true;
                }
            });
        }

        let api_response= await ApiPPOB.ppob_check_price({product_type: type, provider: provider});
        let new_api = api_response.result;
        Logs.create({action: 'api response ppob get_ppob_purchase_check_price ppob_check_price', description: util.inspect(api_response)});

        let results = new_api.map( (result, index) => {

            let _result = {};

            _result.id = index;
            _result.provider = provider;
            _result.id_number = no;
            _result.type = type;
            _result.product_code = result.product_code;
            _result.product_name = result.product_name;

            _result.product_price = Number(result.product_price);
            _result.status = result.status == 'open';

            _result.cashback_customer = 0;
            _result.fee_tpn = Number(result.fee_tpn);

            return _result;
        });

        res.json({results: results});
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal mendapatkan data daftar harga, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function ppob_product_type_and_provider(req, res) {
    const param_type = req.swagger.params.type;
    let type = _.isUndefined(param_type.value)?'':param_type.value;

    let obj = {};

    if(type == 'pulsa' || type == 'pulsa_pascabayar')
        obj.providers = ['TELKOMSEL', 'INDOSAT', 'THREE', 'SMARTFREN', 'XL', 'AXIS'];
    else if(type == 'data')
        obj.providers = ['BOLT', 'AXIS', 'XL', 'TELKOMSEL', 'INDOSAT', 'THREE', 'SMARTFREN'];
    else if(type == 'game')
        obj.providers = ['ZYNGA', 'VIWAWA', 'CHERRY CREDIT', 'ULTIMATE ASCENT', 'TRAVIAN GAME', 'TERACORD', 'GAME STEAM', 'GEMSCOOL', 'MEGAXUS', 'LYTO', 'GARENA', 'DIGICASH', 'MOBILE LEGEND'];
    else if(type == 'voucher')
           obj.providers = ['WAVEGAME','MEGAXUS','MOBILELEGEND','RAGNAROK','GARENA','RAZERGOLD','DANCINGLOVE','GEMSCOOL','GENFLIX','GOOGLEPLAY','STEAMIDR'];
    else if(type == 'pln_prabayar' || type == 'pln_pascabayar')
        obj.providers = ['20.000', '50.000', '100.000', '200.000', '500.000', '1.000.000', '5.000.000'];

    res.json(obj);
}
async function pelunasan_manual(req,res){

    const token = req.swagger.params.token.value;
    const invoice_id = req.swagger.params.invoice_id.value;
    const vacc_number = req.swagger.params.vacc_number.value;
   
    let fmcg_id = 3;

    try{

        let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});
        
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
        if(result_fmcg.dataValues === null) {
            throw new CustomError(400, 'Gagal memproses data invoice, data fmcg tidak tersedia');
        }

        let inv_args = {}
        inv_args.url = result_fmcg.dataValues.url;
        inv_args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        inv_args.fmcg_name = result_fmcg.dataValues.name;
        inv_args.invoice_id = invoice_id;
        inv_args.vacc_number = vacc_number;
        let api_response_fmcg = await ApiPPOB.one_invoice_manual(inv_args);

        Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} get_one_invoice`, description: util.inspect(api_response_fmcg)});


        let detail_toko = await service_user.getOne({valdo_account:vacc_number})

        let api_response = await MoneyAccess.inquiry({mobnum: detail_toko.dataValues.valdoAccount, vaccount: detail_toko.dataValues.valdoAccount, usertype: detail_toko.dataValues.type});
        Logs.create({action: 'api response moneyaccess get_balance inquiry', description: util.inspect(api_response)});

        delete api_response.userid;
        delete api_response.vacc_number;
        
        let out = api_response;

        if(api_response_fmcg.is_paid == true){
            throw new CustomError(400, 'Invoice lunas');
        }
        if(Number(out.balance) < Number(api_response_fmcg.amount)){
            throw new CustomError(400, 'Saldo tidak cukup');
        }
        if(out.balance == 0 ){
           throw new CustomError(400, 'Saldo kosong');
        }

        let args = {}
        args.url = result_fmcg.dataValues.url;
        args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        args.fmcg_name = result_fmcg.dataValues.name;
        args.vaccount = detail_toko.dataValues.valdoAccount;
        args.invoice_id = invoice_id;

        let repayment_response = await ApiPPOB.repayment(args)
        res.status(200).json(repayment_response)
        //res.status(200).json({message:'ok'})

    }catch(err){
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data ppob, '+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}


export { get_invoice }
export { get_one_invoice }
export { top_up_service }
export { repayment }
export { post_ppob_payment_service }
export { get_ppob_receipt }
export { get_ppob_purchase_check_price }
export { ppob_product_type_and_provider }
export { pelunasan_manual }