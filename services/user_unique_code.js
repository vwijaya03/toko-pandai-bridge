'use strict';

import helper from '~/utils/helper'

import { orm } from '~/services/mariadb'
import { UserUniqueCode } from '~/orm/index'
import { FMCG } from '~/orm/index'

import UserUniqueCodeNotFoundError from '~/response/error/UserUniqueCodeNotFoundError'
import UserUniqueCodeIdNotFoundError from '~/response/error/UserUniqueCodeIdNotFoundError'

async function get(filter, options) {
    options = options || {};
    let result_set = '';

    if(filter) {
        result_set = await UserUniqueCode.findAll({
            where: filter
        });
    } else {
        result_set = await UserUniqueCode.findAll();
    }

    if (!result_set || Object.keys(result_set).length == 0) {
        if (options.throwIfEmpty) {
            throw new UserUniqueCodeNotFoundError();
        }
    }

    return result_set;
}

function add({user_id=null, fmcg_id=null, unique_code=null, description=null,position=null}={}) {
    let args = {};
    args.user_id = user_id;
    args.fmcg_id = fmcg_id;
    args.unique_code = unique_code;
    args.description = description;
    args.position = position;
    return UserUniqueCode.create(args);
}

async function getOne(filter, options) {

    options = options || {};
    let result_set = '';

    result_set = await UserUniqueCode.findOne({where: filter});

    if (!result_set || Object.keys(result_set).length == 0) {
        if (options.throwIfEmpty) {
            throw new UserNotFoundError();
        }
    }

    return result_set;
}

function update({id=null, fmcg_id=null, unique_code=null, description=null}={}) {
    return UserUniqueCode.update(
        { fmcg_id: fmcg_id, unique_code: unique_code, description: description },
        { where: { id: id }}
    );
}

function deleteById(id, user_id) {
    return UserUniqueCode.destroy({
        where: {
            id: id,
            user_id: user_id //this will be your id that you want to delete
        }
    });  
}

function getJoinUsersUniqueCodeAndUser({unique_code=null, fmcg_id=null}={}) {
    return orm.query(`
        select uad.value 
        from users_unique_codes as uuc
        inner join users_additional_datas as uad on uad.user_id = uuc.user_id
        where uuc.unique_code = '${unique_code}' and uad.field = 'Nama Toko' and uuc.fmcg_id = '${fmcg_id}' limit 1
    `, { type: orm.QueryTypes.SELECT});
}

function checkUniqueCodeAndNamaToko({data=null, fmcg_id=null}={}) {
    return orm.query(`
        select uad.value as outlet_name, uuc.unique_code
        from users_unique_codes as uuc
        inner join users_additional_datas as uad on uad.user_id = uuc.user_id
        where uad.field = 'Nama Toko' and (uuc.unique_code = '${data}' or uad.value like '%${data}%') and uuc.fmcg_id = '${fmcg_id}'
    `, { type: orm.QueryTypes.SELECT});
}

let obj = {}
obj.add = add
obj.get = get
obj.getOne = getOne
obj.deleteById = deleteById
obj.update = update
obj.getJoinUsersUniqueCodeAndUser = getJoinUsersUniqueCodeAndUser
obj.checkUniqueCodeAndNamaToko = checkUniqueCodeAndNamaToko

export default obj