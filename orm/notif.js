import { orm } from '~/services/mariadb'
import Sequelize from 'sequelize'

const Notification = orm.define('notifications', {
    id: { type: Sequelize.INTEGER, field: 'id', primaryKey: true, autoIncrement: true },
    to: { type: Sequelize.STRING, allowNull:false },
    type: { type: Sequelize.STRING, allowNull:false },
    message: { type: Sequelize.INTEGER, allowNull:false },
    state: { type: Sequelize.STRING, allowNull:false },
    createdAt: { type: 'TIMESTAMP', allowNull:true },
    updatedAt: { type: 'TIMESTAMP', allowNull:true }
});

export default Notification