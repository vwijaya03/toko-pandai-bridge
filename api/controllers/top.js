'use strict';

import _ from 'lodash'
import util from 'util'
import fs from 'fs'
import middleware from '~/api/controllers/auth'
import request from 'request'
import moment from 'moment'

import service_excel from '~/services/excel'
import service_invoice from '~/services/invoice'
import service_fmcg from '~/services/fmcg'
import service_top from '~/services/top'
import service_user from '~/services/user'
import service_user_unique_code from '~/services/user_unique_code'
import service_pelunasan_manual from '~/services/pelunasan_manual'


import helper from '~/utils/helper'
import MoneyAccess from '~/external_api/moneyaccess'
import ApiTop from '~/external_api/api_top'
import ApiFmcg from '~/external_api/api_uli'
import ApiPd from '~/external_api/api_top_pohon_dana'

import { Logs } from '~/orm/index'

import CustomError from '~/response/error/CustomError'
import CustomSuccess from '~/response/success/CustomSuccess'

const createCsvWriter = require('csv-writer').createObjectCsvWriter;

const acl_get_invoices = ['retailer', 'distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];
const acl_get_one_invoice = ['retailer', 'distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];
const acl_repayment = ['retailer', 'distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];
const acl_get_retur = ['retailer', 'distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];
const acl_check_process_invoices = ['retailer', 'distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];
const acl_task_list = ['retailer', 'distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];
const acl_save_csv = ['retailer', 'distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];
const acl_save_xls = ['retailer', 'distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];
const acl_process_invoice = ['retailer', 'distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];

// let top_api = "tokopandai-development.pinjammodal.id";
// let top_api_key = "0ff0170259be55375551f943d9ebf04eb097d0b55661a5e8f13dac3ef9062820";

// let apigatekey= 'webKEY123456789';
// let url_trf = '192.168.13.2'

let top_api = "api.pinjammodal.id:8443/tokopandai";
let top_api_key = "904071eb7378b9f0cc7152b7ada253d7f9493e0e4a1ef9a0634ebf101b7e84ca"
let apigatekey = 'webKEY123456789';
let url_trf = '192.168.12.2'

async function get_invoice(req, res) {
    const token = req.swagger.params.token.value;
    const param_lecode = req.swagger.params.le_code;
    const param_dtcode = req.swagger.params.dt_code;
    const param_invoice_payment_status = req.swagger.params.invoice_payment_status;
    const param_invoice_due_date = req.swagger.params.invoice_due_date;
    const param_page = req.swagger.params.page;
    const param_items_per_page = req.swagger.params.items_per_page;
    const param_overdue = req.swagger.params.overdue;
    const param_sorting = req.swagger.params.sorting;

    let le_code = _.isUndefined(param_lecode.value) ? null : param_lecode.value;
    let page = _.isUndefined(param_page.value) ? 0 : param_page.value;
    let overdue = _.isUndefined(param_overdue.value) ? 0 : param_overdue.value;
    let items_per_page = _.isUndefined(param_items_per_page.value) ? 15 : param_items_per_page.value;
    let dt_code = _.isUndefined(param_dtcode.value) ? null : param_dtcode.value;
    let invoice_payment_status = _.isUndefined(param_invoice_payment_status.value) ? null : param_invoice_payment_status.value;
    let invoice_due_date = _.isUndefined(param_invoice_due_date.value) ? null : param_invoice_due_date.value;
    let sorting = _.isUndefined(param_sorting.value) ? null : param_sorting.value;

    const iso8601format = "YYYY-MM-DD";
    let data_unique_code = '';
    let fmcg_top = 1;
    let fmcg_id = 13;

    try {
        Logs.create({action: 'get_one', description: helper.getSimpleSwaggerParams(req.swagger.params)});
        let auth_user = await middleware.verify({token: token, acl: acl_get_invoices, is_allowed_access_token_static: false});
        let result_fmcg_top = await service_fmcg.getOneFmcgWithFilter({id: fmcg_top});
        if (result_fmcg_top.dataValues === null) {
            throw new CustomError(400, 'Gagal memproses data invoice, data fmcg tidak tersedia');
        }
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
        if (result_fmcg.dataValues === null) {
            throw new CustomError(400, 'Gagal memproses data invoice, data fmcg tidak tersedia');
        }
        let dist_detail = await service_pelunasan_manual.get_dt_code({user_id: auth_user.dataValues.user_id})


        let dist_dt_code = dist_detail[0].unique_code
        if (auth_user.dataValues.type === 1 || auth_user.dataValues.type === 3) {
            let data_dist = {
                url: result_fmcg_top.dataValues.url,
                fmcg_token: result_fmcg_top.dataValues.fmcg_token,
                fmcg_name: result_fmcg_top.dataValues.name,
                dist_dt_code: dist_dt_code
            }
            let get_all_le_code = await ApiTop.get_all_le_code(data_dist)
            for (let i = 0; i < get_all_le_code.length; i++) {
                if (i === 0 && get_all_le_code.length === 1) {
                    le_code = get_all_le_code[i].le_code
                } else if (i === 0 && get_all_le_code.length !== 1) {
                    le_code = get_all_le_code[i].le_code + ","
                } else if (i === get_all_le_code.length - 1 && get_all_le_code.length !== 1) {
                    le_code += get_all_le_code[i].le_code
                } else {
                    le_code += get_all_le_code[i].le_code + ","
                }

            }
        }

        let invoice_args = {};
        invoice_args.url = result_fmcg_top.dataValues.url;
        invoice_args.fmcg_token = result_fmcg_top.dataValues.fmcg_token;
        invoice_args.fmcg_name = result_fmcg_top.dataValues.name;
        invoice_args.le_code = le_code;
        invoice_args.user_id = auth_user.dataValues.user_id;
        invoice_args.role_type = auth_user.dataValues.type;
        invoice_args.dt_code = dt_code;
        invoice_args.overdue = overdue;
        invoice_args.invoice_payment_status = invoice_payment_status;
        invoice_args.invoice_due_date = invoice_due_date;
        invoice_args.dist_dt_code = dist_dt_code;
        invoice_args.page = page;
        invoice_args.items_per_page = items_per_page;
        invoice_args.sorting = sorting;

        let api_response_fmcg = await ApiTop.get_invoice(invoice_args);
        let api = api_response_fmcg.invoices;
        
        for (let i = 0; i < api.length; i++) {
            let data = {
                dt_code: api[i].dt_code,
                url: result_fmcg.dataValues.url,
                fmcg_token: result_fmcg.dataValues.fmcg_token,
                fmcg_name: result_fmcg.dataValues.name

            }
            let dist = await ApiTop.get_one_distributor(data);
            let toko_data = await service_pelunasan_manual.get_le_aditional_name({user_id: api[i].user_id});
            
            let nama_toko = '';
            if (toko_data.length === 0)
            {
                nama_toko = 'Tidak ada nama';
            } else {
                nama_toko = toko_data[0].value;
            }
            api[i].distributor.name = dist.transactions[0].name
            api[i].outlet_name = nama_toko;//outlet[0].outlet_name
            api[i].tenor = api[i].total_tenor.fulfillmentValue + ' minggu'
//            let tanggal = api[i].paid_data_uli.fulfillmentValue[0]
//            api[i].paid_date_uli = tanggal
//            delete api[i].paid_data_uli
            delete api[i].total_tenor
            delete api[i].total_amountt
        }

        Logs.create({action: `api response fmcg get one top`, description: util.inspect(api_response_fmcg)});
        let out = {}
        out.page = api_response_fmcg.page
        out.total_pages = api_response_fmcg.total_pages
        out.size = api_response_fmcg.size
        out.items_per_page = api_response_fmcg.items_per_page
        out.invoices = api

        res.json(out);
    } catch (err) {
        if (err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data top, ' + err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function get_one_invoice(req, res) {
    const token = req.swagger.params.token.value;
    const param_le_code = req.swagger.params.le_code;
    const param_invoice_id = req.swagger.params.invoice_id;
    const param_dt_code = req.swagger.params.dt_code;
    const param_tenor = req.swagger.params.tenor;

    const iso8601format = "YYYY-MM-DD";

    let le_code = _.isUndefined(param_le_code.value) ? null : param_le_code.value;
    let dt_code = _.isUndefined(param_dt_code.value) ? null : param_dt_code.value;
    let invoice_id = _.isUndefined(param_invoice_id.value) ? null : param_invoice_id.value;
    let tenor = _.isUndefined(param_tenor.value) ? null : param_tenor.value;

    let data_unique_code = '';
    let fmcg_id = 1;
    var ten = tenor.split(" ", 1);
    try {
        Logs.create({action: 'get_one', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_get_invoices, is_allowed_access_token_static: false});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});


        if (result_fmcg.dataValues == {}) {
            throw new CustomError(400, 'Gagal memproses data invoice, data fmcg tidak tersedia');
        }
        let invoice_args = {};
        invoice_args.url = result_fmcg.dataValues.url;
        invoice_args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        invoice_args.fmcg_name = result_fmcg.dataValues.name;
        invoice_args.le_code = le_code;
        invoice_args.invoice_id = invoice_id;
        invoice_args.dt_code = dt_code;
        invoice_args.tenor = ten;
        invoice_args.user_id = auth_user.dataValues.user_id;
        let api_response_fmcg = await ApiTop.get_one_invoice(invoice_args);
        
        

        Logs.create({action: `api response fmcg get one top`, description: util.inspect(api_response_fmcg)});


        res.json(api_response_fmcg);
    } catch (err) {
        if (err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data top, ' + err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function cek_saldo(req, res) {
    const token = req.swagger.params.token.value;
    const param_le_code = req.swagger.params.le_code;

    const iso8601format = "YYYY-MM-DD";

    let le_code = _.isUndefined(param_le_code.value) ? null : param_le_code.value;

    let data_unique_code = '';

    try {
        Logs.create({action: 'get_one', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_get_invoices, is_allowed_access_token_static: false});

        let api_loan = {};

        api_loan.le_code = le_code;

        let api_response_fmcg = await ApiTop.cek_saldo(api_loan);

        Logs.create({action: `api response fmcg  get one top`, description: util.inspect(api_response_fmcg)});

        let invoices = api_response_fmcg.invoices;

        res.json({code: 200, "Saldo FIT": api_response_fmcg, "Saldo BFI": api_response_fmcg});
    } catch (err) {
        if (err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data top, ' + err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function calculator(req, res) {
    const token = req.swagger.params.token.value;
    const param_amount = req.swagger.params.amount;
    const param_tenor = req.swagger.params.tenor;
    const iso8601format = "YYYY-MM-DD";

    let amount = _.isUndefined(param_amount.value) ? 0 : param_amount.value;
    let tenor = _.isUndefined(param_tenor.value) ? 1 : param_tenor.value;
    let data_unique_code = '';
    let fmcg_id = 13;
    let discounts = 0;
    var ten = amount.split(".", 1);

    try {
        Logs.create({action: 'calculator top', description: helper.getSimpleSwaggerParams(req.swagger.params)});


        let auth_user = await middleware.verify({token: token, acl: acl_get_invoices, is_allowed_access_token_static: false});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});

        let today = new Date()
        var weekday = new Array(7);
        weekday[0] = "Minggu";
        weekday[1] = "Senin";
        weekday[2] = "Selasa";
        weekday[3] = "Rabu";
        weekday[4] = "Kamis";
        weekday[5] = "Jumat";
        weekday[6] = "Sabtu";
        let day = weekday[today.getDay()];
        let current_time = today.getHours();
        let time_setting = moment(today.setHours(current_time)).format('HH:mm:ss');

//        if(day === 'Jumat' && time_setting > '17:00:00'){
//            throw new CustomError(400, 'Waktu transaksi telah ditutup!');
//        }
//        else if(day === 'Sabtu' || day === 'Minggu'){
//            throw new CustomError(400, 'Waktu transaksi telah ditutup!');
//        }

        if (tenor === 0) {
            throw new CustomError(400, 'Tenor tidak boleh 0');
        }
        if (amount < 100000) {
            throw new CustomError(400, 'Invoice Minimal Rp. 100.000');
        }
        request({
            headers: {
                'Content-Type': 'application/json'
            },
            url: "https://" + top_api,
            method: "GET",
            json: true
        }, function (error, response, body) {
            if (response.statusCode === 502) {
                res.status(400).json({code: response.statusCode, message: "PayLater Sedang Dalam Perbaikan"});
            } else {


                let args = {
                    "amount": parseInt(ten[0]),
                    "discount": 0,
                    "tenor": tenor
                }
                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'api_key': top_api_key
                    },
                    url: "https://" + top_api + "/loan/calculator-tokopandai",
                    method: "POST",
                    json: true,
                    body: args
                }, function (error, response, body) {
                    if (body.status === 'success') {
                    discounts = body.installmentLoanDetail[0].loan_payback - body.installmentLoanDetail[0].loan_amount;
                    }
                    
                    let dis = discounts / 100 * 0;
                    let data = {
                        "amount": parseInt(ten[0]),
                        "discount": parseInt(dis),
                        "tenor": tenor
                    }
                    request({
                        headers: {
                            'Content-Type': 'application/json',
                            'api_key': top_api_key
                        },
                        url: "https://" + top_api + "/loan/calculator-tokopandai",
                        method: "POST",
                        json: true,
                        body: data
                    }, function (error, response, body) {                       
                        let week = 0;
                        if (body.status === 'success') {
                             week = [{
                                "loan_amount": body.installmentLoanDetail[0].loan_amount + ".0000",
                                "loan_payback": body.installmentLoanDetail[0].loan_payback + ".0000",
                                "administrasi": (body.installmentLoanDetail[0].loan_payback + body.installmentLoanDetail[0].discount - body.installmentLoanDetail[0].loan_amount) + ".0000",
                                "loan_length": body.installmentLoanDetail[0].loan_length,
                                "installment": body.installmentLoanDetail[0].installment + ".0000",
                                "total_potongan": body.installmentLoanDetail[0].discount + ".0000",
                            }]
                         }
                         else
                         {
                             week = ({respon:body.message});
                         }
                        res.json({week});
                    });
                });
            }
        })
    } catch (err) {
        if (err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data calculator TOP, ' + err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function repayment(req, res) {
    const token = req.swagger.params.token.value;
    const pin = req.swagger.params.pin.value;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_tenor = req.swagger.params.tenor;
    const param_amount = req.swagger.params.amount;
    const param_invoice_number = req.swagger.params.invoice_id;
    const param_le_code = req.swagger.params.le_code;
    const param_dt_code = req.swagger.params.dt_code;

    const iso8601format = "YYYY-MM-DD";

    let fmcg_id = _.isUndefined(param_fmcg_id.value) ? null : param_fmcg_id.value;
    let amount = _.isUndefined(param_amount.value) ? null : param_amount.value;
    let tenor = _.isUndefined(param_tenor.value) ? null : param_tenor.value;
    let invoice_number = _.isUndefined(param_invoice_number.value) ? null : param_invoice_number.value;
    let le_code = _.isUndefined(param_le_code.value) ? null : param_le_code.value;
    let dt_code = _.isUndefined(param_dt_code.value) ? null : param_dt_code.value;

    let data_unique_code = '';
    let vacc_fit = '21000000003';
    let vacc_topan = '2010001136';
    let fmcg_top = 1;
    let out = 0;

    try {
        Logs.create({action: 'repayment', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_repayment, is_allowed_access_token_static: false});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
        if (result_fmcg == null) {
            throw new CustomError(400, 'Gagal menampilkan data invoice, data fmcg tidak tersedia');
        }
        let cek = await ApiTop.cek_saldo({le_code: le_code});
        if (cek.code == 502) {
            throw new CustomError(400, 'Server Sedang Maintenance');
        }
        let result_fmcg_top = await service_fmcg.getOneFmcgWithFilter({id: fmcg_top});
        if (result_fmcg_top == null) {
            throw new CustomError(400, 'Gagal menampilkan data invoice, data fmcg tidak tersedia');
        }

        let actual_pin = await service_user.getHashedPassword(auth_user.dataValues.salt, pin);
        let expected_pin = auth_user.dataValues.pin;
        if (expected_pin != actual_pin) {
            throw new CustomError(400, 'PIN salah');
        }

        let today = new Date()
        let current_time = today.getHours()
        let time_setting = moment(today.setHours(current_time)).format('HH:mm:ss')

//        if (time_setting > '21:00:00') {
//            throw new CustomError(400, 'Waktu transaksi telah ditutup!');
//        }

        let vacc_toko = auth_user.dataValues.valdo_account
        let nama_toko = auth_user.dataValues.fullname

        let invoice_args = {};
        invoice_args.url = result_fmcg_top.dataValues.url;
        invoice_args.fmcg_token = result_fmcg_top.dataValues.fmcg_token;
        invoice_args.fmcg_name = result_fmcg_top.dataValues.name;
        invoice_args.le_code = le_code;
        invoice_args.invoice_id = invoice_number;
        invoice_args.dt_code = dt_code;
        invoice_args.tenor = tenor;
        invoice_args.user_id = auth_user.dataValues.user_id

        let args = {}
        args.url = result_fmcg.dataValues.url;
        args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        args.fmcg_name = result_fmcg.dataValues.name;
        args.invoice_id = invoice_number;
        args.dt_code = dt_code

        let api_response = await MoneyAccess.inquiry({mobnum: auth_user.dataValues.valdo_account, vaccount: auth_user.dataValues.valdo_account, usertype: auth_user.dataValues.type});

        out = api_response;


        if (out.balance < amount) {
            throw new CustomError(400, 'Saldo tidak cukup');
        }
        let input_to_fit = {
            le_code: le_code,
            dt_code: dt_code,
            invoice_number: invoice_number,
            url: result_fmcg_top.dataValues.url,
            fmcg_token: result_fmcg_top.dataValues.fmcg_token,
            fmcg_name: result_fmcg_top.dataValues.name
        }

        let get_top_invoice = await ApiTop.get_one_invoice(invoice_args)
        let get_uli_invoice = await ApiFmcg.get_one_invoice(args)
        let get_all_tenor = await ApiTop.get_invoice_all_tenor(input_to_fit);

        let unpaid_invoices = await get_all_tenor["data"].filter(function(item){
            return item.invoice_payment_status == "UNPAID"
        })
        for(let i = 0; i < unpaid_invoices.length; i++){
            
            if(Number(tenor) > Number(unpaid_invoices[i]["cash_memo_doc_no"].split("-")[1])){
                throw new CustomError(400,'Invoice cicilan ke - '+unpaid_invoices[i]["cash_memo_doc_no"].split("-")[1]+' Anda belum dibayar')
                //res.status(400).json({code:400,message:'Invoice cicilan ke - '+unpaid_invoices[i]["cash_memo_doc_no"].split("-")[1]+' Anda belum dibayar'}) 
            }
            else if(Number(tenor) === Number(unpaid_invoices[i]["cash_memo_doc_no"].split("-")[1])){
           
        if (Object.keys(get_top_invoice).length !== 1) {


            let payment = {
                trans_type: "transfer",
                status: "success",
                vacc_from: vacc_toko,
                vacc_to: [
                    {vacc_number: vacc_fit, amount: parseInt(amount), desc: `Pembayaran invoice ${invoice_number}-${parseInt(tenor)} (${le_code}) Rp. ${helper.IDRformat(amount)} dari ${nama_toko} (${vacc_toko}) ke Limit Pandai (${vacc_fit})`}
                ],
                pin: pin
            }

            request({
                headers: {
                    'apigatekey': apigatekey,
                    'Content-Type': 'application/json'
                },
                url: "http://" + url_trf + "/api/v1/multi_vacc/trf",
                method: "POST",
                json: true,
                body: payment
            }, async function (error, result, body) {
                let tofit_money_access = body.result
                let tofit_money_access_response = {
                    code: body.status.code,
                    desc: body.result.desc
                }
                Logs.create({action: `api money access TOP`, description: 'response' + tofit_money_access});
                if (body.status.code === 200) {

                    let args = {
                        "dt_code": dt_code,
                        "le_code": le_code,
                        "invoice_number": invoice_number,
                        "amount": amount
                    }
                    request({
                        headers: {
                            'Content-Type': 'application/json',
                            'api_key': top_api_key
                        },
                        url: "https://" + top_api + "/loan/repayment-tokopandai",
                        method: "POST",
                        json: true,
                        body: args
                    }, async function (error, response, body) {
                        let fit_response = {
                            code: body.status,
                            message: body.message
                        }
                        Logs.create({action: 'TOP response', description: 'fmcg_top:' + fmcg_top + 'response:' + fit_response})
                        if (body.status === 'success') {
                            let pd_on_inv = {};
                                pd_on_inv.token = token;
                                pd_on_inv.le_code = le_code;
                            let invoicess = await ApiPd.get_one_pd_inv(pd_on_inv);
                            let invoice_args = {};
                                invoice_args.url = result_fmcg_top.dataValues.url;
                                invoice_args.fmcg_token = result_fmcg_top.dataValues.fmcg_token;
                                invoice_args.fmcg_name = result_fmcg_top.dataValues.name;
                                invoice_args.le_code = le_code;
                                invoice_args.invoice_id = invoice_number;
                                invoice_args.dt_code = dt_code;
                                invoice_args.tenor = tenor;
                                invoice_args.user_id = auth_user.dataValues.user_id
                            let get_one_inv = await ApiTop.get_one_invoice(invoice_args);
                            let input_to_transactions = {};

                            input_to_transactions.fmcg_id = 1;
                            input_to_transactions.dt_code = dt_code;
                            input_to_transactions.invoice_id = invoice_number+'-'+tenor;
                            input_to_transactions.amount = amount;
                            input_to_transactions.invoice_code = dt_code+invoice_number+'-'+tenor;
                            input_to_transactions.valdo_tx_id = "TR"+invoice_number+moment().format('YYYYMMDDHHmmss');
                            input_to_transactions.distributor_id = 0;
                            input_to_transactions.retailer_id = 0;  
                            input_to_transactions.from_user_id = 0;
                            input_to_transactions.to_user_id =0;

                            let insert_manual_transaction = await ApiPd.multifmcg_transactions(input_to_transactions)

                            let out = {
                                response: fit_response,
                                to_fit_money_access_desc: tofit_money_access_response
                            }
                            res.status(200).json({code: 200, body: out})
                        }
                    });
                } else
                {
                    res.json({code: 400, body: "saldo tidak cukup"});
                }
            })


        } else {
                    throw new CustomError(400, 'Invoice tidak sesuai!')
                }
            }
        }

    } catch (err) {
        if (err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data invoice, ' + err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}


async function apply_loan(req, res) {
    const token = req.swagger.params.token.value;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_le_code = req.swagger.params.le_code;
    const param_dt_code = req.swagger.params.dt_code;
    const param_amount = req.swagger.params.amount;
    const param_invoice_id = req.swagger.params.invoice_id;
    const param_tenor = req.swagger.params.tenor;
    const param_principal_code = req.swagger.params.principal_code;
    const param_total_potongan = req.swagger.params.total_potongan;
    const param_pin = req.swagger.params.pin;


    const iso8601format = "YYYY-MM-DD";

    let pin_user = _.isUndefined(param_pin.value) ? null : param_pin.value;
    let fmcg_id = _.isUndefined(param_fmcg_id.value) ? null : param_fmcg_id.value;
    let dt_code = _.isUndefined(param_dt_code.value) ? null : param_dt_code.value;
    let le_code = _.isUndefined(param_le_code.value) ? null : param_le_code.value;
    let amount = _.isUndefined(param_amount.value) ? null : param_amount.value;
    let invoice_number = _.isUndefined(param_invoice_id.value) ? null : param_invoice_id.value;
    let tenor = _.isUndefined(param_tenor.value) ? null : param_tenor.value;
    let principal_code = _.isUndefined(param_principal_code.value) ? null : param_principal_code.value;
    let total_potongan = _.isUndefined(param_total_potongan.value) ? null : param_total_potongan.value;
    let discounts = 0;
    let data_unique_code = '';
    let filter_money = fmcg_id == 2?200000:100000; 


    try {
        Logs.create({action: 'apply_loan', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_get_one_invoice, is_allowed_access_token_static: false});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});

        if (result_fmcg == null) {
            throw new CustomError(400, 'Gagal menampilkan data apply loan, data fmcg tidak tersedia');
        }

        let result_fit = await service_fmcg.getOneFmcgWithFilter({id: 1});

        if (result_fit == null) {
            throw new CustomError(400, 'Gagal menampilkan data apply loan, data fmcg tidak tersedia');
        }
        if(invoice_number.slice(-2) == "_R") {
            throw new CustomError(400, 'Gagal apply loan, Invoice Rokok Tidak bisa memakai Limit');
        }

        let user_salt = auth_user.dataValues.salt;

        let newpin = await service_user.getHashedPassword(user_salt, pin_user.toString());
        if (auth_user.dataValues.pin != newpin) {
            throw new CustomError(400, 'Pin Salah');
        }

        let invoice_args = {};
        invoice_args.url = result_fmcg.dataValues.url;
        invoice_args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        invoice_args.fmcg_name = result_fmcg.dataValues.name;
        invoice_args.invoice_id = invoice_number;
        invoice_args.dt_code = dt_code;
        
        let api_response_fmcg = await ApiFmcg.get_one_invoice(invoice_args);
        let api_transact = await ApiPd.checktransactions({dt_code:dt_code,invoice_id:invoice_number,fmcg_id:fmcg_id});
        if (api_transact.totalTransaksi != 0){
          throw new CustomError(400, 'Pengajuan sebagian ditolak');
        }

        if (amount< filter_money) {
            throw new CustomError(400, 'Invoice Minimal Rp. '+filter_money+'');
        }


        let api_unpaid = await ApiPd.check_fintech_unpaid({le_code:le_code});

        if (api_unpaid.totalInvoice !== 0){
          throw new CustomError(400, 'Maaf pengajuan limit tidak dapat diproses. Selesaikan pembayaran tagihan Anda');
        }

        let args = {
            "dt_code": dt_code,
            "le_code": le_code,
            "invoice_number": invoice_number,
            "amount": amount,
            "tenor": tenor,
            "principle_code": principal_code,
            "discount": parseInt(total_potongan)
        };
        request({
            headers: {
                'Content-Type': 'application/json',
                'api_key': top_api_key
            },
            url: "https://" + top_api + "/loan/apply-tokopandai",
            method: "POST",
            json: true,
            body: args
        }, function (error, response, body) {
            if (body.status === 'success') {
                let invoice_update = {};
                invoice_update.le_code = le_code;
                invoice_update.invoice_id = invoice_number;
                invoice_update.amount = amount;
                invoice_update.fmcg_id = fmcg_id;
                invoice_update.paid_by = "Limit Pandai"
                   
                ApiPd.update_payment_fmcg(invoice_update);

                let apply_invoice = {
                url: result_fit.dataValues.url,
                fmcg_token : result_fit.dataValues.fmcg_token,
                fmcg_name : result_fit.dataValues.name,
                user_id:auth_user.dataValues.user_id,
                le_code:le_code,
                dt_code:dt_code,
                amount:amount,
                total_amount_invoice:amount,
                invoice_id:invoice_number,
                tenor:tenor,
                loan_type:"Credit",
                loan_by:"Pinjam Modal",
                principal_code:fmcg_id,
                total_potongan:total_potongan
              }
               ApiTop.insert_loan_history(apply_invoice)
               res.status(200).json(body);
                }
              else{
                  res.status(400).json(body)
              }
        });
      }

     catch (err) {
        if (err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan update invoice, ' + err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}


async function payment_distributor(req, res) {

    const token = req.swagger.params.token.value;
    const param_le_code = req.swagger.params.le_code;
    const param_dt_code = req.swagger.params.dt_code;
    const param_invoice_no = req.swagger.params.invoice_no;
    const param_principal_code = req.swagger.params.principal_code;

    const iso8601format = "YYYY-MM-DD";

    let dt_code = _.isUndefined(param_dt_code.value) ? 0 : param_dt_code.value;
    let le_code = _.isUndefined(param_le_code.value) ? 0 : param_le_code.value;
    let invoice_no = _.isUndefined(param_invoice_no.value) ? 0 : param_invoice_no.value;
    let principal_code = _.isUndefined(param_principal_code.value) ? 0 : param_principal_code.value;

    let vacc_fit = '21000000004'
    let data_unique_code = '';
    let fmcg_id = 13
    let fmcg_top = 1

    try {
        Logs.create({action: 'payment_distributor', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
        if (result_fmcg.dataValues == null) {
            throw new CustomError(400, 'Gagal menampilkan data payment_distributor, data fmcg tidak tersedia');
        }
        let result_fmcg_top = await service_fmcg.getOneFmcgWithFilter({id: fmcg_top});
        if (result_fmcg_top.dataValues == null) {
            throw new CustomError(400, 'Gagal menampilkan data payment_distributor, data fmcg tidak tersedia');
        }

        let invoice = {
            url: result_fmcg_top.dataValues.url,
            fmcg_token: result_fmcg_top.dataValues.fmcg_token,
            fmcg_name: result_fmcg_top.dataValues.name,
            le_code: le_code,
            dt_code: dt_code,
            invoice_id: invoice_no
        }

        let loan_history = await ApiTop.get_loan_history(invoice);
        let invoice_args = {};
        invoice_args.url = result_fmcg_top.dataValues.url;
        invoice_args.fmcg_name = result_fmcg_top.dataValues.name;
        invoice_args.fmcg_token = result_fmcg_top.dataValues.fmcg_token;
        invoice_args.fmcg_id = fmcg_id;
        invoice_args.dt_code = dt_code;
        invoice_args.le_code = le_code;
        invoice_args.invoice_no = invoice_no;
        invoice_args.tenor = loan_history.tenor;
        invoice_args.amount_invoice = parseInt(loan_history.amount);
        invoice_args.total_amount = parseInt(loan_history.total_amount_invoice);
        invoice_args.principal_code = fmcg_id;

        let api_response = await ApiTop.payment_distributor(invoice_args);

        res.status(200).json({code: 200, status: 'success', body: api_response.body});
    } catch (err) {
        if (err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data invoice, ' + err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function updateStatusPaymentDistributor(req, res) {
    const token = req.swagger.params.token.value;
    const param_invoice_no = req.swagger.params.invoice_no;
    const param_payment_status = req.swagger.params.payment_status;

    let invoice_no = _.isUndefined(param_invoice_no.value) ? null : param_invoice_no.value;
    let payment_status = _.isUndefined(param_payment_status.value) ? null : param_payment_status.value;
    try {
        let check = await service_top.findOne({invoice_no: invoice_no});
        if (check == 1) {
            let args = {
                invoice_no: invoice_no,
                payment_status: payment_status
            };
            service_top.update_payment_status_distributor(args);
            res.status(200).json({code: 200, body: 'payment success'});
        } else {
            throw new CustomError(400, 'Update status gagal');
        }
    } catch (err) {
        if (err.code === undefined) {
            err.code = 400;
            err.message = 'Insert Gagal :' + err;
        }
        err.message = (err.message === undefined) ? err : err.message;
        res.status(400).json({code: 400, message: err.message});
    }
}

async function repayment_history(req, res) {
    const token = req.swagger.params.token.value;

    let fmcg_top = 1

    try {
        Logs.create({action: 'repayment_history', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});

        let result_fmcg_top = await service_fmcg.getOneFmcgWithFilter({id: fmcg_top});

        let invoice_args = {};
        invoice_args.url = result_fmcg_top.dataValues.url;
        invoice_args.fmcg_name = result_fmcg_top.dataValues.name;
        invoice_args.fmcg_token = result_fmcg_top.dataValues.fmcg_token;
        if (result_fmcg_top.dataValues === null) {
            throw new CustomError(400, 'Gagal memproses data invoice, data fmcg tidak tersedia');
        }

        let api_history_repayment = await ApiTop.repayment_history(invoice_args)

        res.status(200).json({code: 200, body: api_history_repayment})
    } catch (err) {
        if (err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data invoice, ' + err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function payment_to_distributor(req, res, token) {
    let fmcg_top = 1;
    let fmcg_id = 13;
    let vacc_fit = '21000000004';
    try {
        let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});

        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
        if (result_fmcg.dataValues == null) {
            throw new CustomError(400, 'Gagal menampilkan data payment_distributor, data fmcg tidak tersedia');
        }

        let result_fmcg_top = await service_fmcg.getOneFmcgWithFilter({id: fmcg_top});
        if (result_fmcg_top.dataValues == null) {
            throw new CustomError(400, 'Gagal menampilkan data payment_distributor, data fmcg tidak tersedia');
        }

        let args = {
            url: result_fmcg_top.dataValues.url,
            fmcg_token: result_fmcg_top.dataValues.fmcg_token,
            fmcg_name: result_fmcg_top.dataValues.name
        }

        let unpaid_invoices = await ApiTop.get_unpaid_invoices(args)

        if (unpaid_invoices.length !== 0) {
            for (let i = 0; i < unpaid_invoices.length; i++) {

                let dist = await service_pelunasan_manual.get_dt_unicode({dt_code: unpaid_invoices[i].dt_code})
                let toko = await service_pelunasan_manual.get_le_unicode({le_code: unpaid_invoices[i].le_code})
                let toko_data = await service_pelunasan_manual.get_le_aditional({le_code: unpaid_invoices[i].le_code})
                let nama_toko = '';
                if (toko_data.length === 0)
                {
                    nama_toko = 'Tidak ada nama';
                } else {
                    nama_toko = toko_data[0].value;
                }

                let payment = {
                    trans_type: "transfer",
                    status: "success",
                    vacc_from: vacc_fit,
                    vacc_to: [
                        {vacc_number: dist[0].valdo_account, amount: parseInt(unpaid_invoices[i].amount), desc: `Pembayaran invoice ${unpaid_invoices[i].invoice_no} (${nama_toko})  Rp. ${helper.IDRformat(unpaid_invoices[i].amount)} dari Limit Pandai (${vacc_fit}) ke ${dist[0].fullname} (${dist[0].valdo_account})`}
                    ],
                    pin: '123456'
                }
                request({
                    headers: {
                        'apigatekey': apigatekey,
                        'Content-Type': 'application/json'
                    },
                    url: 'http://' + url_trf + '/api/v1/multi_vacc/trf',
                    method: "POST",
                    json: true,
                    body: payment
                }, async function (error, result, body) {

                    if (body.status.code === 200) {
                        let update_data = {
                            invoice_no: unpaid_invoices[i].invoice_no,
                            payment_status: 1,
                            fmcg_token: result_fmcg_top.dataValues.fmcg_token,
                            fmcg_name: result_fmcg_top.dataValues.name,
                            url: result_fmcg_top.dataValues.url
                        }
                        let update_payment_status = await ApiTop.update_payment_status_distributor(update_data)

                        let input_to_transactions = {};
                        input_to_transactions.token = token;
                        input_to_transactions.url = result_fmcg.dataValues.url;
                        input_to_transactions.fmcg_token = result_fmcg.dataValues.fmcg_token;
                        input_to_transactions.fmcg_name = result_fmcg.dataValues.name;
                        input_to_transactions.le_code = unpaid_invoices[i].le_code;
                        input_to_transactions.dt_code = unpaid_invoices[i].dt_code;
                        input_to_transactions.invoice_id = unpaid_invoices[i].invoice_no;
                        input_to_transactions.invoice_code = unpaid_invoices[i].dt_code+unpaid_invoices[i].invoice_no;
                        input_to_transactions.amount = unpaid_invoices[i].amount;
                        input_to_transactions.valdo_tx_id = "TR"+moment().format('YYYYMMDDHHmmss');
                        input_to_transactions.transact_date = moment().format('YYYY-MM-DD HH:mm:ss');
                        input_to_transactions.currency_code = "IDR";
                        input_to_transactions.type = 0;
                        input_to_transactions.distributor_id = dist[0].id;
                        input_to_transactions.retailer_id = toko[0].id;  
                        input_to_transactions.from_user_id = 0;
                        input_to_transactions.to_user_id = dist[0].user_id;
                        let input_to_transactions_res = await ApiTop.insert_manual_transaction(input_to_transactions); 
                        
                   }
                })

                Logs.create({action: 'repayment_to_distributor', description: 'token:' + token + ',le_code:' + unpaid_invoices[i].le_code + ',dt_code:' + unpaid_invoices[i].le_code + ',invoice_no:' + unpaid_invoices[i].invoice_no});
            }
        } else {
            throw new CustomError(400, 'Unpaid invoice is not found!')
        }

        let out = {
            total_unpaid_invoices: unpaid_invoices.length,
            message: 'Success'
        }
    } catch (err) {
        if (err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data invoice, ' + err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function repayment_to_distributor(req, res) {
    const token = req.swagger.params.token.value;

    let fmcg_top = 1;
    let fmcg_id = 13;
    let vacc_fit = '21000000004';

    try {
        let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});

        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
        if (result_fmcg.dataValues == null) {
            throw new CustomError(400, 'Gagal menampilkan data payment_distributor, data fmcg tidak tersedia');
        }

        let result_fmcg_top = await service_fmcg.getOneFmcgWithFilter({id: fmcg_top});
        if (result_fmcg_top.dataValues == null) {
            throw new CustomError(400, 'Gagal menampilkan data payment_distributor, data fmcg tidak tersedia');
        }

        let args = {
            url: result_fmcg_top.dataValues.url,
            fmcg_token: result_fmcg_top.dataValues.fmcg_token,
            fmcg_name: result_fmcg_top.dataValues.name
        }
        

        let unpaid_invoices = await ApiTop.get_unpaid_invoices(args)
        if (unpaid_invoices.length !== 0) {
            for (let i = 0; i < unpaid_invoices.length; i++) {

                let dist = await service_pelunasan_manual.get_dt_unicode({dt_code: unpaid_invoices[i].dt_code})
                let toko = await service_pelunasan_manual.get_le_unicode({le_code: unpaid_invoices[i].le_code})
                let toko_data = await service_pelunasan_manual.get_le_aditional({le_code: unpaid_invoices[i].le_code})
                let nama_toko = '';
                if (toko_data.length === 0)
                {
                    nama_toko = 'Tidak ada nama';
                } else {
                    nama_toko = toko_data[0].value;
                }
                
                let payment = {
                    trans_type: "transfer",
                    status: "success",
                    vacc_from: vacc_fit,
                    vacc_to: [
                        {vacc_number: dist[0].valdo_account, amount: parseInt(unpaid_invoices[i].amount), desc: `Pembayaran invoice ${unpaid_invoices[i].invoice_no} (${nama_toko})  Rp. ${helper.IDRformat(unpaid_invoices[i].amount)} dari Limit Pandai (${vacc_fit}) ke ${dist[0].fullname} (${dist[0].valdo_account})`}
                    ],
                    pin: '123456'
                }
                 request({
                    headers: {
                        'apigatekey': apigatekey,
                        'Content-Type': 'application/json'
                    },
                    url: 'http://'+url_trf+'/api/v1/multi_vacc/trf',
                    method: "POST",
                    json: true,   
                    body: payment
                }, async function (error, result, body){
                        if(body.status.code === 200){
                            let update_data = {
                                invoice_code:unpaid_invoices[i].invoice_code,
                                payment_status: 1,
                                fmcg_token: result_fmcg_top.dataValues.fmcg_token,
                                fmcg_name: result_fmcg_top.dataValues.name,
                                url: result_fmcg_top.dataValues.url
                            }
                            let update_payment_status = await ApiTop.update_payment_status_distributor(update_data)

                           let input_to_transactions = {};
                            input_to_transactions.token = token;
                            input_to_transactions.url = result_fmcg.dataValues.url;
                            input_to_transactions.fmcg_token = result_fmcg.dataValues.fmcg_token;
                            input_to_transactions.fmcg_name = result_fmcg.dataValues.name;
                            input_to_transactions.le_code = unpaid_invoices[i].le_code;
                            input_to_transactions.dt_code = unpaid_invoices[i].dt_code;
                            input_to_transactions.invoice_id = unpaid_invoices[i].invoice_no;
                            input_to_transactions.invoice_code = unpaid_invoices[i].dt_code+unpaid_invoices[i].invoice_no;
                            input_to_transactions.amount = unpaid_invoices[i].amount;
                            input_to_transactions.valdo_tx_id = "TR"+moment().format('YYYYMMDDHHmmss');
                            input_to_transactions.transact_date = moment().format('YYYY-MM-DD HH:mm:ss');
                            input_to_transactions.currency_code = "IDR";
                            input_to_transactions.type = 0;
                            input_to_transactions.distributor_id = dist[0].id;
                            input_to_transactions.retailer_id = toko[0].id;  
                            input_to_transactions.from_user_id = 0;
                            input_to_transactions.to_user_id = dist[0].user_id;
                            let input_to_transactions_res = await ApiTop.insert_manual_transaction(input_to_transactions); 
                        }
                    })

                Logs.create({action: 'repayment_to_distributor', description: 'token:' + token + ',le_code:' + unpaid_invoices[i].le_code + ',dt_code:' + unpaid_invoices[i].le_code + ',invoice_no:' + unpaid_invoices[i].invoice_no});
            }
        } else {
            throw new CustomError(400, 'Unpaid invoice is not found!')
        }

        let out = {
            total_unpaid_invoices: unpaid_invoices.length,
            message: 'Success'
        }
        res.status(200).json({code: 200, body: out})
    } catch (err) {
        if (err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data invoice, ' + err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

const sleep = (milliseconds) => {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
}

async function repayment_manual(req, res) {
    const token = req.swagger.params.token.value;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_tenor = req.swagger.params.tenor;
    const param_amount = req.swagger.params.amount;
    const param_invoice_number = req.swagger.params.invoice_number;
    const param_le_code = req.swagger.params.le_code;
    const param_dt_code = req.swagger.params.dt_code;

    const iso8601format = "YYYY-MM-DD";

    let fmcg_id = _.isUndefined(param_fmcg_id.value) ? null : param_fmcg_id.value;
    let amount = _.isUndefined(param_amount.value) ? null : param_amount.value;
    let tenor = _.isUndefined(param_tenor.value) ? null : param_tenor.value;
    let invoice_number = _.isUndefined(param_invoice_number.value) ? null : param_invoice_number.value;
    let le_code = _.isUndefined(param_le_code.value) ? null : param_le_code.value;
    let dt_code = _.isUndefined(param_dt_code.value) ? null : param_dt_code.value;

    let data_unique_code = '';
    let vacc_fit = '21000000003';
    let vacc_topan = '2010001136';

    let fmcg_top = 1

    try {
        Logs.create({action: 'repayment_manual', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
        if (result_fmcg == null) {
            throw new CustomError(400, 'Gagal menampilkan data invoice, data fmcg tidak tersedia');
        }

        let result_fmcg_top = await service_fmcg.getOneFmcgWithFilter({id: fmcg_top});
        if (result_fmcg_top == null) {
            throw new CustomError(400, 'Gagal menampilkan data invoice, data fmcg tidak tersedia');
        }

        let toko = await service_pelunasan_manual.get_le_unicode({le_code: le_code})
        let vacc_toko = toko[0].valdo_account
        let nama_toko = toko[0].fullname

        let api_response = await MoneyAccess.inquiry({mobnum: auth_user.dataValues.valdo_account, vaccount: auth_user.dataValues.valdo_account, usertype: auth_user.dataValues.type});

        out = api_response;


        if (out.balance < amount) {
            throw new CustomError(400, 'Saldo tidak cukup');
        }

        let invoice_args = {};
        invoice_args.url = result_fmcg_top.dataValues.url;
        invoice_args.fmcg_token = result_fmcg_top.dataValues.fmcg_token;
        invoice_args.fmcg_name = result_fmcg_top.dataValues.name;
        invoice_args.le_code = le_code;
        invoice_args.invoice_id = invoice_number;
        invoice_args.dt_code = dt_code;
        invoice_args.tenor = tenor;
        invoice_args.user_id = toko[0].user_id

        let args = {}
        args.url = result_fmcg.dataValues.url;
        args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        args.fmcg_name = result_fmcg.dataValues.name;
        args.invoice_id = invoice_number;
        args.dt_code = dt_code

        let input_to_fit = {
            le_code: le_code,
            dt_code: dt_code,
            invoice_number: invoice_number,
            url: result_fmcg_top.dataValues.url,
            fmcg_token: result_fmcg_top.dataValues.fmcg_token,
            fmcg_name: result_fmcg_top.dataValues.name
        }

        let get_top_invoice = await ApiTop.get_one_invoice(invoice_args)
        let get_uli_invoice = await ApiFmcg.get_one_invoice(args)
        let get_all_tenor = await ApiTop.get_invoice_all_tenor(input_to_fit);
        if (get_top_invoice.length === 1 && (parseInt(get_uli_invoice.total_amount) === parseInt(get_all_tenor.total_inv))) {


            let amount_to_topan = 0.5 / 100 * amount;

            let payment = {
                'vaccount_from': vacc_toko,
                'vaccount_to': vacc_fit,
                'amount': amount,
                'desc': `Pembayaran invoice ${invoice_number}-${tenor} Rp. ${helper.IDRformat(amount)} dari ${nama_toko} (${vacc_toko}) ke Limit Pandai (${vacc_fit})`,
                'transid': "TR" + moment().format('YYYYMMDDHHmmss')
            }

            request({
                headers: {
                    'apigatekey': apigatekey,
                },
                url: "http://" + url_trf + "/api/v1/multi_vacc/trf", //transfer
                method: "POST",
                json: true,
                formData: payment
            }, async function (error, result, body) {
                let tofit_money_access = body.result
                let tofit_money_access_response = {
                    code: body.status.code,
                    message: body.status.desc,
                    desc: body.result.desc
                }
                Logs.create({action: `api money access TOP`, description: 'response' + tofit_money_access});
                if (body.status.code === 200) {
                    let args = {
                        "dt_code": dt_code,
                        "le_code": le_code,
                        "invoice_number": invoice_number,
                        "amount": amount
                    }
                    request({
                        headers: {
                            'Content-Type': 'application/json',
                            'api_key': top_api_key
                        },
                        url: "https://" + top_api + "/loan/repayment-tokopandai",
                        method: "POST",
                        json: true,
                        body: args
                    }, async function (error, response, body) {
                        let fit_response = {
                            code: body.status,
                            message: body.message
                        }
                        Logs.create({action: 'TOP response', description: 'fmcg_top:' + fmcg_top + 'response:' + fit_response})
                        if (body.status === 'success') {

                            let get_one_inv = await ApiTop.get_one_invoice(invoice_args);
                            let distributor = await service_pelunasan_manual.get_dt_unicode({dt_code: dt_code})
                            let retailer = await service_pelunasan_manual.get_le_unicode({le_code: le_code})

                            let input_to_transactions = {};
                            input_to_transactions.token = token;
                            input_to_transactions.url = result_fmcg.dataValues.url;
                            input_to_transactions.fmcg_token = result_fmcg.dataValues.fmcg_token;
                            input_to_transactions.fmcg_name = result_fmcg.dataValues.name;
                            input_to_transactions.le_code = le_code;
                            input_to_transactions.dt_code = dt_code;
                            input_to_transactions.invoice_id = invoice_number;
                            input_to_transactions.invoice_code = dt_code + invoice_number;
                            input_to_transactions.amount = amount;
                            input_to_transactions.valdo_tx_id = "TR" + moment().format('YYYYMMDDHHmmss');
                            input_to_transactions.transact_date = moment().format('YYYY-MM-DD HH:mm:ss');
                            input_to_transactions.currency_code = "IDR";
                            input_to_transactions.type = 0;
                            input_to_transactions.distributor_id = distributor[0].id;
                            input_to_transactions.retailer_id = retailer[0].id;
                            input_to_transactions.from_user_id = toko[0].user_id;
                            input_to_transactions.to_user_id = 1;

                            let input_to_transactions_res = await ApiTop.insert_manual_transaction(input_to_transactions);
                            let out = {
                                response: fit_response,
                                to_fit_money_access_desc: tofit_money_access_response,
                                transc_history: input_to_transactions_res
                            }

                            res.status(200).json({code: 200, body: out})
                        }
                    });
                }
            })
        } else {
            res.status(400).json({code: 400, body: "saldo tidak cukup"})
        }
    } catch (err) {
        if (err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data invoice, ' + err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}



async function manual_repayment_invoice(req,res){
    const token = req.swagger.params.token.value;
    const param_invoice_number = req.swagger.params.invoice_number;
    const param_le_code = req.swagger.params.le_code;
    const param_dt_code = req.swagger.params.dt_code;
    const param_tenor = req.swagger.params.tenor;
    const param_amount = req.swagger.params.amount;

    const iso8601format = "YYYY-MM-DD";
    
    let invoice_number = _.isUndefined(param_invoice_number.value) ? null : param_invoice_number.value;
    let le_code = _.isUndefined(param_le_code.value) ? null : param_le_code.value;
    let dt_code = _.isUndefined(param_dt_code.value) ? null : param_dt_code.value;
    let tenor = _.isUndefined(param_tenor.value) ? 0 : param_tenor.value;
    let amount = _.isUndefined(param_amount.value) ? 0 : param_amount.value;

    let data_unique_code = '';
    let vacc_fit = '21000000003';
    let vacc_topan = '2010001136';
    let fmcg_id = 13;
    let fmcg_top = 1
    let out = 0;
   
    try {
        Logs.create({action: 'repayment_manual', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});

         let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
        if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal menampilkan data invoice, data fmcg tidak tersedia');
        }

        let result_fmcg_top = await service_fmcg.getOneFmcgWithFilter({id: fmcg_top});
        if(result_fmcg_top == null) {
            throw new CustomError(400, 'Gagal menampilkan data invoice, data fmcg tidak tersedia');
        }
        
        let cekfit = {}
        cekfit.le_code = le_code;
        cekfit.dt_code = dt_code;
        cekfit.invoice_number = invoice_number;
        cekfit.tenor = tenor;
        let cek_fit = await ApiTop.get_inv_fit(cekfit)
//
//        if(tenor===2){
//            let argss = {}
//            argss.le_code = le_code;
//            argss.dt_code = dt_code;
//            argss.invoice_number = invoice_number;
//            argss.tenor = 1;
//            let cek_fit = await ApiTop.get_inv_fit(argss)
//            if(cek_fit.invoice_payment_status == "UNPAID") {
//               throw new CustomError(400, 'INVOICE KE 1 BELUM LUNAS');
//            }
//        }
//        if(tenor===3){
//            let argsss = {}
//            argsss.le_code = le_code;
//            argsss.dt_code = dt_code;
//            argsss.invoice_number = invoice_number;
//            argsss.tenor = 2;
//            let cek_fit = await ApiTop.get_inv_fit(argsss)
//            if(cek_fit.invoice_payment_status == "UNPAID") {
//               throw new CustomError(400, 'INVOICE KE 2 BELUM LUNAS');
//            }
//        }
//        if(tenor===4){
//            let argssss = {}
//            argssss.le_code = le_code;
//            argssss.dt_code = dt_code;
//            argssss.invoice_number = invoice_number;
//            argssss.tenor = 3;
//            let cek_fit = await ApiTop.get_inv_fit(argssss)
//            if(cek_fit.invoice_payment_status == "UNPAID") {
//               throw new CustomError(400, 'INVOICE KE 3 BELUM LUNAS');
//            }
//        }


       if(cek_fit==="undefined"){
            throw new CustomError(400, 'INVOICE YANG DIMASUKAN TIDAK ADA');
       }
       if(cek_fit.total_yg_harus_dibayar == 0) {
           throw new CustomError(400, 'INVOICE SUDAH LUNAS');
       }
       if(cek_fit.invoice_payment_status == "PAID") {
           throw new CustomError(400, 'INVOICE SUDAH LUNAS');
       }
        
        
        let toko = await service_pelunasan_manual.get_le_unicode({le_code:le_code})
        let vacc_toko = toko[0].valdo_account
        let nama_toko = toko[0].fullname

        let api_response = await MoneyAccess.inquiry({mobnum: toko[0].valdo_account, vaccount: toko[0].valdo_account, usertype: toko[0].type});
        out = api_response;
        let payment = {
            'vaccount_from' : vacc_toko,
            'vaccount_to' : vacc_fit,
            'amount' : parseInt(amount),
            'desc' : `Pembayaran invoice ${invoice_number}-${tenor} (${le_code}) Rp. ${helper.IDRformat(amount)} dari ${nama_toko} (${vacc_toko}) ke Limit Pandai (${vacc_fit})`,
            'transid' : "TR"+moment().format('YYYYMMDDHHmmss')
        }
            
            request({
                headers: {
                    'apigatekey': apigatekey,
                },
                url: "http://"+url_trf+"/api/v1/admin/trf", //transfer
                method: "POST",
                json: true,   
                formData: payment
            }, async function (error, result, body){
                let tofit_money_access = body.result
                let tofit_money_access_response = {
                    code: body.status.code,
                    message: body.status.desc,
                    desc: body.result.desc
                }
                Logs.create({action: `api money access TOP`, description: 'response'+tofit_money_access});
                if(body.status.code === 200){
                    let args ={
                        "dt_code": dt_code,
                        "le_code": le_code,
                        "invoice_number": invoice_number,
                        "amount": parseInt(amount)
                        }
                        request({
                            headers: {
                            'Content-Type': 'application/json',
                            'api_key' : top_api_key
                            },
                            url: "https://"+top_api+"/loan/repayment-tokopandai",
                            method: "POST",
                            json: true,
                            body: args
                            }, async function (error, response, body){
                                let fit_response = {
                                    code: body.status,
                                    message: body.message
                                }
                                Logs.create({action:'TOP response', description: 'fmcg_top:'+fmcg_top+'response:'+fit_response})
                                if(body.status === 'success'){
                                 let invoice_args = {};
                                invoice_args.url = result_fmcg_top.dataValues.url;
                                invoice_args.fmcg_token = result_fmcg_top.dataValues.fmcg_token;
                                invoice_args.fmcg_name = result_fmcg_top.dataValues.name;
                                invoice_args.le_code = le_code;
                                invoice_args.invoice_id = invoice_number;
                                invoice_args.dt_code = dt_code;
                                invoice_args.tenor = tenor;
                                invoice_args.user_id = toko[0].user_id

                                let get_one_inv = await ApiTop.get_one_invoice(invoice_args);
                                let input_to_transactions = {};

                                input_to_transactions.fmcg_id = 5;
                                input_to_transactions.dt_code = dt_code;
                                input_to_transactions.invoice_id = invoice_number+'-'+tenor;
                                input_to_transactions.amount = cek_fit.total_yg_harus_dibayar;
                                input_to_transactions.invoice_code = dt_code+invoice_number+'-'+tenor;
                                input_to_transactions.valdo_tx_id = "TR"+invoice_number+moment().format('YYYYMMDDHHmmss');
                                input_to_transactions.distributor_id = 0;
                                input_to_transactions.retailer_id = 0;  
                                input_to_transactions.from_user_id = 0;
                                input_to_transactions.to_user_id =0;

                                let insert_manual_transaction = await ApiPd.multifmcg_transactions(input_to_transactions)

                                let report = {
                                    response : fit_response,
                                    to_fit_money_access_desc : tofit_money_access_response
                                } 

                                res.status(200).json({code:200, body: report})
                            }
                    });
                }
            })
            
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data invoice, '+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }

}

async function admin_get_update(req,res){
    const token = req.swagger.params.token.value;
    const fmcg_top = 1

    try{

        let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});
        let result_fmcg_top = await service_fmcg.getOneFmcgWithFilter({id: fmcg_top});

        if(result_fmcg_top.dataValues === null) {
            throw new CustomError(400, 'Gagal memproses data invoice, data fmcg tidak tersedia');
        }            
            let invoice_args = {};
            invoice_args.url = result_fmcg_top.dataValues.url;
            invoice_args.fmcg_token = result_fmcg_top.dataValues.fmcg_token;
            invoice_args.fmcg_name = result_fmcg_top.dataValues.name;
            await ApiTop.get_inv_insert(invoice_args);
        res.status(200).json({code: 200, message: 'Success'})

    }catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data top, '+err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function get_one_user_topss(req,res){
    const token = req.swagger.params.token.value;
    const le_code = req.swagger.params.le_code.value;
    let fmcg_id = 13

    try{
        let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
        if(result_fmcg.dataValues === null) {
            throw new CustomError(400, 'Gagal memproses data invoice, data fmcg tidak tersedia');
        }

        let args = {
            url : result_fmcg.dataValues.url,
            fmcg_token : result_fmcg.dataValues.fmcg_token,
            fmcg_name : result_fmcg.dataValues.name,
            le_code : le_code
        }
        let get_dt_code = await ApiTop.get_top_user(args)
        let user_detail = await service_pelunasan_manual.user_detail_by_lecode({fmcg_id:1, le_code:le_code})


        let out = {}
        out.dt_code = get_dt_code.body.dt_code
        out.distributor_name = get_dt_code.body.distributor_name
        out.le_code = get_dt_code.body.le_code
        out.va = user_detail[0].valdo_account
        out.phone = user_detail[0].phone
        out.nama_toko = user_detail[0].nama_toko
        out.nama = user_detail[0].nama

        res.status(200).json({code: 200, body: out});

    

    }catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = ''+err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function get_one_user_top(req,res){
    const token = req.swagger.params.token.value;
    const le_code = req.swagger.params.le_code.value;
    let fmcg_id = 13

    try{
        let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
        if(result_fmcg.dataValues === null) {
            throw new CustomError(400, 'Gagal memproses data invoice, data fmcg tidak tersedia');
        }

        let args = {
            url : result_fmcg.dataValues.url,
            fmcg_token : result_fmcg.dataValues.fmcg_token,
            fmcg_name : result_fmcg.dataValues.name,
            le_code : le_code
        }
        let get_dt_code = await ApiTop.get_top_user(args)
        let user_detail = await service_pelunasan_manual.user_detail_by_lecode({fmcg_id:1, le_code:le_code})
        let getdatalecode = await ApiPd.getdatalecode({le_code:le_code});
        let api_response = await MoneyAccess.inquiry({mobnum: user_detail[0].valdo_account, vaccount: user_detail[0].valdo_account, usertype: user_detail[0].type});


        let out = user_detail[0]
        out.dt_code = get_dt_code["body"]["dt_code"];
        out.distributor_name = get_dt_code["body"]["distributor_name"];
        out.data_akuisisi = getdatalecode;
        out.saldo_toko = api_response.balance;
        out.limit_pandai = [];
        out.installment = [];
        
        request({
            headers: {
            'Content-Type': 'application/json',
            'api_key' : top_api_key
            },
             url: "https://"+top_api+"/loan/detail",
             method: "POST",
             json: true,
             body: {le_code:le_code}
           }, function (error, response, body){
               if(body["status"] === 'success'){
                   out.limit_pandai.push({nama: 'FIT',plafond:body["data"]["plafond"]});
                   out.installment = body["data"]["installment"];
               }else{
                   out.limit_pandai = [];
                   out.installment = [];
               }
               res.status(200).json({code: 200, body: out});
           })
    }catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = ''+err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function manual_update_top_payment_distributor(req,res){
    let token = req.swagger.params.token.value;

    let fmcg_top = 1;
    let fmcg_id = 13;

    try{

        Logs.create({action: 'manual payment distributor', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: '', is_allowed_access_token_static: true});
        
        let result_fmcg_top = await service_fmcg.getOneFmcgWithFilter({id: fmcg_top});
        if(result_fmcg_top.dataValues === null) {
            throw new CustomError(400, 'Gagal memproses data invoice, data fmcg tidak tersedia');
        }

        let args = {};
        args.url = result_fmcg_top.dataValues.url;
        args.fmcg_name = result_fmcg_top.dataValues.name;
        args.fmcg_token = result_fmcg_top.dataValues.fmcg_token;

        let loan_history = await ApiTop.get_manual_invoice_loan_history(args)

        for(let i=0;i < loan_history.length; i++){

            let toko_detail = await service_pelunasan_manual.user_detail_by_lecode({fmcg_id:fmcg_id,le_code:loan_history[i]["le_code"]})

            let invoice_args = {}
            invoice_args.url = result_fmcg_top.dataValues.url;
            invoice_args.fmcg_name = result_fmcg_top.dataValues.name;
            invoice_args.fmcg_token = result_fmcg_top.dataValues.fmcg_token;
            invoice_args.le_code = loan_history[i]["le_code"];
            invoice_args.dt_code = loan_history[i]["dt_code"];
            invoice_args.invoice_no = loan_history[i]["invoice_id"];
            invoice_args.amount = parseInt(loan_history[i]["amount"]);
            invoice_args.tenor = loan_history[i]["tenor"];
            invoice_args.fmcg_id = fmcg_id;
            invoice_args.principal_code = loan_history[i]["principal_code"];
            invoice_args.user_id = toko_detail[0]["user_id"]

            let insert_manual_invoice_res = await ApiTop.manual_insert_top_payment_distributor(invoice_args)
        }

        res.status(200).json({code:200, message:'ok'})


    }catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data invoice, '+err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}


export { get_one_user_top }
export { apply_loan }
export { cek_saldo }
export { get_invoice }
export { calculator }
export { repayment }
export { get_one_invoice }
export { payment_distributor }
export { updateStatusPaymentDistributor }
export { repayment_history }
export { repayment_to_distributor }
export { repayment_manual }
export { manual_repayment_invoice }
export { admin_get_update }
export { manual_update_top_payment_distributor }
