'use strict';

import { AcaProduct } from '~/orm/index'
import CustomError from '~/response/error/CustomError.js';



async function getProduct(filter, options) {
    options = options || {};
    let result_set = '';

    if(filter) {
        result_set = await AcaProduct.findAll({where: filter});
    } else {
        result_set = await AcaProduct.findAll();
    }

    if (!result_set || Object.keys(result_set).length == 0) {
        if (options.throwIfEmpty) {
            throw new CustomError(400, 'Data tidak ditemukan!');
        }
    }

    return result_set;
}

function getOne({id=null}={}) {
    return AcaProduct.findOne({
        where: {
            id: id
        }
    });
}

function add({name=null, KdProduk=null, ProductDesc=null, jmlhPremi=null}={}) {
    let args = {};
    args.name = name;
    args.KdProduk = KdProduk;
    args.ProductDesc = ProductDesc;
    args.jmlhPremi = jmlhPremi
    
    return AcaProduct.create(args);
}

let obj = {}
obj.getProduct = getProduct
obj.getOne = getOne
obj.add = add

export default obj
