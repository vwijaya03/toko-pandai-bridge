'use strict';

import _ from 'lodash'
import util from 'util'
import fs from 'fs'
import middleware from '~/api/controllers/auth'
import request from 'request'
import moment from 'moment'

import service_user_unique_code from '~/services/user_unique_code'

import { Logs } from '~/orm/index'

import CustomError from '~/response/error/CustomError'
import CustomSuccess from '~/response/success/CustomSuccess'

const acl_bekraf = ['retailer', 'distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];

let url_bekraf = 'http://192.168.12.6:21000/'

async function get_produsen_list(req,res){
    const token = req.swagger.params.token.value;
    try{

        let auth_user =  await middleware.verify({token: token, acl: acl_bekraf, is_allowed_access_token_static: false});
        
        let check_user_le_code = await service_user_unique_code.get({user_id:auth_user.dataValues.user_id,fmcg_id:10})
        
        if(check_user_le_code.length == 0){
            throw new CustomError(400, 'Tambahkan icon Umum')
        }
        
        request({
                headers: {},
                url: url_bekraf+'ordering/get-produsen-list',
                method: "GET",
		}, function (error, result, body){
            let response = JSON.parse(body)
            if(response.code !== 400){
                res.status(200).json(response)
            }else{
                res.status(400).json(response)
            }
		})
    }catch(err){
        if (err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data Bayar Nanti, ' + err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function add_produsen(req,res){
    const token = req.swagger.params.token.value;
    const param_name = req.swagger.params.name;
    const param_produsen_id = req.swagger.params.produsen_id;

    let name = _.isUndefined(param_name.value)? null : param_name.value
    let produsen_id = _.isUndefined(param_produsen_id.value)? null : param_produsen_id.value

    try{

        await middleware.verify({token: token, acl: acl_bekraf, is_allowed_access_token_static: false});

        request({
			headers: {},
			url: url_bekraf+'ordering/add-produsen',
			method: "POST",
            json: true,
            body: {
                name: name,
                produsen_id: produsen_id
            }
		}, function (error, result, body){
            if(body.code !== 400){
                res.status(200).json(body)
            }else{
                res.status(400).json(body)
            }
		})
    }catch(err){
        if (err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data Bayar Nanti, ' + err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function add_product(req,res){
    const token = req.swagger.params.token.value;
    const param_kode_produk = req.swagger.params.kode_produk;
    const param_produsen_id = req.swagger.params.produsen_id;
    const param_nama_produk = req.swagger.params.nama_produk;
    const param_description = req.swagger.params.description;
    const param_harga = req.swagger.params.harga;
    const param_stok = req.swagger.params.stok;

    let nama_produk = _.isUndefined(param_nama_produk.value)? null : param_nama_produk.value
    let kode_produk = _.isUndefined(param_kode_produk.value)? null : param_kode_produk.value
    let produsen_id = _.isUndefined(param_produsen_id.value)? null : param_produsen_id.value
    let description = _.isUndefined(param_description.value)? null : param_description.value
    let harga = _.isUndefined(param_harga.value)? null : param_harga.value
    let stok = _.isUndefined(param_stok.value)? null : param_stok.value

    try{

        await middleware.verify({token: token, acl: acl_bekraf, is_allowed_access_token_static: false});

        request({
			headers: {},
			url: url_bekraf+'ordering/add-product',
			method: "POST",
            json: true,
            body: {
                produsen_id:produsen_id,
                kode_produk:kode_produk,
                nama_produk:nama_produk,
                description:description,
                harga:harga,
                stok:stok
            }
		}, function (error, result, body){
            if(body.code !== 400){
                res.status(200).json(body)
            }else{
                res.status(400).json(body)
            }
		})
    }catch(err){
        if (err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data Bayar Nanti, ' + err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function order_product(req,res){
    const token = req.swagger.params.token.value;
    const param_orders = req.swagger.params.data;
    const param_potongan = req.swagger.params.potongan;

    let orders = _.isUndefined(param_orders.value)? null : param_orders.value
    let potongan = _.isUndefined(param_potongan.value)? null : param_potongan.value

    let data = JSON.parse(orders)
    try{

        await middleware.verify({token: token, acl: acl_bekraf, is_allowed_access_token_static: false});

        request({
			headers: {},
			url: url_bekraf+'ordering/order-product',
			method: "POST",
            json: true,
            body: {
                data:data,
                potongan:potongan
            }
		}, function (error, result, body){
            if(body.code !== 400){
                res.status(200).json(body)
            }else{
                res.status(400).json(body)
            }
		})
    }catch(err){
        if (err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data Bayar Nanti, ' + err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function order_products(req,res){
    const token = req.swagger.params.token.value;
    const param_orders = req.swagger.params.data;
    const param_potongan = req.swagger.params.potongan;

    let orders = _.isUndefined(param_orders.value)? null : param_orders.value
    let potongan = _.isUndefined(param_potongan.value)? null : param_potongan.value

    let data = JSON.parse(orders)
    try{

        await middleware.verify({token: token, acl: acl_bekraf, is_allowed_access_token_static: false});

        request({
			headers: {},
			url: url_bekraf+'ordering/order-products',
			method: "POST",
            json: true,
            body: {
                data:data,
                potongan:potongan
            }
		}, function (error, result, body){
            if(body.code !== 400){
                res.status(200).json(body)
            }else{
                res.status(400).json(body)
            }
		})
    }catch(err){
        if (err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data Bayar Nanti, ' + err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function insert_potongan(req,res){
    const token = req.swagger.params.token.value;
    const param_dt_code = req.swagger.params.dt_code;
    const param_le_code = req.swagger.params.le_code;
    const param_invoice_id = req.swagger.params.invoice_id;
    const param_amount = req.swagger.params.amount;
    const param_description = req.swagger.params.description;
    const param_jenis = req.swagger.params.jenis;

    let dt_code = _.isUndefined(param_dt_code.value)? null : param_dt_code.value
    let le_code = _.isUndefined(param_le_code.value)? null : param_le_code.value
    let invoice_id = _.isUndefined(param_invoice_id.value)? null : param_invoice_id.value
    let amount = _.isUndefined(param_amount.value)? null : param_amount.value
    let description = _.isUndefined(param_description.value)? null : param_description.value
    let jenis = _.isUndefined(param_jenis.value)? null : param_jenis.value

    try{

        await middleware.verify({token: token, acl: acl_bekraf, is_allowed_access_token_static: false});

        request({
			headers: {},
			url: url_bekraf+'ordering/insert-potongan',
			method: "POST",
            json: true,
            body: {
                dt_code:dt_code,
                le_code:le_code,
                invoice_id:invoice_id,
                amount:amount,
                description:description,
                jenis:jenis
            }
		}, function (error, result, body){
            if(body.code !== 400){
                res.status(200).json(body)
            }else{
                res.status(400).json(body)
            }
		})
    }catch(err){
        if (err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data Bayar Nanti, ' + err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function payment_invoice(req,res){
    const token = req.swagger.params.token.value;
    const param_user_id = req.swagger.params.user_id;
    const param_le_code = req.swagger.params.le_code;
    const param_invoice_id = req.swagger.params.invoice_id;
    const param_amount = req.swagger.params.amount;
    const param_potongan = req.swagger.params.potongan;
    const param_pin = req.swagger.params.pin;

    let user_id = _.isUndefined(param_user_id.value)? null : param_user_id.value
    let le_code = _.isUndefined(param_le_code.value)? null : param_le_code.value
    let invoice_id = _.isUndefined(param_invoice_id.value)? null : param_invoice_id.value
    let amount = _.isUndefined(param_amount.value)? null : param_amount.value
    let potongan = _.isUndefined(param_potongan.value)? null : param_potongan.value
    let pin = _.isUndefined(param_pin.value)? null : param_pin.value

    try{

        await middleware.verify({token: token, acl: acl_bekraf, is_allowed_access_token_static: false});

        request({
			headers: {},
			url: url_bekraf+'ordering/payment',
			method: "POST",
            json: true,
            body: {
                user_id:user_id,
                le_code:le_code,
                invoice_id:invoice_id,
                amount:amount,
                potongan:potongan,
                pin:pin
            }
		}, function (error, result, body){
            if(body.code !== 400){
                res.status(200).json(body)
            }else{
                res.status(400).json(body)
            }
		})
    }catch(err){
        if (err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data Bayar Nanti, ' + err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function get_one_invoice(req,res){
    const token = req.swagger.params.token.value;
    const param_user_id = req.swagger.params.user_id;
    const param_invoice_id = req.swagger.params.invoice_id;

    let user_id = _.isUndefined(param_user_id.value)? null : param_user_id.value
    let invoice_id = _.isUndefined(param_invoice_id.value)? null : param_invoice_id.value


    try{

        await middleware.verify({token: token, acl: acl_bekraf, is_allowed_access_token_static: false});

        request({
			headers: {},
			url: url_bekraf+`ordering/get-one-invoice?user_id=${user_id}&invoice_id=${invoice_id}`,
			method: "GET"
		}, function (error, result, body){
            let response = JSON.parse(body)
            if(response.code !== 400){
                res.status(200).json(response)
            }else{
                res.status(400).json(response)
            }
		})
    }catch(err){
        if (err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data Bayar Nanti, ' + err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function get_all_invoices(req,res){
    const token = req.swagger.params.token.value;
    const param_user_id = req.swagger.params.user_id;
    const param_page = req.swagger.params.page;
    const param_items_per_page = req.swagger.params.items_per_page;

    let user_id = _.isUndefined(param_user_id.value)? null : param_user_id.value
    let page = _.isUndefined(param_page.value)? null : param_page.value
    let items_per_page = _.isUndefined(param_items_per_page.value)? null : param_items_per_page.value


    try{

        await middleware.verify({token: token, acl: acl_bekraf, is_allowed_access_token_static: false});

        request({
			headers: {},
			url: url_bekraf+`ordering/get-all-invoices?user_id=${user_id}&page=${page}&items_per_page=${items_per_page}`,
			method: "GET"
		}, function (error, result, body){
            let response = JSON.parse(body)
            if(response.code !== 400){
                res.status(200).json(response)
            }else{
                res.status(400).json(response)
            }
		})
    }catch(err){
        if (err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data Bayar Nanti, ' + err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

export { get_produsen_list }
export { add_produsen }
export { add_product}
export { order_product }
export { order_products }
export { insert_potongan }
export { payment_invoice }
export { get_one_invoice }
export { get_all_invoices }
