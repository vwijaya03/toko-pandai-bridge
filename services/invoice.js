import { orm } from '~/services/mariadb'
import { Invoice } from '~/orm/index'
import { Informasi } from '~/orm/index'
import { Op } from '~/utils/helper'

async function getcsv() {  
    let result_set;

    result_set = await Invoice.findAll();

    if (!result_set || Object.keys(result_set).length == 0) {
        if (options.throwIfEmpty) {
            throw new CustomError(400, 'Data invoice tidak di temukan');
        }
    }

    return result_set;
}

async function get(boolean_paid, boolean_unpaid, from_invoice_due_date, end_invoice_due_date) {
    if(boolean_paid != true)
        boolean_paid = false;
    if(boolean_unpaid != true)
        boolean_unpaid = false;
    
    let result_set;
    
    try {
        if(from_invoice_due_date == undefined && end_invoice_due_date == undefined){
            if(boolean_paid == true && boolean_unpaid == true){
                result_set = await Invoice.findAll();
                return result_set;
            }

            if(boolean_paid == true && boolean_unpaid == false){
                result_set = await Invoice.findAll({
                    where: {
                        cash_memo_balance_amount: {
                            [Op.gte]: 0
                          }
                    }
                });
                return result_set;
            }

            if(boolean_paid == false && boolean_unpaid == true){
                result_set = await Invoice.findAll({
                    where: {
                        cash_memo_balance_amount: {
                            [Op.lt]: 0
                        }
                    }
                });

                return result_set;
            } 

            if(boolean_paid == false && boolean_unpaid == false){
                return null;
            }    
        } else {
            if(boolean_paid == true && boolean_unpaid == true){
                result_set = await Invoice.findAll({
                    where: {
                        invoice_due_date: {
                            [Op.between]: [from_invoice_due_date, end_invoice_due_date]
                        }
                    }
                });

                return result_set;
            }

            if(boolean_paid == true && boolean_unpaid == false){
                result_set = await Invoice.findAll({
                    where: {
                        cash_memo_balance_amount: {
                            [Op.gte]: 0
                        },
                        invoice_due_date: {
                            [Op.between]: [from_invoice_due_date, end_invoice_due_date]
                        }
                    }
                });

                return result_set;
            }

            if(boolean_paid == false && boolean_unpaid == true){
                result_set = await Invoice.findAll({
                    where: {
                        cash_memo_balance_amount: {
                            [Op.lt]: 0
                        },
                        invoice_due_date: {
                            [Op.between]: [from_invoice_due_date, end_invoice_due_date]
                        }
                    }
                });

                return result_set;
            }

            if(boolean_paid == false && boolean_unpaid == false) {
                return null;
            }

            return result_set;
        }
    } catch(err){
        return err;
    }
}

async function modify() {
    let result_set;
    result_set = await Invoice.update(
        { session_id: 20, file_name: 'good'  },
        { where: { id: 108 }}
    );

    if (!result_set || Object.keys(result_set).length == 0) {
        if (options.throwIfEmpty) {
            throw new CustomError(400, 'Data invoice tidak di temukan');
        }
    }

    return result_set;
}

let obj = {}
obj.get = get
obj.getcsv = getcsv
obj.modify = modify

export default obj