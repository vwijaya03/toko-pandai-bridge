import request from 'request'
import util from 'util'

import { Logs } from '~/orm/index'
import FmcgGetInvoicesError from '~/response/error/FmcgGetInvoicesError'


const api_path_calculator = '/top-ext/calculator';
const api_path_get_loan = '/top-ext/get-loan';
const api_path_get_invoice_loan_history = '/top-ext/get-invoice-loan-history';
const api_path_invoice_loan_history = '/top-ext/insert-invoice-loan-history';
const api_path_get_all_tenor = '/top-ext/get-all-tenor';
const api_path_insert_invoice_transaction =  "/top-ext/insert-invoice-transaction";
const api_path_get_top_payment_dist = '/top-ext/get-top-payment-distributor';
const api_path_payment_distributor = '/top-ext/payment-distributor';
const api_path_get_unpaid_invoices = '/top-ext/get-unpaid-invoices-distributor';
const api_path_payment_status_distributor = '/top-ext/payment-status-distributor';
const api_path_one_invoice_loanid = '/top-ext/one-invoice-loan-id'

function commonHeaders(token) {
    return {
        'Cache-Control': 'no-cache',
        'token': token
    };
}

function handleResponse(rawJsonResponse, errorClass, resolve, reject, requestError, function_name, fmcg_name) {

  try {
    let successJsonResp = JSON.parse(rawJsonResponse);

    if(requestError != null) {
      reject(new errorClass(requestError));
    }

    if(successJsonResp.code == 400 || successJsonResp.code == 401) {
      reject(new errorClass(successJsonResp.message));
    } else {
      resolve(successJsonResp);
    }
  } catch(err) {
    Logs.create({action: `error ${function_name} ${fmcg_name}`, description: util.inspect(err), additional_description: util.inspect(rawJsonResponse)});
    reject(new errorClass('terjadi kesalahan silahkan coba lagi'));
  }

}

function handleResponseError(errorClass, resolve, reject, err, function_name, fmcg_name) {
  Logs.create({action: `error ${function_name} ${fmcg_name}`, description: util.inspect(err)});
  reject(new errorClass(err));
}

function calculator({url=null, fmcg_token=null, amount=null,fmcg_name=null,discount=null,tenor=null, admin=null}={}) {
  let invoice_args = {};
  invoice_args.url = url;
    invoice_args.fmcg_token = fmcg_token;
    invoice_args.fmcg_name = fmcg_name;
  invoice_args.amount = amount;
    invoice_args.discount = discount;
    invoice_args.tenor = tenor
  invoice_args.admin = admin

  return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_calculator = url+api_path_calculator;
      request.get( {url:api_calculator, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
        if(err) {
          return err;
        }
        handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
      }).on('error', function(err) {
      handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
    });
    });
}

function get_loan({url=null, fmcg_token=null,fmcg_name=null,dt_code=null,le_code=null,invoice_id=null}={}) {
  let invoice_args = {};
  invoice_args.url = url;
    invoice_args.fmcg_token = fmcg_token;
    invoice_args.fmcg_name = fmcg_name;
  invoice_args.dt_code = dt_code;
    invoice_args.le_code = le_code;
    invoice_args.invoice_id = invoice_id

  return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_get_loan = url+api_path_get_loan;
      request.get( {url:api_get_loan, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
        if(err) {
          return err;
        }
        handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
      }).on('error', function(err) {
      handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
    });
    });
}

function get_loan_history({url=null,fmcg_token=null,fmcg_name=null,le_code=null,dt_code=null,invoice_id=null,invoice_code=null}={}){

  let invoice = {}
  invoice.url = url;
  invoice.fmcg_token = fmcg_token;
  invoice.fmcg_name = fmcg_name;
  invoice.le_code = le_code,
  invoice.dt_code = dt_code,
  invoice.invoice_id = invoice_id,
  invoice.invoice_code = invoice_code

  return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_get_invoice_loan_history = url+api_path_get_invoice_loan_history;
      request.get( {url:api_get_invoice_loan_history, headers:headers, qs:invoice}, function(err, httpResponse, body) {
        if(err) {
          return err;
        }
        handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'loan history', fmcg_name);
      }).on('error', function(err) {
      handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'loan history', fmcg_name);
    });
    });
}

function get_data_disburse({url=null,fmcg_token=null,fmcg_name=null,le_code=null,dt_code=null,invoice_no=null,invoice_code=null,principal_code=null}={}){

  let invoice = {}
  invoice.url = url;
  invoice.fmcg_token = fmcg_token;
  invoice.fmcg_name = fmcg_name;
  invoice.le_code = le_code,
  invoice.dt_code = dt_code,
  invoice.invoice_no = invoice_no,
  invoice.invoice_code = invoice_code
  invoice.principal_code = principal_code

  return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_get_disburse = url+api_path_get_top_payment_dist;
      request.get( {url:api_get_disburse, headers:headers, qs:invoice}, function(err, httpResponse, body) {
        if(err) {
          return err;
        }
        handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'top disburse', fmcg_name);
      }).on('error', function(err) {
      handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'top disburse', fmcg_name);
    });
    });
}

function update_payment_fmcg({le_code=null,fmcg_id=null,invoice_id=null,amount=null,paid_by=null}){
    let options = { method: 'POST',
    url: 'http://192.168.13.5:10012/topext',
    headers: 
     { 'postman-token': 'a0e838a6-c4d9-8510-5ee9-0802548f0a8c',
       'cache-control': 'no-cache',
       'content-type': 'application/json' },
    body: 
     { le_code: le_code,
       invoice_id: invoice_id,
       fmcg_id: fmcg_id,
       amount: amount,
       paid_by : paid_by
        },
    json: true };
  request(options, function (error, response, body) {
    if (error) throw new Error(error);
  });
}

function insert_loan_history({url=null,fmcg_token=null,fmcg_name=null,user_id=null,le_code=null,dt_code=null,total_amount_invoice=null,
  amount=null,invoice_id=null,tenor=null,loan_type=null,loan_by=null,principal_code=null,total_potongan=null,discount=null,admin=null,denda=null}={}){

  let args = {}
  args.url = url;
  args.fmcg_token = fmcg_token;
  args.fmcg_name = fmcg_name;
  args.user_id = user_id;
  args.le_code = le_code;
  args.dt_code = dt_code;
  args.amount = amount;
  args.total_amount_invoice = total_amount_invoice;
  args.invoice_id = invoice_id;
  args.tenor = tenor;
  args.loan_type = loan_type;
  args.loan_by = loan_by;
  args.principal_code = principal_code;
  args.total_potongan = total_potongan;
  args.discount = discount;
  args.admin = admin
  args.denda = denda

  return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_invoice_loan_history = url+api_path_invoice_loan_history;
      request.post( {url:api_invoice_loan_history, headers:headers, qs:args}, function(err, httpResponse, body) {
        if(err) {
          return err;
        }
        handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'loan history', fmcg_name);
      }).on('error', function(err) {
      handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'loan history', fmcg_name);
    });
    });
}

function get_all_tenor({fmcg_name=null,fmcg_token=null,url=null,invoice_number=null,invoice_payment_status=null}){
  let data = {};
  data.url = url;
  data.fmcg_name=fmcg_name;
  data.fmcg_token=fmcg_token;
  data.invoice_number=invoice_number;
  data.invoice_payment_status=invoice_payment_status;
    var request = require('request');
  return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
    let api_get_all_tenor = url+api_path_get_all_tenor;
    request.get( {url:api_get_all_tenor, headers:headers, qs:data}, function(err, httpResponse, body) {
      if(err) {
        return err;
      }
      handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
    }).on('error', function(err) {
      handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
    });
  });
}

function insert_transactions({url=null,fmcg_token=null,fmcg_name=null,fmcg_id=null,dt_code=null,valdo_tx_id=null,
  amount=null,invoice_id=null,invoice_code=null,from_user_id=null,to_user_id=null}={}){

  let args = {}
  args.url = url;
  args.fmcg_token = fmcg_token;
  args.fmcg_name = fmcg_name;
  args.fmcg_id = fmcg_id;
  args.dt_code = dt_code;
  args.amount = amount;
  args.invoice_id = invoice_id;
  args.invoice_code = invoice_code;
  args.valdo_tx_id = valdo_tx_id;
  args.from_user_id = from_user_id
  args.to_user_id = to_user_id

  return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_insert_transaction = url+api_path_insert_invoice_transaction;
      request.post( {url:api_insert_transaction, headers:headers, qs:args}, function(err, httpResponse, body) {
        if(err) {
          return err;
        }
        handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'loan history', fmcg_name);
      }).on('error', function(err) {
      handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'loan history', fmcg_name);
    });
    });
}

function payment_distributor({fmcg_id=null,le_code=null,dt_code=null,invoice_no=null,total_tagihan=null,principal_code=null,
  tenor=null,fmcg_name=null,fmcg_token=null,url=null,amount=null}={}) {
  let invoice_args = {};
  invoice_args.fmcg_token=fmcg_token;
  invoice_args.fmcg_name=fmcg_name;
  invoice_args.url = url
  invoice_args.fmcg_id = fmcg_id;
  invoice_args.tenor = tenor;
  invoice_args.le_code = le_code;
  invoice_args.dt_code = dt_code;
  invoice_args.invoice_no = invoice_no;
  invoice_args.total_tagihan = total_tagihan;
  invoice_args.amount = amount;
  invoice_args.principal_code = principal_code;
        

  return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_payment_distributor = url+api_path_payment_distributor;
      request.post( {url:api_payment_distributor, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
        if(err) {
          return err;
        }
        handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
      }).on('error', function(err) {
      handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
    });
    });
}

function get_unpaid_invoices({fmcg_name=null,fmcg_token=null,url=null}={}){
  let args = {};
  args.fmcg_token=fmcg_token;
  args.fmcg_name=fmcg_name;
  args.url = url

  return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_get_unpaid_invoices = url+api_path_get_unpaid_invoices;
      request.post( {url:api_get_unpaid_invoices, headers:headers, qs:args}, function(err, httpResponse, body) {
        if(err) {
          return err;
        }
        handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
      }).on('error', function(err) {
      handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
  });
});
}

function update_payment_status_distributor({invoice_no=null,order_id=null,invoice_code=null,payment_status=null,fmcg_name=null,fmcg_token=null,url=null}={}){
  let update_data = {};
  update_data.fmcg_token=fmcg_token;
  update_data.fmcg_name=fmcg_name;
  update_data.url = url;
  update_data.invoice_no = invoice_no;
  update_data.order_id = order_id;
  update_data.invoice_code = invoice_code

  update_data.payment_status = payment_status;

  return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_payment_status_distributor = url+api_path_payment_status_distributor;
      request.post( {url:api_payment_status_distributor, headers:headers, qs:update_data}, function(err, httpResponse, body) {
        if(err) {
          return err;
        }
        handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
      }).on('error', function(err) {
      handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
  });
});
}

function one_invoice_loanid({fmcg_name=null,fmcg_token=null,url=null,loan_id=null}){
  let data = {};
  data.url = url;
  data.fmcg_name=fmcg_name;
  data.fmcg_token=fmcg_token;
  data.loan_id=loan_id;

    var request = require('request');
  return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
    let api_get_inv = url+api_path_one_invoice_loanid;
    request.get( {url:api_get_inv, headers:headers, qs:data}, function(err, httpResponse, body) {
      if(err) {
        return err;
      }
      handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
    }).on('error', function(err) {
      handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
    });
  });
}


let obj = {}
obj.calculator = calculator
obj.get_loan = get_loan
obj.get_loan_history = get_loan_history
obj.get_data_disburse = get_data_disburse
obj.update_payment_fmcg = update_payment_fmcg
obj.insert_loan_history = insert_loan_history
obj.get_all_tenor = get_all_tenor
obj.insert_transactions = insert_transactions
obj.payment_distributor = payment_distributor
obj.update_payment_status_distributor = update_payment_status_distributor
obj.get_unpaid_invoices = get_unpaid_invoices
obj.one_invoice_loanid = one_invoice_loanid

export default obj
