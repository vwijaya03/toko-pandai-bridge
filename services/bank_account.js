import { orm } from '~/services/mariadb'
import { BankAccount } from '~/orm/index'
import { Bank } from '~/orm/index'
import { User } from '~/orm/index'

function get({user_id=null}={}) {
    return BankAccount.findAll({
        attributes: ['id', 'user_id', 'no_rekening', 'bank_id', 'name'],
        include: [{
            attributes: ['user_id', 'fullname', 'email', 'valdoAccount'],
            model: User,
            required: true,

        }, {model: Bank, required: true }],
        where: {
            user_id: user_id
        }
    });
}

function getBankAccountOnly({user_id=null}={}) {
    return BankAccount.findAll({
        attributes: ['id', 'user_id', 'no_rekening', 'bank_id', 'name'],
        include: [{model: Bank, required: true }],
        where: {
            user_id: user_id
        }
    });
}

function getOneBankAccountAfterInsert({id=null}={}) {
    return BankAccount.findOne({
        attributes: ['id', 'user_id', 'no_rekening', 'bank_id', 'name'],
        include: [{
            attributes: ['user_id', 'fullname', 'email', 'valdoAccount'],
            model: User,
            required: true,

        }, {model: Bank, required: true }],
        where: {
            id: id
        }
    });
}

function getOne({user_id=null, no_rekening=null, bank_id}={}) {
    return BankAccount.findOne({
        where: {
            user_id: user_id,
            no_rekening: no_rekening,
            bank_id: bank_id
        }
    });
}

function createBankAccount({user_id=null, name=null, no_rekening=null, bank_id=null}={})
{
    let args =
    {
        user_id: user_id,
        name: name,
        no_rekening: no_rekening,
        bank_id: bank_id
    };

    return BankAccount.create(args);
}

function updateBankAccount({user_id=null, id_bank_account=null, name=null, no_rekening=null, bank_id=null}={}) {
    return BankAccount.update(
        { name: name, no_rekening: no_rekening, bank_id: bank_id },
        { where: { id: id_bank_account, user_id: user_id }}
    );
}

function updateBankAccountAndroid({user_id=null, id_bank_account=null, no_rekening=null}={}) {
    return BankAccount.update(
        { no_rekening: no_rekening },
        { where: { id: id_bank_account, user_id: user_id }}
    );
}

function destroyBankAccount({user_id=null, id_bank_account=null}={}) {
    return BankAccount.destroy(
        { where: { id: id_bank_account, user_id: user_id }}
    );
}

async function getOneBankAccount(filter, options) {

    options = options || {};
    let result_set = '';

    result_set = await BankAccount.findOne({where: filter});

    if (!result_set || Object.keys(result_set).length == 0) {
        if (options.throwIfEmpty) {
            throw new UserNotFoundError();
        }
    }

    return result_set;
}

let obj = {}
obj.get = get
obj.getBankAccountOnly = getBankAccountOnly
obj.getOne = getOne
obj.getOneBankAccount = getOneBankAccount
obj.getOneBankAccountAfterInsert = getOneBankAccountAfterInsert
obj.createBankAccount = createBankAccount
obj.updateBankAccount = updateBankAccount
obj.updateBankAccountAndroid = updateBankAccountAndroid
obj.destroyBankAccount = destroyBankAccount

export default obj