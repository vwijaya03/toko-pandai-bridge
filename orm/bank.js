import { orm } from '~/services/mariadb'
import Sequelize from 'sequelize'

const Bank = orm.define('banks', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: Sequelize.STRING, allowNull:true
    },
    icon: {
        type: Sequelize.STRING, allowNull:true
    },
    createdAt: { 
        type: 'TIMESTAMP', allowNull:true 
    },
    updatedAt: { 
        type: 'TIMESTAMP', allowNull:true 
    }
});

export default Bank