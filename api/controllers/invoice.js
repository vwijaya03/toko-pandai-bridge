'use strict';

import _ from 'lodash'
import util from 'util'
import fs from 'fs'
import moment from 'moment'

import middleware from '~/api/controllers/auth'

import service_excel from '~/services/excel'
import service_invoice from '~/services/invoice'
import service_fmcg from '~/services/fmcg'
import service_user from '~/services/user'
import service_user_unique_code from '~/services/user_unique_code'

import helper from '~/utils/helper'

import ApiUli from '~/external_api/api_uli'
import ApiTop from '~/external_api/api_top'
import ApiFmcg from '~/external_api/api_fmcg'
import ApiPd from '~/external_api/api_top_pohon_dana'
import ApiBackend from '~/external_api/api_backend'

import { Logs } from '~/orm/index'

import CustomError from '~/response/error/CustomError'
import CustomSuccess from '~/response/success/CustomSuccess'

const createCsvWriter = require('csv-writer').createObjectCsvWriter;

const acl_get_invoices = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center','finance'];
const acl_get_one_invoice = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center','finance'];
const acl_get_retur = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center','finance'];
const acl_check_process_invoices = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center','finance'];
const acl_task_list = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center','finance'];
const acl_save_csv = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center','finance'];
const acl_save_xls = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center','finance'];
const acl_process_invoice = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center','finance'];
const acl_create_retur = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center','finance'];


async function save_csv(req, res) {
    const token = req.swagger.params.token.value;
    let namafile = req.swagger.params.namafile.value;

    try{
        Logs.create({action: 'save_csv', description: `param req token: ${token}, namafile: ${namafile}`});

        let auth_user = await middleware.verify({token: token, acl: acl_save_csv, is_allowed_access_token_static: false});
        let data = await service_invoice.getcsv();

        let mypath = '/home/arispratama/Documents/ITP/toko-pandai-bridge/static_file/report/' + namafile + '.csv';

        const csvWriter = createCsvWriter({
            path: mypath,
            header: [
                {id: 'id', title: 'id'},
                {id: 'session_id', title: 'session_id'},
                {id: 'file_name', title: 'file_name'},
                {id: 'dt_code', title: 'dt_code'},
                {id: 'le_code', title: 'le_code'},
                {id: 'outlet_code', title: 'outlet_code'},
                {id: 'cash_memo_total_amount', title: 'cash_memo_total_amount'},
                {id: 'cash_memo_balance_amount', title: 'cash_memo_balance_amount'},
                {id: 'cashmemo_type', title: 'cashmemo_type'},
                {id: 'invoice_id', title: 'invoice_id'},
                {id: 'invoice_sales_date', title: 'invoice_sales_date'},
                {id: 'invoice_due_date', title: 'invoice_due_date'},
                {id: 'invoice_payment_status_paid', title: 'invoice_payment_status_paid'},
                {id: 'invoice_payment_status_unpaid', title: 'invoice_payment_status_unpaid'},
                {id: 'invoice_details', title: 'invoice_details'},
                {id: 'approval_date', title: 'approval_date'},
                {id: 'status', title: 'status'}
            ]
        });
        csvWriter.writeRecords(data)       // returns a promise
        .then(() => {
       });

        res.json(data);
    }
    catch(err){
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menyimpan csv, '+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }

}

async function save_xls(req, res) {
    const token = req.swagger.params.token.value;
    let file_name = req.swagger.params.namafile.value;
    let param_paid = req.swagger.params.paid;
    let param_unpaid = req.swagger.params.unpaid;
    let param_from_invoice_due_date = req.swagger.params.from_invoice_due_date;
    let param_end_invoice_due_date = req.swagger.params.end_invoice_due_date;

    try{
        Logs.create({action: 'save_xls', description: `param req token: ${token}, file_name: ${file_name}, param_paid: ${param_paid.value}, param_unpaid: ${param_unpaid.value}, param_from_invoice_due_date: ${param_from_invoice_due_date.value}, param_end_invoice_due_date: ${param_end_invoice_due_date.value}`});

        let auth_user = await middleware.verify({token: token, acl: acl_save_xls, is_allowed_access_token_static: false});
        let data = await service_invoice.get(param_paid.value,param_unpaid.value,param_from_invoice_due_date.value,param_end_invoice_due_date.value);
        let data1 = JSON.parse(JSON.stringify(data));
        let save_path = '/home/arispratama/Documents/ITP/toko-pandai-bridge/static_file/report/';

        let header = 'Ini Header';
        let footer = 'Ini Footer';
        let wb = await service_excel.generateExcel(data,header,footer);
        wb.write(save_path + file_name + '.xlsx', function (err, stats) {
            if (err) {
            } else {
           }
        });
        res.json(data1);
    } catch(err){
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menyimpan xls, '+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function get_invoices(req, res) {
	const token = req.swagger.params.token.value;
    const platform = req.swagger.params.platform.value;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_outlet_code = req.swagger.params.outlet_code;
    const param_dt_code = req.swagger.params.dt_code;
    const param_le_code = req.swagger.params.le_code;
    const param_paid = req.swagger.params.paid;
    const param_unpaid = req.swagger.params.unpaid;
    const param_filter1 = req.swagger.params.filter1;
    const param_filter2 = req.swagger.params.filter2;
    const param_page = req.swagger.params.page;
    const param_items_per_page = req.swagger.params.items_per_page;
    const param_show_products = req.swagger.params.show_products;
    const param_sorting = req.swagger.params.sorting;
    const param_sortingBy = req.swagger.params.sortingBy;
    const param_filter_by = req.swagger.params.filter_by;
    const param_paid_by = req.swagger.params.paid_by;
    
    const iso8601format = "YYYY-MM-DD";

    let paid_by = _.isUndefined(param_paid_by.value)?null:param_paid_by.value;
    let unpaid = "";
    let paid = "";
    let fmcg_id = _.isUndefined(param_fmcg_id.value)?null:param_fmcg_id.value;
    let itemsPerPage = 1000;
    if(fmcg_id ==24 || fmcg_id ==19){
        let paid_res = param_unpaid.value?false:null;
        paid = param_paid.value?param_paid.value:paid_res;
        itemsPerPage = _.isUndefined(param_items_per_page.value)?1000:param_items_per_page.value;
    }else{
        unpaid = param_unpaid.value?param_unpaid.value:false;
        paid = param_paid.value?param_paid.value:false;
        itemsPerPage = _.isUndefined(param_items_per_page.value)?15:param_items_per_page.value;

    }
    let outlet_code = _.isUndefined(param_outlet_code.value)?null:param_outlet_code.value;
    
    let distributor_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
    let le_code = _.isUndefined(param_le_code.value)?null:param_le_code.value;
    let startPage = _.isUndefined(param_page.value)?0:param_page.value;
    let sorting = _.isUndefined(param_sorting.value)?null:param_sorting.value;
    let sortingBy = _.isUndefined(param_sortingBy.value)?null:param_sortingBy.value;
    let filter_by = _.isUndefined(param_filter_by.value)?null:param_filter_by.value;
    let filter1 = _.isUndefined(param_filter1.value)?null:param_filter1.value.trim();
    let filter2 = _.isUndefined(param_filter2.value)?null:param_filter2.value.trim();
    let data_unique_code = '';

    startPage = (startPage < 1 ) ? 1 : startPage;

    try {
        Logs.create({action: 'get_invoices', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_get_invoices, is_allowed_access_token_static: false});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});

        if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal menampilkan data invoice, data fmcg tidak tersedia');
        }

        let result_unique_code = await service_user_unique_code.get({fmcg_id: fmcg_id,user_id: auth_user.dataValues.user_id});

        await result_unique_code.map( (val, index) => {
            if(index == 0) {
                data_unique_code += "'"+val.unique_code+"'";
            } else {
                data_unique_code += ",'"+val.unique_code+"'";
            }
        });

        let invoice_args = {};
        invoice_args.url = result_fmcg.dataValues.url;
        invoice_args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        invoice_args.fmcg_name = result_fmcg.dataValues.name;
        invoice_args.platform = platform;
        invoice_args.usertype = auth_user.dataValues.type;
        invoice_args.outlet_code = outlet_code;
        invoice_args.distributor_code = distributor_code;
        invoice_args.le_code = le_code;
        invoice_args.unique_code = data_unique_code;
        invoice_args.paid = paid;
        invoice_args.paid_by = paid_by;
        invoice_args.unpaid = unpaid;
        invoice_args.page = startPage;
        invoice_args.items_per_page = itemsPerPage;
        invoice_args.sorting = sorting;
        invoice_args.sortingBy = sortingBy;
        invoice_args.filter_by = filter_by;

        if(filter_by == 'sales_date' || filter_by == 'paid_date' || filter_by == 'due_date') {
            if(filter1 != null) {
                if(!filter1.match(/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/)) {
                    throw new CustomError(400, 'Format tanggal awal tidak sesuai.');
                } else {
                    invoice_args.filter1 = filter1;
                }
            }

            if(filter2 != null) {
                if(!filter2.match(/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/)) {
                    throw new CustomError(400, 'Format tanggal akhir tidak sesuai.');
                } else {
                    invoice_args.filter2 = filter2;
                }
            }
        } else {
            invoice_args.filter1 = filter1;
            invoice_args.filter2 = filter2;
        }

        if(auth_user.dataValues.type == 4) {
            let result = await service_user_unique_code.checkUniqueCodeAndNamaToko({data: le_code, fmcg_id: result_fmcg.dataValues.id});
            let data_le_code_from_sales = '';

            if(result.length > 0) {
                await result.map( (val, index) => {
                    if(index == 0) {
                        data_le_code_from_sales += "'"+val.unique_code+"'";
                    } else {
                        data_le_code_from_sales += ",'"+val.unique_code+"'";
                    }
                });

                invoice_args.le_code = data_le_code_from_sales;
            } else {
                invoice_args.le_code = null;
            }
            
            
        }

        invoice_args.join_date = helper.formatDateTime(auth_user.dataValues.join_date);

        let api_response_fmcg = await ApiUli.get_invoices(invoice_args);

        Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} get_invoices`, description: util.inspect(api_response_fmcg)});

        let invoices = api_response_fmcg.invoices;
        for(let i=0;i < invoices.length;i++){
            let uuc = await service_user_unique_code.getJoinUsersUniqueCodeAndUser({unique_code: invoices[i].le_code, fmcg_id: result_fmcg.dataValues.id});

            if(uuc.length == 0) {
                invoices[i].outlet_name = '';
            } else {
                if(uuc[0].value == null || uuc[0].value == '') {
                    uuc[0].value = '';
                }
                invoices[i].outlet_name = uuc[0].value;
            }
            if(fmcg_id == 19){
            invoices[i].model_invoice= '1';
            }
        }

        res.json(api_response_fmcg);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data invoice, '+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

//async function get_one_invoice(req, res) {
//    const token = req.swagger.params.token.value;
//    const param_fmcg_id = req.swagger.params.fmcg_id;
//    const param_invoice_id = req.swagger.params.invoice_id;
//    const param_dt_code = req.swagger.params.dt_code;
//
//    let fmcg_id = _.isUndefined(param_fmcg_id.value)?null:param_fmcg_id.value;
//    let invoice_id = _.isUndefined(param_invoice_id.value)?null:param_invoice_id.value;
//    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
//
//    try {
//        Logs.create({action: 'get_one_invoice', description: helper.getSimpleSwaggerParams(req.swagger.params)});
//
//        let auth_user = await middleware.verify({token: token, acl: acl_get_one_invoice, is_allowed_access_token_static: false});
//        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
//
//        if(result_fmcg == null) {
//            throw new CustomError(400, 'Gagal menampilkan data invoice, data fmcg tidak tersedia');
//        }
//        let invoice_args = {};
//        invoice_args.url = result_fmcg.dataValues.url;
//        invoice_args.fmcg_token = result_fmcg.dataValues.fmcg_token;
//        invoice_args.fmcg_name = result_fmcg.dataValues.name;
//        invoice_args.invoice_id = invoice_id;
//        invoice_args.dt_code = dt_code;
//        let api_response_fmcg = await ApiUli.get_one_invoice(invoice_args);
//        Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} get_one_invoice`, description: util.inspect(api_response_fmcg)});
//        let invoice = api_response_fmcg;
//        let uuc = await service_user_unique_code.getJoinUsersUniqueCodeAndUser({unique_code: invoice.le_code, fmcg_id: result_fmcg.dataValues.id});
//        if(fmcg_id == 19){
//            invoice.available_giro= true;
//            invoice.model_invoice= '1';
//        }else{  
//            invoice.available_giro= false;
//        }
//        if(fmcg_id == 21 || fmcg_id == 15 || fmcg_id == 14){
//            invoice.retur_show =true
//        }else{
//            invoice.retur_show =false
//        }
//        if(fmcg_id == 21){invoice.allow_retur_detail =true}
//        else{invoice.allow_retur_detail =false}
//        let getdatalecode = await ApiBackend.getdatalecode({le_code:api_response_fmcg.le_code});
//        invoice.paid_by = getdatalecode.fintech.fintech;
//        invoice.outlet_type = getdatalecode.fintech.toko;
//        
//        if(getdatalecode.fintech.fintech === "aspirasi"){
//             if(fmcg_id == 13){invoice.cod =false}
//               invoice.payment_method ="partial";
//               invoice.denda =getdatalecode.fintech.fintech_detail[0].denda;
//               invoice.admin =getdatalecode.fintech.fintech_detail[0].administrasi;
//               if(getdatalecode.fintech.toko === "Credit"){
//            invoice.week =
//                  [{"loan_length": 1}]
//        }else{
//             if(fmcg_id == 13){invoice.cod =true}
//        if(parseInt(api_response_fmcg.balance_amount) > 3000000){
//            invoice.week =
//                  [{"loan_length": 1}]
//        }
//        else{
//            invoice.week =
//                [{"loan_length": 1}]
//        }}
//        }else{
//            invoice.payment_method ="full"
//        if(getdatalecode.fintech.toko === "Credit"){
//            invoice.week =
//                  [{"loan_length": 1}]
//        }else{
//        if(parseInt(api_response_fmcg.balance_amount) > 3000000){
//            invoice.week =
//                  [{"loan_length": 1}]
//        }
//        else{
//            invoice.week =
//                [{"loan_length": 1}]
//        }}}
//
//        if(uuc.length == 0) {
//            invoice.outlet_name = '';
//        } else {
//            if(uuc[0].value == null || uuc[0].value == '') {
//                uuc[0].value = '';
//            }
//
//            invoice.outlet_name = uuc[0].value;
//        }
//
//        res.json(invoice);
//    } catch(err) {
//        if(err.code === undefined) {
//            err.code = 400;
//            err.err_code = '';
//            err.message = 'Gagal menampilkan data invoice, '+err;
//        }
//
//        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
//    }
//}

async function get_one_invoice(req, res) {
    const token = req.swagger.params.token.value;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_invoice_id = req.swagger.params.invoice_id;
    const param_dt_code = req.swagger.params.dt_code;

    let fmcg_id = _.isUndefined(param_fmcg_id.value)?null:param_fmcg_id.value;
    let invoice_id = _.isUndefined(param_invoice_id.value)?null:param_invoice_id.value;
    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;

    try {
        Logs.create({action: 'get_one_invoice', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_get_one_invoice, is_allowed_access_token_static: false});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});

        if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal menampilkan data invoice, data fmcg tidak tersedia');
        }
        let invoice_args = {};
        invoice_args.url = result_fmcg.dataValues.url;
        invoice_args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        invoice_args.fmcg_name = result_fmcg.dataValues.name;
        invoice_args.invoice_id = invoice_id;
        invoice_args.dt_code = dt_code;
        let api_response_fmcg = await ApiUli.get_one_invoice(invoice_args);
        Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} get_one_invoice`, description: util.inspect(api_response_fmcg)});
        let invoice = api_response_fmcg;
        let uuc = await service_user_unique_code.getJoinUsersUniqueCodeAndUser({unique_code: invoice.le_code, fmcg_id: result_fmcg.dataValues.id});
        if(fmcg_id == 19){
            invoice.available_giro= true;
            invoice.model_invoice= '1';
        }else{  
            invoice.available_giro= false;
        }
        
        if(fmcg_id == 21 || fmcg_id == 15 || fmcg_id == 14){
            invoice.retur_show =true
        }else{
            invoice.retur_show =false
        }
        
        if(fmcg_id == 21){
            invoice.allow_retur_detail =true
        }else{
            invoice.allow_retur_detail =false
        }
        let getdatalecode = await ApiBackend.getdatalecode({le_code:api_response_fmcg.le_code});
        invoice.paid_by = getdatalecode.fintech.fintech;
        invoice.status_fit= getdatalecode.fintech.is_active;
        invoice.outlet_type = getdatalecode.fintech.toko;
         let tenor_asp =[{"loan_length": 1}];
        if(getdatalecode.fintech.fintech === "aspirasi"){
            if(fmcg_id == 13){invoice.cod =false}

            invoice.payment_method ="partial";
            invoice.denda =getdatalecode.fintech.fintech_detail[0].denda;
            invoice.admin =getdatalecode.fintech.fintech_detail[0].administrasi;
           
            if(getdatalecode.fintech.fintech_detail[0].tenor == 1){
                tenor_asp =[{"loan_length": 1}]
            }else if(getdatalecode.fintech.fintech_detail[0].tenor == 2){
                 tenor_asp=[{"loan_length": 1},{"loan_length": 2}]
            }

            if(getdatalecode.fintech.toko === "Credit"){
                invoice.week =tenor_asp
            }else{
                if(fmcg_id == 13){invoice.cod =true}
                if(parseInt(api_response_fmcg.balance_amount) > 3000000){
                    invoice.week =tenor_asp
                }
                else{
                    invoice.week =tenor_asp
                }
            }
    
        }else{
            invoice.payment_method ="full"
            if(getdatalecode.fintech.toko === "Credit"){
                invoice.week =
                      tenor_asp
            }else{
                invoice.cod =true
                invoice.week =
                      tenor_asp
            }
        }

        if(uuc.length == 0) {
            invoice.outlet_name = '';
        } else {
            if(uuc[0].value == null || uuc[0].value == '') {
                uuc[0].value = '';
            }

            invoice.outlet_name = uuc[0].value;
        }

        res.json(invoice);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data invoice, '+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function check_process_invoice(req, res) {
    const token = req.swagger.params.token.value;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_dt_code = req.swagger.params.dt_code;

    let fmcg_id = _.isUndefined(param_fmcg_id.value)?null:param_fmcg_id.value;
    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
    let data_unique_code = '';

    try {
        Logs.create({action: 'check_process_invoice', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_check_process_invoices, is_allowed_access_token_static: false});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});

        if(result_fmcg == null) {
            throw new CustomError(400, 'Pengecekan proses invoice gagal, data fmcg tidak tersedia');
        }

        if(auth_user.dataValues.type == 1 || auth_user.dataValues.type == 3) {
            let result_unique_code = await service_user_unique_code.get({user_id: auth_user.dataValues.user_id});

            await result_unique_code.map( (val, index) => {
                if(index == 0) {
                    data_unique_code += "'"+val.unique_code+"'";
                } else {
                    data_unique_code += ",'"+val.unique_code+"'";
                }
            });

            dt_code = data_unique_code;
        } else {
            if(!dt_code || dt_code == null) {
                throw new CustomError(400, 'Kode distributor tidak boleh kosong');
            }
        }

        let args = {};
        args.url = result_fmcg.dataValues.url;
        args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        args.fmcg_name = result_fmcg.dataValues.name;
        args.dt_code = dt_code;

        let api_response_fmcg = await ApiUli.check_process_invoice(args);

        Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} check_process_invoice`, description: util.inspect(api_response_fmcg)});

        res.json(api_response_fmcg);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Pengecekan proses invoice gagal, '+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function get_unbound_retur(req, res) {
    const token = req.swagger.params.token.value;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_le_code = req.swagger.params.le_code;

    let fmcg_id = _.isUndefined(param_fmcg_id.value)?null:param_fmcg_id.value;
    let le_code = _.isUndefined(param_le_code.value)?null:param_le_code.value;

    try {
        Logs.create({action: 'get_unbound_retur', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_get_one_invoice, is_allowed_access_token_static: false});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});

        if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal menampilkan data unbound retur, data fmcg tidak tersedia');
        }

        let invoice_args = {};
        invoice_args.url = result_fmcg.dataValues.url;
        invoice_args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        invoice_args.fmcg_name = result_fmcg.dataValues.name;
        invoice_args.le_code = le_code;

        let api_response_fmcg = await ApiUli.get_unbound_retur(invoice_args);

        Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} get_unbound_retur`, description: util.inspect(api_response_fmcg)});

        let invoice = api_response_fmcg;

        res.json(invoice);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data unbound retur, '+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function get_retur(req, res) {
    const token = req.swagger.params.token.value;
    const platform = req.swagger.params.platform.value;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_outlet_code = req.swagger.params.outlet_code;
    const param_dt_code = req.swagger.params.dt_code;
    const param_le_code = req.swagger.params.le_code;
    const param_start_date = req.swagger.params.start_date;
    const param_end_date = req.swagger.params.end_date;
    const param_page = req.swagger.params.page;
    const param_items_per_page = req.swagger.params.items_per_page;
    const param_show_products = req.swagger.params.show_products;
    const param_sorting = req.swagger.params.sorting;
    const param_sortingBy = req.swagger.params.sortingBy;
    const param_filter_by = req.swagger.params.filter_by;
    const iso8601format = "YYYY-MM-DD";

    let outlet_code = _.isUndefined(param_outlet_code.value)?null:param_outlet_code.value;
    let fmcg_id = _.isUndefined(param_fmcg_id.value)?null:param_fmcg_id.value;
    let distributor_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
    let le_code = _.isUndefined(param_le_code.value)?null:param_le_code.value;
    let startPage = _.isUndefined(param_page.value)?0:param_page.value;
    let itemsPerPage = _.isUndefined(param_items_per_page.value)?15:param_items_per_page.value;
    let sorting = _.isUndefined(param_sorting.value)?null:param_sorting.value;
    let sortingBy = _.isUndefined(param_sortingBy.value)?null:param_sortingBy.value;
    let filter_by = _.isUndefined(param_filter_by.value)?null:param_filter_by.value;
    let start_date = _.isUndefined(param_start_date.value)?null:param_start_date.value.trim();
    let end_date = _.isUndefined(param_end_date.value)?null:param_end_date.value.trim();
    let data_unique_code = '';

    startPage = (startPage < 1 ) ? 1 : startPage;

    try {
        Logs.create({action: 'get_retur', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_get_retur, is_allowed_access_token_static: false});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});

        if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal menampilkan data invoice, data fmcg tidak tersedia');
        }

        let result_unique_code = await service_user_unique_code.get({user_id: auth_user.dataValues.user_id});

        await result_unique_code.map( (val, index) => {
            if(index == 0) {
                data_unique_code += "'"+val.unique_code+"'";
            } else {
                data_unique_code += ",'"+val.unique_code+"'";
            }
        });

        let retur_args = {};
        retur_args.url = result_fmcg.dataValues.url;
        retur_args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        retur_args.fmcg_name = result_fmcg.dataValues.name;
        retur_args.platform = platform;
        retur_args.usertype = auth_user.dataValues.type;
        retur_args.outlet_code = outlet_code;
        retur_args.distributor_code = distributor_code;
        retur_args.le_code = le_code;
        retur_args.unique_code = data_unique_code;
        retur_args.page = startPage;
        retur_args.items_per_page = itemsPerPage;
        retur_args.sorting = sorting;
        retur_args.sortingBy = sortingBy;
        retur_args.filter_by = filter_by;

        if(start_date != null) {
            if(!start_date.match(/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/)) {
                throw new CustomError(400, 'Format tanggal awal tidak sesuai.');
            } else {
                retur_args.start_date = start_date;
            }
        }

        if(end_date != null) {
            if(!end_date.match(/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/)) {
                throw new CustomError(400, 'Format tanggal akhir tidak sesuai.');
            } else {
                retur_args.end_date = end_date;
            }
        }

        if(auth_user.dataValues.type == 4) {
            let result = await service_user_unique_code.checkUniqueCodeAndNamaToko({data: le_code, fmcg_id: result_fmcg.dataValues.id});
            let data_le_code_from_sales = '';

            if(result.length > 0) {
                await result.map( (val, index) => {
                    if(index == 0) {
                        data_le_code_from_sales += "'"+val.unique_code+"'";
                    } else {
                        data_le_code_from_sales += ",'"+val.unique_code+"'";
                    }
                });

                retur_args.le_code = data_le_code_from_sales;
            }
        }

        retur_args.join_date = helper.formatDateTime(auth_user.dataValues.join_date);

        let api_response_fmcg = await ApiUli.get_retur(retur_args);

        Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} get_retur`, description: util.inspect(api_response_fmcg)});

        let invoices = api_response_fmcg.invoices;

        for (let i = 0; i < invoices.length; i++) {
            let uuc = await service_user_unique_code.getJoinUsersUniqueCodeAndUser({unique_code: invoices[i].le_code, fmcg_id: result_fmcg.dataValues.id});

            if(uuc.length == 0) {
                invoices[i].outlet_name = '';
            } else {
                if(uuc[0].value == null || uuc[0].value == '') {
                    uuc[0].value = '';
                }

                invoices[i].outlet_name = uuc[0].value;
            }
        }

        res.json(api_response_fmcg);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data retur, '+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function task_list(req, res) {
    const token = req.swagger.params.token.value;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_dt_code = req.swagger.params.dt_code;
    const param_email = req.swagger.params.email;
    const param_page = req.swagger.params.page;
    const param_item_per_page = req.swagger.params.item_per_page;
    const param_sorting = req.swagger.params.sorting;
    const param_sortingBy = req.swagger.params.sortingBy;

    let fmcg_id = _.isUndefined(param_fmcg_id.value)?null:param_fmcg_id.value;
    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
    let email = _.isUndefined(param_email.value)?null:param_email.value;
    let page = _.isUndefined(param_page.value)?0:param_page.value;
    let item_per_page = _.isUndefined(param_item_per_page.value)?15:param_item_per_page.value;
    let sorting = _.isUndefined(param_sorting.value)?null:param_sorting.value;
    let sortingBy = _.isUndefined(param_sortingBy.value)?null:param_sortingBy.value;
    let data_unique_code = '';

    // normalize startPage to 1
    page = (page < 1 ) ? 1 : page;

    try {
        Logs.create({action: 'task_list', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_task_list, is_allowed_access_token_static: false});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});

        if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal menampilkan data invoice yang sudah terupload, data fmcg tidak tersedia');
        }

        if(auth_user.dataValues.type == 1 || auth_user.dataValues.type == 3) {
            let result_unique_code = await service_user_unique_code.get({user_id: auth_user.dataValues.user_id});

            await result_unique_code.map( (val, index) => {
                if(index == 0) {
                    data_unique_code += "'"+val.unique_code+"'";
                } else {
                    data_unique_code += ",'"+val.unique_code+"'";
                }
            });

            dt_code = data_unique_code;
        }

        let args = {};
        args.url = result_fmcg.dataValues.url;
        args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        args.fmcg_name = result_fmcg.dataValues.name;
        args.dt_code = dt_code;
        args.email = email;
        args.page = page;
        args.item_per_page = item_per_page;
        args.sorting = sorting;
        args.sortingBy = sortingBy;
        args.usertype = auth_user.dataValues.type;
        let api_response_fmcg = await ApiUli.get_task_list(args);
        Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} task_list`, description: util.inspect(api_response_fmcg)});
        for (let i = 0; i < api_response_fmcg.tasks.length; i++) {
            let user = await service_user.getOne({user_id: api_response_fmcg.tasks[i].user_id});
            api_response_fmcg.tasks[i].created_by = (user != null) ? user.fullname : '';
        }
        res.json(api_response_fmcg);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data invoice yang sudah terupload, '+err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function process_invoice(req, res) {
    const token = req.swagger.params.token.value;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_dt_code = req.swagger.params.dt_code;
    const param_filename = req.swagger.params.filename;

    let fmcg_id = _.isUndefined(param_fmcg_id.value)?null:param_fmcg_id.value;
    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
    let filename = _.isUndefined(param_filename.value)?null:param_filename.value;
    let data_unique_code = '';

    try {
        Logs.create({action: 'process_invoice', description: helper.getSimpleSwaggerParams(req.swagger.params)});
        let auth_user = await middleware.verify({token: token, acl: acl_task_list, is_allowed_access_token_static: false});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
        if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal memproses data invoice, data fmcg tidak tersedia');
        }
        if(auth_user.dataValues.type == 1 || auth_user.dataValues.type == 3) {
            let result_unique_code = await service_user_unique_code.get({user_id: auth_user.dataValues.user_id, fmcg_id: fmcg_id});

            await result_unique_code.map( (val, index) => {
                if(index == 0) {
                    data_unique_code += val.unique_code;
                } else {
                    data_unique_code += ",'"+val.unique_code+"'";
                }
            });
        } else {
            if(dt_code == null || dt_code == '') {
                throw new CustomError(400, 'Gagal memproses data invoice, dt code tidak boleh kosong');
            } else {
                data_unique_code = dt_code;
            }
        }
        let args = {};
        args.url = result_fmcg.dataValues.url;
        args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        args.fmcg_name = result_fmcg.dataValues.name;
        args.filename = filename;
        args.dt_code = data_unique_code;
        args.user_id = auth_user.dataValues.user_id;
        let api_response_fmcg = await ApiUli.process_invoice(args);
        Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} process_invoice`, description: util.inspect(api_response_fmcg)});
        res.json(api_response_fmcg);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal memproses data invoice, '+err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function upload_pot(req, res) {
    const token = req.swagger.params.token.value;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_filename = req.swagger.params.filename;
    const param_dt_code = req.swagger.params.dt_code;

    let fmcg_id = _.isUndefined(param_fmcg_id.value)?null:param_fmcg_id.value;
    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
    let filename = _.isUndefined(param_filename.value)?null:param_filename.value;
    let data_unique_code = '';

    try {
        Logs.create({action: 'upload_pot', description: helper.getSimpleSwaggerParams(req.swagger.params)});
        let auth_user = await middleware.verify({token: token, acl: acl_task_list, is_allowed_access_token_static: false});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
        if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal upload data potongan, data fmcg tidak tersedia');
        }
        if(auth_user.dataValues.type == 1 || auth_user.dataValues.type == 3) {
            let result_unique_code = await service_user_unique_code.get({user_id: auth_user.dataValues.user_id, fmcg_id: fmcg_id});

            await result_unique_code.map( (val, index) => {
                if(index == 0) {
                    data_unique_code += val.unique_code;
                } else {
                    data_unique_code += ",'"+val.unique_code+"'";
                }
            });
        } else {
            if(dt_code == null || dt_code == '') {
                throw new CustomError(400, 'Gagal upload data potongan, dt code tidak boleh kosong');
            } else {
                data_unique_code = dt_code;
            }
        }
        let args = {};
        args.url = result_fmcg.dataValues.url;
        args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        args.fmcg_name = result_fmcg.dataValues.name;
        args.filename = filename;
        args.dt_code = data_unique_code;
        args.user_id = auth_user.dataValues.user_id;
        let api_response_fmcg = await ApiUli.upload_data_potongan(args);
        Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} upload_pot`, description: util.inspect(api_response_fmcg)});
        res.json(api_response_fmcg);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal upload data potongan, '+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function upload_pembatalan_pot(req, res) {
    const token = req.swagger.params.token.value;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_filename = req.swagger.params.filename;
    const param_dt_code = req.swagger.params.dt_code;

    let fmcg_id = _.isUndefined(param_fmcg_id.value)?null:param_fmcg_id.value;
    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
    let filename = _.isUndefined(param_filename.value)?null:param_filename.value;
    let data_unique_code = '';

    try {
        Logs.create({action: 'upload_pembatalan_pot', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_task_list, is_allowed_access_token_static: false});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});

        if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal upload data pembatalan potongan, data fmcg tidak tersedia');
        }

        if(auth_user.dataValues.type == 1 || auth_user.dataValues.type == 3) {
            let result_unique_code = await service_user_unique_code.get({user_id: auth_user.dataValues.user_id, fmcg_id: fmcg_id});

            await result_unique_code.map( (val, index) => {
                if(index == 0) {
                    data_unique_code += val.unique_code;
                } else {
                    data_unique_code += ",'"+val.unique_code+"'";
                }
            });
        } else {
            if(dt_code == null || dt_code == '') {
                throw new CustomError(400, 'Gagal upload data pembatalan potongan, dt code tidak boleh kosong');
            } else {
                data_unique_code = dt_code;
            }
        }

        let args = {};
        args.url = result_fmcg.dataValues.url;
        args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        args.fmcg_name = result_fmcg.dataValues.name;
        args.filename = filename;
        args.dt_code = data_unique_code;
        args.user_id = auth_user.dataValues.user_id;
        let api_response_fmcg = await ApiUli.upload_data_pembatalan_potongan(args);
        Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} upload_pembatalan_pot`, description: util.inspect(api_response_fmcg)});

        res.json(api_response_fmcg);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal upload data pembatalan potongan, '+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function upload_pembatalan_inv(req, res) {
    const token = req.swagger.params.token.value;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_filename = req.swagger.params.filename;
    const param_dt_code = req.swagger.params.dt_code;

    let fmcg_id = _.isUndefined(param_fmcg_id.value)?null:param_fmcg_id.value;
    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
    let filename = _.isUndefined(param_filename.value)?null:param_filename.value;
    let data_unique_code = '';

    try {
        Logs.create({action: 'upload_pembatalan_inv', description: helper.getSimpleSwaggerParams(req.swagger.params)});
        let auth_user = await middleware.verify({token: token, acl: acl_task_list, is_allowed_access_token_static: false});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
        if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal upload data pembatalan inv, data fmcg tidak tersedia');
        }
        if(auth_user.dataValues.type == 1 || auth_user.dataValues.type == 3) {
            let result_unique_code = await service_user_unique_code.get({user_id: auth_user.dataValues.user_id, fmcg_id: fmcg_id});

            await result_unique_code.map( (val, index) => {
                if(index == 0) {
                    data_unique_code += "'"+val.unique_code+"'";
                } else {
                    data_unique_code += ",'"+val.unique_code+"'";
                }
            });
        } else {
            if(dt_code == null || dt_code == '') {
                throw new CustomError(400, 'Gagal upload data pembatalan invoice, dt code tidak boleh kosong');
            } else {
                data_unique_code = dt_code;
            }
        }
        let args = {};
        args.url = result_fmcg.dataValues.url;
        args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        args.fmcg_name = result_fmcg.dataValues.name;
        args.filename = filename;
        args.dt_code = data_unique_code;
        args.user_id = auth_user.dataValues.user_id;
        let api_response_fmcg = await ApiUli.upload_data_pembatalan_invoice(args);
        Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} upload_pembatalan_inv`, description: util.inspect(api_response_fmcg)});
        res.json(api_response_fmcg);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal upload data pembatalan invoice, '+err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function task_potongan(req, res) {
    const token = req.swagger.params.token.value;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_dt_code = req.swagger.params.dt_code;
    const param_email = req.swagger.params.email;
    const param_page = req.swagger.params.page;
    const param_item_per_page = req.swagger.params.item_per_page;
    const param_sorting = req.swagger.params.sorting;
    const param_sortingBy = req.swagger.params.sortingBy;

    let fmcg_id = _.isUndefined(param_fmcg_id.value)?null:param_fmcg_id.value;
    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
    let email = _.isUndefined(param_email.value)?null:param_email.value;
    let page = _.isUndefined(param_page.value)?0:param_page.value;
    let item_per_page = _.isUndefined(param_item_per_page.value)?15:param_item_per_page.value;
    let sorting = _.isUndefined(param_sorting.value)?null:param_sorting.value;
    let sortingBy = _.isUndefined(param_sortingBy.value)?null:param_sortingBy.value;
    let data_unique_code = '';
    page = (page < 1 ) ? 1 : page;

    try {
        Logs.create({action: 'task_potongan', description: helper.getSimpleSwaggerParams(req.swagger.params)});
        let auth_user = await middleware.verify({token: token, acl: acl_task_list, is_allowed_access_token_static: false});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
        if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal menampilkan data potongan yang sudah terupload, data fmcg tidak tersedia');
        }
        if(auth_user.dataValues.type == 1 || auth_user.dataValues.type == 3) {
            let result_unique_code = await service_user_unique_code.get({user_id: auth_user.dataValues.user_id});

            await result_unique_code.map( (val, index) => {
                if(index == 0) {
                    data_unique_code += "'"+val.unique_code+"'";
                } else {
                    data_unique_code += ",'"+val.unique_code+"'";
                }
            });
            dt_code = data_unique_code;
        }
        let args = {};
        args.url = result_fmcg.dataValues.url;
        args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        args.fmcg_name = result_fmcg.dataValues.name;
        args.dt_code = dt_code;
        args.email = email;
        args.page = page;
        args.item_per_page = item_per_page;
        args.sorting = sorting;
        args.sortingBy = sortingBy;
        args.usertype = auth_user.dataValues.type;
        let api_response_fmcg = await ApiUli.get_task_list_potongan(args);
        Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} task_potongan`, description: util.inspect(api_response_fmcg)});
        for (let i = 0; i < api_response_fmcg.tasks.length; i++) {
            let user = await service_user.getOne({user_id: api_response_fmcg.tasks[i].user_id});
            api_response_fmcg.tasks[i].created_by = (user != null) ? user.fullname : '';
        }
        res.json(api_response_fmcg);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data potongan yang sudah terupload, '+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function task_pembatalan_potongan(req, res) {
    const token = req.swagger.params.token.value;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_dt_code = req.swagger.params.dt_code;
    const param_email = req.swagger.params.email;
    const param_page = req.swagger.params.page;
    const param_item_per_page = req.swagger.params.item_per_page;
    const param_sorting = req.swagger.params.sorting;
    const param_sortingBy = req.swagger.params.sortingBy;

    let fmcg_id = _.isUndefined(param_fmcg_id.value)?null:param_fmcg_id.value;
    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
    let email = _.isUndefined(param_email.value)?null:param_email.value;
    let page = _.isUndefined(param_page.value)?0:param_page.value;
    let item_per_page = _.isUndefined(param_item_per_page.value)?15:param_item_per_page.value;
    let sorting = _.isUndefined(param_sorting.value)?null:param_sorting.value;
    let sortingBy = _.isUndefined(param_sortingBy.value)?null:param_sortingBy.value;
    let data_unique_code = '';
    page = (page < 1 ) ? 1 : page;

    try {
        Logs.create({action: 'task_pembatalan_potongan', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_task_list, is_allowed_access_token_static: false});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});

        if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal menampilkan data pembatalan potongan yang sudah terupload, data fmcg tidak tersedia');
        }

        if(auth_user.dataValues.type == 1 || auth_user.dataValues.type == 3) {
            let result_unique_code = await service_user_unique_code.get({user_id: auth_user.dataValues.user_id});

            await result_unique_code.map( (val, index) => {
                if(index == 0) {
                    data_unique_code += "'"+val.unique_code+"'";
                } else {
                    data_unique_code += ",'"+val.unique_code+"'";
                }
            });
            dt_code = data_unique_code;
        }
        let args = {};
        args.url = result_fmcg.dataValues.url;
        args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        args.fmcg_name = result_fmcg.dataValues.name;
        args.dt_code = dt_code;
        args.email = email;
        args.page = page;
        args.item_per_page = item_per_page;
        args.sorting = sorting;
        args.sortingBy = sortingBy;
        args.usertype = auth_user.dataValues.type;
        let api_response_fmcg = await ApiUli.get_task_list_pembatalan_potongan(args);
        Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} task_pembatalan_potongan`, description: util.inspect(api_response_fmcg)});
        for (let i = 0; i < api_response_fmcg.tasks.length; i++) {
            let user = await service_user.getOne({user_id: api_response_fmcg.tasks[i].user_id});
            api_response_fmcg.tasks[i].created_by = (user != null) ? user.fullname : '';
        }

        res.json(api_response_fmcg);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data pembatalan potongan yang sudah terupload, '+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function task_pembatalan_invoice(req, res) {
    const token = req.swagger.params.token.value;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_dt_code = req.swagger.params.dt_code;
    const param_email = req.swagger.params.email;
    const param_page = req.swagger.params.page;
    const param_item_per_page = req.swagger.params.item_per_page;
    const param_sorting = req.swagger.params.sorting;
    const param_sortingBy = req.swagger.params.sortingBy;

    let fmcg_id = _.isUndefined(param_fmcg_id.value)?null:param_fmcg_id.value;
    let dt_code = _.isUndefined(param_dt_code.value)?null:param_dt_code.value;
    let email = _.isUndefined(param_email.value)?null:param_email.value;
    let page = _.isUndefined(param_page.value)?0:param_page.value;
    let item_per_page = _.isUndefined(param_item_per_page.value)?15:param_item_per_page.value;
    let sorting = _.isUndefined(param_sorting.value)?null:param_sorting.value;
    let sortingBy = _.isUndefined(param_sortingBy.value)?null:param_sortingBy.value;
    let data_unique_code = '';

    // normalize startPage to 1
    page = (page < 1 ) ? 1 : page;

    try {
        Logs.create({action: 'task_pembatalan_invoice', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_task_list, is_allowed_access_token_static: false});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});

        if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal menampilkan data pembatalan invoice yang sudah terupload, data fmcg tidak tersedia');
        }

        if(auth_user.dataValues.type == 1 || auth_user.dataValues.type == 3) {
            let result_unique_code = await service_user_unique_code.get({user_id: auth_user.dataValues.user_id});

            await result_unique_code.map( (val, index) => {
                if(index == 0) {
                    data_unique_code += "'"+val.unique_code+"'";
                } else {
                    data_unique_code += ",'"+val.unique_code+"'";
                }
            });

            dt_code = data_unique_code;
        }

        let args = {};
        args.url = result_fmcg.dataValues.url;
        args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        args.fmcg_name = result_fmcg.dataValues.name;
        args.dt_code = '"'+dt_code+'"';
        args.email = email;
        args.page = page;
        args.item_per_page = item_per_page;
        args.sorting = sorting;
        args.sortingBy = sortingBy;
        args.usertype = auth_user.dataValues.type;

        let api_response_fmcg = await ApiUli.get_task_list_pembatalan_invoice(args);

        Logs.create({action: `api response fmcg ${result_fmcg.dataValues.name} task_pembatalan_invoice`, description: util.inspect(api_response_fmcg)});

        for (let i = 0; i < api_response_fmcg.tasks.length; i++) {
            let user = await service_user.getOne({user_id: api_response_fmcg.tasks[i].user_id});
            api_response_fmcg.tasks[i].created_by = (user != null) ? user.fullname : '';
        }

        res.json(api_response_fmcg);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data pembatalan invoice yang sudah terupload, '+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function create_retur(req,res){
    const token = req.swagger.params.token.value;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_amount_potongan = req.swagger.params.amount_potongan;
    const param_dt_code = req.swagger.params.dt_code;
    const param_le_code = req.swagger.params.le_code;
    const param_invoice_id = req.swagger.params.invoice_id;
    const param_remark_potongan = req.swagger.params.remark_potongan;

    let amount_potongan = _.isUndefined(param_amount_potongan.value)? 0 : param_amount_potongan.value;
    let dt_code = _.isUndefined(param_dt_code.value)? null : param_dt_code.value;
    let le_code = _.isUndefined(param_le_code.value)? null : param_le_code.value;
    let invoice_id = _.isUndefined(param_invoice_id.value)? null : param_invoice_id.value;
    let remark_potongan = _.isUndefined(param_remark_potongan.value)? null : param_remark_potongan.value;
    let fmcg_id = _.isUndefined(param_fmcg_id.value)? 13 : param_fmcg_id.value;
    
    try {
        Logs.create({action: 'create retur', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_create_retur, is_allowed_access_token_static: false});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});

        let args = {
            url : result_fmcg.dataValues.url,
            fmcg_token : result_fmcg.dataValues.fmcg_token,
            fmcg_name : result_fmcg.dataValues.name,
            amount_potongan : amount_potongan,
            le_code : le_code,
            dt_code : dt_code,
            remark_potongan:remark_potongan,
            invoice_id :invoice_id
        }

        let create_retur_res = await ApiTop.create_retur(args);
        res.status(200).json({create_retur_res})
        
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data potongan, '+err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}


export { get_invoices }
export { get_one_invoice }
export { check_process_invoice }
export { get_retur }
export { get_unbound_retur }
export { task_list }
export { task_potongan }
export { task_pembatalan_potongan }
export { task_pembatalan_invoice }
export { save_csv }
export { save_xls }
export { process_invoice }
export { upload_pot }
export { upload_pembatalan_pot }
export { upload_pembatalan_inv }
export { create_retur }