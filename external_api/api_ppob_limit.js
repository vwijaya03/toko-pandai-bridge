import request from 'request'
import util from 'util'

import { Logs } from '~/orm/index'

import ResponseAdapter from '~/adapter/ResponseAdapter'
import FmcgGetInvoicesError from '~/response/error/FmcgGetInvoicesError'
import PPOBCheckPriceError from '~/response/error/PPOBCheckPriceError'
import PPOBInquiryError from '~/response/error/PPOBInquiryError'
import PPOBPaymentServiceError from '~/response/error/PPOBPaymentServiceError'
import PPOBTopupServiceError from '~/response/error/PPOBTopupServiceError'
import PPOBReceiptError from '~/response/error/PPOBReceiptError'

const protocol = 'http://';
const host = '192.168.12.2';
const prefix = '/api/v1/';

const url_ppob_receipt = protocol+host+prefix+'trx_his/detil';
const url_ppob_price_service = protocol+host+prefix+'ppob_limit/price_service';
const url_ppob_topup_service = protocol+host+prefix+'ppob_limit/topup_service';
const url_ppob_inquiry_service = protocol+host+prefix+'ppob/inquiry_service';
const url_ppob_payment_service = protocol+host+prefix+'ppob_limit/payment_service';
const api_path_get_invoice = '/get-invoice';
const api_path_get_one_invoice = '/get-one-invoice';
const api_path_top_up_service = '/top-up';
const api_path_repayment = '/repayment';

//let token = 'webKEY123456789';




function commonHeaders(token) {
    return {
        'Accept': 'application/json',
        'Cache-Control': 'no-cache',
        'apigatekey': 'webKEY123456789'
    };
}

function commonHeaderslimit(token) {
    return {
        'Cache-Control': 'no-cache',
        'token': token
    };
}


function handleResponse(rawJsonResponse, errorClass, resolve, reject, requestError, function_name, fmcg_name) {

	try {
		let successJsonResp = JSON.parse(rawJsonResponse);
                console.log(successJsonResp)
		if(requestError != null) {
			reject(new errorClass(requestError));
		}

		if(successJsonResp.code == 400 || successJsonResp.code == 401) {
			reject(new errorClass(successJsonResp.message));
		} else {
			resolve(successJsonResp);
		}
	} catch(err) {
		Logs.create({action: `error ${function_name} ${fmcg_name}`, description: util.inspect(err), additional_description: util.inspect(rawJsonResponse)});

		reject(new errorClass('terjadi kesalahan silahkan coba lagi'));
	}

}

function handleResponseError(errorClass, resolve, reject, err, function_name, fmcg_name) {
	Logs.create({action: `error ${function_name} ${fmcg_name}`, description: util.inspect(err)});

	reject(new errorClass(err));
}

function get_invoice({url=null, fmcg_token=null, fmcg_name=null, user_id=null, vaccount=null, paid_unpaid_status=null, invoice_id=null, filter1=null, filter2=null, 
    page=null, items_per_page=null, sorting=null}={}){
    
    let args = {}
    args.url = url;
    args.fmcg_token = fmcg_token;
    args.fmcg_name = fmcg_name;
    args.user_id = user_id;
    args.vaccount = vaccount;
    args.paid_unpaid_status = paid_unpaid_status;
    args.invoice_id = invoice_id;
    args.filter1 = filter1;
    args.filter2 = filter2;
    args.page = page;
    args.items_per_page = items_per_page;
    args.sorting = sorting;
	return new Promise(function(resolve, reject) {

        let headers = commonHeaderslimit(fmcg_token);
        let url_get_invoice = url+api_path_get_invoice;
	    request.get( {url:url_get_invoice, headers:headers, qs:args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}

function get_one_invoice({url=null, fmcg_token=null, fmcg_name=null, user_id=null, invoice_id=null}={}){

    let args = {}
    args.url = url;
    args.fmcg_token = fmcg_token;
    args.fmcg_name = fmcg_name;
    args.user_id = user_id;
    args.invoice_id = invoice_id;
	return new Promise(function(resolve, reject) {

        let headers = commonHeaderslimit(fmcg_token);
        let url_get_one_invoice = url+api_path_get_one_invoice;
	    request.get( {url:url_get_one_invoice, headers:headers, qs:args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}

	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}

function top_up_service({url=null, fmcg_token=null, fmcg_name=null, user_id=null, vaccount=null, model=null,
    product_code=null,customer_number=null,pin=null}={}){

    let args = {}
    args.url = url;
    args.fmcg_token = fmcg_token;
    args.fmcg_name = fmcg_name;
    args.user_id = user_id;
    args.vaccount = vaccount;
    args.model = model;
    args.product_code = product_code;
    args.customer_number = customer_number;
    args.pin = pin;

	return new Promise(function(resolve, reject) {

        let headers = commonHeaderslimit(fmcg_token);
        let url_top_up_service = url+api_path_top_up_service;
	    request.post( {url:url_top_up_service, headers:headers, qs:args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}

	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}

function repayment({url=null, fmcg_token=null, fmcg_name=null, user_id=null, vaccount=null, invoice_id=null}={}){

    let args = {}
    args.url = url;
    args.fmcg_token = fmcg_token;
    args.fmcg_name = fmcg_name;
    args.user_id = user_id;
    args.vaccount = vaccount;
    args.invoice_id = invoice_id;

	return new Promise(function(resolve, reject) {

        let headers = commonHeaderslimit(fmcg_token);
        let url_repayment = url+api_path_repayment;
	    request.post( {url:url_repayment, headers:headers, qs:args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}

	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}

function ppob_topup_service({model=null, product_code=null, customer_number1=null, vaccount=null, pin=null}={}) {

    return new Promise(function(resolve, reject) {
        let form = {model:model, product_code:product_code, customer_number1:customer_number1, vaccount: vaccount, pin:pin};
        let headers = commonHeaders();
        request.post( {url:url_ppob_topup_service, headers:headers, form:form}, function(err, httpResponse, body) {
            if(err) {
	    		return err;
            }
            
            handleResponse(body, PPOBTopupServiceError, resolve, reject, err, 'ppob_topup_service');
        }).on('error', function(err) {
			handleResponseError(PPOBTopupServiceError, resolve, reject, err, 'ppob_topup_service');
		});
    });
}

function ppob_check_price({product_type=null, provider=null}={}) {

    return new Promise(function(resolve, reject) {
        let form = { product_type: product_type, provider: provider};
        let headers = commonHeaders();

        request.post( {url:url_ppob_price_service, headers:headers, form:form}, function(err, httpResponse, body) {
            if(err) {
	    		return err;
            }
            
            handleResponse(body, PPOBCheckPriceError, resolve, reject, err, 'ppob_check_price');
        }).on('error', function(err) {
			handleResponseError(PPOBCheckPriceError, resolve, reject, err, 'ppob_check_price');
		});
    });
}

function ppob_payment_service({product_code=null, customer_number1=null, customer_number2=null, customer_number3=null, nominal=null, ref_number=null, misc=null, vaccount=null, pin=null}={})
{
    return new Promise(function(resolve, reject) {
        let form = {product_code:product_code, customer_number1:customer_number1, customer_number2:customer_number2, customer_number3:customer_number3, nominal:nominal, ref_number:ref_number, misc:misc, vaccount:vaccount, pin:pin};
        let headers = commonHeaders();

        request.post( {url:url_ppob_payment_service, headers:headers, form:form}, function(err, httpResponse, body) {
            if(err) {
                return err;
            }
            
            handleResponse(body, PPOBPaymentServiceError, resolve, reject, err, 'ppob_payment_service');
        }).on('error', function(err) {
            handleResponseError(PPOBPaymentServiceError, resolve, reject, err, 'ppob_payment_service');
        });
    });
}

function one_invoice_manual({url=null, fmcg_token=null, fmcg_name=null, invoice_id=null, vacc_number=null}={}){

    let args = {}
    args.url = url;
    args.fmcg_token = fmcg_token;
    args.fmcg_name = fmcg_name;
    args.invoice_id = invoice_id;
    args.vacc_number = vacc_number;
    return new Promise(function(resolve, reject) {

        let headers = commonHeaderslimit(fmcg_token);
        let url_one_invoice_manual = url+api_path_one_invoice_manual;
        request.get( {url:url_one_invoice_manual, headers:headers, qs:args}, function(err, httpResponse, body) {
            if(err) {
                return err;
            }

            handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
        }).on('error', function(err) {
            handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
        });
    });
}

let obj = {}
obj.get_invoice = get_invoice
obj.get_one_invoice = get_one_invoice
obj.top_up_service = top_up_service
obj.repayment = repayment
obj.ppob_topup_service = ppob_topup_service
obj.ppob_check_price =ppob_check_price
obj.ppob_payment_service = ppob_payment_service
obj.one_invoice_manual = one_invoice_manual

export default obj

