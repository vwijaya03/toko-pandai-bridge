import { orm } from '~/services/mariadb'
import Sequelize from 'sequelize'

const Otp = orm.define('otps', {
    id: { type: Sequelize.INTEGER, field: 'id', primaryKey: true, autoIncrement: true },
    ipaddress: { type: Sequelize.STRING, allowNull:true },
    secret_key: { type: Sequelize.STRING, allowNull:true },
    token_user: { type: Sequelize.STRING, allowNull:true },

    otp: { type: Sequelize.STRING, allowNull:true },
    valid: { type: Sequelize.BOOLEAN, allowNull:true },

    purpose: { type: Sequelize.INTEGER, allowNull:true },
    to: { type: Sequelize.STRING, allowNull:true },

    createdAt: { type: 'TIMESTAMP', allowNull:true },
    updatedAt: { type: 'TIMESTAMP', allowNull:true }
});

export default Otp


/*

0 - general

1 - change imei

2 - reset password

3 - ganti phone

4 - ganti email

*/
