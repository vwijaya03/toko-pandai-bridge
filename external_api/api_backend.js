import request from 'request'
import util from 'util'

import { Logs } from '~/orm/index'

import ResponseAdapter from '~/adapter/ResponseAdapter'

const url_notif = "192.168.12.7:10013";


let tokenkey = 'gUxOsYYXbeywpM2xaV37RaNvdZ5nJdI8d8AdYvy4';

import FmcgGetInvoicesError from '~/response/error/FmcgGetInvoicesError'
import FmcgCheckProcessInvoiceError from '~/response/error/FmcgCheckProcessInvoiceError'

function commonHeaders() {
    return {
        'Accept': 'application/json',
        'Cache-Control': 'no-cache',
        'apigatekey': 'webKEY123456789'
    };
}

function authHeaders(headers) {
    headers = headers || {};
    headers['Authorization'] = 'Bearer ' + tokenkey;
    return headers;
}

function handleResponse(rawJsonResponse, errorClass, resolve, reject, requestError, function_name, fmcg_name) {

	try {
		let successJsonResp = JSON.parse(rawJsonResponse);
	
		if(requestError != null) {
			reject(new errorClass(requestError));
		}

		if(successJsonResp.code == 400 || successJsonResp.code == 401) {
			reject(new errorClass(successJsonResp.message));
		} else {
			resolve(successJsonResp);
		}
	} catch(err) {
		Logs.create({action: `error ${function_name} ${fmcg_name}`, description: util.inspect(err), additional_description: util.inspect(rawJsonResponse)});

		reject(new errorClass('terjadi kesalahan silahkan coba lagi'));
	}
	
}

function handleResponses(rawJsonResponse, errorClass, resolve, reject, requestError, function_name) {
    try {
		let {error:err, response:successJsonResp, pagination: pagination} = ResponseAdapter.parse({response:JSON.parse(rawJsonResponse)});
        
		if(requestError != null) {
			reject(new errorClass(requestError));
		}

		if(successJsonResp.code == 400 || successJsonResp.code == 401 || err) {
			reject(new errorClass(err.message));
		} else {
                    resolve(successJsonResp);
		}
	} catch(err) {
		Logs.create({action: `error ${function_name}`, description: util.inspect(err), additional_description: util.inspect(rawJsonResponse)});

		reject(new errorClass('terjadi kesalahan silahkan coba lagi'));
    }
}

function handleResponseError(errorClass, resolve, reject, err, function_name) {
	Logs.create({action: `error ${function_name}`, description: util.inspect(err)});
	
	reject(new errorClass(err));
}


function update_version({user_id=null, version=null}={}) {
     return new Promise(function(resolve, reject) {
     let url= 'http://'+url_notif+'/updatedVersion';
          let args ={  user_id: user_id, version: version }
          let headers={ 'Postman-Token': '3f73a5de-f0f9-4c47-9947-096dad7321a8',
               'cache-control': 'no-cache',
               'Content-Type': 'application/json',
               token: '1f8l4ckp1nk1ny0ur4r34th3n91rlS93n3r4t10nm4k3y0urf33lth3h34tth1S1SSh0S13y0ukn0w' }
            request.post( {url:url, headers:headers, form: args}, function(err, httpResponse, body) {
            if(err) {
                return err;
            }
            handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'update_version', "");
        }).on('error', function(err) {
            handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'update_version', "");
        });
     });
}

function get_running_text({user_id=null,token=null}={}) {
    return new Promise(function(resolve, reject) {
          let url= `http://192.168.12.7:10013/notifications/running-text`;
          let args ={ user_id }
          let headers={ 'Postman-Token': '3f73a5de-f0f9-4c47-9947-096dad7321a8',
               'cache-control': 'no-cache',
               'Content-Type': 'application/json',
               token: token }
            request.get( {url:url, headers:headers, qs: args}, function(err, httpResponse, body) {
            if(err) {
                return err;
            }
            handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get_running_text', "");
        }).on('error', function(err) {
            handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get_running_text', "");
        });
    });
}

function getdatalecode({le_code=null}={}) {
    return new Promise(function(resolve, reject) {
     let url= `http://192.168.12.7:10012/v2/toko`;
          let args ={ le_code: le_code };
          let headers={ 'Postman-Token': '3f73a5de-f0f9-4c47-9947-096dad7321a8',
               'cache-control': 'no-cache',
               'Content-Type': 'application/json',
               token: '1f8l4ckp1nk1ny0ur4r34th3n91rlS93n3r4t10nm4k3y0urf33lth3h34tth1S1SSh0S13y0ukn0w' }
            request.get( {url:url, headers:headers, qs: args}, function(err, httpResponse, body) {
            if(err) {
                return err;
            }
            handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'getdatalecode', "");
        }).on('error', function(err) {
            handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'getdatalecode', "");
        });
     });
}

function agent_response({le_code=null, dt_code=null,response=null,invoice_id=null}={}) {
    return new Promise(function(resolve, reject) {
     let url= `http://192.168.12.7:10013/agent/response`;
          let args ={ le_code: le_code,
           dt_code: dt_code,
           response: response,
           invoice_id: invoice_id }
          let headers={ 'Postman-Token': '3f73a5de-f0f9-4c47-9947-096dad7321a8',
               'cache-control': 'no-cache',
               'Content-Type': 'application/json',
               token: '1f8l4ckp1nk1ny0ur4r34th3n91rlS93n3r4t10nm4k3y0urf33lth3h34tth1S1SSh0S13y0ukn0w' }
            request.post( {url:url, headers:headers, form: args}, function(err, httpResponse, body) {
            if(err) {
                return err;
            }
            handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'agent_response', "");
        }).on('error', function(err) {
            handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'agent_response', "");
        });
     });
}

function agent_faktur({le_code=null,dt_code=null,invoice_id=null}={}) {
    return new Promise(function(resolve, reject) {
          let url= `http://192.168.12.7:10013/agent/faktur`;
          let args ={ le_code: le_code,dt_code:dt_code, invoice_id: invoice_id }
          let headers={ 'Postman-Token': '3f73a5de-f0f9-4c47-9947-096dad7321a8',
               'cache-control': 'no-cache',
               'Content-Type': 'application/json',
               token: '1f8l4ckp1nk1ny0ur4r34th3n91rlS93n3r4t10nm4k3y0urf33lth3h34tth1S1SSh0S13y0ukn0w' }
            request.get( {url:url, headers:headers, qs: args}, function(err, httpResponse, body) {
            if(err) {
                return err;
            }
            handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'agent_faktur', "");
        }).on('error', function(err) {
            handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'agent_faktur', "");
        });
    });
}


function agent_ceknik({nik_agent=null,invoice_id=null}={}) {
    return new Promise(function(resolve, reject) {
        let url= `http://192.168.12.7:10013/agent/cek`;
          let args ={ nik_agent: nik_agent, invoice_id: invoice_id }
          let headers={ 'Postman-Token': 'c036372f-f3b5-4c30-810c-23948f0e436f',
               'cache-control': 'no-cache',
               'Content-Type': 'application/json',
               token: '1f8l4ckp1nk1ny0ur4r34th3n91rlS93n3r4t10nm4k3y0urf33lth3h34tth1S1SSh0S13y0ukn0w' }
            request.get( {url:url, headers:headers, qs: args}, function(err, httpResponse, body) {
            if(err) {
                return err;
            }
            handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'agent_ceknik', "");
        }).on('error', function(err) {
            handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'agent_ceknik', "");
        });
    });
}


function cek_transactions({fmcg_id=null,invoice_id=null,dt_code=null}={}) {
    return new Promise(function(resolve, reject) { 
        let url= `http://192.168.12.7:10012/v2/transactions`;
          let args ={ dt_code: dt_code,
           invoice_id: invoice_id,
           fmcg_id: fmcg_id }
          let headers={ 'Postman-Token': '758eb9ae-c215-4352-ad3d-f63db8d913ce',
           'cache-control': 'no-cache',
           token: '1f8l4ckp1nk1ny0ur4r34th3n91rlS93n3r4t10nm4k3y0urf33lth3h34tth1S1SSh0S13y0ukn0w',
           'Content-Type': 'application/json' } 
            request.get( {url:url, headers:headers, qs: args}, function(err, httpResponse, body) {
            if(err) {
                return err;
            }
            handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'cek_transactions', "");
        }).on('error', function(err) {
            handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'cek_transactions', "");
        });
       
    });
}

function notifications_response({le_code=null, dt_code=null,confirm=null,invoice_id=null,token=null}={}) {
    return new Promise(function(resolve, reject) {
     let url= `http://192.168.12.7:10013/notifications/respon`;
          let args ={  le_code: le_code,
           dt_code: dt_code,
           invoice_id: invoice_id,
           confirm: confirm }
          let headers={ 'Postman-Token': '3f73a5de-f0f9-4c47-9947-096dad7321a8',
               'cache-control': 'no-cache',
               'Content-Type': 'application/json',
               token: '1f8l4ckp1nk1ny0ur4r34th3n91rlS93n3r4t10nm4k3y0urf33lth3h34tth1S1SSh0S13y0ukn0w' }
            request.post( {url:url, headers:headers, qs: args}, function(err, httpResponse, body) {
            if(err) {
                return err;
            }
            handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'notifications_response', "");
        }).on('error', function(err) {
            handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'notifications_response', "");
        });
     });
}

function get_aca_produk({token=null}={}) {
    return new Promise(function(resolve, reject) {
          let url= 'http://192.168.12.6:21002/insurance/get-product';
          let args ="";
          let headers={token:token};
            request.get( {url:url, headers:headers, qs: args}, function(err, httpResponse, body) {
            if(err) {
                return err;
            }
            handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get_aca_produk', "");
        }).on('error', function(err) {
            handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get_aca_produk', "");
        });
    });
}

function getAllInsuranceData({page=null,items_per_page=null,token=null}={}) {
    return new Promise(function(resolve, reject) {
          let url= `http://192.168.12.6:21002/insurance/get-all-customer-detail?page=${page}&items_per_page=${items_per_page}`;
          let args ="";
          let headers={token:token};
            request.get( {url:url, headers:headers, qs: args}, function(err, httpResponse, body) {
            if(err) {
                return err;
            }
            handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'getAllInsuranceData', "");
        }).on('error', function(err) {
            handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'getAllInsuranceData', "");
        });
    });
}

function post_lecode_akuisisi({le_code=null, nama_toko=null,alamat=null}={}) {
    return new Promise(function(resolve, reject) {
     let url= `http://support.tokopandai.id:9002/api_agenttokopandai/v2/updateGP`;
     let args ={ le_code: le_code,
           le_code: le_code,
           nama_toko: nama_toko,
           alamat: alamat }
     let headers={ 'Postman-Token': 'b06b7b98-6db3-4e58-a5c3-8cc3194da8df',
            'cache-control': 'no-cache',
            'Content-Type': 'application/json',
            'x-api-key': '$2a$10$QNB/3KKnXvzSRQMd/stp1eDEHbtZHlAaKfeTKKJ9R5.OtUnEgnrA6' }
            request.post( {url:url, headers:headers, form: args}, function(err, httpResponse, body) {
            if(err) {
                return err;
            }
            handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'post_lecode_akuisisi', "");
        }).on('error', function(err) {
            handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'post_lecode_akuisisi', "");
        });
     });
}

function post_toko_grosir({le_code=null, dt_code=null,nama_toko=null,fmcg_id=null,email=null,firstname=null,lastname=null,phone=null,address=null}={}) {
    return new Promise(function(resolve, reject) {
     let url= `https://orderpandai.tokopandai.id/api/40/toko`;
          let args ={ 
            le_code: le_code,
            dt_code: dt_code,
            fmcg_id: fmcg_id,
            email: email,
            firstname: firstname,
            lastname: lastname,
            address: address,
            phone: phone,
            limit: 0,
            limit_mutasi_tipe: 'plus',
            jenis_transaksi: 'cicilan'
          }
          let headers={ 'Postman-Token': '9b7cdd70-c0a6-4e1a-aa7b-de636e619507',
                'cache-control': 'no-cache',
                'Content-Type': 'application/json',
                Authorization: 'Basic c3luY2RhdGFAbG9jYWxob3N0LnRlc3Q6cTU2bjN6eEk0VWxYMXk0amU3YkE3MzhyMTg0NzMxSjQ=',
                Accept: 'application/json' }
            request.post( {url:url, headers:headers, form: args}, function(err, httpResponse, body) {
            if(err) {
                return err;
            }
            handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'post_toko_grosir', "");
        }).on('error', function(err) {
            handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'post_toko_grosir', "");
        });
     });
}

function updateGiroPayment({invoice_id=null, dt_code=null,fmcg_id=null}) {
    return new Promise(function(resolve, reject) {
     let url= `http://192.168.12.7:10024/payment/confirm`;
     let args ={ 
      fmcg_id:fmcg_id,
      dt_code:dt_code,
      invoice_id:invoice_id,
      status:'success',
      desc:'',
     }
     console.log(args)
    let headers={ 'Postman-Token': 'aeb5e18f-b143-4ab9-a2fa-9fa3410e0cf1',
     'cache-control': 'no-cache',
     'Content-Type': 'application/json',
     Accept: 'application/json',
     token: '1f8l4ckp1nk1ny0ur4r34th3n91rlS93n3r4t10nm4k3y0urf33lth3h34tth1S1SSh0S13y0ukn0w' };
            request.post({url:url, headers:headers, form: args}, function(err, httpResponse, body) {
            if(err) {
                return err;
            }
            handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'Giro Payment', "");
        }).on('error', function(err) {
            handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'Giro Payment', "");
        });
     });
}

function send_message_gratika({pesan=null,phone_no=null}={}) {
     return new Promise(function(resolve, reject) {
     let url= 'http://ginsms.gratika.id:8100/sendSms.do?';
     let args ={
         username:"tokopandai",
         password:"TokoPandai1234",
         senderid:"TOKOPANDAI",
         pesan: pesan,
         msisdn: phone_no
        }
         let headers={}
            request.post( {url:url, headers:headers, qs: args}, function(err, httpResponse, body) {
            if(err) {
                return err;
            }
            handleResponses(body, FmcgGetInvoicesError, resolve, reject, err, 'update_version', "");
        }).on('error', function(err) {
            handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'update_version', "");
        });
     });
}



let obj = {};

obj.update_version = update_version
obj.get_running_text = get_running_text
obj.getdatalecode = getdatalecode
obj.agent_response= agent_response
obj.agent_faktur = agent_faktur
obj.agent_ceknik = agent_ceknik
obj.cek_transactions = cek_transactions
obj.notifications_response = notifications_response
obj.get_aca_produk = get_aca_produk
obj.getAllInsuranceData = getAllInsuranceData
obj.post_lecode_akuisisi = post_lecode_akuisisi
obj.post_toko_grosir = post_toko_grosir
obj.updateGiroPayment =updateGiroPayment
obj.send_message_gratika = send_message_gratika

export default obj
