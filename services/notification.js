import { Notification } from '~/orm/index'

import NotificationNotFoundError from '~/response/error/NotificationNotFoundError'

async function get(filter, options) {

	options = options || {};
	let result_set = '';

	if(filter) {
        result_set = await Notification.findAll({where: filter});
    } else {
        result_set = await Notification.findAll();
    }

    if (!result_set || Object.keys(result_set).length == 0) {
        if (options.throwIfEmpty) {
            throw new NotificationNotFoundError();
        }
    }

    return result_set;
}

function addNotification({to=null, type=null, message=null, state=null, createdAt=null, updatedAt=null}={}) {

	let args = {
        to:            to,
        type:          type,
        message:       message,
        state:         state, 
        createdAt:     createdAt,
        updatedAt:     updatedAt
    }

    return Notification.create(args);
}

let obj = {};
obj.get = get;
obj.addNotification = addNotification;

export default obj