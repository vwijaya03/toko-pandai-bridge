import Sequelize from 'sequelize'
import notp from 'notp'

import service_notification from '~/services/notification'

import { Op } from '~/utils/helper'
import { Otp } from '~/orm/index'
import { otp_secret_key } from '~/properties'

async function addOtp({ipaddress=null, type=null, token_user=null, to=null, purpose=null,createdAt=null, updatedAt=null} = {}) {

//	let result_otp = await Otp.findOne({ 
//        where: { 
//            ipaddress: ipaddress, 
//            purpose: purpose,
//            to: to,
//            valid: true, 
//            createdAt: {
//                [Op.between]: [Sequelize.fn('DATE_SUB', Sequelize.fn('NOW'), Sequelize.literal('INTERVAL 5 MINUTE')), Sequelize.fn('NOW')]
//            }
//        }
//    });

//    if (!result_otp) {
    	let secret_key = (new Date()).getTime().toString() + token_user;
        let otp = notp.totp.gen(secret_key, {});
        let args = {
            ipaddress:      ipaddress,
            otp:            otp,
            secret_key:     secret_key,
            token_user:     token_user,
            to:             to,
            valid:          true,
            purpose:        purpose,
            createdAt:      createdAt,
            updatedAt:      updatedAt
        };

        let result_create_otp = await Otp.create(args);
        let message = { title:'OTP', body:"TokoPandai . OTP anda adalah " + result_create_otp.otp + " Berlaku untuk 5 menit"  };
        switch (purpose){
            case 2:{
                let url = `https://charlie.tokopandai.id/toko-pandai-api/v1/user-resetpassword-confirm?from=${encodeURIComponent(to)}&otp=${result_create_otp.otp}`;
                message.title = 'Konfirmasi Reset Password - Toko Pandai';
                message.body = `Untuk mereset password anda, klik <a href="${url}">Link ini</a><br/><br/>`+
                `${url}` +
                `<br/>berlaku untuk 5 menit.<br/><br/>`+
                `Abaikan email ini bila anda tidak ingin merubah password.`;
            } break;
        }
        let notification_args = {
            to: to,
            type: type,
            message: JSON.stringify(message),
            state: 0,
            createdAt: createdAt,
            updatedAt: updatedAt
        }

        service_notification.addNotification(notification_args);

        return result_create_otp;
//    } else {
//        return result_otp;
//    }
}

async function verifyOtp(token, otp, to, purpose) {

    let replacement = { 
        otp: otp,
        valid: true,
        to: to,
        purpose: purpose
    }
    
    if (token) {
        replacement.token_user = token; 
    }

    let response = await Otp.findOne({ where: replacement });

    if (response == null || response == undefined) {
        return [];
    }

    let valid = true;
    let verified = notp.totp.verify(response.otp, response.secret_key);

    if(!verified) {
        valid = false;
    } else {
        if (verified.delta < -10) {
            valid = false;
        }
    }

    await Otp.update({ valid: 0 }, { where: replacement});

    if (valid) {
        response.valid = false;
        return response;
    } else {
        return [];
    }
}

let obj = {}
obj.addOtp = addOtp
obj.verifyOtp = verifyOtp

export default obj
