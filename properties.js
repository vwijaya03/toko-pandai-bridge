import fs from 'fs'
import path from 'path'

const ACCESS_TOKEN_WEBADMIN = '80a16ffa-a7e3-11e6-80f5-76304dec7eb7';
const ACCESS_TOKEN_INTERNAL_WEBADMIN = '0913ad38-b9e6-4e72-a859-f1f359cf7dfb';
const ACCESS_TOKEN_INTERNAL_DEV = '2b3e6865-bdae-4881-ba71-ea9c5f123e8d';
const access_token_static = [];
access_token_static.push(ACCESS_TOKEN_WEBADMIN);
access_token_static.push(ACCESS_TOKEN_INTERNAL_WEBADMIN);
access_token_static.push(ACCESS_TOKEN_INTERNAL_DEV);

const moneyaccess_apikey = 'webKEY123456789';
const server_secret = 'w1thgre4tp0w3rc0m3sgr34tres0ns1b1l1ty';
const otp_secret_key = 'xrj-0nh#lb1fsw7p6k!)vdn36^ywbuzu)7h@rl6i-+f*@5%!ct';

const data = JSON.parse(fs.readFileSync(path.join(__dirname, 'config', 'database.json')).toString());

const allowed_user_status = [ {'status': '0'}, {'status': '1'} ];

const USERTYPES = {};

const ROLE_RETAILER = 0;
const ROLE_DISTRIBUTOR = 1;
const ROLE_PICKUP_AGENT = 2;
const ROLE_DISTRIBUTOR_MANAGER = 3;
const ROLE_SALES = 4;
const ROLE_UNILEVER_ADMIN = 9;
const ROLE_SUPER_ADMIN = 10;
const ROLE_CALL_CENTER = 11;
const ROLE_FINANCE = 12;

const RETAILER = {'retailer':ROLE_RETAILER}
const DISTRIBUTOR = {'distributor':ROLE_DISTRIBUTOR}
const DISTRIBUTOR_MANAGER = {'distributor_manager':ROLE_DISTRIBUTOR_MANAGER}
const SALES = {'sales':ROLE_SALES}
const PICKUP_AGENT = {'pickup_agent':ROLE_PICKUP_AGENT}
const UNILEVER_ADMIN = {'unilever_admin':ROLE_UNILEVER_ADMIN}
const SUPER_ADMIN = {'super_admin':ROLE_SUPER_ADMIN}
const CALL_CENTER = {'call_center':ROLE_CALL_CENTER}
const FINANCE = {'finance':ROLE_FINANCE}

Object.assign(USERTYPES, RETAILER, DISTRIBUTOR, DISTRIBUTOR_MANAGER, SALES, PICKUP_AGENT, SUPER_ADMIN, UNILEVER_ADMIN, CALL_CENTER,FINANCE);

export { data }
export { USERTYPES }
export { allowed_user_status }
export { ACCESS_TOKEN_WEBADMIN }
export { ACCESS_TOKEN_INTERNAL_WEBADMIN }
export { ACCESS_TOKEN_INTERNAL_DEV }
export { server_secret }
export { moneyaccess_apikey }
export { otp_secret_key }
export { access_token_static }
