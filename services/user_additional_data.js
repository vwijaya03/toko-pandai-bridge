import helper from '~/utils/helper'

import { UserAdditionalData } from '~/orm/index'

import UserAdditionalDataNotFoundError from '~/response/error/UserAdditionalDataNotFoundError'

async function get(filter, options) {
    options = options || {};
    let result_set = '';

    if(filter) {
        result_set = await UserAdditionalData.findAll({where: filter});
    } else {
        result_set = await UserAdditionalData.findAll();
    }

    if (!result_set || Object.keys(result_set).length == 0) {
        if (options.throwIfEmpty) {
            throw new UserAdditionalDataNotFoundError();
        }
    }

    return result_set;
}

function add({user_id=null, field=null, value=null}={}) {
    let args = {};
    args.user_id = user_id;
    args.field = field;
    args.value = value;
    
    return UserAdditionalData.create(args);
}

function edit({user_id=null, field=null, value=null}={}) {
    return UserAdditionalData.update(
        { value: value},
        { where: { user_id: user_id, field: field }}
    );
}

let obj = {}
obj.add = add
obj.edit = edit
obj.get = get

export default obj