function parse({response=null}={}) {
    
    let error = null;
    let hasError = false;
    let outResp = response;
    let pagination = null;

    try {
        if (response.status) {
            hasError = (response.status.code == 400);
            if (hasError) {
                error = {code:response.errors.error, message:response.errors.error_desc};
            } else {
                outResp = response.result;
                pagination = response.pagination;
            }
        }
    } catch (err) {
        
    }

    let out = {error:error, response:outResp, pagination:pagination};
    
    return out;
}

function parseMandiri({response=null}={}) {
    
    let error = null;
    let hasError = false;
    let outResp = response;
    let pagination = null;

    try {
        if (response.status) {
            hasError = (response.status.code == 400);
            if (hasError) {
                error = {code:response.errors.error, message:response.errors.error_desc};
            } else {
                outResp = response.mandiri_response;
                pagination = response.pagination;
            }
        }
    } catch (err) {
        
    }

    let out = {error:error, response:outResp, pagination:pagination};
    
    return out;
}

let obj = {};
obj.parse = parse;
obj.parseMandiri = parseMandiri;

export default obj