'use strict';

import helper from '~/utils/helper'

import { orm } from '~/services/mariadb'
import { UserUniqueCode } from '~/orm/index'
import { FMCG } from '~/orm/index'

import UserUniqueCodeNotFoundError from '~/response/error/UserUniqueCodeNotFoundError'
import UserUniqueCodeIdNotFoundError from '~/response/error/UserUniqueCodeIdNotFoundError'



function get_le_unicode({le_code=null}={}){
    try{
        return orm.query('select new_users.fullname,new_users.email,new_users.nama_toko,new_users.phone,new_users.tanggal_lahir,new_users.tempat_lahir,new_users.ktp,new_users.join_date,new_users.valdo_account,users_unique_codes.* from new_users join users_unique_codes on users_unique_codes.user_id=new_users.user_id where users_unique_codes.unique_code = "'+le_code+ '" and type=0', { type: orm.QueryTypes.SELECT})
    }catch(err){
    }
}

function get_dt_unicode({dt_code=null}={}){
    try{
        return orm.query('select new_users.fullname,new_users.email,new_users.nama_toko,new_users.phone,new_users.tanggal_lahir,new_users.tempat_lahir,new_users.ktp,new_users.join_date,new_users.valdo_account,users_unique_codes.* from new_users join users_unique_codes on users_unique_codes.user_id=new_users.user_id where users_unique_codes.unique_code = "'+dt_code+'" and type=1', { type: orm.QueryTypes.SELECT})
    }catch(err){
    }
}

function get_le_aditional({le_code=null}={}){
    try{
        return orm.query('select users_additional_datas.* from users_additional_datas join users_unique_codes on users_unique_codes.user_id=users_additional_datas.user_id where users_unique_codes.unique_code = "'+le_code+ '"', { type: orm.QueryTypes.SELECT})
    }catch(err){
    }
}
function get_le_aditional_name({user_id=null}={}){
    try{
        return orm.query('select * from users_additional_datas where user_id = "'+user_id+ '"', { type: orm.QueryTypes.SELECT})
    }catch(err){
    }
}


function get_dt_code({user_id=null}={}){
    try{
        return orm.query('select new_users.fullname,new_users.email,new_users.nama_toko,new_users.phone,new_users.tanggal_lahir,new_users.tempat_lahir,new_users.ktp,new_users.join_date,new_users.valdo_account,users_unique_codes.* from new_users join users_unique_codes on users_unique_codes.user_id=new_users.user_id where users_unique_codes.user_id = "'+user_id+'"', { type: orm.QueryTypes.SELECT})
    }catch(err){
    }
}
function get_agent(){
    try{
        return orm.query('select fullname,email,phone,valdo_account from new_users where type=2;',{ type: orm.QueryTypes.SELECT})
    }catch(err){
    }
}

function user_detail({fmcg_id=null, startPage=1,itemsPerPage=15,le_code=null}={}){
    startPage = (startPage < 1 ) ? 1 : startPage;
    const limit = itemsPerPage;
    const offset = ( startPage - 1 ) * itemsPerPage;
    try{
        return orm.query(`SELECT a.user_id, a.fmcg_id, a.unique_code as le_code, b.fullname AS nama, b.valdo_account, b.phone, c.VALUE AS nama_toko FROM users_unique_codes a JOIN new_users b ON a.user_id = b.user_id JOIN users_additional_datas c ON a.user_id = c.user_id where a.fmcg_id = ${fmcg_id} and a.unique_code =${le_code} limit ${limit} offset ${offset};`,{ type: orm.QueryTypes.SELECT})
    }catch(err){
    }
}

function countData({fmcg_id=null, startPage=1,itemsPerPage=15,le_code=null}={}){
    startPage = (startPage < 1 ) ? 1 : startPage;
    try{
        return orm.query(`SELECT COUNT(*) as total FROM users_unique_codes a JOIN new_users b ON a.user_id = b.user_id JOIN users_additional_datas c ON a.user_id = c.user_id where a.fmcg_id = ${fmcg_id} and a.unique_code = ${le_code} ;` , {type: orm.QueryTypes.SELECT})
    }catch(err){
    }
}

function detail_toko({user_id = null}={}){
    try{
        return orm.query(`SELECT new_users.user_id, new_users.fullname as nama, new_users.phone, new_users.valdo_account, users_additional_datas.value as nama_toko FROM new_users JOIN users_additional_datas ON new_users.user_id = users_additional_datas.user_id WHERE new_users.user_id = ${user_id}`,{ type: orm.QueryTypes.SELECT})
    }catch(err){
    }
}

function user_detail_by_lecode({fmcg_id=null, le_code=null}={}){
    try{
        return orm.query(`SELECT a.user_id, a.fmcg_id, a.unique_code as le_code, b.fullname AS nama,b.access_token as token, b.valdo_account, b.phone, c.VALUE AS nama_toko FROM users_unique_codes a JOIN new_users b ON a.user_id = b.user_id JOIN users_additional_datas c ON a.user_id = c.user_id where a.unique_code = '${le_code}'`,{ type: orm.QueryTypes.SELECT})
    }catch(err){
    }
}


let obj = {}
obj.get_dt_unicode = get_dt_unicode
obj.get_le_unicode = get_le_unicode
obj.get_dt_code = get_dt_code
obj.get_le_aditional = get_le_aditional
obj.get_le_aditional_name = get_le_aditional_name
obj.get_agent = get_agent
obj.user_detail = user_detail
obj.user_detail_by_lecode = user_detail_by_lecode
obj.countData = countData
obj.detail_toko = detail_toko

export default obj

