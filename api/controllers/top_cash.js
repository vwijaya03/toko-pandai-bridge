'use strict';
import _ from 'lodash'
import util from 'util'
import middleware from '~/api/controllers/auth'
import moment from 'moment'
import request from 'request'

import service_fmcg from '~/services/fmcg'
import service_user from '~/services/user'
import helper from '~/utils/helper'


import ApiTop from '~/external_api/api_top'
import ApiFmcg from '~/external_api/api_uli'
import ApiPohonDana from '~/external_api/api_top_pohon_dana'


import { Logs } from '~/orm/index'

import CustomError from '~/response/error/CustomError'

const acl_get_invoices = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];
const acl_get_one_invoice = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];
const acl_create_retur = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];

//
// let top_api = "tokopandai-development.pinjammodal.id";
// let top_api_key = "0ff0170259be55375551f943d9ebf04eb097d0b55661a5e8f13dac3ef9062820";

let top_api = "api.pinjammodal.id:8443/tokopandai";
let top_api_key = "904071eb7378b9f0cc7152b7ada253d7f9493e0e4a1ef9a0634ebf101b7e84ca"




async function apply_loan(req, res) {
    const token = req.swagger.params.token.value;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_le_code = req.swagger.params.le_code;
    const param_dt_code = req.swagger.params.dt_code;
    const param_amount = req.swagger.params.amount;
    const param_invoice_id = req.swagger.params.invoice_id;
    const param_principal_code = req.swagger.params.principal_code;
    const param_total_potongan = req.swagger.params.total_potongan;
    const param_tenor = req.swagger.params.tenor;
    const param_pin = req.swagger.params.pin;


    const iso8601format = "YYYY-MM-DD";
    
    let pin_user = _.isUndefined(param_pin.value)?null:param_pin.value;
    let fmcg_id = _.isUndefined(param_fmcg_id.value)?null:param_fmcg_id.value;
    let dt_code = _.isUndefined(param_dt_code.value) ? null : param_dt_code.value;
    let le_code = _.isUndefined(param_le_code.value) ? null : param_le_code.value;
    let amount = _.isUndefined(param_amount.value) ? null : param_amount.value;
    let invoice_number = _.isUndefined(param_invoice_id.value) ? null : param_invoice_id.value;
    let principal_code = _.isUndefined(param_principal_code.value) ? null : param_principal_code.value;
    let total_potongan = _.isUndefined(param_total_potongan.value) ? null : param_total_potongan.value;
    let tenor = _.isUndefined(param_tenor.value) ? null : param_tenor.value;
    let discounts =0;
    let data_unique_code = '';

    try {
        Logs.create({action: 'apply_loan', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_get_one_invoice, is_allowed_access_token_static: false});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});

        if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal menampilkan data apply loan, data fmcg tidak tersedia');
        }
        
        let result_fit = await service_fmcg.getOneFmcgWithFilter({id: 1});

        if(result_fit == null) {
            throw new CustomError(400, 'Gagal menampilkan data apply loan, data fmcg tidak tersedia');
        }
        
        let user_salt = auth_user.dataValues.salt;
        
        let newpin = await service_user.getHashedPassword(user_salt, pin_user.toString());
        if(auth_user.dataValues.pin != newpin) {
            throw new CustomError(400, 'Pin Salah');
        }
        let api_unpaid = await ApiPohonDana.check_fintech_unpaid({le_code:le_code});

        if (api_unpaid.totalInvoice !== 0){
          throw new CustomError(400, 'Maaf pengajuan limit tidak dapat diproses. Selesaikan pembayaran tagihan Anda');
        }
        let api_transact = await ApiPohonDana.checktransactions({dt_code:dt_code,invoice_id:invoice_number,fmcg_id:fmcg_id});
        if (api_transact.totalTransaksi != 0){
          throw new CustomError(400, 'Pengajuan sebagian ditolak');
        }

        if (amount < 100000){
          throw new CustomError(400, 'Invoice Minimal Rp. 100.000');
        }
    
        let args ={
           "dt_code": dt_code,
           "le_code": le_code,
           "invoice_number": invoice_number,
           "amount": amount,
           "tenor": tenor,
           "principle_code": fmcg_id,
           "discount": parseInt(total_potongan)
         };
       request({
           headers: {
           'Content-Type': 'application/json',
           'api_key' : top_api_key
           },
            url: "https://"+top_api+"/loan/apply-tokopandai",
            method: "POST",
            json: true,
            body: args
          }, function (error, response, body){
              if(body.status === 'success'){
                  let invoice_update = {};
                   invoice_update.le_code = le_code;
                   invoice_update.invoice_id = invoice_number;
                   invoice_update.amount = amount;
                   invoice_update.fmcg_id = fmcg_id;
                   invoice_update.paid_by = "Limit Pandai"
                   ApiPohonDana.update_payment_fmcg(invoice_update);
                   
                    
                   let argss = {
                    url: result_fit.dataValues.url,
                    fmcg_token : result_fit.dataValues.fmcg_token,
                    fmcg_name : result_fit.dataValues.name,
                    user_id:auth_user.dataValues.user_id,
                    le_code:le_code,
                    dt_code:dt_code,
                    amount:amount,
                    total_amount_invoice:amount,
                    invoice_id:invoice_number,
                    tenor:tenor,
                    principal_code:fmcg_id,
                    total_potongan:total_potongan,
                    loan_type : "Cash",
                    loan_by : "Pinjam Modal"
                  }
                  ApiTop.insert_loan_history(argss)
                   res.status(200).json(body);
              }else
              {
                  res.status(400).json(body);
              }

        });
        
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan update invoice, '+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}


async function calculator(req, res) {
    const token = req.swagger.params.token.value;
    const param_amount = req.swagger.params.amount;
    //const param_tenor = req.swagger.params.tenor;
    const iso8601format = "YYYY-MM-DD";

    let amount = _.isUndefined(param_amount.value) ? 0 : param_amount.value;
    let tenor = 1
    let data_unique_code = '';
    let fmcg_id = 13;
    let discounts = 0;
    var ten = amount.split(".", 1);

    try {
        Logs.create({action: 'calculator top', description: helper.getSimpleSwaggerParams(req.swagger.params)});


        let auth_user = await middleware.verify({token: token, acl: acl_get_invoices, is_allowed_access_token_static: false});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});

        let today = new Date()
        var weekday = new Array(7);
        weekday[0] = "Minggu";
        weekday[1] = "Senin";
        weekday[2] = "Selasa";
        weekday[3] = "Rabu";
        weekday[4] = "Kamis";
        weekday[5] = "Jumat";
        weekday[6] = "Sabtu";
        let day = weekday[today.getDay()];
        let current_time = today.getHours();
        let time_setting = moment(today.setHours(current_time)).format('HH:mm:ss');

//        if(day === 'Jumat' && time_setting > '17:00:00'){
//            throw new CustomError(400, 'Waktu transaksi telah ditutup!');
//        }
//        else if(day === 'Sabtu' || day === 'Minggu'){
//            throw new CustomError(400, 'Waktu transaksi telah ditutup!');
//        }

        if (tenor === 0) {
            throw new CustomError(400, 'Tenor tidak boleh 0');
        }
        if (amount < 100000) {
            throw new CustomError(400, 'Invoice Minimal Rp. 100.000');
        }
        request({
            headers: {
                'Content-Type': 'application/json'
            },
            url: "https://" + top_api,
            method: "GET",
            json: true
        }, function (error, response, body) {
            if (response.statusCode === 502) {
                res.status(400).json({code: response.statusCode, message: "PayLater Sedang Dalam Perbaikan"});
            } else {


                let args = {
                    "amount": parseInt(ten[0]),
                    "discount": 0,
                    "tenor": tenor
                }
                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'api_key': top_api_key
                    },
                    url: "https://" + top_api + "/loan/calculator-tokopandai",
                    method: "POST",
                    json: true,
                    body: args
                }, function (error, response, body) {
                    if (body.status === 'success') {
                    discounts = body.installmentLoanDetail[0].loan_payback - body.installmentLoanDetail[0].loan_amount;
                    }
                    
                    let dis = discounts / 100 * 0;
                    let data = {
                        "amount": parseInt(ten[0]),
                        "discount": parseInt(dis),
                        "tenor": tenor
                    }
                    request({
                        headers: {
                            'Content-Type': 'application/json',
                            'api_key': top_api_key
                        },
                        url: "https://" + top_api + "/loan/calculator-tokopandai",
                        method: "POST",
                        json: true,
                        body: data
                    }, function (error, response, body) {                       
                        let week = 0;
                        if (body.status === 'success') {
                             week = [{
                                "loan_amount": body.installmentLoanDetail[0].loan_amount + ".0000",
                                "loan_payback": body.installmentLoanDetail[0].loan_payback + ".0000",
                                "administrasi": (body.installmentLoanDetail[0].loan_payback + body.installmentLoanDetail[0].discount - body.installmentLoanDetail[0].loan_amount) + ".0000",
                                "loan_length": body.installmentLoanDetail[0].loan_length,
                                "installment": body.installmentLoanDetail[0].installment + ".0000",
                                "total_potongan": body.installmentLoanDetail[0].discount + ".0000",
                            }]
                         }
                         else
                         {
                             week = ({respon:body.message});
                         }
                        res.json({week});
                    });
                });
            }
        })
    } catch (err) {
        if (err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data calculator TOP, ' + err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function create_retur(req,res){
    const token = req.swagger.params.token.value;
    const param_amount_potongan = req.swagger.params.amount_potongan;
    const param_dt_code = req.swagger.params.dt_code;
    const param_le_code = req.swagger.params.le_code;
    const param_invoice_id = req.swagger.params.invoice_id;
    const param_remark_potongan = req.swagger.params.remark_potongan;

    let amount_potongan = _.isUndefined(param_amount_potongan.value)? 0 : param_amount_potongan.value;
    let dt_code = _.isUndefined(param_dt_code.value)? null : param_dt_code.value;
    let le_code = _.isUndefined(param_le_code.value)? null : param_le_code.value;
    let invoice_id = _.isUndefined(param_invoice_id.value)? null : param_invoice_id.value;
    let remark_potongan = _.isUndefined(param_remark_potongan.value)? null : param_remark_potongan.value;
    let fmcg_id =13;

    
    try {
        Logs.create({action: 'create retur', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_create_retur, is_allowed_access_token_static: false});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});

        let args = {
            url : result_fmcg.dataValues.url,
            fmcg_token : result_fmcg.dataValues.fmcg_token,
            fmcg_name : result_fmcg.dataValues.name,
            amount_potongan : amount_potongan,
            le_code : le_code,
            dt_code : dt_code,
            invoice_id :invoice_id,
            remark_potongan : remark_potongan
        }
        let create_retur_res = await ApiTop.create_retur(args)
        res.status(200).json(create_retur_res)
        
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data potongan, '+err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function calculator_new(req, res) {
    const token = req.swagger.params.token.value;
    const param_amount = req.swagger.params.amount;
    const param_tenor = req.swagger.params.tenor;
    const iso8601format = "YYYY-MM-DD";

    let amount = _.isUndefined(param_amount.value) ? 0 : param_amount.value;
    let tenor = _.isUndefined(param_tenor.value) ? 1 : param_tenor.value;
    let data_unique_code = '';
    let fmcg_id = 13;
    let discounts = 0;
    var ten = amount.split(".", 1);

    try {
        Logs.create({action: 'calculator top', description: helper.getSimpleSwaggerParams(req.swagger.params)});


        let auth_user = await middleware.verify({token: token, acl: acl_get_invoices, is_allowed_access_token_static: false});
       
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});

        let today = new Date()
        var weekday = new Array(7);
        weekday[0] = "Minggu";
        weekday[1] = "Senin";
        weekday[2] = "Selasa";
        weekday[3] = "Rabu";
        weekday[4] = "Kamis";
        weekday[5] = "Jumat";
        weekday[6] = "Sabtu";
        let day = weekday[today.getDay()];
        let current_time = today.getHours();
        let time_setting = moment(today.setHours(current_time)).format('HH:mm:ss');

//        if(day === 'Jumat' && time_setting > '17:00:00'){
//            throw new CustomError(400, 'Waktu transaksi telah ditutup!');
//        }
//        else if(day === 'Sabtu' || day === 'Minggu'){
//            throw new CustomError(400, 'Waktu transaksi telah ditutup!');
//        }

        if (tenor === 0) {
            throw new CustomError(400, 'Tenor tidak boleh 0');
        }
        if (amount < 100000) {
            throw new CustomError(400, 'Invoice Minimal Rp. 100.000');
        }
        request({
            headers: {
                'Content-Type': 'application/json'
            },
            url: "https://" + top_api,
            method: "GET",
            json: true
        }, function (error, response, body) {
            if (response.statusCode === 502) {
                res.status(400).json({code: response.statusCode, message: "PayLater Sedang Dalam Perbaikan"});
            } else {


                let args = {
                    "amount": parseInt(ten[0]),
                    "discount": 0,
                    "tenor": tenor
                }
                request({
                    headers: {
                        'Content-Type': 'application/json',
                        'api_key': top_api_key
                    },
                    url: "https://" + top_api + "/loan/calculator-tokopandai",
                    method: "POST",
                    json: true,
                    body: args
                }, function (error, response, body) {
                    if (body.status === 'success') {
                    discounts = body.installmentLoanDetail[0].loan_payback - body.installmentLoanDetail[0].loan_amount;
                    }
                    
                    let dis = discounts / 100 * 0;
                    let data = {
                        "amount": parseInt(ten[0])+1,
                        "discount": parseInt(dis),
                        "tenor": tenor
                    }
                    request({
                        headers: {
                            'Content-Type': 'application/json',
                            'api_key': top_api_key
                        },
                        url: "https://" + top_api + "/loan/calculator-tokopandai",
                        method: "POST",
                        json: true,
                        body: data
                    }, function (error, response, body) {                       
                        let week = 0;
                        if (body.status === 'success') {
                             week = [{
                                "loan_amount": body.installmentLoanDetail[0].loan_amount + ".0000",
                                "loan_payback": body.installmentLoanDetail[0].loan_payback + ".0000",
                                "administrasi": (body.installmentLoanDetail[0].loan_payback + body.installmentLoanDetail[0].discount - body.installmentLoanDetail[0].loan_amount) + ".0000",
                                "loan_length": body.installmentLoanDetail[0].loan_length,
                                "installment": body.installmentLoanDetail[0].installment + ".0000",
                                "total_potongan": body.installmentLoanDetail[0].discount + ".0000",
                            }]
                         }
                         else
                         {
                             week = ({respon:body.message});
                         }
                        res.json({week});
                    });
                });
            }
        })
    } catch (err) {
        if (err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data calculator TOP, ' + err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function get_tenor_toko(req, res) {
    const token = req.swagger.params.token.value;
    const param_amount = req.swagger.params.amount;
    const iso8601format = "YYYY-MM-DD";

    let amount = _.isUndefined(param_amount.value) ? 0 : param_amount.value;

    try {
        let tenor ={}
        if(parseInt(amount) > 2500000){
            tenor.week =
                  [{"loan_length": 1}]
        }
        else{
            tenor.week =
                [{"loan_length": 1}]
        }
      res.status(200).json(tenor)
    } catch (err) {
        if (err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data calculator TOP, ' + err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

export { get_tenor_toko }
export { apply_loan }
export { calculator }
export { create_retur }
export { calculator_new }