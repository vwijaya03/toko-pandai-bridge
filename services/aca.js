'use strict';

import moment from 'moment'

import { orm } from '~/services/mariadb'
import { Aca } from '~/orm/index'

function findProduct({id_KdProduk=null}){
    try{
        return orm.query('select name, KdProduk, jmlhPremi from aca_products where id = '+id_KdProduk, { type: orm.QueryTypes.SELECT })
    }
    catch(err) {
    }
}

function checking_data({Nama=null,KTP=null,KdProduk=null}){
    try{
        return orm.query('select count(*) as hasil from aca_insurance where Nama = "'+Nama+'" and KTP = "'+KTP+'" and KdProduk = "'+KdProduk+'"', {type: orm.QueryTypes.SELECT})
    } catch(err){
    }
}

function insert({user_id=null,id_KdProduk=null,KdProduk=null,tglTransaksi=null,kdTracking=null,kdsource=null,kdOutlet=null,
    Nama=null,KTP=null,MSISDN=null,Alamat=null,kodePos=null,tglLahir=null, AhliWaris=null,Email=null,
    Reff0=null,Reff1=null,Reff2=null,Reff3=null,Reff4=null,Reff5=null,Reff6=null, Reff7=null,img_file_name=null}={}) {

    return Aca.create(
    {
      user_id:user_id,
      id_KdProduk: id_KdProduk,
      KdProduk:KdProduk,
      tglTransaksi: tglTransaksi,
      kdTracking: kdTracking,
      kdOutlet: kdOutlet,
      kdsource: kdsource,
      Nama: Nama,
      KTP: KTP,
      MSISDN: MSISDN,
      Alamat: Alamat,
      kodePos: kodePos,
      tglLahir: tglLahir,
      AhliWaris: AhliWaris,
      Email: Email,
      Reff0: Reff0, Reff1: Reff1, Reff2: Reff2, Reff3: Reff3,
      Reff4: Reff4, Reff5: Reff5, Reff6: Reff6, Reff7: Reff7,
      createdAt: moment(Date.now()).format('YYYY-MM-DD h:mm:ss'),
      updatedAt: moment(Date.now()).format('YYYY-MM-DD h:mm:ss'),
      img_file_name :img_file_name,
    });
}

function saveImg({image=null,kdTracking=null,Nama=null}={}){
    return Aca.update({
        img_file_name : image
    },{where:{Nama:Nama, kdTracking:kdTracking}})
}

function updateResponse({NoTransaksi=null,TglRespon=null,tglAwal=null,tglAkhir=null,
    PIN=null,SerialNo=null,Premi=null,Barcode=null,SupportInfo=null,Response=null,ResponseDesc=null,kdTracking=null,Nama=null}={}){
        return Aca.update({
            NoTransaksi:NoTransaksi,
            TglRespon:TglRespon,
            tglAwal:tglAwal,
            tglAkhir:tglAkhir,
            PIN:PIN,
            SerialNo:SerialNo,
            Premi:Premi,
            Barcode:Barcode,
            SupportInfo:SupportInfo,
            Response:Response,
            ResponseDesc:ResponseDesc
        },{ where: {kdTracking:kdTracking, Nama:Nama}})
}

function updatePaymentSuccess({kdTracking=null,Nama=null}={}){
        return Aca.update({
            payment_status: 1
        },{ where: {kdTracking:kdTracking,Nama:Nama}})
}

function updatePaymentFailed({kdTracking=null,Nama=null}={}){
    return Aca.update({
        payment_status: 2
    },{ where: {kdTracking:kdTracking,Nama:Nama}})
}

function countData({PIN=null, user_id=null, sorting=null,sortingBy=null, startPage=1, itemsPerPage=15}={}){
    startPage = (startPage < 1 ) ? 1 : startPage;
    var orderByColumnQuery = '';
    var orderByAscOrDescQuery = '';
    var filterByNoPolis = '';

    if(PIN !== null){
        filterByNoPolis = ' and PIN = '+PIN
    }
    if(sorting == null)
    {
        orderByAscOrDescQuery = 'desc';
    }
    else
    {
        orderByAscOrDescQuery = sorting;
    }
    if(sortingBy == null)
    {
        orderByColumnQuery = 'order by aca_insurance.tglTransaksi '+orderByAscOrDescQuery;
    }
    else if(sortingBy == 'jenis_product')
    {
        orderByColumnQuery = 'order by aca_products.name '+orderByAscOrDescQuery;
    }

    try{
        return orm.query('SELECT count(*) as total from aca_insurance where user_id = '+user_id+filterByNoPolis+ ' ' + orderByColumnQuery,{type: orm.QueryTypes.SELECT})
    }
    catch(err){
    }
}

function getAllAca({PIN=null,user_id=null, sorting=null,sortingBy=null, startPage=1, itemsPerPage=15}={}){
    startPage = (startPage < 1 ) ? 1 : startPage;
    const limit = itemsPerPage;
    const offset = ( startPage - 1 ) * itemsPerPage;

    var orderByColumnQuery = '';
    var orderByAscOrDescQuery = '';
    var filterByNoPolis = '';

    if(PIN !== null){
        filterByNoPolis = ' and PIN = '+PIN
    }
    if(sorting == null)
    {
        orderByAscOrDescQuery = 'desc';
    }
    else
    {
        orderByAscOrDescQuery = sorting;
    }
    if(sortingBy == null)
    {
        orderByColumnQuery = 'order by aca_insurance.tglTransaksi '+orderByAscOrDescQuery;
    }
    else if(sortingBy == 'jenis_product')
    {
        orderByColumnQuery = 'order by aca_products.name '+orderByAscOrDescQuery;
    }

    try{
        return orm.query('SELECT NoTransaksi, kdTracking, PIN, SerialNo, Barcode, tglTransaksi, TglRespon, name AS jenis_asuransi, ProductDesc, KTP, Nama, Alamat, kodePos, MSISDN, Email, Premi, tglAwal, tglAkhir, Response, ResponseDesc, status, payment_status FROM  aca_insurance join aca_products on aca_insurance.id_KdProduk = aca_products.id where aca_insurance.Response = "00" and aca_insurance.user_id = '+user_id+filterByNoPolis+ ' ' + orderByColumnQuery +' limit '+ limit + ' offset '+offset, { type: orm.QueryTypes.SELECT})
    }
    catch(err) {
    }
}
function detail_toko({user_id = null}={}){
    try{
        return orm.query(`SELECT new_users.user_id, new_users.fullname as nama, new_users.phone, new_users.valdo_account, users_additional_datas.value as nama_toko FROM new_users JOIN users_additional_datas ON new_users.user_id = users_additional_datas.user_id WHERE new_users.user_id = ${user_id}`,{ type: orm.QueryTypes.SELECT})
    }catch(err){
    }
}

function saveCertificate({certificate=null,PIN=null}={}){
    return Aca.update({
        certificate: certificate
    },{ where: {PIN:PIN}})
}


let obj = {}
obj.insert = insert
obj.updateResponse = updateResponse
obj.updatePaymentSuccess = updatePaymentSuccess
obj.updatePaymentFailed = updatePaymentFailed
obj.findProduct = findProduct
obj.getAllAca = getAllAca
obj.countData = countData
obj.checking_data = checking_data
obj.saveImg = saveImg
obj.detail_toko = detail_toko
obj.saveCertificate = saveCertificate

export default obj

