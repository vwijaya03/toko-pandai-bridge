'use strict';
import _ from 'lodash'
import util from 'util'
import middleware from '~/api/controllers/auth'
import moment from 'moment'
import request from 'request'

import service_fmcg from '~/services/fmcg'
import service_user from '~/services/user'
import service_pelunasan_manual from '~/services/pelunasan_manual'
import helper from '~/utils/helper'



import ApiTop from '~/external_api/api_top'
import ApiFmcg from '~/external_api/api_uli'
import ApiPohonDana from '~/external_api/api_top_pohon_dana'
import MoneyAccess from '~/external_api/moneyaccess'

let apigatekey= 'webKEY123456789';
let url_trf = '192.168.12.2'


import { Logs } from '~/orm/index'

import CustomError from '~/response/error/CustomError'

const acl_calculator = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];
const acl_apply_loan = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];
const acl_get_invoices = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];
const acl_repayment = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];


async function calculator(req, res) {
    const token = req.swagger.params.token.value;
    const param_amount = req.swagger.params.amount;
    const iso8601format = "YYYY-MM-DD";

    let amount = _.isUndefined(param_amount.value) ? 0 : param_amount.value;
    let discount = 0;
    let tenor = 1
    let data_unique_code = '';
    let fmcg_top =5;
    
    
    try {
        Logs.create({action: 'calculator top', description: helper.getSimpleSwaggerParams(req.swagger.params)});
        let auth_user = await middleware.verify({token: token, acl: acl_calculator, is_allowed_access_token_static: false});
        let result_fmcg_top = await service_fmcg.getOneFmcgWithFilter({id: fmcg_top});   

        let invoice_args = {
            url: result_fmcg_top.dataValues.url,
            fmcg_token: result_fmcg_top.dataValues.fmcg_token,
            fmcg_name: result_fmcg_top.dataValues.name,
            amount: amount,
            tenor: tenor,
            discount: discount
        }
        let calculator = await ApiPohonDana.calculator(invoice_args)
        let week = {};
        if (calculator.status === 'Success') {
            let potongan = parseInt(calculator.week[0].administrasi)*0
                week = [{
                   "loan_amount": parseInt(calculator.week[0].loan_amount)+".0000",
                   "loan_payback": parseInt(calculator.week[0].loan_payback)-potongan+".0000",
                   "administrasi": parseInt(calculator.week[0].administrasi)+".0000",
                   "loan_length": calculator.week[0].loan_length,
                   "installment": parseInt((parseInt(calculator.week[0].loan_payback)-potongan)/calculator.week[0].loan_length)+".0000",
                   "total_potongan": potongan+".0000",
               }]
            }
            else
            {
                week = (calculator);
            }
            let weeks = {}
            weeks.status = calculator.status;
            weeks.message = calculator.status;
            weeks.week = week;
        
            res.status(200).json(weeks)
        
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data calculator TOP, '+err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function apply_loan(req, res) {
    const token = req.swagger.params.token.value;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_le_code = req.swagger.params.le_code;
    const param_dt_code = req.swagger.params.dt_code;
    const param_amount = req.swagger.params.amount;
    const param_invoice_id = req.swagger.params.invoice_id;
    const param_tenor = req.swagger.params.tenor;
    const param_principal_code = req.swagger.params.principal_code;
    const param_total_potongan = req.swagger.params.total_potongan;
    const param_pin = req.swagger.params.pin;


    const iso8601format = "YYYY-MM-DD";
    
    let pin_user = _.isUndefined(param_pin.value)?null:param_pin.value;
    let fmcg_id = _.isUndefined(param_fmcg_id.value)?null:param_fmcg_id.value;
    let dt_code = _.isUndefined(param_dt_code.value) ? null : param_dt_code.value;
    let le_code = _.isUndefined(param_le_code.value) ? null : param_le_code.value;
    let amount = _.isUndefined(param_amount.value) ? null : param_amount.value;
    let invoice_number = _.isUndefined(param_invoice_id.value) ? null : param_invoice_id.value;
    let tenor = _.isUndefined(param_tenor.value) ? null : param_tenor.value;
    let principal_code = _.isUndefined(param_principal_code.value) ? null : param_principal_code.value;
    let total_potongan = _.isUndefined(param_total_potongan.value) ? null : param_total_potongan.value;
    let discounts = '';
    let data_unique_code = '';

    try {
        Logs.create({action: 'apply_loan', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_apply_loan, is_allowed_access_token_static: false});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});
        if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal menampilkan data apply loan, data fmcg tidak tersedia');
        }
        let result_fmcg_top = await service_fmcg.getOneFmcgWithFilter({id: 5});
        if(result_fmcg_top == null) {
            throw new CustomError(400, 'Gagal menampilkan data apply loan, data fmcg tidak tersedia');
        }

        let api_unpaid = await ApiPohonDana.check_fintech_unpaid({le_code:le_code});

        if (api_unpaid.totalInvoice !== 0){
          throw new CustomError(400, 'Maaf pengajuan limit tidak dapat diproses. Selesaikan pembayaran tagihan Anda');
        }

     
        let api_transact = await ApiPohonDana.checktransactions({dt_code:dt_code,invoice_id:invoice_number,fmcg_id:fmcg_id});

        if (api_transact.totalTransaksi !== 0){
          throw new CustomError(400, 'Pengajuan sebagian ditolak');
        }


        if (amount < 100000){
          throw new CustomError(400, 'Invoice Minimal Rp. 100.000');
        }
    
        let args ={
           "dt_code": dt_code,
           "le_code": le_code+"",
           "invoice_number": invoice_number,
           "amount": amount,
           "tenor": tenor,
           "discount" : parseFloat(total_potongan)
         };
       request({
           headers: {
           'Content-Type': 'application/json'
           },
            url: "http://192.168.12.7:3005/pinjaman/appPinjaman",
            method: "POST",
            json: true,
            body: args
          }, function (error, response, body){
              if(body.status === 'Success'){
                   
                   let argss = {
                    url: result_fmcg_top.dataValues.url,
                    fmcg_token : result_fmcg_top.dataValues.fmcg_token,
                    fmcg_name : result_fmcg_top.dataValues.name,
                    user_id:auth_user.dataValues.user_id,
                    le_code:le_code,
                    dt_code:dt_code,
                    amount:amount,
                    total_amount_invoice:amount,
                    invoice_id:invoice_number,
                    tenor:tenor,
                    loan_type:"Cash",
                    loan_by:"Pohon Dana",
                    principal_code:fmcg_id,
                    total_potongan:total_potongan
                  }
                  let invoice_update = {};
                   invoice_update.le_code = le_code;
                   invoice_update.invoice_id = invoice_number;
                   invoice_update.amount = amount;
                   invoice_update.fmcg_id = fmcg_id;
                   invoice_update.paid_by = "Limit Pandai";

                  ApiPohonDana.update_payment_fmcg(invoice_update);
                  ApiPohonDana.insert_loan_history(argss)
                  res.status(200).json(body);
              }else
              {
                  res.status(400).json(body);
              }
        });
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan update invoice, '+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}
async function calculator_new(req, res) {
    const token = req.swagger.params.token.value;
    const param_amount = req.swagger.params.amount;
    const param_discount = req.swagger.params.discount;
    const param_tenor = req.swagger.params.tenor;
    const iso8601format = "YYYY-MM-DD";

    let amount = _.isUndefined(param_amount.value) ? 0 : param_amount.value;
    let discount = _.isUndefined(param_discount.value) ? 0 : param_discount.value;
    let tenor = _.isUndefined(param_tenor.value) ? 0 : param_tenor.value;
    let data_unique_code = '';
    let fmcg_top =5;
    let admin= 0;
    try {
        Logs.create({action: 'calculator top', description: helper.getSimpleSwaggerParams(req.swagger.params)});
        let auth_user = await middleware.verify({token: token, acl: acl_calculator, is_allowed_access_token_static: false});
        let result_fmcg_top = await service_fmcg.getOneFmcgWithFilter({id: fmcg_top});   
        let invoice_args = {
            url: result_fmcg_top.dataValues.url,
            fmcg_token: result_fmcg_top.dataValues.fmcg_token,
            fmcg_name: result_fmcg_top.dataValues.name,
            amount: amount,
            tenor: tenor,
            discount: discount
        }
        let calculator = await ApiPohonDana.calculator(invoice_args)
        let week = {};
        if (calculator.status === 'Success') {
                let potongan = parseInt(calculator.week[0].administrasi)* 0
                if((((calculator.week[0].loan_payback)-potongan)/calculator.week[0].loan_length % 1) !== 0)
                {
                    admin= parseInt((parseInt(calculator.week[0].loan_payback)-potongan)/calculator.week[0].loan_length)+1;
                }
                else{
                    admin= parseInt((parseInt(calculator.week[0].loan_payback)-potongan)/calculator.week[0].loan_length);
                }
                week = [{
                   "loan_amount": parseInt(calculator.week[0].loan_amount)+".0000",
                   "loan_payback": parseInt(calculator.week[0].loan_payback)-potongan+".0000",
                   "administrasi": parseInt(calculator.week[0].administrasi)+".0000",
                   "loan_length": calculator.week[0].loan_length,
                   "installment": parseInt(admin)+".0000",
                   "total_potongan": potongan+".0000",
               }]
            }
            else
            {
                week = (calculator);
            }
            let weeks = {}
            weeks.status = calculator.status;
            weeks.message = calculator.status;
            weeks.week = week;
            res.status(200).json(weeks)
        
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data calculator TOP, '+err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

export { calculator }
export { apply_loan }
export { calculator_new }