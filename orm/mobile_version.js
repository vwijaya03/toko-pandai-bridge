import { orm } from '~/services/mariadb'
import Sequelize from 'sequelize'

const MobileVersion = orm.define('mobile_versions', {
    id: { type: Sequelize.INTEGER, field: 'id', primaryKey: true, autoIncrement: true },
    version: { type: Sequelize.STRING, allowNull:true },
    download_link: { type: Sequelize.STRING, allowNull:true },
    force: { type: Sequelize.BOOLEAN, allowNull:true },
    update_description: { type: Sequelize.STRING, allowNull:true },
    createdAt: { type: Sequelize.DATE, allowNull:true },
    updatedAt: { type: Sequelize.DATE, allowNull:true }
});

export default MobileVersion