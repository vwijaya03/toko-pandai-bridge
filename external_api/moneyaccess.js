import request from 'request'
import util from 'util'

import ResponseAdapter from '~/adapter/ResponseAdapter'

import { Logs } from '~/orm/index'

import { moneyaccess_apikey } from '~/properties'

import MoneyAccessResetPinError from '~/response/error/MoneyAccessResetPinError'
import MoneyAccessChangePinError from '~/response/error/MoneyAccessChangePinError'
import MoneyAccessAccountInquiryNameError from '~/response/error/MoneyAccessAccountInquiryNameError'
import MoneyAccessRegisterError from '~/response/error/MoneyAccessRegisterError'
import MoneyAccessDoPaymentError from '~/response/error/MoneyAccessDoPaymentError'
import MoneyAccessTransferError from '~/response/error/MoneyAccessTransferError'
import MoneyAccessInquiryError from '~/response/error/MoneyAccessInquiryError'
import MoneyAccessGetHistoryError from '~/response/error/MoneyAccessGetHistoryError'

// 192.168.13.2 Retailer Development
// 192.168.13.3 Pickup Development

// 192.168.12.2 Retailer Production
// 192.168.12.3 Pickup Production


let retailer_host = '192.168.12.2';
let pickup_host = '192.168.12.2';

// Charlie, jadi 1
// let retailer_host = '192.168.13.2:84';

let apikey = moneyaccess_apikey;
let protocol = 'http';
let private_prefix = '/api/v1';
let public_prefix = private_prefix;

let protohost = protocol+"://"+retailer_host;
let proto_pickup_host = protocol+"://"+pickup_host;

// let proto_pickup_host = protohost;

let tokenkey = 'gUxOsYYXbeywpM2xaV37RaNvdZ5nJdI8d8AdYvy4';

let pickup_transfer_retailer_endpoint = proto_pickup_host + private_prefix + "/pickup/trf";
let register_endpoint = protohost + private_prefix + "/customer/reg";
let register_pickup_endpoint = proto_pickup_host + private_prefix + "/customer/reg";

let transfer_endpoint = protohost + public_prefix + "/vacc/trf";
let transfer_pickup_endpoint = proto_pickup_host + public_prefix + "/vacc/trf"; 

let inquiry_endpoint = protohost + public_prefix + "/vacc/inq";
let inquiry_pickup_endpoint = proto_pickup_host + public_prefix + "/vacc/inq";

let history_endpoint = protohost + public_prefix + "/vacc/hist";
let history_pickup_endpoint = proto_pickup_host + public_prefix + "/vacc/hist";

let custlist_endpoint = protohost + public_prefix + "/customer/custlist";
let updatecust_endpoint = protohost + public_prefix + "/customer/updcust";

let resetpin_endpoint = protohost + public_prefix + "/customer/rpin";
let resetpin_pickup_endpoint = proto_pickup_host + public_prefix + "/customer/rpin";

let changepin_endpoint = protohost + public_prefix + "/customer/cpin";
let changepin_pickup_endpoint = proto_pickup_host + public_prefix + "/customer/cpin";

let dopayment_endpoint = protohost + public_prefix + "/manacc/dopayment";
let dopayment_pickup_endpoint = proto_pickup_host + public_prefix + "/manacc/dopayment";

let account_inquiry_name_endpoint = protohost + public_prefix + "/manacc/accinqname";
let account_pickup_inquiry_name_endpoint = proto_pickup_host + public_prefix + "/manacc/accinqname";

function commonHeaders() {
    return {
        'Accept': 'application/json',
        'Cache-Control': 'no-cache',
        'apigatekey': apikey
    };
}

function authHeaders(headers) {
    headers = headers || {};
    headers['Authorization'] = 'Bearer ' + tokenkey;
    return headers;
}

function handleResponse(rawJsonResponse, errorClass, resolve, reject, function_name) {
    
    try {
        let {error:err, response:successJsonResp, pagination: pagination} = ResponseAdapter.parse({response:JSON.parse(rawJsonResponse)});

        if (err) {
            if(function_name == 'history') {
                resolve(err);
            }

            reject(new errorClass(err.message));
        } else {
            successJsonResp = Object.assign(successJsonResp, pagination);
            resolve(successJsonResp);
        }
    } catch(err) {
        Logs.create({action: `error ${function_name}`, description: util.inspect(err), additional_description: util.inspect(rawJsonResponse)});

        reject(new errorClass('terjadi kesalahan silahkan coba lagi'));
    }
}

function handleResponsep(rawJsonResponse, errorClass, resolve, reject, requestError, function_name, fmcg_name) {

	 try {
		let {error:err, response:successJsonResp, pagination: pagination} = ResponseAdapter.parse({response:JSON.parse(rawJsonResponse)});
        
		if(requestError != null) {
			reject(new errorClass(requestError));
		}

		if(successJsonResp.code == 400 || successJsonResp.code == 401 || err) {
			reject(new errorClass(err.message));
		} else {
                    resolve(successJsonResp);
		}
	} catch(err) {
		Logs.create({action: `error ${function_name}`, description: util.inspect(err), additional_description: util.inspect(rawJsonResponse)});

		reject(new errorClass('terjadi kesalahan silahkan coba lagi'));
    }
	
}

function handleMandiriResponse(rawJsonResponse, errorClass, resolve, reject) {
    let {error:err, response:successJsonResp, pagination: pagination} = ResponseAdapter.parseMandiri({response:JSON.parse(rawJsonResponse)});

    if (err) {
        reject(new errorClass(err.message));

    } else {
        successJsonResp = Object.assign(successJsonResp, pagination);
        console.log(successJsonResp,"+++++++++++++++++++++++++++++++++++++++++++++")
        resolve(successJsonResp);
    }
}

function handleResponseError(errorClass, resolve, reject, err, function_name) {
    Logs.create({action: `error ${function_name}`, description: util.inspect(err)});
    
    reject(new errorClass(err));
}

async function resetpin({vaccount=null, usertype=null}={}) {
    
    return new Promise(function(resolve, reject) {
        
        let headers = authHeaders(commonHeaders());
	    let form = {vaccount: vaccount};
	    let url;

	    if(usertype==2)
	        url = resetpin_pickup_endpoint;
	    else
	        url = resetpin_endpoint;

	    request.post( {url:url, headers:headers, form:form}, function(err, httpResponse, body) {
            if(err) {
                return err;
            }

	    	handleResponse(body, MoneyAccessResetPinError, resolve, reject, 'resetpin');
	    }).on('error', function(err) {
            handleResponseError(MoneyAccessResetPinError, resolve, reject, err, 'resetpin');
        });
    });
}

function changepin({vaccount=null, old_pin=null, new_pin=null, retype_new_pin=null, usertype=null}={}) {
    
    return new Promise(function(resolve, reject) {

    	let headers = authHeaders(commonHeaders());
	    let form = {vaccount:vaccount, old_pin:old_pin, new_pin:new_pin, retype_new_pin:retype_new_pin };
	    let url;

	    if(usertype==2)
	        url = changepin_pickup_endpoint;
	    else
	        url = changepin_endpoint;

	    request.post( {url:url, headers:headers, form:form}, function(err, httpResponse, body) {
            if(err) {
                return err;
            }

	    	handleResponse(body, MoneyAccessChangePinError, resolve, reject, 'changepin');
	    }).on('error', function(err) {
            handleResponseError(MoneyAccessChangePinError, resolve, reject, err, 'changepin');
        }); 
    });
}

function account_inquiry_name({accountnumber=null, usertype=null}={}) {

    return new Promise(function(resolve, reject) {
        let form = { accountnumber: accountnumber};
        let headers = commonHeaders();
        let url;

        if(usertype==2)
            url = account_pickup_inquiry_name_endpoint;
        else
            url = account_inquiry_name_endpoint;

        request.post( {url:url, headers:headers, form:form}, function(err, httpResponse, body) {
            if(err) {
                return err;
            }

            handleMandiriResponse(body, MoneyAccessAccountInquiryNameError, resolve, reject, 'account_inquiry_name');
        }).on('error', function(err) {
            handleResponseError(MoneyAccessAccountInquiryNameError, resolve, reject, err, 'account_inquiry_name');
        });
    });
}

function register({mobnum=null, fname=null, lname=null, email=null, pin=null, dob=null, pob=null, usertype=null}={}) {

    return new Promise(function(resolve, reject) {
        let form = { mobnum:mobnum, fname:fname, lname:lname, dob:dob, pob:pob, email:email, pin:pin };
        let headers = commonHeaders();
        let url;

        if(usertype==2)
            url = register_pickup_endpoint;
        else
            url = register_endpoint;

        request.post( {url:url, headers:headers, form:form}, function(err, httpResponse, body) {
            if(err) {
                return err;
            }

            handleResponse(body, MoneyAccessRegisterError, resolve, reject, 'register');
        }).on('error', function(err) {
            handleResponseError(MoneyAccessRegisterError, resolve, reject, err, 'register');
        });
    });
}

function dopayment({credit_accountno=null, value_date=null, value_amount=null, destination_bankcode=null, beneficiary_name=null, preferred_transfer_method_id=null, remark1=null, extended_payment_detail=null,beneficiary_email_address=null, payment_method=null, reserved_field1=null, reserved_field2=null, vacc_number=null,pin=null, transid=null, usertype=null, desc=null}={}) {

    if (!transid) {
        throw new CustomError(400, 'TransId tidak boleh kosong');
    }

    return new Promise(function(resolve, reject) {
        let headers = authHeaders(commonHeaders());
        let form = {
            credit_accountno: credit_accountno, 
            value_date: value_date, 
            value_amount: value_amount, 
            destination_bankcode: destination_bankcode, 
            beneficiary_name: beneficiary_name, 
            preferred_transfer_method_id: preferred_transfer_method_id,  
            remark1: remark1, 
            extended_payment_detail: extended_payment_detail, 
            beneficiary_email_address: beneficiary_email_address, 
            payment_method: payment_method, 
            reserved_field1: reserved_field1, 
            reserved_field2: reserved_field2, 
            vacc_number: vacc_number,
            desc: desc,
            pin: pin,
            transid: transid
        };
        let url;

        if(usertype==2)
            url = dopayment_pickup_endpoint;
        else
            url = dopayment_endpoint;

        request.post( {url:url, headers:headers, form:form}, function(err, httpResponse, body) {
            if(err) {
                return err;
            }

            handleMandiriResponse(body, MoneyAccessDoPaymentError, resolve, reject, 'dopayment');
        }).on('error', function(err) {
            handleResponseError(MoneyAccessDoPaymentError, resolve, reject, err, 'dopayment');
        });
    });
}

function pickup_transfer({transId=null, desc=null, vaccount_pickup=null, vaccount_toko=null, amount=null, pin=null, usertype=null}={}) {

    if (! transId) {
        throw new MoneyAccessTransferError('Gagal melakukan transfer');
    }

    if (! desc) {
        desc = `transfer dengan jumlah ${amount}, vaccount pickup ${vaccount_pickup} vaccount toko ${vaccount_toko}`;
    }

    return new Promise(function(resolve, reject) {
        let headers = authHeaders(commonHeaders());
        let form = {transid: transId, desc: desc, vaccount_pickup: vaccount_pickup, vaccount_toko: vaccount_toko, amount: amount, pin: pin };
        let url;

        if(usertype == 2)
            url = pickup_transfer_retailer_endpoint;
        else
            url = pickup_transfer_retailer_endpoint;
        
        request.post( {url:url, headers:headers, form:form}, function(err, httpResponse, body) {
            if(err) {
                return err;
            }

            handleMandiriResponse(body, MoneyAccessTransferError, resolve, reject, 'pickup_transfer');
        }).on('error', function(err) {
            handleResponseError(MoneyAccessTransferError, resolve, reject, err, 'pickup_transfer');
        });
    });
}

function inquiry({mobnum=null, vaccount=null, usertype=null}={}) {
    return new Promise(function(resolve, reject) {
        let headers = authHeaders(commonHeaders());
        let form = {mobnum:mobnum, vaccount:vaccount};
        let url;

        if(usertype==2)
            url = inquiry_pickup_endpoint;
        else
            url = inquiry_endpoint;

        request.post( {url:url, headers:headers, form:form}, function(err, httpResponse, body) {
            if(err) {
                return err;
            }

            handleResponse(body, MoneyAccessInquiryError, resolve, reject, 'inquiry');
        }).on('error', function(err) {
            handleResponseError(MoneyAccessInquiryError, resolve, reject, err, 'inquiry');
        });
    });
}

function transfer({transId=null, desc=null, mobnum=null, from=null, to=null, amount=null, pin=null, usertype=null}={}) {
    if (! transId) {
        throw new MoneyAccessTransferError('Gagal melakukan transfer');
    }

    if (! desc) {
        desc = `transfer dengan jumlah ${amount}, dari ${from} kepada ${to}`;
    }

    return new Promise(function(resolve, reject) {
        let headers = authHeaders(commonHeaders());
        let form = {transid: transId, desc: desc, mobnum: mobnum, vaccount_from: from, vaccount_to: to, amount: amount, pin: pin };
        let url;

        if(usertype == 2)
            url = transfer_pickup_endpoint;
        else
            url = transfer_endpoint;
        
        request.post( {url:url, headers:headers, form:form}, function(err, httpResponse, body) {
            if(err) {
                return err;
            }

            handleResponse(body, MoneyAccessTransferError, resolve, reject, 'transfer');
        }).on('error', function(err) {
            handleResponseError(MoneyAccessTransferError, resolve, reject, err, 'transfer');
        });
    });
}

function history({vacc_number=null, type=null, start_date=null, end_date=null, pagenum=1, item_per_page=15, usertype=null}={}) {
    return new Promise(function(resolve, reject) {
        let headers = commonHeaders();
        let url;

        if(usertype==2)
            url = history_pickup_endpoint;
        else
            url = history_endpoint;

        let form = {};
        form.vacc_number = vacc_number;
        form.pagenum = pagenum;
        form.item_per_page = item_per_page;
        form.start_date = start_date;
        form.end_date = end_date;

        if (type) {
            form.trans_type = type;
        }

        request.post( {url:url, headers:headers, form:form}, function(err, httpResponse, body) {
            if(err) {
                return err;
            }

            handleResponse(body, MoneyAccessGetHistoryError, resolve, reject, 'history');
        }).on('error', function(err) {
            handleResponseError(MoneyAccessGetHistoryError, resolve, reject, err, 'history');
        });
    });

}

function pickup_toko_transfer({transId=null, desc=null, vaccount_pickup=null, vaccount_toko=null, amount=null, pin=null, usertype=null}={}) {

    if (! transId) {
        throw new MoneyAccessTransferError('Gagal melakukan transfer');
    }

    if (! desc) {
        desc = `transfer dengan jumlah ${amount}, vaccount pickup ${vaccount_pickup} vaccount toko ${vaccount_toko}`;
    }

    return new Promise(function(resolve, reject) {
        let headers = authHeaders(commonHeaders());
        let form = { desc: desc, vaccount_pickup: vaccount_pickup, vaccount_toko: vaccount_toko, amount: amount, transid: transId,pin: pin };
        let url;

        url = pickup_transfer_retailer_endpoint;
        
        request.post( {url:url, headers:headers, form:form}, function(err, httpResponse, body) {
            if(err) {
                return err;
            }

            handleMandiriResponse(body, MoneyAccessTransferError, resolve, reject, 'pickup_transfer');
        }).on('error', function(err) {
            handleResponseError(MoneyAccessTransferError, resolve, reject, err, 'pickup_transfer');
        });
    });
}

function cashout_other_bank({vacc_number=null,account_no=null,account_name=null,amount=null,bank_name=null}={}) {
    return new Promise(function(resolve, reject) {
        let headers = authHeaders(commonHeaders());
        let form = {
            vacc_number: vacc_number, 
            account_no: account_no,
            account_name: account_name,
            amount : amount,
            bank_name : bank_name
        };
        console.log(form,"=============================================================")
        let url = "http://192.168.12.2/api/v1/Cashout_trx_ob/dopayment";
        request.post( {url:url, headers:headers, form:form}, function(err, httpResponse, body) {
            if(err) {
                return err;
            }
            handleMandiriResponse(body, MoneyAccessDoPaymentError, resolve, reject, 'dopayment');
        }).on('error', function(err) {
            handleResponseError(MoneyAccessDoPaymentError, resolve, reject, err, 'dopayment');
        });
    });
}

function new_payment({trans_type=null,vacc_from=null,vacc_to=null,amount=null,desc=null,transId=null,pin=null}={}) {
    return new Promise(function(resolve, reject) {
//        let headers ={ 
//            'Content-Type':'application/json;charset=UTF-8',
//            'X-Requested-With': 'XMLHttpRequest',
//            'Accept':'application/json, text/plain, */*',
//            'User-Agent': 'UserAgent',
//            'apigatekey': 'webKEY123456789'}
//        let form = {
//            trans_type:trans_type,
//            status:"success", 
//            vacc_from: vacc_from,
//            vacc_to:[{
//                        vacc_number:vacc_to, 
//                        amount: parseInt(amount), 
//                        desc:desc,
//                        transid:transId
//                    }],        
//            pin:pin,
//            tanggal_mutasi: ''
//        }
        let options = { method: 'POST',
        url: 'http://192.168.12.2/api/v1/multi_vacc_v3/trf',
        headers: 
         { 'Postman-Token': '6d5c2d48-ba40-446e-819a-db8d120b796e',
           'cache-control': 'no-cache',
           'Content-Type': 'application/json',
           apigatekey: 'webKEY123456789' },
        body: 
         { trans_type: 'payment_ksayur',
           status: 'success',
           vacc_from: '1010004469',
           vacc_to: 
            [ { vacc_number: '21000000606',
                amount: 5,
                desc: 'Pembayaran invoice OD899 Rp. 5 dari Ferry Sukarto(YF Toko) (1010004469) ke Toko Pandai (21000000606)',
                transid: 'OD8991603300473360' } ],
           pin: '336126',
           tanggal_mutasi: '' },
        json: true }
        
        let url = "http://192.168.12.2/api/v1/multi_vacc_v3/trf";
        request(options, function(err, httpResponse, body) {
            if(err) {
                return err;
            }handleResponsep(body, MoneyAccessDoPaymentError, resolve, reject, 'dopayment');
        }).on('error', function(err) {
            handleResponseError(MoneyAccessDoPaymentError, resolve, reject, err, 'dopayment');
        });
    });
}

let obj = {};
obj.resetpin = resetpin;
obj.changepin = changepin;
obj.account_inquiry_name = account_inquiry_name;
obj.register = register;
obj.dopayment = dopayment;
obj.pickup_transfer = pickup_transfer;
obj.pickup_toko_transfer = pickup_toko_transfer;
obj.inquiry = inquiry;
obj.transfer = transfer;
obj.history = history;
obj.cashout_other_bank = cashout_other_bank
obj.new_payment = new_payment

export default obj
