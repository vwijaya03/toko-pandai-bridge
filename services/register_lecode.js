'use strict';

import _ from 'lodash';
import moment from 'moment'
import crypto from 'crypto'
import RandomString from 'randomstring'
import NodeMailer from 'nodemailer'

import helper from '~/utils/helper'

import { orm } from '~/services/mariadb'
import { Op } from '~/utils/helper'
import { RegisteredLeCode } from '~/orm/index'
import { UserUniqueCode } from '~/orm/index'
import { allowed_user_status } from '~/properties'
import service_otp from '~/services/otp';
import CustomError from '~/response/error/CustomError'
import UserNotFoundError from '~/response/error/UserNotFoundError'

async function getOne(filter, options) {

    options = options || {};
    let result_set = '';

    result_set = await RegisteredLeCode.findOne({where: filter});

    if (!result_set || Object.keys(result_set).length == 0) {
        if (options.throwIfEmpty) {
            throw new UserNotFoundError();
        }
    }

    return result_set;
}

async function getOnes({le_code=null}={}) {
	let response = await orm.query(`SELECT * FROM data_registered_le_codes WHERE le_code ='${le_code}' and fmcg_id !=1 `);

	if (response == null || response == undefined) {
        return [];
    }
    return response[0];
}

let obj = {}
obj.getOne = getOne
obj.getOnes = getOnes

export default obj