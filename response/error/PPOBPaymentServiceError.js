class PPOBPaymentServiceError extends Error {
	constructor(message) {
        super(message);
        this.code = 400;
        this.err_code = 4200;
        this.message = 'Gagal melakukan pembayaran, '+message;
    }
}

export default PPOBPaymentServiceError