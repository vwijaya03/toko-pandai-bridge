class SigninError extends Error {
	constructor(code, message) {
        super(message);
        this.code = 400;
        this.message = 'Email atau password anda salah';
    }
}

export default SigninError