class FmcgTaskListPotonganError extends Error {
	constructor(message) {
        super(message);
        this.code = 400;
        this.err_code = 4300;
        this.message = 'Gagal menampilkan data potongan yang sudah terupload, '+message;
    }
}

export default FmcgTaskListPotonganError