import request from 'request'
import util from 'util'

import { Logs } from '~/orm/index'
import FmcgGetInvoicesError from '~/response/error/FmcgGetInvoicesError'


const api_path_calculator = '/top-ext/calculator';
const api_path_invoice_loan_history = '/top-ext/insert-invoice-loan-history';
const api_path_get_invoice_loan_history = '/top-ext/get-invoice-loan-history';
const api_path_payment_distributor = '/top-ext/payment-distributor';
const api_path_payment_status_distributor = '/top-ext/payment-status-distributor';
const api_path_get_unpaid_invoices = '/top-ext/get-unpaid-invoices-distributor';
const api_path_get_invoice = '/top-ext/get-invoice';
const api_path_get_one_invoice = '/top-ext/get-one-invoice';
const api_path_cek_saldo = '/top-ext/cek-saldo';
const api_path_get_all_tenor = '/top-ext/get-all-tenor';
const api_path_insert_manual_transaction = '/payment/manual-transaction';
const api_path_update_transc_date = '/top-ext/update-transaction-date';
const api_path_update_invoice ='/top-ext/update-invoice';
const api_path_getdatalecode =  "/top-ext/getdatalecode";
const api_path_checktransactions =  "/top-ext/checktransactions";
const api_path_get_invoice_pd =  "/top-ext/get-inv-pd";
const api_path_check_fintech_unpaid =  "/top-ext/check-fintech-unpaid";

function commonHeaders(token) {
    return {
        'Cache-Control': 'no-cache',
        'token': token
    };
}

function handleResponse(rawJsonResponse, errorClass, resolve, reject, requestError, function_name, fmcg_name) {

	try {
		let successJsonResp = JSON.parse(rawJsonResponse);

		if(requestError != null) {
			reject(new errorClass(requestError));
		}

		if(successJsonResp.code == 400 || successJsonResp.code == 401) {
			reject(new errorClass(successJsonResp.message));
		} else {
			resolve(successJsonResp);
		}
	} catch(err) {
		Logs.create({action: `error ${function_name} ${fmcg_name}`, description: util.inspect(err), additional_description: util.inspect(rawJsonResponse)});
		reject(new errorClass('terjadi kesalahan silahkan coba lagi'));
	}

}

function handleResponseError(errorClass, resolve, reject, err, function_name, fmcg_name) {
	Logs.create({action: `error ${function_name} ${fmcg_name}`, description: util.inspect(err)});
	reject(new errorClass(err));
}

function calculator({url=null, fmcg_token=null, amount=null,fmcg_name=null,discount=null,tenor=null}={}) {
	let invoice_args = {};
	invoice_args.url = url;
    invoice_args.fmcg_token = fmcg_token;
    invoice_args.fmcg_name = fmcg_name;
	invoice_args.amount = amount;
    invoice_args.discount = discount;
    invoice_args.tenor = tenor

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_calculator = url+api_path_calculator;
	    request.get( {url:api_calculator, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}

function insert_loan_history({url=null,fmcg_token=null,fmcg_name=null,user_id=null,le_code=null,dt_code=null,total_amount_invoice=null,
	amount=null,invoice_id=null,tenor=null,loan_type=null,loan_by=null,principal_code=null,total_potongan=null}={}){

	let args = {}
	args.url = url;
	args.fmcg_token = fmcg_token;
	args.fmcg_name = fmcg_name;
	args.user_id = user_id;
	args.le_code = le_code;
	args.dt_code = dt_code;
	args.amount = amount;
	args.total_amount_invoice = total_amount_invoice;
	args.invoice_id = invoice_id;
	args.tenor = tenor;
	args.loan_type = loan_type;
	args.loan_by = loan_by;
	args.principal_code = principal_code;
	args.total_potongan = total_potongan;

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_invoice_loan_history = url+api_path_invoice_loan_history;
	    request.post( {url:api_invoice_loan_history, headers:headers, qs:args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'loan history', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'loan history', fmcg_name);
		});
    });
}

function get_loan_history({url=null,fmcg_token=null,fmcg_name=null,le_code=null,dt_code=null,invoice_id=null}={}){

	let invoice = {}
	invoice.url = url;
	invoice.fmcg_token = fmcg_token;
	invoice.fmcg_name = fmcg_name;
	invoice.le_code = le_code;
	invoice.dt_code = dt_code;
	invoice.invoice_id = invoice_id;

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_get_invoice_loan_history = url+api_path_get_invoice_loan_history;
	    request.get( {url:api_get_invoice_loan_history, headers:headers, qs:invoice}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'loan history', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'loan history', fmcg_name);
		});
    });
}

function payment_distributor({fmcg_id=null,le_code=null,dt_code=null,invoice_no=null,total_tagihan=null,principal_code=null,
	tenor=null,fmcg_name=null,fmcg_token=null,url=null,amount=null}={}) {
	let invoice_args = {};
	invoice_args.fmcg_token=fmcg_token;
	invoice_args.fmcg_name=fmcg_name;
	invoice_args.url = url
	invoice_args.fmcg_id = fmcg_id;
	invoice_args.tenor = tenor;
	invoice_args.le_code = le_code;
	invoice_args.dt_code = dt_code;
	invoice_args.invoice_no = invoice_no;
	invoice_args.total_tagihan = total_tagihan;
	invoice_args.amount = amount;
	invoice_args.principal_code = principal_code;
        

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_payment_distributor = url+api_path_payment_distributor;
	    request.post( {url:api_payment_distributor, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}

function update_payment_status_distributor({invoice_code=null,payment_status=null,fmcg_name=null,fmcg_token=null,url=null}={}){
	let update_data = {};
	update_data.fmcg_token=fmcg_token;
	update_data.fmcg_name=fmcg_name;
	update_data.url = url;
	update_data.invoice_code = invoice_code;
	update_data.payment_status = payment_status;

	return new Promise(function(resolve, reject) {
				let headers = commonHeaders(fmcg_token);
				let api_payment_status_distributor = url+api_path_payment_status_distributor;
			request.post( {url:api_payment_status_distributor, headers:headers, qs:update_data}, function(err, httpResponse, body) {
				if(err) {
					return err;
				}
				handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
			}).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	});
});
}

function get_unpaid_invoices({fmcg_name=null,fmcg_token=null,url=null}={}){
	let args = {};
	args.fmcg_token=fmcg_token;
	args.fmcg_name=fmcg_name;
	args.url = url

	return new Promise(function(resolve, reject) {
				let headers = commonHeaders(fmcg_token);
				let api_get_unpaid_invoices = url+api_path_get_unpaid_invoices;
			request.post( {url:api_get_unpaid_invoices, headers:headers, qs:args}, function(err, httpResponse, body) {
				if(err) {
					return err;
				}
				handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
			}).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	});
});
}

function get_invoice({url=null, fmcg_token=null, le_code=null,fmcg_name=null,user_id=null,invoice_no=null,
	dt_code=null,role_type=null,page=null,items_per_page=null,sorting=null, dist_dt_code=null}={}) {

	let invoice_args = {};

	invoice_args.url = url;
	invoice_args.fmcg_token = fmcg_token;
	invoice_args.fmcg_name = fmcg_name;
	invoice_args.le_code = le_code;
	invoice_args.user_id = user_id;
	invoice_args.dt_code = dt_code;
	invoice_args.invoice_no = invoice_no;
	invoice_args.dist_dt_code = dist_dt_code;
	invoice_args.role_type = role_type;
	invoice_args.page= page;
	invoice_args.items_per_page = items_per_page;
	invoice_args.sorting = sorting;

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_invoice = url+api_path_get_invoice;
	    request.get( {url:api_invoice, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}

function get_one_invoice({url=null,tenor=null, fmcg_token=null, invoice_id=null, dt_code=null, 
	le_code=null,fmcg_name=null, user_id=null}={}) {
	let invoice_args = {};

	invoice_args.url = url;
	invoice_args.fmcg_token = fmcg_token;
	invoice_args.le_code = le_code;
    invoice_args.dt_code = dt_code;
	invoice_args.invoice_id = invoice_id;
	invoice_args.tenor = tenor;
	invoice_args.user_id = user_id
        

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_one_invoice = url+api_path_get_one_invoice;
	    request.get( {url:api_one_invoice, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}

function cek_saldo({le_code=null}={}) {
	let invoice_args = {};
        let url='http://localhost:30333';
        let fmcg_token='2b3e6865-bdae-4881-ba71-ea9c5f123e8d';
        let fmcg_name='TopExt';

	invoice_args.le_code = le_code;

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_cek_saldo = url+api_path_cek_saldo;
	    request.get( {url:api_cek_saldo, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}

function get_all_tenor({fmcg_name=null,fmcg_token=null,url=null,invoice_number=null,invoice_payment_status=null}){
	let data = {};
	data.url = url;
	data.fmcg_name=fmcg_name;
	data.fmcg_token=fmcg_token;
	data.invoice_number=invoice_number;
	data.invoice_payment_status=invoice_payment_status;

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
		let api_get_all_tenor = url+api_path_get_all_tenor;
		request.get( {url:api_get_all_tenor, headers:headers, qs:data}, function(err, httpResponse, body) {
			if(err) {
				return err;
			}
			handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		}).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
	});
}

function insert_manual_transaction({valdo_tx_id=null,transact_date=null,currency_code=null,type=null,distributor_id=null,retailer_id=null,from_user_id=null,to_user_id=null,invoice_code=null,url=null, fmcg_token=null,fmcg_name=null, le_code=null, dt_code=null,invoice_id=null,amount=null}={}) {
    
    let invoice_args = {};

    invoice_args.url = url;
    invoice_args.fmcg_token = fmcg_token;
    invoice_args.le_code = le_code;
    invoice_args.dt_code = dt_code;
    invoice_args.invoice_id = invoice_id;
    invoice_args.invoice_code = invoice_id+dt_code;
    invoice_args.amount = amount;
    invoice_args.valdo_tx_id = valdo_tx_id;
    invoice_args.transact_date = transact_date;
    invoice_args.currency_code = currency_code;
    invoice_args.type = type;
    invoice_args.distributor_id = distributor_id;
    invoice_args.retailer_id = retailer_id;  
    invoice_args.from_user_id = from_user_id;
    invoice_args.to_user_id = to_user_id;
    


	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_payment_manual = url+api_path_insert_manual_transaction;
	      request.post({url:api_payment_manual, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}

function update_transaction_date({url=null,fmcg_token=null,fmcg_name=null,cash_memo_doc_no=null}){
	let input_to_inv_top = {}
	input_to_inv_top.url = url;
	input_to_inv_top.fmcg_token = fmcg_token;
	input_to_inv_top.fmcg_name = fmcg_name;
	input_to_inv_top.cash_memo_doc_no = cash_memo_doc_no;

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
		let api_update_transc_date = url+api_path_update_transc_date;
		request.put( {url:api_update_transc_date, headers:headers, qs:input_to_inv_top}, function(err, httpResponse, body) {
			if(err) {
				return err;
			}
			handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		}).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
	});
}

function update_invoice({url=null,fmcg_token=null,fmcg_name=null}){
	let input_to_inv_top = {}
	input_to_inv_top.url = url;
	input_to_inv_top.fmcg_token = fmcg_token;
	input_to_inv_top.fmcg_name = fmcg_name;

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
		let api_update_transc_date = url+api_path_update_invoice;
		request.get( {url:api_update_transc_date, headers:headers, qs:input_to_inv_top}, function(err, httpResponse, body) {
			if(err) {
				return err;
			}
			handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		}).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
	});
}

function update_payment_fmcg({le_code=null,fmcg_id=null,invoice_id=null,amount=null,paid_by=null}){
    let options = { method: 'POST',
    url: 'http://192.168.12.7:10012/topext',
    headers: 
     { 'postman-token': 'a0e838a6-c4d9-8510-5ee9-0802548f0a8c',
       'cache-control': 'no-cache',
       'content-type': 'application/json' },
    body: 
     { le_code: le_code,
       invoice_id: invoice_id,
       fmcg_id: fmcg_id,
       amount: amount,
       paid_by: paid_by 
   },
    json: true };
  request(options, function (error, response, body) {
    if (error) throw new Error(error);
  });
}

 function getdatalecode({le_code=null}={}) {
    	let invoice_args = {};
        let url='http://localhost:30111';
        let fmcg_token='2b3e6865-bdae-4881-ba71-ea9c5f123e8d';
        let fmcg_name='TopExt';
	invoice_args.le_code = le_code;
        
	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_cek_le_code = url+api_path_getdatalecode;
	request.get( {url:api_cek_le_code, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
        if(err) {
                return err;
        }
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}

function checktransactions({dt_code=null,invoice_id=null,fmcg_id=null}={}) {
    	let invoice_args = {};
        let url='http://localhost:30111';
        let fmcg_token='2b3e6865-bdae-4881-ba71-ea9c5f123e8d';
        let fmcg_name='TopExt';
	    invoice_args.dt_code =dt_code;
        invoice_args.invoice_id = invoice_id;
        invoice_args.fmcg_id = fmcg_id;
        
	    return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_cek_checktransactions = url+api_path_checktransactions;
	    request.get( {url:api_cek_checktransactions, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
        if(err) {
                return err;
        }
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}

function get_inv_pd({le_code=null,dt_code=null,invoice_number=null,tenor=null}={}) {
	let invoice_args = {};
        let url='http://localhost:30333';
        let fmcg_token='2b3e6865-bdae-4881-ba71-ea9c5f123e8d';
        let fmcg_name='TopExt';

	invoice_args.le_code = le_code;
        invoice_args.dt_code = dt_code;
        invoice_args.invoice_number = invoice_number;
        invoice_args.tenor = tenor;

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_cek_le_code = url+api_path_get_invoice_pd;
	    request.get( {url:api_cek_le_code, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}


function multifmcg_transactions({fmcg_id,dt_code=null,invoice_id=null,invoice_code=null,amount=null,valdo_tx_id=null,distributor_id=null,retailer_id=null,from_user_id=null,to_user_id})
	{
   
		var options = { method: 'POST',
		  url: 'http://192.168.12.7:10012/transactions',
		  qs: { fmcg_id: fmcg_id },
		  headers: 
		   { 'postman-token': 'f1645db2-b075-b39c-5546-510d51b54401',
		     'cache-control': 'no-cache',
		     'content-type': 'application/json' },
		  body: 
		   { dt_code: dt_code,
		     invoice_id: invoice_id,
		     invoice_code: invoice_code,
		     amount: amount,
		     valdo_tx_id: valdo_tx_id,
		     currency_code: 'IDR',
		     type: 0,
		     distributor_id: distributor_id,
		     retailer_id: retailer_id,
		     from_user_id: from_user_id,
		     to_user_id: to_user_id },
		  json: true };

		request(options, function (error, response, body) {
		  if (error) throw new Error(error);
		});

}

function repayment_pd({le_code=null,dt_code=null,invoice_number=null,cicilan_ke=null,terbayar=null}){
	var options = { method: 'POST',
	  url: 'http://192.168.12.7:10018/repayment',
	  headers: 
	   { 'postman-token': '8815dbf4-c115-91e8-70d5-0161035c00ad',
	     'cache-control': 'no-cache',
	     'content-type': 'application/json' },
	  body: 
	   { le_code: le_code,
	     dt_code: dt_code,
	     invoice_number: invoice_number,
	     cicilan_ke: cicilan_ke,
	     terbayar: terbayar },
	  json: true };

	request(options, function (error, response, body) {
	  if (error) throw new Error(error);
	});

}


function get_one_pd_inv({le_code=null,token=null}){
var options = { method: 'GET',
  url: 'http://192.168.12.6/toko-pandai-api/v1/multifintech/invoice',
  qs: { le_code: le_code},
  headers: 
   { 'postman-token': '1264d7f2-8367-a14d-4534-6f1adb1e1ff6',
     'cache-control': 'no-cache',
     token: token } };

request(options, function (error, response, body) {
  if (error) throw new Error(error);
});
}

function check_fintech_unpaid({le_code=null}={}) {
	let invoice_args = {};
        let url='http://localhost:30333';
        let fmcg_token='2b3e6865-bdae-4881-ba71-ea9c5f123e8d';
        let fmcg_name='TopExt';
        
	invoice_args.le_code = le_code;

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_cek_le_code = url+api_path_check_fintech_unpaid;
	    request.get( {url:api_cek_le_code, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}

let obj = {}
obj.calculator = calculator
obj.insert_loan_history = insert_loan_history
obj.get_loan_history = get_loan_history
obj.payment_distributor = payment_distributor
obj.update_payment_status_distributor = update_payment_status_distributor
obj.get_unpaid_invoices = get_unpaid_invoices
obj.get_invoice = get_invoice
obj.get_one_invoice = get_one_invoice
obj.cek_saldo = cek_saldo
obj.get_all_tenor = get_all_tenor
obj.insert_manual_transaction = insert_manual_transaction
obj.update_transaction_date = update_transaction_date
obj.update_invoice = update_invoice
obj.update_payment_fmcg = update_payment_fmcg
obj.getdatalecode = getdatalecode
obj.checktransactions = checktransactions
obj.get_inv_pd = get_inv_pd
obj.multifmcg_transactions =multifmcg_transactions
obj.repayment_pd = repayment_pd
obj.get_one_pd_inv = get_one_pd_inv
obj.check_fintech_unpaid = check_fintech_unpaid

export default obj

