import _ from 'lodash'
import util from 'util'
import request from 'request'
import moment from 'moment'

import middleware from '~/api/controllers/auth'

import service_bank_account from '~/services/bank_account'
import service_user from '~/services/user'
import service_notification from '~/services/notification'
import service_user_additional_data from '~/services/user_additional_data'

import helper from '~/utils/helper'

import MoneyAccess from '~/external_api/moneyaccess'

import { Logs } from '~/orm/index'
import { TOPUP } from '~/orm/index'

import CustomError from '~/response/error/CustomError'
import CustomSuccess from '~/response/success/CustomSuccess'

const acl_account_inquiry_name = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center','finance'];
const acl_pull_money = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center','finance'];
const acl_pickup_transfer_money = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center','finance'];
const acl_transfer_money = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center','finance'];
const acl_history = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center','finance'];
const acl_request_topup = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];

async function vaccount_check_owner_bank_account(req, res) {
    const token = req.swagger.params.token.value;
    const param_id_bank_account = req.swagger.params.id_bank_account;

    let id_bank_account = _.isUndefined(param_id_bank_account.value)?null:param_id_bank_account.value;

    try {
        Logs.create({action: 'vaccount_check_owner_bank_account', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_account_inquiry_name, is_allowed_access_token_static: false});
        let bank_account = await service_bank_account.getOneBankAccount({user_id: auth_user.dataValues.user_id, id: id_bank_account});

        if(bank_account == null || !bank_account) {
            throw new CustomError(400, 'Nomor rekening tidak di temukan');
        }
        
        let args = {};
        args.accountnumber = bank_account.no_rekening;
        args.usertype = auth_user.dataValues.type;

        let api_response = await MoneyAccess.account_inquiry_name(args);

        Logs.create({action: 'api response money access vaccount_check_owner_bank_account account_inquiry_name', description: util.inspect(api_response)});

        api_response.response_timestamp = helper.formatDateTime(api_response.response_timestamp);

        res.json(api_response);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal mendapatkan nama akun, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function vaccount_pull_money(req, res) {
    const token = req.swagger.params.token.value;
    const param_id_bank_account = req.swagger.params.id_bank_account;
    const param_value_date = req.swagger.params.value_date;
    const param_value_amount = req.swagger.params.value_amount;
    const param_destination_bankcode = req.swagger.params.destination_bankcode;
    const param_beneficiary_name = req.swagger.params.beneficiary_name;
    const param_preferred_transfer_method_id = req.swagger.params.preferred_transfer_method_id;
    const param_remark1 = req.swagger.params.remark1;
    const param_extended_payment_detail = req.swagger.params.extended_payment_detail;
    const param_beneficiary_email_address = req.swagger.params.beneficiary_email_address;
    const param_payment_method = req.swagger.params.payment_method;
    const param_reserved_field1 = req.swagger.params.reserved_field1;
    const param_reserved_field2 = req.swagger.params.reserved_field2;
    const param_pin = req.swagger.params.pin;

    let id_bank_account = _.isUndefined(param_id_bank_account.value)?null:param_id_bank_account.value;

    try {
        Logs.create({action: 'vaccount_pull_money', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_pull_money, is_allowed_access_token_static: false});
        let pin = _.isUndefined(param_pin.value)?null:param_pin.value;
        let expected_pin = service_user.getHashedPin(auth_user.dataValues.salt, pin);

        if(expected_pin != auth_user.dataValues.pin) {
            throw new CustomError(400, 'Pin salah');
        }

        let bank_account = await service_bank_account.getOneBankAccount({user_id: auth_user.dataValues.user_id, id: id_bank_account});

        if(bank_account == null || !bank_account) {
            throw new CustomError(400, 'Nomor rekening tidak di temukan');
        }

        let args = {};
        args.credit_accountno = bank_account.no_rekening;
        args.value_date = _.isUndefined(param_value_date.value)?null:param_value_date.value;
        args.value_amount = _.isUndefined(param_value_amount.value)?null:param_value_amount.value;
        args.destination_bankcode = _.isUndefined(param_destination_bankcode.value)?null:param_destination_bankcode.value;
        args.beneficiary_name = _.isUndefined(param_beneficiary_name.value)?null:param_beneficiary_name.value;
        args.preferred_transfer_method_id = _.isUndefined(param_preferred_transfer_method_id.value)?null:param_preferred_transfer_method_id.value;
        args.remark1 = _.isUndefined(param_remark1.value)?null:param_remark1.value;

        args.extended_payment_detail = _.isUndefined(param_extended_payment_detail.value)?null:param_extended_payment_detail.value;

        args.desc = "Penarikan Dana Rp. "+helper.IDRformat(args.value_amount)+" ke No.Rek.Mandiri "+args.credit_accountno+" oleh "+auth_user.dataValues.fullname+"("+auth_user.dataValues.valdo_account+")";

        args.beneficiary_email_address = _.isUndefined(param_beneficiary_email_address.value)?null:param_beneficiary_email_address.value;
        args.payment_method = _.isUndefined(param_payment_method.value)?null:param_payment_method.value;
        args.reserved_field1 = _.isUndefined(param_reserved_field1.value)?null:param_reserved_field1.value;
        args.reserved_field2 = _.isUndefined(param_reserved_field2.value)?null:param_reserved_field2.value;
        args.vacc_number = _.isUndefined(auth_user.dataValues.valdo_account)?null:auth_user.dataValues.valdo_account;
        args.pin = pin;
        args.transid = helper.currentMilisecondsTime();
        args.usertype = auth_user.dataValues.type;
        
        let api_response = await MoneyAccess.dopayment(args);

        Logs.create({action: 'api response money access vaccount_pull_money dopayment', description: util.inspect(api_response)});

        api_response.response_timestamp = helper.formatDateTime(api_response.response_timestamp);

        res.json(api_response);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal mutasi, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function vaccount_pickup_transfer_money(req, res) {
    const token = req.swagger.params.token.value;
    const param_to = req.swagger.params.vaccount_to;
    const param_amount = req.swagger.params.amount;
    const param_pin = req.swagger.params.pin;

    try {
        Logs.create({action: 'vaccount_pickup_transfer_money', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_pickup_transfer_money, is_allowed_access_token_static: false});
        
        let tanggal = new Date();
        let vaccount = _.isUndefined(param_to.value)?null:param_to.value;
        let pickup_name = auth_user.dataValues.fullname;

        let user_reponse = await service_user.getOneByVA({vaccount: vaccount});

        if(user_reponse == null || !user_reponse) {
            throw new CustomError(400, 'Nomor VA tujuan tidak valid');
        }

        let args = {};
        args.vaccount_pickup = auth_user.dataValues.valdo_account;
        args.vaccount_toko = vaccount;
        args.amount = _.isUndefined(param_amount.value)?null:param_amount.value;
        args.pin = _.isUndefined(param_pin.value)?null:param_pin.value;
        args.usertype = auth_user.dataValues.type;

        args.desc = "PickUp TRANSFER Rp. "+helper.IDRformat(param_amount.value)+" dari "+pickup_name+ " ("+auth_user.dataValues.valdo_account+")"+" ke "+user_reponse.dataValues.fullname+" ("+param_to.value+")";
        args.transId = tanggal.getTime();
        
        let api_response = await MoneyAccess.pickup_transfer(args);

        Logs.create({action: 'api response money access vaccount_pickup_transfer_money pickup_transfer', description: util.inspect(api_response)});
        
        let pickup_transfer_message = JSON.stringify({
                title: 'Transfer Pickup Berhasil',
                body: 'Pickup transfer ke VA '+param_to.value+' dengan jumlah Rp. '+param_amount.value+' berhasil.'
            });

        let notification_args = {};

        if(user_reponse.dataValues.fcm_id == null) {
            notification_args.to = "";
        } else {
            notification_args.to = user_reponse.dataValues.fcm_id;
        }

        notification_args.type = 'notification';
        notification_args.message = pickup_transfer_message;
        notification_args.state = 0;
        notification_args.createdAt = helper.formatCurrentDateTime();
        notification_args.updatedAt = helper.formatCurrentDateTime();

        let notification_response = await service_notification.addNotification(notification_args);

        res.json(api_response);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal melakukan transfer, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function vaccount_transfer_money(req, res) {
    const token = req.swagger.params.token.value;
    const param_to = req.swagger.params.vaccount_to;
    const param_amount = req.swagger.params.amount;
    const param_pin = req.swagger.params.pin;

    try {
        Logs.create({action: 'vaccount_transfer_money', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_transfer_money, is_allowed_access_token_static: false});
        let tanggal = new Date();

        let args = {};
        args.mobnum = _.isUndefined(auth_user.dataValues.valdo_account)?null:auth_user.dataValues.valdo_account;
        args.from = _.isUndefined(auth_user.dataValues.valdo_account)?null:auth_user.dataValues.valdo_account;
        args.to = _.isUndefined(param_to.value)?null:param_to.value;
        args.amount = _.isUndefined(param_amount.value)?null:param_amount.value;
        args.pin = _.isUndefined(param_pin.value)?null:param_pin.value;
        args.usertype = auth_user.dataValues.type;
        args.transId = tanggal.getTime();

        let api_response = await MoneyAccess.transfer(args);

        Logs.create({action: 'api response money access vaccount_transfer_money transfer', description: util.inspect(api_response)});

        res.json(api_response);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal melakukan transfer, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function vaccount_get_history(req, res) {
    const token = req.swagger.params.token.value;
    const param_page = req.swagger.params.page;
    const param_item_per_page = req.swagger.params.item_per_page;
    const param_type = req.swagger.params.type;
    const param_start_date = req.swagger.params.start_date;
    const param_end_date = req.swagger.params.end_date;

    let page = _.isUndefined(param_page.value)?1:param_page.value;
    let item_per_page = _.isUndefined(param_item_per_page.value)?15:param_item_per_page.value;
    let type = _.isUndefined(param_page.value)?null:param_type.value;

    try {
        Logs.create({action: 'vaccount_get_history', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_history, is_allowed_access_token_static: false});
        let startDate = new Date();
        startDate.setDate(startDate.getDate() - 30);

        let args = {};
        args.vacc_number = _.isUndefined(auth_user.dataValues.valdo_account)?null:auth_user.dataValues.valdo_account;
        args.pagenum = page;
        args.item_per_page = item_per_page;
        args.type = type;
        args.usertype = auth_user.dataValues.type;
        args.start_date = helper.formatDate(startDate);
        args.end_date = helper.formatCurrentDate();

        let api_response = await MoneyAccess.history(args);

        Logs.create({action: 'api response money access vaccount_get_history history', description: util.inspect(api_response)});

        res.json(api_response);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal menampilkan data transaksi, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

//async function vaccount_pull_money_other_bank(req, res) {
//    const token = req.swagger.params.token.value;
//    const param_id_bank_account = req.swagger.params.id_bank_account;
//    const param_vacc_number = req.swagger.params.vacc_number;
//    const param_account_no = req.swagger.params.account_no;
//    const param_account_name = req.swagger.params.account_name;
//    const param_amount = req.swagger.params.amount;
//    const param_bank_name = req.swagger.params.bank_name;
//    const param_pin = req.swagger.params.pin;
//
//    let id_bank_account = _.isUndefined(param_id_bank_account.value)?null:param_id_bank_account.value;
//    let vacc_number = _.isUndefined(param_vacc_number.value)?null:param_vacc_number.value;
//    let account_no = _.isUndefined(param_account_no.value)?null:param_account_no.value;
//    let account_name = _.isUndefined(param_account_name.value)?null:param_account_name.value;
//    let amount = _.isUndefined(param_amount.value)?null:param_amount.value;
//    let bank_name = _.isUndefined(param_bank_name.value)?null:param_bank_name.value;
//
//    try {
//        Logs.create({action: 'vaccount_pull_money', description: helper.getSimpleSwaggerParams(req.swagger.params)});
//
//        let auth_user = await middleware.verify({token: token, acl: acl_pull_money, is_allowed_access_token_static: false});
//        let pin = _.isUndefined(param_pin.value)?null:param_pin.value;
//        let expected_pin = service_user.getHashedPin(auth_user.dataValues.salt, pin);
//
//        if(expected_pin != auth_user.dataValues.pin) {
//            throw new CustomError(400, 'Pin salah');
//        }
//
//        let bank_account = await service_bank_account.getOneBankAccount({user_id: auth_user.dataValues.user_id, bank_id: id_bank_account,no_rekening:account_no});
//        if(bank_account == null || !bank_account) {
//            throw new CustomError(400, 'Nomor rekening tidak di temukan');
//        }
//        if (amount < 10000){
//                throw new CustomError(400, 'Minimum Penarikan Rp. 10.000');
//        }
//        console.log(id_bank_account,"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
//        if(parseInt(id_bank_account) != 2){
//
//        if(parseInt(auth_user.dataValues.type) == 0){
//            if (amount < 20000){
//                throw new CustomError(400, 'Minimum Penarikan Rp. 20.000');
//            }
//           let today = new Date()
//           let current_time = today.getHours();
//           let time_setting = moment(today.setHours(current_time)).format('HH:mm:ss');
//           if(time_setting > '13:00:00'){
//               throw new CustomError(400, 'Waktu transaksi telah ditutup!');
//           }
//        }
//
//        let args = {};
//        args.vacc_number = vacc_number
//        args.account_no = account_no
//        args.account_name = account_name
//        args.amount = amount
//        args.bank_name = bank_name
//        request({
//            headers: {
//                'apigatekey': 'webKEY123456789',
//                'Content-Type' :'application/x-www-form-urlencoded'
//            },
//            url: 'http://192.168.12.2/api/v1/Cashout_trx_ob/dopayment',
//            method: "POST",
//            formData: args
//        }, function (error, result, body){
//            let api_response = JSON.parse(body)
//            Logs.create({action: 'api response money access vaccount_pull_money_other_bank', description: util.inspect(api_response)});
//            if(api_response === ""){
//                throw new CustomError(400, 'Penarikan gagal Coba lagi');
//            }
//
//            if(api_response.status.code === 200){
//                //api_response.response_timestamp = helper.formatDateTime(api_response.response_timestamp);
//                res.json(api_response);
//            }
//            else{
//                res.json({code:api_response.status.code,message:api_response.errors.error_desc});
//            }
//        })}
//    else{
//         let args = {};
//        args.credit_accountno = account_no;
//        args.value_date = moment(today).format('YYYY-MM-DD');
//        args.value_amount = amount;
//        args.destination_bankcode = null;
//        args.beneficiary_name = bank_name;
//        args.preferred_transfer_method_id = null;
//        args.remark1 = null;
//        args.extended_payment_detail = null;
//        args.desc = "Penarikan Dana Rp. "+helper.IDRformat(amount)+" ke No.Rek.Mandiri "+account_no+" oleh "+auth_user.dataValues.fullname+"("+auth_user.dataValues.valdo_account+")";
//        args.beneficiary_email_address = null;
//        args.payment_method = null;
//        args.reserved_field1 = null;
//        args.reserved_field2 = null;
//        args.vacc_number = vacc_number;
//        args.pin = pin;
//        args.transid = helper.currentMilisecondsTime();
//        args.usertype = auth_user.dataValues.type;
//        
//        let api_response = await MoneyAccess.dopayment(args);
//        res.json(api_response);
//    }
//
//
//        
//    } catch(err) {
//        if(err.code === undefined) {
//            err.code = 400;
//            err.message = 'Gagal mutasi, '+err;
//        }
//
//        res.status(err.code).json({code: err.code, message: err.message});
//    }
//}


async function vaccount_toko_transfer_money(req, res) {
    const token = req.swagger.params.token.value;
    const param_to = req.swagger.params.vaccount_to;
    const param_amount = req.swagger.params.amount;
    const param_pin = req.swagger.params.pin;

    try {
        Logs.create({action: 'vaccount_pickup_transfer_money', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_pickup_transfer_money, is_allowed_access_token_static: false});
        
        let tanggal = new Date();
        let vaccount = _.isUndefined(param_to.value)?null:param_to.value;
        let pickup_name = auth_user.dataValues.fullname;
        
        let check_users_type = await service_user.getOneByVA({vaccount:param_to.value});
        let user_reponse = await service_user.getOneByVA({vaccount: vaccount});

        if(user_reponse == null || !user_reponse) {
            throw new CustomError(400, 'Nomor VA tujuan tidak valid');
        }

        let args = {};
        args.vaccount_pickup = auth_user.dataValues.valdo_account;
        args.vaccount_toko = vaccount;
        args.amount = _.isUndefined(param_amount.value)?null:param_amount.value;
        args.pin = _.isUndefined(param_pin.value)?null:param_pin.value;
        args.usertype = auth_user.dataValues.type;

        args.desc = "PickUp TRANSFER Rp. "+helper.IDRformat(param_amount.value)+" dari "+pickup_name+ " ("+auth_user.dataValues.valdo_account+")"+" ke "+user_reponse.dataValues.fullname+" ("+param_to.value+")";
        args.transId = tanggal.getTime();
        
        let api_response = await MoneyAccess.pickup_toko_transfer(args);

        Logs.create({action: 'api response money access vaccount_pickup_transfer_money pickup_transfer', description: util.inspect(api_response)});
        
        let pickup_transfer_message = JSON.stringify({
                title: 'Transfer Pickup Berhasil',
                body: 'Pickup transfer ke VA '+param_to.value+' dengan jumlah Rp. '+param_amount.value+' berhasil.'
            });

        let notification_args = {};

        if(user_reponse.dataValues.fcm_id == null) {
            notification_args.to = "";
        } else {
            notification_args.to = user_reponse.dataValues.fcm_id;
        }

        notification_args.type = 'notification';
        notification_args.message = pickup_transfer_message;
        notification_args.state = 0;
        notification_args.createdAt = helper.formatCurrentDateTime();
        notification_args.updatedAt = helper.formatCurrentDateTime();

        let notification_response = await service_notification.addNotification(notification_args);

        res.json(api_response);
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal melakukan transfer, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function vaccount_request_topup(req, res) {
    const token = req.swagger.params.token.value;
    const param_vacc_number = req.swagger.params.vacc_number;
    const param_amount = req.swagger.params.amount;

    let vaccount = _.isUndefined(param_vacc_number.value)?null:param_vacc_number.value;
    let amount = _.isUndefined(param_amount.value)?null:param_amount.value;

    try {
        Logs.create({action: 'vaccount_pickup_transfer_money', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let user = await middleware.verify({token: token, acl: acl_request_topup, is_allowed_access_token_static: false});

        let detail = await service_user_additional_data.get({user_id:user.dataValues.user_id})

        let args = {}
        args.nama_toko = detail[0].dataValues.value;
        args.va= vaccount;
        args.amount = `${amount}`;
        args.topanid = `${user.dataValues.user_id}`;
            args.user_id = `${user.dataValues.user_id}`;

        request({
            headers: {
                'x-api-key': '$2a$10$QNB/3KKnXvzSRQMd/stp1eDEHbtZHlAaKfeTKKJ9R5.OtUnEgnrA6',
                'Content-Type': 'application/json'
            },
            url: 'http://192.168.13.5:9002/api_agenttokopandai/v2/requestTopup',
            method: "POST",
            json: true,
            body: args
        }, async function (error, result, body){
            if(body.status == 201){

                await TOPUP.create({
                    user_id: user.dataValues.user_id,
                    va: vaccount,
                    nama_toko: detail[0].dataValues.value,
                    amount: amount
                })

                res.status(200).json({status:'success', message:'request top-up success'});
            }
            else{
                res.status(400).json({status:'failed', message:'request top-up failed'});
            }
        })

        
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal melakukan transfer, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

async function vaccount_pull_money_other_bank(req, res) {
    const token = req.swagger.params.token.value;
    const param_id_bank_account = req.swagger.params.id_bank_account;
    const param_vacc_number = req.swagger.params.vacc_number;
    const param_account_no = req.swagger.params.account_no;
    const param_account_name = req.swagger.params.account_name;
    const param_amount = req.swagger.params.amount;
    const param_bank_name = req.swagger.params.bank_name;
    const param_pin = req.swagger.params.pin;

    let id_bank_account = _.isUndefined(param_id_bank_account.value)?null:param_id_bank_account.value;
    let vacc_number = _.isUndefined(param_vacc_number.value)?null:param_vacc_number.value;
    let account_no = _.isUndefined(param_account_no.value)?null:param_account_no.value;
    let account_name = _.isUndefined(param_account_name.value)?null:param_account_name.value;
    let amount = _.isUndefined(param_amount.value)?null:param_amount.value;
    let bank_name = _.isUndefined(param_bank_name.value)?null:param_bank_name.value;

    try {
        Logs.create({action: 'vaccount_pull_money', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_pull_money, is_allowed_access_token_static: false});
        let pin = _.isUndefined(param_pin.value)?null:param_pin.value;
        let expected_pin = service_user.getHashedPin(auth_user.dataValues.salt, pin);

        if(expected_pin != auth_user.dataValues.pin) {
            throw new CustomError(400, 'Pin salah');
        }

        let bank_account = await service_bank_account.getOneBankAccount({user_id: auth_user.dataValues.user_id, bank_id: id_bank_account,no_rekening:account_no});
        
        if(bank_account == null || !bank_account) {
            throw new CustomError(400, 'Nomor rekening tidak di temukan');
        }
        
//        if(parseInt(id_bank_account) != 2){
        if(parseInt(auth_user.dataValues.type) == 0){
        if(parseInt(id_bank_account) != 2){
            if (amount < 20000){
                throw new CustomError(400, 'Minimum Penarikan Rp. 20.000');
            }
           let today = new Date()
           let current_time = today.getHours();
           let time_setting = moment(today.setHours(current_time)).format('HH:mm:ss');
           if(time_setting > '13:00:00'){
               throw new CustomError(400, 'Waktu transaksi telah ditutup!');
           }
        }}
//        if (amount < 10000){
//                throw new CustomError(400, 'Minimum Penarikan Rp. 10.000');
//        }
        let args = {};
        args.vacc_number = vacc_number
        args.account_no = account_no
        args.account_name = account_name
        args.amount = amount
        args.bank_name = bank_name
        let api_response = await MoneyAccess.cashout_other_bank(args);
        res.json(api_response);
//    }else{
//        var today = new Date(); 
//         let args = {};
//        args.credit_accountno = account_no;
//        args.value_date = moment(today).format('YYYY-MM-DD');
//        args.value_amount = amount;
//        args.destination_bankcode = undefined;
//        args.beneficiary_name = bank_name;
//        args.preferred_transfer_method_id = 0;
//        args.remark1 = null;
//        args.extended_payment_detail = null;
//        args.desc = "Penarikan Dana Rp. "+helper.IDRformat(amount)+" ke No.Rek.Mandiri "+account_no+" oleh "+auth_user.dataValues.fullname+"("+auth_user.dataValues.valdo_account+")";
//        args.beneficiary_email_address = null;
//        args.payment_method = null;
//        args.reserved_field1 = null;
//        args.reserved_field2 = null;
//        args.vacc_number = vacc_number;
//        args.pin = pin;
//        args.transid = helper.currentMilisecondsTime();
//        args.usertype = auth_user.dataValues.type;
//        
//        let api_response = await MoneyAccess.dopayment(args);
//        res.json(api_response);
//    }
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.message = 'Gagal mutasi, '+err;
        }

        res.status(err.code).json({code: err.code, message: err.message});
    }
}

export { vaccount_check_owner_bank_account }
export { vaccount_pull_money }
export { vaccount_pickup_transfer_money }
export { vaccount_toko_transfer_money }
export { vaccount_transfer_money }
export { vaccount_get_history }
export { vaccount_pull_money_other_bank }
export { vaccount_request_topup }