'use strict';
import _ from 'lodash'
import util from 'util'
import middleware from '~/api/controllers/auth'
import moment from 'moment'
import request from 'request'

import service_fmcg from '~/services/fmcg'
import service_user from '~/services/user'
import helper from '~/utils/helper'


import ApiTop from '~/external_api/api_top'
import ApiFmcg from '~/external_api/api_fmcg'


import { Logs } from '~/orm/index'

import CustomError from '~/response/error/CustomError'

const acl_get_invoices = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];
const acl_get_one_invoice = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];
const acl_create_retur = ['retailer','distributor', 'distributor_manager', 'sales', 'pickup_agent', 'unilever_admin', 'super_admin', 'call_center'];


//let top_api = "tokopandai-development.pinjammodal.id";
//let top_api_key = "0ff0170259be55375551f943d9ebf04eb097d0b55661a5e8f13dac3ef9062820";

let top_api = "api.pinjammodal.id:8443/tokopandai";
let top_api_key = "904071eb7378b9f0cc7152b7ada253d7f9493e0e4a1ef9a0634ebf101b7e84ca"


async function apply_loan(req, res) {
    const token = req.swagger.params.token.value;
    const param_fmcg_id = req.swagger.params.fmcg_id;
    const param_le_code = req.swagger.params.le_code;
    const param_dt_code = req.swagger.params.dt_code;
    const param_amount = req.swagger.params.amount;
    const param_invoice_id = req.swagger.params.invoice_id;
    const param_principal_code = req.swagger.params.principal_code;
    const param_total_potongan = req.swagger.params.total_potongan;
    const param_pin = req.swagger.params.pin;


    const iso8601format = "YYYY-MM-DD";
    
    let pin_user = _.isUndefined(param_pin.value)?null:param_pin.value;
    let fmcg_id = _.isUndefined(param_fmcg_id.value)?null:param_fmcg_id.value;
    let dt_code = _.isUndefined(param_dt_code.value) ? null : param_dt_code.value;
    let le_code = _.isUndefined(param_le_code.value) ? null : param_le_code.value;
    let amount = _.isUndefined(param_amount.value) ? null : param_amount.value;
    let invoice_number = _.isUndefined(param_invoice_id.value) ? null : param_invoice_id.value;
    let principal_code = _.isUndefined(param_principal_code.value) ? null : param_principal_code.value;
    let total_potongan = _.isUndefined(param_total_potongan.value) ? null : param_total_potongan.value;
    let discounts =0;
    let data_unique_code = '';

    try {
        Logs.create({action: 'apply_loan', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_get_one_invoice, is_allowed_access_token_static: false});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});

        if(result_fmcg == null) {
            throw new CustomError(400, 'Gagal menampilkan data apply loan, data fmcg tidak tersedia');
        }
        
        let result_fit = await service_fmcg.getOneFmcgWithFilter({id: 1});

        if(result_fit == null) {
            throw new CustomError(400, 'Gagal menampilkan data apply loan, data fmcg tidak tersedia');
        }
        
        let user_salt = auth_user.dataValues.salt;
        
        let newpin = await service_user.getHashedPassword(user_salt, pin_user.toString());
        if(auth_user.dataValues.pin != newpin) {
            throw new CustomError(400, 'Pin Salah');
        }

        let tenor = 1
    
        let invoice_args = {};
        invoice_args.url = result_fmcg.dataValues.url;
        invoice_args.fmcg_token = result_fmcg.dataValues.fmcg_token;
        invoice_args.fmcg_name = result_fmcg.dataValues.name;
        invoice_args.invoice_id = invoice_number;
        invoice_args.dt_code = dt_code;
        
        let api_response_fmcg = await ApiFmcg.get_one_invoice(invoice_args);
	if (amount !== parseInt(api_response_fmcg["total_amount"])){
          throw new CustomError(400, 'Pengajuan sebagian invoice ditolak');
        }

        if (api_response_fmcg["balance_amount"] == 0){
          throw new CustomError(400, 'Invoice sudah lunas');
        }

        if (api_response_fmcg["balance_amount"] < 100000){
          throw new CustomError(400, 'Invoice Minimal Rp. 100.000');
        }
    
        let args ={
           "dt_code": dt_code,
           "le_code": le_code,
           "invoice_number": invoice_number,
           "amount": amount,
           "tenor": tenor,
           "principle_code": principal_code,
           "discount": parseInt(total_potongan)
         };
       request({
           headers: {
           'Content-Type': 'application/json',
           'api_key' : top_api_key
           },
            url: "https://"+top_api+"/loan/apply-tokopandai",
            method: "POST",
            json: true,
            body: args
          }, function (error, response, body){
              if(body.status === 'success'){
                  let invoice_update = {};
                   invoice_update.url = result_fmcg.dataValues.url;
                   invoice_update.fmcg_token = result_fmcg.dataValues.fmcg_token;
                   invoice_update.fmcg_name = result_fmcg.dataValues.name;
                   invoice_update.le_code = le_code;
                   invoice_update.dt_code = dt_code;
                   invoice_update.amount = 0;
                   invoice_update.invoice_id = invoice_number;
                   invoice_update.paid_by = "Pinjam Modal";
                   
                    
                   let argss = {
                    url: result_fit.dataValues.url,
                    fmcg_token : result_fit.dataValues.fmcg_token,
                    fmcg_name : result_fit.dataValues.name,
                    user_id:auth_user.dataValues.user_id,
                    le_code:le_code,
                    dt_code:dt_code,
                    amount:amount,
                    total_amount_invoice:api_response_fmcg["total_amount"],
                    invoice_id:invoice_number,
                    tenor:tenor,
                    principal_code:fmcg_id,
                    total_potongan:total_potongan,
                    loan_type : "Cash",
                    loan_by : "Pinjam Modal"
                  }
                  ApiFmcg.updatePaymentPembiayaan(invoice_update);
                  ApiTop.insert_loan_history(argss)
                   res.status(200).json(body);
              }else
              {
                  res.status(400).json(body);
              }

        });
        
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan update invoice, '+err;
        }

        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function calculator(req, res) {
    const token = req.swagger.params.token.value;
    const param_amount = req.swagger.params.amount;
    const iso8601format = "YYYY-MM-DD";

    let amount = _.isUndefined(param_amount.value) ? 0 : param_amount.value;
    let data_unique_code = '';
    let fmcg_id =13;
    let discounts = 0;
    var ten = amount.split(".", 1);
    
    try {
        Logs.create({action: 'calculator top', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        
        let auth_user = await middleware.verify({token: token, acl: acl_get_invoices, is_allowed_access_token_static: false});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});

        let tenor = 1
        
        let today = new Date()
        var weekday = new Array(7);
        weekday[0] = "Minggu";
        weekday[1] = "Senin";
        weekday[2] = "Selasa";
        weekday[3] = "Rabu";
        weekday[4] = "Kamis";
        weekday[5] = "Jumat";
        weekday[6] = "Sabtu";
        let day = weekday[today.getDay()];
        let current_time = today.getHours();
        let time_setting = moment(today.setHours(current_time)).format('HH:mm:ss');

        if (amount < 100000){
          throw new CustomError(400, 'Invoice Minimal Rp. 100.000');
        }
        request({
            headers: {
            'Content-Type': 'application/json'
            },
             url: "https://"+top_api,
             method: "GET",
             json: true
           }, function (error, response, body){
                if(response.statusCode === 502 ){
                    res.status(400).json({code: response.statusCode, message: "PayLater Sedang Dalam Perbaikan"});
                }
                else{

        
        let args ={
            "amount" : parseInt(ten[0]),
            "discount": 0,
            "tenor":tenor
        }
        request({
            headers: {
            'Content-Type': 'application/json',
            'api_key' : top_api_key
            },
             url: "https://"+top_api+"/loan/calculator-tokopandai",
             method: "POST",
             json: true,
             body: args
           }, function (error, response, body){
               
            discounts = body.installmentLoanDetail[0].loan_payback - body.installmentLoanDetail[0].loan_amount;

            let dis = discounts/100*20;
            let diss =0;
            if(dis  % 1 !== 0 ){
                diss= parseInt(dis)+1;
            }
            else
            {
                diss=dis;
            }
           let data ={
                "amount" : parseInt(ten[0]),
                "discount": diss,
                "tenor" : tenor
             }
        request({
            headers: {
            'Content-Type': 'application/json',
            'api_key' : top_api_key
            },
             url: "https://"+top_api+"/loan/calculator-tokopandai",
             method: "POST",
             json: true,
             body: data
           }, function (error, response, body){
                let week = [{
                "loan_amount" :body.installmentLoanDetail[0].loan_amount+".0000",
                "loan_payback":body.installmentLoanDetail[0].loan_payback+".0000",
                "administrasi":(body.installmentLoanDetail[0].loan_payback+body.installmentLoanDetail[0].discount-body.installmentLoanDetail[0].loan_amount)+".0000",
                "loan_length":body.installmentLoanDetail[0].loan_length,
                "installment":body.installmentLoanDetail[0].installment+".0000",
                "total_potongan":body.installmentLoanDetail[0].discount+".0000",
            }]
                res.json({week});
         });
         
         });
                        }
                   })
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data calculator TOP, '+err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}

async function create_retur(req,res){
    const token = req.swagger.params.token.value;
    const param_amount_potongan = req.swagger.params.amount_potongan;
    const param_dt_code = req.swagger.params.dt_code;
    const param_le_code = req.swagger.params.le_code;
    const param_invoice_id = req.swagger.params.invoice_id;
    

    let amount_potongan = _.isUndefined(param_amount_potongan.value)? 0 : param_amount_potongan.value;
    let dt_code = _.isUndefined(param_dt_code.value)? null : param_dt_code.value;
    let le_code = _.isUndefined(param_le_code.value)? null : param_le_code.value;
    let invoice_id = _.isUndefined(param_invoice_id.value)? null : param_invoice_id.value;
    let fmcg_id =13;

    
    try {
        Logs.create({action: 'create retur', description: helper.getSimpleSwaggerParams(req.swagger.params)});

        let auth_user = await middleware.verify({token: token, acl: acl_create_retur, is_allowed_access_token_static: false});
        let result_fmcg = await service_fmcg.getOneFmcgWithFilter({id: fmcg_id});

        let args = {
            url : result_fmcg.dataValues.url,
            fmcg_token : result_fmcg.dataValues.fmcg_token,
            fmcg_name : result_fmcg.dataValues.name,
            amount_potongan : amount_potongan,
            le_code : le_code,
            dt_code : dt_code,
            invoice_id :invoice_id
        }

        let create_retur_res = await ApiFmcg.create_retur(args)
        res.status(200).json({create_retur_res})
        
    } catch(err) {
        if(err.code === undefined) {
            err.code = 400;
            err.err_code = '';
            err.message = 'Gagal menampilkan data potongan, '+err;
        }
        res.status(err.code).json({code: err.code, err_code: err.err_code, message: err.message});
    }
}


export { apply_loan }
export { calculator }
export { create_retur }
