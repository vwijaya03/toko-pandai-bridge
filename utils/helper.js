import _ from 'lodash'
import Sequelize from 'sequelize'
import URLSafeBase64 from 'urlsafe-base64'
import moment from 'moment'
import util from 'util'

import { orm } from '~/services/mariadb'
import { RequestLimit } from '~/orm/index'

import CustomError from '~/response/error/CustomError'

const Op = Sequelize.Op;

// moment().format('YYYY-MM-DD HH:mm:ss')
// 2018-10-10 21:10:10

async function requestLimit({accessToken=null, lastRequestDate=null, limitRequestDate=null}={}) {
	let requestLimit = await orm.query(`select lastRequestDate, limitRequestDate from request_limits where accessToken = "${accessToken}" order by lastRequestDate desc limit 1`, { type: orm.QueryTypes.SELECT});
	if(requestLimit.length > 0) {
		if(lastRequestDate <= requestLimit[0].limitRequestDate) {
			throw new CustomError(400, 'Duplicate request, please try again');
		} else {
			await RequestLimit.create({accessToken: accessToken, lastRequestDate: lastRequestDate, limitRequestDate: limitRequestDate});
		}
	} else {
		await RequestLimit.create({accessToken: accessToken, lastRequestDate: lastRequestDate, limitRequestDate: limitRequestDate});
	}
}

function modifiedSequelizeTimestamp(data) {
	let d = new Date(data);
	d.setHours(d.getHours() + 7);

	return d;
}

function modifiedSequelizeTimestampStandart(data) {	
	let d = new Date(data);

	return d;
}

function formatCurrentDateTime() {

	let d = new Date();
	return d.getFullYear()+"-"+("0" + (d.getMonth() + 1)).slice(-2)+"-"+("0" + d.getDate()).slice(-2)+" "+("0" + d.getHours()).slice(-2)+":"+("0" + d.getMinutes()).slice(-2)+":"+("0" + d.getSeconds()).slice(-2);
}

function formatCurrentDateTimeWithoutSeparator() {

	let d = new Date();
	return d.getFullYear()+("0" + (d.getMonth() + 1)).slice(-2)+("0" + d.getDate()).slice(-2)+("0" + d.getHours()).slice(-2)+("0" + d.getMinutes()).slice(-2)+("0" + d.getSeconds()).slice(-2);
}

function formatCurrentDateTimeAdd7Hours() {

	let d = new Date();
	d.setHours(d.getHours());
	return d.getFullYear()+"-"+("0" + (d.getMonth() + 1)).slice(-2)+"-"+("0" + d.getDate()).slice(-2)+" "+("0" + d.getHours()).slice(-2)+":"+("0" + d.getMinutes()).slice(-2)+":"+("0" + d.getSeconds()).slice(-2);
}

function formatDateTime(data) {

	let d = new Date(data);
	return d.getFullYear()+"-"+("0" + (d.getMonth() + 1)).slice(-2)+"-"+("0" + d.getDate()).slice(-2)+" "+("0" + d.getHours()).slice(-2)+":"+("0" + d.getMinutes()).slice(-2)+":"+("0" + d.getSeconds()).slice(-2);
}

function formatDateTimeAdd7Hours(data) {

	let d = new Date(data);
	d.setHours(d.getHours() + 7);
	return d.getFullYear()+"-"+("0" + (d.getMonth() + 1)).slice(-2)+"-"+("0" + d.getDate()).slice(-2)+" "+("0" + d.getHours()).slice(-2)+":"+("0" + d.getMinutes()).slice(-2)+":"+("0" + d.getSeconds()).slice(-2);
}

function formatCustomCurrentDateTime(month, date, hour, minute, second) {
	let d = new Date();

	if(month) {
		d.setMonth(d.getMonth() + month);
	}

	if(date) {
		d.setDate(d.getDate() + date);
	}

	if(hour) {
		d.setHours(d.getHours() + hour);
	}

	if(minute) {
		d.setMinutes(d.getMinutes() + minute);
	}

	if(second) {
		d.setSeconds(d.getSeconds() + second);
	}

	return d.getFullYear()+"-"+("0" + (d.getMonth() + 1)).slice(-2)+"-"+("0" + d.getDate()).slice(-2)+" "+("0" + d.getHours()).slice(-2)+":"+("0" + d.getMinutes()).slice(-2)+":"+("0" + d.getSeconds()).slice(-2);
}

function formatCustomCurrentDateTimeWithParam(value, month, date, hour, minute, second) {
	let d = new Date(value);

	if(month) {
		d.setMonth(d.getMonth() + month);
	}

	if(date) {
		d.setDate(d.getDate() + date);
	}

	if(hour) {
		d.setHours(d.getHours() + hour);
	}

	if(minute) {
		d.setMinutes(d.getMinutes() + minute);
	}

	if(second) {
		d.setSeconds(d.getSeconds() + second);
	}

	return d.getFullYear()+"-"+("0" + (d.getMonth() + 1)).slice(-2)+"-"+("0" + d.getDate()).slice(-2)+" "+("0" + d.getHours()).slice(-2)+":"+("0" + d.getMinutes()).slice(-2)+":"+("0" + d.getSeconds()).slice(-2);
}

function createVANumber(user_id, usertype) {
	let kode_perusahaan = '01';
	let code_role = '';

    if(usertype == 1 || usertype == 3)
        code_role = 2;
    else if(usertype == 0)
        code_role = 1;
    else if(usertype == 2)
        code_role = 251;
    else if(usertype == 11)
        code_role = 11;

	let str = "" + user_id;
    let pad = "0000000";

    if(usertype == 2){
        pad = "00000";
    } else if(usertype == 11) {
        pad = "00000";
    }

    let unique_number = pad.substring(0, pad.length - str.length) + str;

    return code_role+kode_perusahaan+unique_number;
}

function isPhoneNumberOnly(str) {
	let regex = /^[0-9]{1,14}$/;

	if(str.match(regex)) {
		return true;
	} else {
		return false;
	}
}

function currentMilisecondsTime() {
	let d = new Date();
	return d.getTime();
}

function removeNullFromObject(data) {
	let keys = Object.keys(data);

	for (let i = 0; i < keys.length; i++) {
		let key = keys[i];
		if(data[key] == null) {
			data[key] = "";
		}
	}

	return data;
}

function splitString(str) {
	if(str == null || str.length == 0) {
		return "";
	} else {
		return str.split(" ");
	}
}

function createReceiptField(field, value, type, compact) {
    return {
        field: field,
        value: value,
        type: type,
        compact :compact
    }
}

function formatMilisecondToReadableHour(value) {
    var months_arr = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var date = new Date(value*1000);
    var year = date.getFullYear();
    var month = months_arr[date.getMonth()];
    var day = date.getDate();
    var hours = date.getHours();
    var minutes = "0" + date.getMinutes();
    var seconds = "0" + date.getSeconds();
    var convdataTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);

    return convdataTime;
//    let d = new Date(value);
//    return ("0" + d.getHours()).slice(-2)+":"+("0" + d.getMinutes()).slice(-2)+":"+("0" + d.getSeconds()).slice(-2);
}

function formatMilisecondToReadableDate(value) {
//    let months = ["Jan", "Feb", "Mar", "April", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
//
//    let d = new Date(value);
//    return ("0" + d.getDate()).slice(-2)+" "+months[d.getMonth()]+" "+d.getFullYear();
    var months_arr = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var date = new Date(value*1000);
    var year = date.getFullYear();
    var month = months_arr[date.getMonth()];
    var day = date.getDate();
    var hours = date.getHours();
    var minutes = "0" + date.getMinutes();
    var seconds = "0" + date.getSeconds();
    var convdataTime = day+' '+month+' '+year;

    return convdataTime;
}

function formatMilisecondToReadableDates(value) {
    let months = ["Jan", "Feb", "Mar", "April", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    let d = new Date(value);
    return ("0" + d.getDate()).slice(-2)+" "+months[d.getMonth()]+" "+d.getFullYear();
}

function IDRformat(angka) {
    let temprev = parseInt(angka, 10);
    let positive = true;
    
    if(temprev < 0) {
        temprev = temprev * -1;
        positive = false;
    }

    let rev = temprev.toString().split('').reverse().join('');
    let rev2 = '';
    for(let i = 0; i < rev.length; i++){
        rev2  += rev[i];
        if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
            rev2 += '.';
        }
    }

    if(!positive) {
        return '(IDR ' + rev2.split('').reverse().join('') + ')';
    }

    // return 'IDR ' + rev2.split('').reverse().join('');
    return rev2.split('').reverse().join('');
}

function customSuccessResponse(initial, key, value) {
	let response = {};

	response.code = initial.code;
	response.message = initial.message;
	response[key] = value;
	
	return response;
}

function formatCurrentDate() {

	let d = new Date();
	return d.getFullYear()+"-"+("0" + (d.getMonth() + 1)).slice(-2)+"-"+("0" + d.getDate()).slice(-2);
}

function formatDate(data) {

	let d = new Date(data);
	return d.getFullYear()+"-"+("0" + (d.getMonth() + 1)).slice(-2)+"-"+("0" + d.getDate()).slice(-2);
}

function transactionType(type) {
	let trans_type = "";

	if(type == 0)
        trans_type = "transfer";
    else if(type == 1)
        trans_type = "topup";
    else if(type == 2)
        trans_type = "topup-reversal";
    else if(type == 3)
        trans_type = "disbursment";
    else if(type == 4)
        trans_type = "pickup";
    else if(type == 5)
        trans_type = "transfer_cl";
    else if(type == 6)
        trans_type = "ppob";
    else if(type == 7)
        trans_type = "cashback";
    else if(type == 8)
        trans_type = "ppob_pbl";
    else
    	trans_type = "";
    return trans_type;
}

function getTransId({userId=null}={}) {
    let encodedUserId = URLSafeBase64.encode(userId+'');
    let timestamp = moment.utc().format('YYYYMMDDHHmmssSSS');
    let transId = 'TX' + encodedUserId + timestamp;
    
    return transId;
}

function getPayInvoiceTransId({user_id=null}={}) {
    let encodedUserId = URLSafeBase64.encode(user_id+'');
    let timestamp = moment.utc().format('YYYYMMDDHHmmssSSS');
    let transId = 'PI' + encodedUserId + timestamp;
    
    return transId;
}

function getSimpleSwaggerParams(params) {
    return util.inspect(_.map(_.keys(params), (key) => {
        let param = params[key];
        return {
            key: key,
            value: param.value,
            valid: param.valid,
            error: param.error,
            definition: param.parameterObject.definition
        }
    }));
}

let obj = {};
obj.modifiedSequelizeTimestamp = modifiedSequelizeTimestamp
obj.modifiedSequelizeTimestampStandart = modifiedSequelizeTimestampStandart
obj.formatCurrentDateTime = formatCurrentDateTime
obj.formatCurrentDateTimeAdd7Hours = formatCurrentDateTimeAdd7Hours
obj.formatDateTime = formatDateTime
obj.formatDateTimeAdd7Hours = formatDateTimeAdd7Hours
obj.formatCustomCurrentDateTime = formatCustomCurrentDateTime
obj.createVANumber = createVANumber
obj.isPhoneNumberOnly = isPhoneNumberOnly
obj.currentMilisecondsTime = currentMilisecondsTime
obj.removeNullFromObject = removeNullFromObject
obj.splitString = splitString
obj.createReceiptField = createReceiptField
obj.formatMilisecondToReadableHour = formatMilisecondToReadableHour
obj.formatMilisecondToReadableDate = formatMilisecondToReadableDate
obj.formatMilisecondToReadableDates = formatMilisecondToReadableDates
obj.IDRformat = IDRformat
obj.formatCurrentDate = formatCurrentDate
obj.formatDate = formatDate
obj.customSuccessResponse = customSuccessResponse
obj.transactionType = transactionType
obj.getTransId = getTransId
obj.getPayInvoiceTransId = getPayInvoiceTransId
obj.getSimpleSwaggerParams = getSimpleSwaggerParams;
obj.formatCurrentDateTimeWithoutSeparator = formatCurrentDateTimeWithoutSeparator;
obj.formatCustomCurrentDateTimeWithParam = formatCustomCurrentDateTimeWithParam;
obj.requestLimit = requestLimit;

export default obj
export { Op }
