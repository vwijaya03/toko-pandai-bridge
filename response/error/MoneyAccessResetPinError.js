class MoneyAccessResetPinError extends Error {
	constructor(message) {
        super(message);
        this.code = 400;
        this.err_code = 4100;
        this.message = 'Reset pin gagal, '+message;
    }
}

export default MoneyAccessResetPinError