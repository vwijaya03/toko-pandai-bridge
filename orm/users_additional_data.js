import { orm } from '~/services/mariadb'
import Sequelize from 'sequelize'

const UserAdditionalData = orm.define('users_additional_datas', {
    id: { type: Sequelize.INTEGER, field: 'id', primaryKey: true, autoIncrement: true },
    user_id: { type: Sequelize.STRING, allowNull:false },
    field: { type: Sequelize.STRING, allowNull:false },
    value: { type: Sequelize.STRING, allowNull:true },
    createdAt: { type: 'TIMESTAMP', allowNull:true },
    updatedAt: { type: 'TIMESTAMP', allowNull:true }
});

export default UserAdditionalData