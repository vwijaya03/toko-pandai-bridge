import request from 'request'
import util from 'util'

import { Logs } from '~/orm/index'

import CustomError from '~/response/error/CustomError'
import FmcgGetInvoicesError from '~/response/error/FmcgGetInvoicesError'
import FmcgGetReturError from '~/response/error/FmcgGetReturError'
import FmcgGetTotalUnpaidAmountError from '~/response/error/FmcgGetTotalUnpaidAmountError'
import FmcgCheckProcessInvoiceError from '~/response/error/FmcgCheckProcessInvoiceError'
import FmcgTaskListError from '~/response/error/FmcgTaskListError'
import FmcgMakePaymentError from '~/response/error/FmcgMakePaymentError'
import FmcgProcessInvoiceError from '~/response/error/FmcgProcessInvoiceError'
import FmcgUploadDataPotonganError from '~/response/error/FmcgUploadDataPotonganError'
import FmcgUploadDataPembatalanPotonganError from '~/response/error/FmcgUploadDataPembatalanPotonganError'
import FmcgTaskListPotonganError from '~/response/error/FmcgTaskListPotonganError'
import FmcgTaskListPembatalanPotonganError from '~/response/error/FmcgTaskListPembatalanPotonganError'

const api_path_get_invoice = '/top-ext/get-invoice';
const api_path_get_one_invoice = '/top-ext/get-one-invoice';
const api_path_cek_saldo = '/top-ext/cek-saldo';
const api_path_calculator = '/top-ext/calculator';
const api_path_apply_loan = '/top-ext/apply-loan';
const api_path_repayment= '/top-ext/repayment';
const api_path_payment_distributor = '/top-ext/payment-distributor';

function commonHeaders(token) {
    return {
        'Cache-Control': 'no-cache',
        'token': token
    };
}
function handleResponse(rawJsonResponse, errorClass, resolve, reject, requestError, function_name, fmcg_name) {

	try {
		let successJsonResp = JSON.parse(rawJsonResponse);

		if(requestError != null) {
			reject(new errorClass(requestError));
		}

		if(successJsonResp.code == 400 || successJsonResp.code == 401) {
			reject(new errorClass(successJsonResp.message));
		} else {
			resolve(successJsonResp);
		}
	} catch(err) {
		Logs.create({action: `error ${function_name} ${fmcg_name}`, description: util.inspect(err), additional_description: util.inspect(rawJsonResponse)});

		reject(new errorClass('terjadi kesalahan silahkan coba lagi'));
	}

}

function get_invoice({url=null,tenor=null, fmcg_token=null,user_id=null, fmcg_name=null, dt_code=null, le_code=null,sorting=null,sortingBy=null,page=null,items_per_page=null}={}) {
	let invoice_args = {};

	invoice_args.url = url;
	invoice_args.fmcg_token = fmcg_token;
	invoice_args.le_code = le_code;
        invoice_args.dt_code = dt_code;
        
        invoice_args.user_id = user_id;
        invoice_args.sorting = sorting;
        invoice_args.sortingBy = sortingBy;
        invoice_args.page = page;
        invoice_args.item_per_page = items_per_page;
        
	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_invoice = url+api_path_get_invoice;
	    request.get( {url:api_invoice, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}

function get_one_invoice({url=null,tenor=null, fmcg_token=null,user_id=null, fmcg_name=null, dt_code=null, le_code=null,sorting=null,sortingBy=null,page=null,items_per_page=null}={}) {
	let invoice_args = {};

	invoice_args.url = url;
	invoice_args.fmcg_token = fmcg_token;
	invoice_args.le_code = le_code;
        invoice_args.dt_code = dt_code;
	invoice_args.invoice_id = invoice_id;
        invoice_args.tenor = tenor;

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_one_invoice = url+api_path_get_one_invoice;
	    request.get( {url:api_one_invoice, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}

function cek_saldo({le_code=null}={}) {
	let invoice_args = {};
        let url='http://localhost:30111';
        let fmcg_token='2b3e6865-bdae-4881-ba71-ea9c5f123e8d';
        let fmcg_name='TopExt';

	invoice_args.le_code = le_code;

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_cek_saldo = url+api_path_cek_saldo;
	    request.get( {url:api_cek_saldo, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}


function calculator({url=null, fmcg_token=null, amount=null,fmcg_name=null,discount=null}={}) {
	let invoice_args = {};

	invoice_args.url = url;
	invoice_args.fmcg_token = fmcg_token;
	invoice_args.amount = amount;
	invoice_args.discount = discount;

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_calculator = url+api_path_calculator;
        request.get( {url:api_calculator, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
            if(err) {
                    return err;
            }
            handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
        }).on('error', function(err) {
                    handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
            });
    });
}

function apply_loan({url=null,fmcg_token=null,dt_code =null,le_code =null,invoice_number=null,tenor=null , amount=null, principal_code =null,total_potongan=null,fmcg_name=null}={}) {
	let invoice_args = {};

	invoice_args.url = url;
	invoice_args.fmcg_token = fmcg_token;
	invoice_args.dt_code = dt_code;
	invoice_args.le_code = le_code;
	invoice_args.invoice_number = invoice_number;
	invoice_args.tenor = tenor;
	invoice_args.amount = amount;
	invoice_args.principal_code = principal_code;
	invoice_args.total_potongan = total_potongan;

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_apply_loan = url+api_path_apply_loan;
	    request.post( {url:api_apply_loan, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
        });
}


function repayment({url=null,fmcg_id=null,vaccount=null, fmcg_token=null, amount=null,fmcg_name=null,invoice_number=null,le_code=null,dt_code=null}={}) {
	let invoice_args = {};

	invoice_args.url = url;
	invoice_args.fmcg_token = fmcg_token;
        invoice_args.fmcg_id = fmcg_id;
	invoice_args.amount = amount;
	invoice_args.invoice_number = invoice_number;
        invoice_args.le_code = le_code;
	invoice_args.dt_code = dt_code;

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_repayment = url+api_path_repayment;
	    request.post( {url:api_repayment, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}

function payment_distributor({url=null, fmcg_token=null, le_code=null,dt_code=null,invoice_no=null,amount=null,principal_code=null,fmcg_name=null}={}) {
	let invoice_args = {};
	invoice_args.url = url;
	invoice_args.fmcg_token = fmcg_token;
	invoice_args.le_code = le_code;
	invoice_args.dt_code = dt_code;
	invoice_args.invoice_no = invoice_no;
	invoice_args.amount = amount;
	invoice_args.principal_code = principal_code;

	return new Promise(function(resolve, reject) {
        let headers = commonHeaders(fmcg_token);
        let api_payment_distributor = url+api_path_payment_distributor;
	    request.post( {url:api_payment_distributor, headers:headers, qs:invoice_args}, function(err, httpResponse, body) {
	    	if(err) {
	    		return err;
	    	}
	    	handleResponse(body, FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
	    }).on('error', function(err) {
			handleResponseError(FmcgGetInvoicesError, resolve, reject, err, 'get invoices', fmcg_name);
		});
    });
}



let obj = {}

obj.get_invoice = get_invoice
obj.get_one_invoice = get_one_invoice
obj.cek_saldo = cek_saldo
obj.calculator = calculator
obj.apply_loan = apply_loan
obj.repayment = repayment
obj.payment_distributor = payment_distributor

export default obj

