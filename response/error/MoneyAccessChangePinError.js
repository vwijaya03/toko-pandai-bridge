class MoneyAccessChangePinError extends Error {
	constructor(message) {
        super(message);
        this.code = 400;
        this.err_code = 4100;
        this.message = 'Pin gagal di ubah, '+message;
    }
}

export default MoneyAccessChangePinError