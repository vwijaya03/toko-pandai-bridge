'use strict';

import moment from 'moment'
import crypto from 'crypto'
import RandomString from 'randomstring'
import NodeMailer from 'nodemailer'

import { Op } from '~/utils/helper'
import { orm } from '~/services/mariadb'
import { Bfi } from '~/orm/index'
import { allowed_user_status } from '~/properties'

function getOneStatusTracking({cust_appid=null}={}){
   return  orm.query('SELECT id,customer_name,address,mobile_phone,asset_name,manufacturing_year,funding_amount,fundingamount,installment,tenor,cust_appid,telestatus,teledatetime,telereason,surveydscheduledatetime,surveyrescheduledatetime,surveystatus,surveydatetime,surveyreason,approvalstatus,approvaldatetime,golivestatus,golivedatetime,result,notes,create_at FROM bfi_leasing where cust_appid = "'+cust_appid+'"', { type: orm.QueryTypes.SELECT})
          .then(function(tasks){
           tasks.forEach(result => {
                result.golivestatus=result.golivestatus == null ? "Pending"  : result.golivestatus;
                result.telestatus=result.telestatus == null ? "Pending"  : result.telestatus;
                result.teledatetime=result.teledatetime == null ? "Pending"  : result.teledatetime;
                result.telereason=result.telereason == null ? "Pending"  : result.telereason;
                result.surveydscheduledatetime=result.surveydscheduledatetime == null ? "Pending"  : result.surveydscheduledatetime;
                result.surveyrescheduledatetime=result.surveyrescheduledatetime == null ? "Pending"  : result.surveyrescheduledatetime;
                result.surveystatus=result.surveystatus == null ? "Pending"  : result.surveystatus;
                result.surveydatetime=result.surveydatetime == null ? "Pending"  : result.surveydatetime;
                result.surveyreason=result.surveyreason == null ? "Pending"  : result.surveyreason;
                result.approvalstatus=result.approvalstatus == null ? "Pending"  : result.approvalstatus;
                result.approvaldatetime= moment(result.update_at).format('YYYY-MM-DD HH:mm:ss');
                result.golivedatetime=result.golivedatetime == null ? "Pending"  : result.golivedatetime;
                result.golivestatus=result.golivestatus == null ? "Pending"  : result.golivestatus;
                result.result=result.result == null ? "Pending"  : result.result;
                result.notes= result.notes == null ? "Pending"  : result.notes;
                result.createAt = moment(result.created_at).format('YYYY-MM-DD HH:mm:ss');
                result.updatedAt = moment(result.updated_at).format('YYYY-MM-DD HH:mm:ss');
              });
              return tasks;

          });
}

function getAllBfi({cust_appid=null,user_id=null,type=null,date=null,sorting=null,sortingBy=null,name=null,
    filter_date_from=null,filter_date_to=null,startPage=1,itemsPerPage=15}={}){
    startPage = (startPage < 1 ) ? 1 : startPage;
    const limit = itemsPerPage;
    const offset = ( startPage - 1 ) * itemsPerPage;

    var orderByColumnQuery = '';
    var orderByAscOrDescQuery = '';
    var filterByDate = '';

    if(filter_date_from !== null && filter_date_to !== null){
        filterByDate = ' where update_at between "'+filter_date_from+ '" and "'+filter_date_to+'"'
    }
    else if(filter_date_from !== null && filter_date_to === null){
        filterByDate = ' where update_at > "'+filter_date_from+'"'
    }
    else if(filter_date_from === null && filter_date_to !== null){
        filterByDate = ' where update_at < "'+filter_date_to+'"'
    }

    if(sorting == null)
    {
        orderByAscOrDescQuery = 'desc';
    }
    else
    {
        orderByAscOrDescQuery = sorting;
    }

    if(sortingBy == null)
    {
        orderByColumnQuery = 'order by bfi_leasing.update_at '+orderByAscOrDescQuery;
    }
    else if(sortingBy == 'sales_date')
    {
        orderByColumnQuery = 'order by bfi_leasing.update_at '+orderByAscOrDescQuery;
    }
      if(cust_appid != null)
      {
          return orm.query('SELECT id,customer_name,address,mobile_phone,asset_name,manufacturing_year,funding_amount as funding_amount_pengajuan,fundingamount as funding_amount_approved,installment,tenor,cust_appid,telestatus,teledatetime,telereason,surveydscheduledatetime,surveyrescheduledatetime,surveystatus,surveydatetime,surveyreason,approvalstatus,approvaldatetime,golivestatus,golivedatetime,result,notes, user_id FROM bfi_leasing where cust_appid = "'+cust_appid+'" '+orderByColumnQuery+' limit '+limit+' offset '+offset, { type: orm.QueryTypes.SELECT})
          .then(function(tasks){
              tasks.forEach(result => {
                result.golivestatus=result.golivestatus == null ? "Pending"  : result.golivestatus;
                result.funding_amount_pengajuan=result.funding_amount_pengajuan == null ? Number(0)  : Number(result.funding_amount_pengajuan);
                result.funding_amount_approved=result.funding_amount_approved == null ? Number(0)  : Number(result.funding_amount_approved);
                result.funding_amount_approved=result.installment == null ? Number(0)  : Number(result.installment);

                result.telestatus=result.telestatus == null ? "Pending"  : result.telestatus;
                result.manufacturing_year=result.manufacturing_year == null ? Number(0)  : Number(result.manufacturing_year);
                result.cust_appid=result.cust_appid == null ? Number(0) : Number(result.cust_appid);
                result.mobile_phone=result.mobile_phone == null ? Number(0) : Number(result.mobile_phone);
                result.teledatetime=result.teledatetime == null ? moment().format('YYYY-MM-DD HH:mm:ss')  : result.teledatetime;
                result.telereason=result.telereason == null ? "Pending"  : result.telereason;
                result.surveydscheduledatetime=result.surveydscheduledatetime == null ? moment().format('YYYY-MM-DD HH:mm:ss')  : result.surveydscheduledatetime;
                result.surveyrescheduledatetime=result.surveyrescheduledatetime == null ? moment().format('YYYY-MM-DD HH:mm:ss')  : result.surveyrescheduledatetime;
                result.surveystatus=result.surveystatus == null ? "Pending"  : result.surveystatus;
                result.surveydatetime=result.surveydatetime == null ? moment().format('YYYY-MM-DD HH:mm:ss')  : result.surveydatetime;
                result.surveyreason=result.surveyreason == null ? "Pending"  : result.surveyreason;
                result.approvalstatus=result.approvalstatus == null ? "Pending"  : result.approvalstatus;
                result.approvaldatetime=result.approvaldatetime == null ? moment().format('YYYY-MM-DD HH:mm:ss')  : result.approvaldatetime;
                result.golivedatetime=result.golivedatetime == null ? moment().format('YYYY-MM-DD HH:mm:ss')  : result.golivedatetime;
                result.golivestatus=result.golivestatus == null ? "Pending"  : result.golivestatus;
                result.result=result.result == null ? "Peding"  : result.result;
                result.notes= result.notes == null ? "Pending"  : result.notes;
                result.createAt = moment(result.created_at).format('YYYY-MM-DD HH:mm:ss');
                result.updatedAt = moment(result.update_at).format('YYYY-MM-DD HH:mm:ss');
                result.user_id = result.user_id == null ? null : result.user_id
              });
              return orm.query('select count(id) as total_tasks from bfi_leasing where cust_appid = "'+cust_appid+'"', { type: orm.QueryTypes.SELECT})
              .then(function(total_tasks){
                  return {list:tasks, total:total_tasks[0].total_tasks};
              });
          });
      }
      else if(type === 0)
      {
          return orm.query('SELECT id,customer_name,address,mobile_phone,asset_name,manufacturing_year,funding_amount as funding_amount_pengajuan,fundingamount as funding_amount_approved,installment,tenor,cust_appid,telestatus,teledatetime,telereason,surveydscheduledatetime,surveyrescheduledatetime,surveystatus,surveydatetime,surveyreason,approvalstatus,approvaldatetime,golivestatus,golivedatetime,result,notes, user_id FROM bfi_leasing where user_id = '+user_id+' '+orderByColumnQuery+' limit '+limit+' offset '+offset, { type: orm.QueryTypes.SELECT})
          .then(function(tasks){
              tasks.forEach(result => {
                result.golivestatus=result.golivestatus == null ? "Pending"  : result.golivestatus;
                result.funding_amount_pengajuan=result.funding_amount_pengajuan == null ? Number(0)  : Number(result.funding_amount_pengajuan);
                result.funding_amount_approved=result.funding_amount_approved == null ? Number(0)  : Number(result.funding_amount_approved);
                result.funding_amount_approved=result.installment == null ? Number(0)  : Number(result.installment);

                result.telestatus=result.telestatus == null ? "Pending"  : result.telestatus;
                result.manufacturing_year=result.manufacturing_year == null ? Number(0)  : Number(result.manufacturing_year);
                result.cust_appid=result.cust_appid == null ? Number(0) : Number(result.cust_appid);
                result.mobile_phone=result.mobile_phone == null ? Number(0) : result.mobile_phone;
                result.teledatetime=result.teledatetime == null ? moment().format('YYYY-MM-DD HH:mm:ss')  : result.teledatetime;
                result.telereason=result.telereason == null ? "Pending"  : result.telereason;
                result.surveydscheduledatetime=result.surveydscheduledatetime == null ? moment().format('YYYY-MM-DD HH:mm:ss')  : result.surveydscheduledatetime;
                result.surveyrescheduledatetime=result.surveyrescheduledatetime == null ? moment().format('YYYY-MM-DD HH:mm:ss')  : result.surveyrescheduledatetime;
                result.surveystatus=result.sur37e642cca64af56c1c84b50be99c7928208ccc97aeddb8f30872db6bfd700a0cveystatus == null ? "Pending"  : result.surveystatus;
                result.surveydatetime=result.surveydatetime == null ? moment().format('YYYY-MM-DD HH:mm:ss')  : result.surveydatetime;
                result.surveyreason=result.surveyreason == null ? "Pending"  : result.surveyreason;
                result.approvalstatus=result.approvalstatus == null ? "Pending"  : result.approvalstatus;
                result.approvaldatetime=result.approvaldatetime == null ? moment().format('YYYY-MM-DD HH:mm:ss')  : result.approvaldatetime;
                result.golivedatetime=result.golivedatetime == null ? moment().format('YYYY-MM-DD HH:mm:ss')  : result.golivedatetime;
                result.golivestatus=result.golivestatus == null ? "Pending"  : result.golivestatus;
                result.result=result.result == null ? "Peding"  : result.result;
                result.notes= result.notes == null ? "Pending"  : result.notes;
                result.createAt = moment(result.created_at).format('YYYY-MM-DD HH:mm:ss');
                result.updatedAt = moment(result.update_at).format('YYYY-MM-DD HH:mm:ss');
                result.user_id = result.user_id == null ? null : result.user_id
                });
              return orm.query('select count(id) as total_tasks from bfi_leasing where user_id = '+user_id+'', { type: orm.QueryTypes.SELECT})
              .then(function(total_tasks){
                  return {list:tasks, total:total_tasks[0].total_tasks};
              });
          });
      }
      else if(type === 10)
      {
          return orm.query('SELECT id,customer_name,address,mobile_phone,asset_name,manufacturing_year,funding_amount as funding_amount_pengajuan,fundingamount as funding_amount_approved,installment,tenor,cust_appid,telestatus,teledatetime,telereason,surveydscheduledatetime,surveyrescheduledatetime,surveystatus,surveydatetime,surveyreason,approvalstatus,approvaldatetime,golivestatus,golivedatetime,result,notes, user_id FROM bfi_leasing'+filterByDate+' '+orderByColumnQuery+' limit '+limit+' offset '+offset, { type: orm.QueryTypes.SELECT})
          .then(function(tasks){
              tasks.forEach(result => {
                result.golivestatus=result.golivestatus == null ? "Pending"  : result.golivestatus;
                result.funding_amount_pengajuan=result.funding_amount_pengajuan == null ? Number(0)  : Number(result.funding_amount_pengajuan);
                result.funding_amount_approved=result.funding_amount_approved == null ? Number(0)  : Number(result.funding_amount_approved);
                result.funding_amount_approved=result.installment == null ? Number(0)  : Number(result.installment);

                result.telestatus=result.telestatus == null ? "Pending"  : result.telestatus;
                result.manufacturing_year=result.manufacturing_year == null ? Number(0)  : Number(result.manufacturing_year);
                result.cust_appid=result.cust_appid == null ? Number(0) : Number(result.cust_appid);
                result.mobile_phone=result.mobile_phone == null ? Number(0) : result.mobile_phone;
                result.teledatetime=result.teledatetime == null ? moment().format('YYYY-MM-DD HH:mm:ss')  : result.teledatetime;
                result.telereason=result.telereason == null ? "Pending"  : result.telereason;
                result.surveydscheduledatetime=result.surveydscheduledatetime == null ? moment().format('YYYY-MM-DD HH:mm:ss')  : result.surveydscheduledatetime;
                result.surveyrescheduledatetime=result.surveyrescheduledatetime == null ? moment().format('YYYY-MM-DD HH:mm:ss')  : result.surveyrescheduledatetime;
                result.surveystatus=result.sur37e642cca64af56c1c84b50be99c7928208ccc97aeddb8f30872db6bfd700a0cveystatus == null ? "Pending"  : result.surveystatus;
                result.surveydatetime=result.surveydatetime == null ? moment().format('YYYY-MM-DD HH:mm:ss')  : result.surveydatetime;
                result.surveyreason=result.surveyreason == null ? "Pending"  : result.surveyreason;
                result.approvalstatus=result.approvalstatus == null ? "Pending"  : result.approvalstatus;
                result.approvaldatetime=result.approvaldatetime == null ? moment().format('YYYY-MM-DD HH:mm:ss')  : result.approvaldatetime;
                result.golivedatetime=result.golivedatetime == null ? moment().format('YYYY-MM-DD HH:mm:ss')  : result.golivedatetime;
                result.golivestatus=result.golivestatus == null ? "Pending"  : result.golivestatus;
                result.result=result.result == null ? "Peding"  : result.result;
                result.notes= result.notes == null ? "Pending"  : result.notes;
                result.createAt = moment(result.created_at).format('YYYY-MM-DD HH:mm:ss');
                result.updatedAt = moment(result.update_at).format('YYYY-MM-DD HH:mm:ss');
                result.user_id = result.user_id == null ? null : result.user_id
                });
              return orm.query('select count(id) as total_tasks from bfi_leasing'+filterByDate, { type: orm.QueryTypes.SELECT})
              .then(function(total_tasks){
                  return {list:tasks, total:total_tasks[0].total_tasks};
              });
          });
      }
}

function input({customer_name=null,user_id=null, address=null,mobile_phone=null,cust_appid=null,create_at=null, asset_name = null}={}) {
    Bfi.findOne({
            mobile_phone: mobile_phone,
        }).then(result => {
            if(result.asset_name ==asset_name){
            }

            if(result == null) {
                return Bfi.create(
                { user_id:user_id,customer_name: customer_name, address: address,mobile_phone:mobile_phone ,cust_appid:cust_appid,create_at:create_at},
                );
            }else {
                return Bfi.create(
                { user_id:user_id,customer_name: customer_name, address: address,mobile_phone:mobile_phone ,cust_appid:cust_appid,create_at:create_at},
                );
            }
})}

function delete_pengajuan({cust_appid=null}){
    Bfi.destroy(
		        {
					where: {cust_appid: cust_appid,
						  }
						});

}

function update({asset_name=null, manufacturing_year=null,mobile_phone=null,funding_amount=null,installment=null,tenor=null,cust_appid=null}={}) {
    return Bfi.update(
    { asset_name: asset_name,
      manufacturing_year: manufacturing_year,
      funding_amount:funding_amount,
      installment:installment,
      tenor:tenor
    }, {where:{cust_appid:cust_appid}});
}

function updateStatus({fundingamount=null,telestatus=null,teledatetime=null,telereason=null,surveydscheduledatetime=null,surveyrescheduledatetime=null,surveystatus=null,surveydatetime=null,surveyreason=null,approvalstatus=null,approvaldatetime=null,golivestatus=null,golivedatetime=null,result=null,notes=null,cust_appid=null}={}) {
    return Bfi.update(
    { fundingamount: fundingamount,
      telestatus: telestatus,
      teledatetime:teledatetime,
      telereason:telereason,
      surveydscheduledatetime:surveydscheduledatetime,
      surveyrescheduledatetime:surveyrescheduledatetime,
      surveystatus:surveystatus,
      surveydatetime:surveydatetime,
      surveyreason:surveyreason,
      approvalstatus:approvalstatus,
      approvaldatetime:approvaldatetime,
      golivestatus:golivestatus,
      golivedatetime:golivedatetime,
      result:result,
      notes:notes
    }, {where:{cust_appid:cust_appid}});
}

function insert({customer_name=null,address=null,mobile_phone=null,cust_appid=null,user_id=null}={}) {

    return Bfi.create(
    {
      customer_name:customer_name,
      mobile_phone:mobile_phone,
      address:address,
      cust_appid:cust_appid,
      user_id:user_id
    });

}

function getStatusAll({startPage=1, itemsPerPage=15}={}){
  startPage = (startPage < 1 ) ? 1 : startPage;
  const limit = itemsPerPage;
  const offset = ( startPage - 1 ) * itemsPerPage;
  return orm.query('select * from  bfi_leasing limit '+limit+' offset '+offset, { type: orm.QueryTypes.SELECT})
  .then(function(getbfi){
      return orm.query('select  * from  bfi_leasing ', { type: orm.QueryTypes.SELECT})
      .then(function(total_bfi){
          return {list:getbfi, total:total_bfi[0].total_bfi};
      });
  });
}

function getTotal({}={}){
  return Bfi.count({
  });
}

function getResultandCount({}={}){
  return Bfi.findAndCountAll({
  });
}

let obj = {}
obj.input = input
obj.update = update
obj.getAllBfi = getAllBfi
obj.insert =insert
obj.getStatusAll = getStatusAll
obj.getTotal = getTotal
obj.getOneStatusTracking =getOneStatusTracking
obj.updateStatus =updateStatus
obj.delete_pengajuan = delete_pengajuan

export default obj
