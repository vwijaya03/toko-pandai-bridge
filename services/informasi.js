import { orm } from '~/services/mariadb'
import { Informasi } from '~/orm/index'

import CustomError from '~/response/error/CustomError'

async function getOne({id=null, is_delete=0}={}) {
    let response = await orm.query(`select * from informasis where informasis.id = ${id} and is_delete = ${is_delete}`, { type: orm.QueryTypes.SELECT});
    if (response == null || response == undefined) {
        return {};
    } else {
        return response[0];
    }
}

async function count(filter, options) {
    options = options || {};
    let result_set;

    options.limit = options.limit ? (options.limit) : 15;
    options.page = options.page ? ((options.page - 1) * options.limit) :0;
    result_set = await Informasi.count({
        where: filter ? filter:{}
    });

    if (!result_set || Object.keys(result_set).length == 0) {
        if (options.throwIfEmpty) {
            throw new CustomError(400, 'Total Data informasi dan promosi tidak di temukan');
        }
    }
    return result_set;
}

async function get(filter, options) {
    options = options || {};
    let result_set;

    if (filter) {
        options.limit = options.limit ? (options.limit) : 15;
        options.page = options.page ? ((options.page - 1) * options.limit) :0;

        result_set = await Informasi.findAll({
            where: filter,
            offset: options.page, 
            limit: options.limit,
            order: [ ['id', 'DESC'] ]
        });
    } else {
        result_set = await Informasi.findAll();
    }

    if (!result_set || Object.keys(result_set).length == 0) {
        if (options.throwIfEmpty) {
            throw new CustomError(400, 'Data informasi dan promosi tidak di temukan');
        }
    }
    return result_set;
}

function createInformasiDanPromosi({title=null, content=null, img_path=null, createdAt=null, updatedAt=null, fmcg_id=null,user_type=Buffer}={}) {
    let args =
    {
        title: title,
        content: content,
        img_path: img_path,
        fmcg_id: fmcg_id,
        user_type: user_type,
        createdAt: createdAt,
        updatedAt: updatedAt,
        is_delete: 0
    }
    
    return Informasi.create(args);
}

function hide({id=null,is_delete=null}={}) {
    
    return Informasi.update({
        is_delete:is_delete
    }, {
        where: {
            id:id
        }
    });
}

let obj = {};
obj.get = get;
obj.getOne = getOne;
obj.createInformasiDanPromosi = createInformasiDanPromosi;
obj.count = count;
obj.hide = hide;

export default obj;