import _ from 'lodash'

import service_user from '~/services/user'
import helper from '~/utils/helper'

import UnauthorizedRequestError from '~/response/error/UnauthorizedRequestError'
import TokenExpiredError from '~/response/error/TokenExpiredError'

import { Logs } from '~/orm/index'
import { USERTYPES } from '~/properties'
import { access_token_static } from '~/properties'

async function verify({token=null, acl=null, is_allowed_access_token_static=null}={}) {

	let is_access_token_static = access_token_static.includes(token);

	if(is_access_token_static && is_allowed_access_token_static) {
		return;
	} else {
		let result = await service_user.authUser({token: token});

		if(result == null || !result) {
			throw new UnauthorizedRequestError();
		} else {
			let role = await getUSERTYPESKey(USERTYPES, result.dataValues.type);

			if(role === undefined) {
				throw new UnauthorizedRequestError();
			} else {
				let acceptedAcl = acl.includes(role);

				if(acceptedAcl) {

					let expiry_date_token = await helper.modifiedSequelizeTimestamp(result.dataValues.access_token_expiry).getTime();
					
					await checkTokenExpiry({expirySeconds: expiry_date_token});

					return result;
					
				} else {
					throw new UnauthorizedRequestError();
				}
			}
		}
	}
}

function getUSERTYPESKey(data, type) {
	return Object.keys(data).filter(k => {
        return data[k] == type;
    }).pop();
}

function checkTokenExpiry({expirySeconds=null}={}) {
    if (!expirySeconds) {
        throw new TokenExpiredError();
    }
    const nowSeconds = new Date().getTime();

    if (nowSeconds > expirySeconds) {
        throw new TokenExpiredError();
    }
}

let obj = {};
obj.verify = verify;
obj.getUSERTYPESKey = getUSERTYPESKey;

export default obj