'use strict';

import { orm } from '~/services/mariadb'


function get_list_le_code({user_id=null}){
    try{
        return orm.query('SELECT unique_code AS le_code FROM new_users as a JOIN users_unique_codes as b ON a.user_id = b.user_id WHERE a.user_id = '+user_id, { type: orm.QueryTypes.SELECT})
    } catch(err){
    }
}

function get_outlet_name({le_code=null}){
    try{
        return orm.query('SELECT VALUE AS outlet_name FROM users_additional_datas a JOIN users_unique_codes b ON a.user_id = b.user_id WHERE b.unique_code = '+le_code, { type: orm.QueryTypes.SELECT})
    }catch(err){
    }
}


let obj = {}
obj.get_list_le_code = get_list_le_code
obj.get_outlet_name = get_outlet_name
export default obj
